###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# - Locate FastJet library
# Defines:
#
#  FJContrib_FOUND
#  FJContrib_INCLUDE_DIR
#  FJContrib_LIBRARIES
#
# Imports:
#
#  FJContrib::<contrib>
#  (See fastjet.fr for a list of available contribs)

# Find the fastjet contrib headers and libraries
find_path(FJContrib_INCLUDE_DIR NAMES fastjet/contrib/SoftDrop.hh)
file(GLOB FJContrib_LIBRARIES "${FJContrib_INCLUDE_DIR}/../lib/*.a")

INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(FJContrib DEFAULT_MSG
  FJContrib_INCLUDE_DIR FJContrib_LIBRARIES)

mark_as_advanced(FJContrib_FOUND FJContrib_INCLUDE_DIR FJContrib_LIBRARIES)

if(FJContrib_FOUND)
  foreach(CONTRIB ${FJContrib_LIBRARIES})
    string(REGEX REPLACE ".*/lib([^/]+)\\.a" "\\1" COMPONENT ${CONTRIB})
    add_library(FJContrib::${COMPONENT} STATIC IMPORTED)
      set_target_properties(FJContrib::${COMPONENT} PROPERTIES
      INTERFACE_INCLUDE_DIRECTORIES "${FJContrib_INCLUDE_DIR}"
      IMPORTED_LOCATION "${CONTRIB}"
    )
  endforeach()
endif()