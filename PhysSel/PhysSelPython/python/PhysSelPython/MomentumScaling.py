#!/usr/bin/env python
# -*- coding: utf-8 -*-
###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# =============================================================================
# @file MomentumSclaing.py
# Dedicated ``selections'' for MomentumScaling
#
# Pseudo-selection that allows to embedd the momentum
# scaling algorithm into the overall flow
# @code
# selection = ....  ## some selection
# selection = MomentumScaling ( selection ,  Turbo = False , Year = '2012' )
# @endcode
#
# Do not forget to configure DaVinci properly:
# @code
# the_year = ... ##  e.g.  '2012'
# dv = DaVinci (
#   DataType    = the_year  ,
#   InputType   =  ...      ,
#   # RootInTES =  ...      , ## specify if run over MDST-streams
# )
# ## MANDATORY:
# from Configurables import CondDB
# CondDB ( LatestGlobalTagByDataType = the_year )
# @endcode
# @see PayloadSelection
# @see TrackScaleState
# =============================================================================
""" Dedicated ``selections'' for MomentumScaling
- see PayloadSelection
- see TrackScaleState

>>> selection = ....  ## some selection
>>> selection = MomentumScaling ( selection ,  Turbo = True , Year = 2015 )

Do not forget to configure DaVinci properly:

>>> the_year = ... ##  e.g.  '2015'
>>> dv = DaVinci (
... DataType    = the_year  ,
... InputType   =  ...      ,
... RootInTES   =  ...      ,
)

Mandatory:
>>> from Configurables import CondDB
>>> CondDB ( LatestGlobalTagByDataType = the_year )

"""
from __future__ import print_function
from builtins import str
from builtins import range
# =============================================================================
__author__ = "Vanya BELYAEV Ivan.Belyaev@itep.ru"
__all__ = (
    'MomentumScaling',  ## apply momentum scaling calibration
    'ConstantMomentumScaling',  ## apply momentum scaling calibration
)
# =============================================================================
## import the base class
from PhysSelPython.Wrappers import PayloadSelection


# =============================================================================
## @class _MomentumScaling
#  Helper class to implement the pseudo-selection that allows to embedd the momentum
#  scaling algorithm into the overall flow
#  @see TrackScaleState
#  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
#  @date 2016-02-25
class _MomentumScaling(PayloadSelection):
    """Helper class that allows to implement the ``pseudo-selection''
    that allows to embedd the momentum
    scaling algorithm into the overall flow
    """
    ## keep track of used momentum scaling settings
    _settings = []  # keep track of used momentum scaling settings

    # =========================================================================
    ##  Helper class to implement the pseudo-selection that allows to embedd the momentum
    #  scaling algorithm into the overall flow
    def __init__(self, RequiredSelection,
                 **kwargs):  ## configuration for TrackStateScaler

        from Configurables import TrackScaleState as _SCALER_
        scaler = _SCALER_(**kwargs)

        from Gaudi.Configuration import log
        log.debug('Momentum scaling is configured as: %s' % scaler)

        PayloadSelection.__init__(
            self,
            RequiredSelection,
            scaler,
            IgnoreDecision=False,
            NameFormat='SCALE:%s')


# =============================================================================
## @class MomentumScaling
#  Pseudo-selection that allows to embedd the momentum
#  scaling algorithm into the overall flow
#  @code
#  selection = ....  ## some selection
#  selection = MomentumScaling ( selection ,  Turbo = False , Year = '2012' )
#  @endcode
#  @see TrackScaleState
#
#  Do not forget to configure DaVinci properly:
#  @code
#  the_year = ... ##  e.g.  '2012'
#  dv = DaVinci (
#    DataType    = the_year  ,
#    InputType   =  ...      ,
#    # RootInTES =  ...      , ## specify if run over MDST-streams
#  )
#  ## MANDATORY:
#  from Configurables import CondDB
#  CondDB ( LatestGlobalTagByDataType = the_year )
#  @endcode
#  For ``PersistReco'' case argument ``Turbo'' should be 'PersistReco'
#  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
#  @date 2016-02-25
class MomentumScaling(_MomentumScaling):
    """Pseudo-selection that allows to embedd the momentum
    scaling algorithm into the overall flow

    >>> selection = ....  ## some selection
    >>> selection = MomentumScaling ( selection ,  Turbo = True )

    Do not forget to configure DaVinci properly:

    >>> the_year = ... ##  e.g.  '2012'
    >>> dv = DaVinci (
    ... DataType    = the_year  ,
    ... InputType   =  ...      ,
    ... # RootInTES =  ...      , ## specify if run over MDST-streams
    )
    ## MANDATORY:
    >>> from Configurables import CondDB
    >>> CondDB ( LatestGlobalTagByDataType = the_year )
    """

    # =========================================================================
    ##  Pseudo-selection that allows to embedd the momentum
    #  scaling algorithm into the overall algorithm flow
    #
    #  - Regular DST/(u)DST
    #  @code
    #  selection = ....  ## some selection
    #  selection = MomentumScaling ( selection )
    #  @endcode
    #
    #  - Turbo/2015
    #  @code
    #  selection = ....  ## some selection
    #  selection = MomentumScaling ( selection , Turbo = True , Year = 2015  )
    #  @endcode
    #
    #  - Turbo/2016
    #  @code
    #  selection = ....  ## some selection
    #  selection = MomentumScaling ( selection , Turbo = True , Year = 2016  )
    #  @endcode
    #
    #  - Turbo+PersistReco
    #  @code
    #  selection = ....  ## some selection
    #  selection = MomentumScaling ( selection , Turbo = 'PersistReco' )
    #  @endcode
    #
    #  @param RequiredSelection input required selection
    #  @param Turbo  True/False/'PersistsReco'
    #  @param Year   the year integer or string
    def __init__(self, RequiredSelection, Turbo=None, Year=None,
                 **kwargs):  ## arguments for scaling algorithm
        """Pseudo-selection that allows to embedd the momentum
        scaling algorithm into the overall flow

        Regular DST/(u)DST:
        >>> selection = ...  ## some selection
        >>> selection = MomentumScaling ( selection )

        Turbo/2015:
        >>> selection = MomentumScaling ( selection , Turbo = True , Year == 2015 )

        Turbo/2016:
        >>> selection = MomentumScaling ( selection , Turbo = True , Year == 2016 )

        Turbo+PersistReco:
        >>> selection = MomentumScaling ( selection , Turbo = 'PersistReco' )
        """
        PersistReco = str(Turbo).upper()

        if 'PropertiesPrint' not in kwargs:
            kwargs['PropertiesPrint'] = True

        ##
        if not Turbo:

            kwargs['name'] = 'SCALER'

        elif Turbo and Year in (2017, '2017', 2018, '2018'):

            kwargs['name'] = 'SCALER'
            kwargs['Input'] = 'Track/Best/Long'

        elif 'PERSISTRECO' == PersistReco:

            ## i) rescale the input selection
            RequiredSelection = MomentumScaling(
                RequiredSelection, Turbo=True, Year=Year, **kwargs)
            ## ii) chain it with rescaling of all persist reco tracks
            kwargs['name'] = 'SCALER_PERSISTRECO'
            kwargs['Input'] = 'Hlt2/TrackFitted/Long'

        elif Turbo is True and '2015' == Year:

            ## for Turbo/2015  tracks are stored separately per each particle selection
            #  NB: it should be selection directly from the tape (no filtering, combining, etc..)

            name_ = 'SCALER:{selection}'
            oloc = RequiredSelection.outputLocation()
            olocs = oloc.split('/')
            if olocs[-1] == 'Particles':
                name_ = name_.format(selection=olocs[-2])
                kwargs['name'] = name_
                kwargs['Input'] = '/'.join(olocs[:-1] + ['Tracks'])
            else:
                raise AttributeError(
                    "MomentumScaling:can't deduce ``Input'' from %s" % oloc)

        ## as default configuration for Turbo use Turbo/2016
        elif Turbo is True:

            kwargs['name'] = 'SCALER'
            kwargs['Input'] = 'Tracks'  ## all tracks in single container

        else:

            from Gaudi.Configuration import log
            log.warning(
                'MomentumScaling: unknown configuration "%s/%s", use "Turbo/2016"-setting'
                % (Turbo, Year))
            kwargs['name'] = 'SCALER'
            kwargs['Input'] = 'Tracks'  ## all tracks in single container

        _MomentumScaling.__init__(self, RequiredSelection, **kwargs)
        alg = self.algorithm()
        self._settings.append((Turbo, Year, alg))


# =============================================================================
## helper function to check MomentumScaling setting versus DaVinci configuration
#  It performs a brutal check at the end of configurtaion step for consistency
#  of MomentumScale settings
#  - internal consistency for various instances of MomentumScaling
#  - consistency between MomentumScaling and DaVinci settings
#  - consistency between CondDB and DaVinci settings  (for real data only)
def _check_momentum_scaling_():
    """Helper function to check MomentumScaling setting versus DaVinci configuration.
    It performs a brutal check at the end of configurtaion step for consistency
    of MomentumScale settings
    - internal consistency for various instances of MomentumScaling
    - consistency between MomentumScaling and DaVinci settings
    - consistency between CondDB and DaVinci settings  (for real data only)
    """

    ## nothing to do...
    ms = _MomentumScaling._settings
    if not ms: return
    ms = list(set(ms))  ## eliminate duplicates

    from Configurables import DaVinci
    from DaVinci.Configuration import getLogger
    log = getLogger('MomentumScaling')
    dv = DaVinci()

    data_type = dv.getProp('DataType') if dv.isPropertySet('DataType') else ''
    input_type = dv.getProp('InputType').upper() if dv.isPropertySet(
        'InputType') else ''
    root_in_tes = dv.getProp('RootInTES') if dv.isPropertySet(
        'RootInTES') else ''
    simulation = dv.getProp('Simulation') if dv.isPropertySet(
        'Simulation') else False

    ## for real data check consistency between CondDB and DaVinci
    if not simulation:

        from Configurables import CondDB
        db = CondDB()
        data_db = db.getProp('LatestGlobalTagByDataType') if db.isPropertySet(
            'LatestGlobalTagByDataType') else ''
        if data_db != data_type:
            ## It is a warning here, since instead of LatestGlobalTagByDataType
            #  can get the desired result using the exact set of local tags.
            log.warning(
                'Inconsistent setting of DataType&LatestGlobalTagByDataType for DaVinci(%s)&CondDB(%s)'
                % (data_type, data_db))

    ## check also persist reco & turbo flags
    preco = False
    turbo = False
    for e in ms:
        if e[0]: turbo = True
        pr = str(e[0]).upper()
        if 'PERSISTRECO' == pr: preco = True

    if turbo:
        from Configurables import DstConf
        dc = DstConf()
        dc = dc.getProp('Turbo') if dc.isPropertySet('Turbo') else False
        if not dc: log.warning("Inconsistent setting of Turbo for DstConf")

    if preco:
        from Configurables import TurboConf
        tc = TurboConf()
        tc = tc.getProp('PersistReco') if tc.isPropertySet(
            'PersistReco') else False
        if not tc:
            log.warning("Inconsistent setting of PersistReco for TurboConf")

    ## check the internal consistency between various scalers
    for i in range(len(ms)):

        msi = ms[i]
        turbo_i = msi[0]
        year_i = msi[1]

        if year_i and data_type != year_i:
            ## If 'Year' is specified for MomentumScaling, it must agree with DaVinci
            log.error(
                'Inconsistent setting of DataType&Year for DaVinci(%s)&MomentumScaling(%s)'
                % (data_type, year_i))
            ## if 'Turbo' DaVinci must have correct setting MDST with RootInTES='/Event/Turbo'
        if turbo_i and (input_type != 'MDST'
                        or not root_in_tes.endswith('/Turbo')):
            log.error(
                'Inconsistent setting of InputType/RootInTES&Turbo for DaVinci(%s,%s)&MomentumScaling(%s)'
                % (input_type, root_in_tes, turbo_i))
        for j in range(i + 1, len(ms)):

            msj = ms[j]
            turbo_j = msj[0]
            year_j = msj[1]

            if bool(turbo_i) != bool(turbo_j) or year_i != year_j:
                ## if turbo_i  != turbo_j or year_i != year_j :
                ## Turbo & Year  settings must be the same for all instances
                log.error(
                    'Inconsistent setting of two MomentumScalings: %s vs %s ' %
                    (str(msi), str(msj)))

        algo = msi[2]
        name = algo.name()
        input = algo.getProp('Input')
        log.info(
            "Scaler '%s' scales the tracks from '%s' location" % (name, input))

    del log


from Gaudi.Configuration import appendPostConfigAction
appendPostConfigAction(_check_momentum_scaling_)


# ==================================================================================
## @class ConstantMomentumScaling
#  Apply constant momentum scaling factor
#    e.g. for systematic studies on MC-DST samples,
#  for -uDST and MC-TURBO samples a bit more work is needed
#  @see TrackScaleState
#  @code
#  selection = ...
#  scaled    = ConstantMomentumScaling ( selection , DeltaScale = 3.e-4 )
#  @endcode
#  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
#  @date 2018-08-23
class ConstantMomentumScaling(_MomentumScaling):
    """ Apply constant momentum scaling, e.g. for systematic studies on MC-DST samples,
    for MC-uDST and MC-TURBO samples a bit more work is needed

    Example
    -------

    selection = ...
    scaled    = ConstantMomentumScaling ( selection , DeltaScale = 3.e-4 )

    """

    def __init__(self, RequiredSelection, **kwargs):

        kw = {}
        kw.update(kwargs)

        if 'CONDDBpath' not in kw: kw['CONDDBpath'] = ''

        import ROOT
        toXml = ROOT.Gaudi.Utils.Histos.HistoStrings.toString

        add_dir = ROOT.TH1.AddDirectoryStatus()

        ROOT.TH1.AddDirectory(False)

        ## the main scaling tx/ty-histograms
        if 'IdpPlusHisto' not in kw:
            h2 = 'tmp-scaling-txty-IDP', 'IdpPlusHisto', 2, -2, 2, 2, -2, 2
            h2 = ROOT.TH2D(*h2)
            kw['IdpPlusHisto'] = toXml(h2)
            del h2

        ## the main scaling tx/ty-histograms
        if 'IdpMinusHisto' not in kw:
            h2 = 'tmp-scaling-txty-IDM', 'IdpMinusHisto', 2, -2, 2, 2, -2, 2
            h2 = ROOT.TH2D(*h2)
            kw['IdpMinusHisto'] = toXml(h2)
            del h2

        ## run-dependent offsets
        if 'RunOffsets' not in kw:
            hr = 'tmp-runoffsets', 'RunOffsets', 2, 0, 1e+10
            hr = ROOT.TH1D(*hr)
            kw['RunOffsets'] = toXml(hr)
            del hr

        ## scaling of covariance matrix
        if 'CovScaleHisto' not in kw:
            from GaudiKernel.SystemOfUnits import TeV
            hc = 'tmp-covscale', '', 2, 0, 20 * TeV
            hc = ROOT.TH1D(*hc)
            hc.SetBinContent(1, 1)
            hc.SetBinContent(2, 1)
            kw['CovScaleHisto'] = toXml(hc)
            del hc

        ROOT.TH1.AddDirectory(add_dir)

        ## initialize the base class
        _MomentumScaling.__init__(self, RequiredSelection, **kw)


# =============================================================================
if '__main__' == __name__:

    print(80 * '*')
    print(__doc__)
    print(' Author  : %s ' % __author__)
    print(' Symbols : %s ' % list(__all__))
    print(80 * '*')

    from sys import modules
    _this = modules[__name__]
    for o in __all__:
        obj = getattr(_this, o, None)
        if obj and obj.__doc__:
            doc = obj.__doc__.replace('\n', '\n#')
            print("# %s\n# %s" % (o, doc))
    print(80 * '*')

# =============================================================================
# The END
# =============================================================================
