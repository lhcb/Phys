/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef COPYSIGNALMCPARTICLES_H
#define COPYSIGNALMCPARTICLES_H 1

// STL
#include <memory>
#include <string>
#include <vector>

// From MicroDST
#include "MicroDST/ICloneMCParticle.h"
#include "MicroDST/ICloneProtoParticle.h"
#include "MicroDST/MicroDSTAlgorithm.h"

// Event model
#include "Event/MCParticle.h"

// MC association
#include "Kernel/Particle2MCLinker.h"

/** @class CopySignalMCParticles CopySignalMCParticles.h
 *
 *  Clones all 'signal' MCParticles.
 *
 *  @author Chris Jones
 *  @date   2015-03-24
 */

class CopySignalMCParticles final : public MicroDSTAlgorithm {

public:
  /// Standard constructor
  CopySignalMCParticles( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode initialize() override; ///< Algorithm initialization
  StatusCode execute() override;    ///< Algorithm execution

private:
  typedef std::vector<const LHCb::ProtoParticle*> ProtosVector;

  /// Get the protos associated to a given MCParticle
  ProtosVector getProtos( const LHCb::MCParticle* ) const;

private:
  /// Type of MCParticle cloner
  std::string m_mcpClonerName;

  /// MCParticle Cloner
  ICloneMCParticle* m_cloner = nullptr;

  /// Type of ProtoParticle cloner
  std::string m_ppClonerName;

  /// ProtoParticle cloner
  ICloneProtoParticle* m_ppCloner = nullptr;

  /// Location of MCParticles to clone
  std::string m_mcPsLoc;

  /// Flag to turn on the cloning of assocaited Reco level info.
  bool m_saveRecoInfo;

  std::unique_ptr<ProtoParticle2MCLinker> m_pCPPAsct; ///< Charged protoparticle associator
  std::unique_ptr<ProtoParticle2MCLinker> m_pNPPAsct; ///< Neutral protoparticle associator
};

#endif // COPYSIGNALMCPARTICLES_H
