/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef COPYPARTICLE2LHCBIDS_H
#define COPYPARTICLE2LHCBIDS_H 1

// Include files
// from Gaudi
#include "MicroDST/MicroDSTAlgorithm.h"

struct ITriggerTisTos;

namespace DaVinci {
  namespace Map {
    class Particle2LHCbIDs;
  }
} // namespace DaVinci

/** @class CopyParticle2LHCbIDs CopyParticle2LHCbIDs.h
 *
 *  MicroDSTAlgorithm to take sets of LHCb::Particles, get an std::vector
 *  containing all the LHCb::LHCbIDs that contribured to the particle, and
 *  place a Particle->std::vector<LHCb::LHCbID> map on the TES.
 *
 *  Property <b>FullDecayTree</b> extends this to all the decay products of
 *  each input particle. The maps are places in a TES location obtained from
 *  <OutputPrefix>/Particle2LHCbIDMap.
 *
 *  @author Juan Palacios
 *  @date   2010-08-18
 */

class CopyParticle2LHCbIDs : public MicroDSTAlgorithm {

public:
  /// Standard constructor
  CopyParticle2LHCbIDs( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode initialize() override; ///< Algorithm initialization
  StatusCode execute() override;    ///< Algorithm execution

private:
  void executeLocation( const std::string& particleLocation );

  void storeLHCbIDs( const LHCb::Particle* part );

private:
  std::string m_outputLocation;

  bool m_fullTree = false;

  ITriggerTisTos* m_iTisTos = nullptr;

  DaVinci::Map::Particle2LHCbIDs* m_p2LHCbID = nullptr;
};

#endif // COPYPARTICLE2LHCBIDS_H
