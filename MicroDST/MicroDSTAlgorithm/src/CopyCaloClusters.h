/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef COPYCALOCLUSTERS_H
#define COPYCALOCLUSTERS_H 1

// Include files
// from MicroDST
#include "MicroDST/BindType2ClonerDef.h"
#include <MicroDST/ICloneCaloCluster.h>
#include <MicroDST/KeyedContainerClonerAlg.h>
// from LHCb
#include <Event/CaloCluster.h>

/** @class CopyCaloClusters CopyCaloClusters.h
 *
 * MicroDSTAlgorithm to clone LHCb::CaloCluster and related objects from one TES
 * location to a parallel one.
 * It inherits the std::string properties InputLocation and OutputPrefix from
 * MicroDSTCommon.
 * The LHCb::CaloCluster objects are taken from the TES location defined by
 * InputLocation, and are cloned and put in TES location "/Event" +
 * OutputPrefix + InputLocation.
 * If InputLocation already contains a leading "/Event" it is removed.
 * The actual cloning of individual LHCb::CaloCluster objects is performed by the
 * ICloneCaloCluster tool, the implementation of which is set by the
 * ClonerType property (default: CaloClusterCloner).
 *
 * Usually, one would clone LHCb::CaloCluster objects implicitly by using
 * CopyProtoParticles, which copies the associated LHCb::CaloCluster objects.
 * The CopyCaloClusters algorithm if useful for when one wants to clone objects
 * that one knows may not be associated to LHCb::ProtoParticles objects, but
 * which one wants to copy anyway.
 *
 * @see ICloneCaloCluster
 * @see CaloClusterCloner
 *
 * <b>Example</b>: Clone CaloClusters from "/Event/Hlt/Calo/EcalClusters" to
 * "/Event/MyLocation/Hlt/Calo/EcalClusters"
 *
 *  @code
 *  copyClusters = CopyCaloClusters()
 *  copyClusters.OutputPrefix = "MyLocation"
 *  copyClusters.InputLocation = "Hlt/Calo/EcalClusters"
 *  // Add the CopyCaloClusters instance to a selection sequence
 *  mySeq = GaudiSequencer("SomeSequence")
 *  mySeq.Members += [copyClusters]
 *  @endcode
 */
template <>
struct BindType2Cloner<LHCb::CaloCluster> {
  typedef LHCb::CaloCluster Type;
  typedef ICloneCaloCluster Cloner;
};
//=============================================================================
template <>
struct Defaults<LHCb::CaloCluster> {
  const static std::string clonerType;
};
const std::string Defaults<LHCb::CaloCluster>::clonerType = "CaloClusterCloner";
//=============================================================================
template <>
struct Location<LHCb::CaloCluster> {
  const static std::string Default;
};
const std::string Location<LHCb::CaloCluster>::Default = "";
//=============================================================================
typedef MicroDST::KeyedContainerClonerAlg<LHCb::CaloCluster> CopyCaloClusters;
DECLARE_COMPONENT_WITH_ID( CopyCaloClusters, "CopyCaloClusters" )
#endif // COPYCALOCLUSTERS_H
