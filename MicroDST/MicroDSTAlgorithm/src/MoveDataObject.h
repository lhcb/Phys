/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

// Include files
// from Gaudi
#include "MicroDST/MicroDSTAlgorithm.h"

/** @class MoveDataObject MoveDataObject.h
 *
 *  Move a DataObject from one TES location to another.
 *
 *  <b>Example</b> Move RecVertices from "/Event/SpecialRec/Vertex/V0" to
 *  "/Event/MyLocation/SpecialRec/Vertex/V0". The object is removed from
 *  the original location, so never do this unless you are sure nothing else
 *  is going to try to acces it!
 *
 *  @code
 *
 *  mySeq = GaudiSequencer("SomeSequence")
 *  moveV0=MoveDataObject('MoveV0',
 *                        InputLocations = ['/Event/SpecialRec/Vertex/V0']
 *                        OutputPrefix = 'MyLocation')
 *  mySeq.Members += [copyPV]
 *  @endcode
 *
 *  @author Juan Palacios
 *  @date   2010-10-05
 */
class MoveDataObject : public MicroDSTAlgorithm {

public:
  /// Standard constructor
  using MicroDSTAlgorithm::MicroDSTAlgorithm;

  StatusCode execute() override; ///< Algorithm execution

private:
  void executeLocation( const std::string& particleLocation );

private:
  Gaudi::Property<std::string> m_outputTESLocation{this, "OutputLocation", "", "Output TES location"};
};
