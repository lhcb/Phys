/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef COPYCALOHYPOS_H
#define COPYCALOHYPOS_H 1

// Include files
// from MicroDST
#include "MicroDST/BindType2ClonerDef.h"
#include <MicroDST/ICloneCaloHypo.h>
#include <MicroDST/KeyedContainerClonerAlg.h>
// from LHCb
#include <Event/CaloHypo.h>

/** @class CopyCaloHypos CopyCaloHypos.h
 *
 * MicroDSTAlgorithm to clone LHCb::CaloHypo and related objects from one TES
 * location to a parallel one.
 * It inherits the std::string properties InputLocation and OutputPrefix from
 * MicroDSTCommon.
 * The LHCb::CaloHypo objects are taken from the TES location defined by
 * InputLocation, and are cloned and put in TES location "/Event" +
 * OutputPrefix + InputLocation.
 * If InputLocation already contains a leading "/Event" it is removed.
 * The actual cloning of individual LHCb::CaloHypo objects is performed by the
 * ICloneCaloHypo algorithm, the implementation of which is set by the
 * ClonerType property (default: CaloHypoCloner).
 *
 * Usually, one would clone LHCb::CaloHypo objects implicitly by using
 * CopyProtoParticles, which copies the associated LHCb::CaloHypo objects.
 * The CopyCaloHypos algorithm if useful for when one wants to clone objects
 * that one knows may not be associated to LHCb::ProtoParticles objects, but
 * which one wants to copy anyway.
 *
 * @see ICloneCaloHypo
 * @see CaloHypoCloner
 *
 * <b>Example</b>: Clone CaloHypos from "/Event/MC/Particles" to
 * "/Event/MyLocation/MC/Particles"
 *
 *  @code
 *  copyHypos = CopyCaloHypos()
 *  copyHypos.OutputPrefix = "MyLocation"
 *  copyHypos.InputLocation = "MC/Particles"
 *  // Add the CopyCaloHypos instance to a selection sequence
 *  mySeq = GaudiSequencer("SomeSequence")
 *  mySeq.Members += [copyHypos]
 *  @endcode
 */
template <>
struct BindType2Cloner<LHCb::CaloHypo> {
  typedef LHCb::CaloHypo Type;
  typedef ICloneCaloHypo Cloner;
};
//=============================================================================
template <>
struct Defaults<LHCb::CaloHypo> {
  const static std::string clonerType;
};
const std::string Defaults<LHCb::CaloHypo>::clonerType = "CaloHypoCloner";
//=============================================================================
template <>
struct Location<LHCb::CaloHypo> {
  const static std::string Default;
};
const std::string Location<LHCb::CaloHypo>::Default = "";
//=============================================================================
typedef MicroDST::KeyedContainerClonerAlg<LHCb::CaloHypo> CopyCaloHypos;
DECLARE_COMPONENT_WITH_ID( CopyCaloHypos, "CopyCaloHypos" )
#endif // COPYCALOHYPOS_H
