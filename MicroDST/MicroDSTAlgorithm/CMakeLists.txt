###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
MicroDST/MicroDSTAlgorithm
--------------------------
#]=======================================================================]

gaudi_add_module(MicroDSTAlgorithm
    SOURCES
        src/CopyCaloClusters.cpp
        src/CopyCaloHypos.cpp
        src/CopyFlavourTag.cpp
        src/CopyGaudiNumbers.cpp
        src/CopyHltDecReports.cpp
        src/CopyL0DUReport.cpp
        src/CopyLinePersistenceLocations.cpp
        src/CopyMCHeader.cpp
        src/CopyMCParticles.cpp
        src/CopyODIN.cpp
        src/CopyPVWeights.cpp
        src/CopyParticle2BackgroundCategory.cpp
        src/CopyParticle2LHCbIDs.cpp
        src/CopyParticle2MCRelations.cpp
        src/CopyParticle2PVMap.cpp
        src/CopyParticle2PVRelations.cpp
        src/CopyParticle2PVRelationsFromLinePersistenceLocations.cpp
        src/CopyParticle2PVWeightedRelations.cpp
        src/CopyParticle2RelatedInfo.cpp
        src/CopyParticle2RelatedInfoFromLinePersistenceLocations.cpp
        src/CopyParticle2TisTosDecisions.cpp
        src/CopyParticles.cpp
        src/CopyPrimaryVertices.cpp
        src/CopyProtoParticle2MCRelations.cpp
        src/CopyProtoParticles.cpp
        src/CopyRecHeader.cpp
        src/CopyRecSummary.cpp
        src/CopySignalMCParticles.cpp
        src/MoveDataObject.cpp
    LINK
        Gaudi::GaudiAlgLib
        Gaudi::GaudiKernel
        LHCb::DAQEventLib
        LHCb::HltEvent
        LHCb::HltInterfaces
        LHCb::L0Event
        LHCb::LHCbKernel
        LHCb::MCAssociatorsLib
        LHCb::MCEvent
        LHCb::MicroDstLib
        LHCb::PhysEvent
        LHCb::RecEvent
        LHCb::RelationsLib
        Phys::DaVinciInterfacesLib
        Phys::DaVinciKernelLib
        Phys::DaVinciMCKernelLib
        Phys::DaVinciTypesLib
        Phys::DaVinciUtilsLib
        Phys::MicroDSTBaseLib
        Phys::MicroDSTInterfacesLib
)
