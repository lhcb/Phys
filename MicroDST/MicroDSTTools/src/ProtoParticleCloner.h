/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef MICRODST_PROTOPARTICLEPARTICLECLONER_H
#  define MICRODST_PROTOPARTICLECLONER_H 1

#  include "ObjectClonerBase.h"

#  include <MicroDST/ICloneCaloHypo.h>
#  include <MicroDST/ICloneProtoParticle.h>
#  include <MicroDST/ICloneTrack.h>

#  include "Event/CaloHypo.h"
#  include "Event/MuonPID.h"
#  include "Event/Particle.h"
#  include "Event/ProtoParticle.h"
#  include "Event/RichPID.h"
#  include "Event/Track.h"

// MC association
#  include "Kernel/Particle2MCLinker.h"

/** @class ProtoParticleCloner ProtoParticleCloner.h src/ProtoParticleCloner.h
 *
 *  Clone an LHCb::ProtoParticle, it's associated LHCb::Track, LHCb::RichPID
 *  and LHCb::MuonPID. The LHCb::Track cloning is performed by an
 *  implementation of the ICloneTrack interface, property ICloneTrack, with
 *  default value TrackCloner. The MuonPID and RichPID are cloned using a
 *  simple copy, and this tool takes care of correctly setting their
 *  LHCb::Track* fields.
 *
 *  @author Juan PALACIOS
 *  @date   2008-04-01
 */
class ProtoParticleCloner : public extends1<ObjectClonerBase, ICloneProtoParticle> {

public:
  /// Standard constructor
  ProtoParticleCloner( const std::string& type, const std::string& name, const IInterface* parent );

  /// Initialisation
  StatusCode initialize() override;

  LHCb::ProtoParticle* operator()( const LHCb::ProtoParticle* protoParticle ) override;

  /// Clone the given ProtoParticle
  LHCb::ProtoParticle* clone( const LHCb::ProtoParticle* protoParticle,
                              const LHCb::Particle*      parent = nullptr ) override;

private:
  typedef MicroDST::BasicItemCloner<LHCb::ProtoParticle> BasicProtoParticleCloner;
  typedef MicroDST::BasicCopy<LHCb::MuonPID>             MuonPIDCloner;
  typedef MicroDST::BasicCopy<LHCb::RichPID>             RichPIDCloner;
  typedef MicroDST::BasicItemCloner<LHCb::CaloHypo>      CaloHypoCloner;

  ICloneTrack*    m_trackCloner    = nullptr;
  ICloneCaloHypo* m_caloHypoCloner = nullptr;

  std::string m_trackClonerName;
  std::string m_caloHypoClonerName;
};

#endif // MICRODST_PROTOPARTICLECLONER_H
