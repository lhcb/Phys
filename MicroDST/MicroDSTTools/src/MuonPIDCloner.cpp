/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files

// from LHCb
#include "Event/MuonPID.h"

// local
#include "MuonPIDCloner.h"

//-----------------------------------------------------------------------------
// Implementation file for class : MuonPIDCloner
//
// 2008-04-01 : Juan PALACIOS
//-----------------------------------------------------------------------------

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
MuonPIDCloner::MuonPIDCloner( const std::string& type, const std::string& name, const IInterface* parent )
    : base_class( type, name, parent ) {}

//=============================================================================

LHCb::MuonPID* MuonPIDCloner::operator()( const LHCb::MuonPID* pid ) { return this->clone( pid ); }

//=============================================================================

LHCb::MuonPID* MuonPIDCloner::clone( const LHCb::MuonPID* pid ) {
  if ( !pid ) return NULL;

  LHCb::MuonPID* pidClone = cloneKeyedContainerItem<BasicMuonPIDCloner>( pid );

  LHCb::Track* idTrackClone = cloneKeyedContainerItem<BasicTrackCloner>( pid->idTrack() );

  LHCb::Track* muonTrackClone = cloneKeyedContainerItem<BasicTrackCloner>( pid->muonTrack() );

  pidClone->setIDTrack( idTrackClone );

  pidClone->setMuonTrack( muonTrackClone );

  return pidClone;
}

//=============================================================================

// Declaration of the Tool Factory
DECLARE_COMPONENT( MuonPIDCloner )
