/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// from LHCb
#include "Event/RecVertex.h"

// local
#include "RecVertexClonerNoTracks.h"

//-----------------------------------------------------------------------------
// Implementation file for class : RecVertexClonerNoTracks
//
// 2007-12-05 : Juan PALACIOS
//-----------------------------------------------------------------------------

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
RecVertexClonerNoTracks::RecVertexClonerNoTracks( const std::string& type, const std::string& name,
                                                  const IInterface* parent )
    : RecVertexCloner( type, name, parent ) {}

//=============================================================================

LHCb::RecVertex* RecVertexClonerNoTracks::clone( const LHCb::RecVertex* vertex ) {
  LHCb::RecVertex* new_vert = RecVertexCloner::clone( vertex );
  if ( new_vert ) { new_vert->clearTracks(); }
  return new_vert;
}

//=============================================================================

// Declaration of the Tool Factory
DECLARE_COMPONENT( RecVertexClonerNoTracks )
