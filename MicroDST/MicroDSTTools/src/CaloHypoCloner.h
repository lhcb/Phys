/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

// STL
#include <algorithm>

#include "GaudiKernel/IIncidentListener.h"
#include "GaudiKernel/IIncidentSvc.h"

#include "ObjectClonerBase.h"

#include "MicroDST/ICloneMCParticle.h"
#include <MicroDST/Functors.hpp>
#include <MicroDST/ICloneCaloDigit.h>
#include <MicroDST/ICloneCaloHypo.h>

// from LHCb
#include "Event/MCParticle.h"

// linker stuff
#include "Linker/LinkerTool.h"
#include "Linker/LinkerWithKey.h"

#include "CaloUtils/CaloAlgUtils.h"

namespace LHCb {
  class CaloDigit;
  class CaloCluster;
} // namespace LHCb

class ICloneCaloCluster;

/** @class CaloHypoCloner CaloHypoCloner.h src/CaloHypoCloner.h
 *
 *  Clone an LHCb::CaloHypo. Recursively clones its member CaloHypos.
 *  Clones member CaloDigits, CaloClusters.
 *
 *  @author Juan PALACIOS
 *  @date   2010-08-13
 */

class CaloHypoCloner : public extends<ObjectClonerBase, ICloneCaloHypo, IIncidentListener> {

public:
  /// Standard constructor
  CaloHypoCloner( const std::string& type, const std::string& name, const IInterface* parent );

  StatusCode initialize() override;

  /** Implement the handle method for the Incident service.
   *  This is used to inform the tool of software incidents.
   *
   *  @param incident The incident identifier
   */
  void handle( const Incident& incident ) override;

public:
  LHCb::CaloHypo* operator()( const LHCb::CaloHypo* hypo ) override;

  /// Clone a Hypo
  LHCb::CaloHypo* clone( const LHCb::CaloHypo* hypo, const LHCb::Particle* parent = nullptr ) override;

private:
  typedef MicroDST::BasicItemCloner<LHCb::CaloHypo>    BasicCaloHypoCloner;
  typedef MicroDST::BasicItemCloner<LHCb::CaloDigit>   BasicCaloDigitCloner;
  typedef MicroDST::BasicItemCloner<LHCb::CaloCluster> BasicCaloClusterCloner;
  typedef std::vector<const LHCb::CaloHypo*>           HypoList;

private:
  /// Clone MC Links for the given Hypo and its clone
  void cloneMCLinks( const LHCb::CaloHypo* hypo, const LHCb::CaloHypo* cloneHypo );

  /// Static list of cloned hypos
  HypoList& clonedHypoList() {
    static HypoList list;
    return list;
  }

  /// Access on demand the MCParticle cloner
  ICloneMCParticle& mcPCloner() {
    if ( !m_mcPcloner ) { m_mcPcloner = tool<ICloneMCParticle>( m_mcpClonerName, this->parent() ); }
    return *m_mcPcloner;
  }

private:
  /// Calo cluster cloner
  ICloneCaloCluster* m_caloClusterCloner = nullptr;
  /// cluster cloner name
  std::string m_caloClusterClonerName{"CaloClusterCloner"};

  /// calo digit cloner
  ICloneCaloDigit* m_caloDigitCloner = nullptr;

  bool m_cloneHypos; ///< Flag to turn on the cloning of associated Hypos

  bool m_cloneDigitsNeuP;   ///< Flag to turn on the cloning of associated Digits for neutrals
  bool m_cloneClustersNeuP; ///< Flag to turn on the cloning of associated Clusters for neutrals

  bool m_cloneDigitsChP;   ///< Flag to turn on the cloning of associated Digits for charged
  bool m_cloneClustersChP; ///< Flag to turn on the cloning of associated Clusters for charged

  /// Flag to always clone associated CaloCluster/CaloDigit information,
  /// regardless of parent particle. Overrides NeuP and ChP settings.
  bool m_cloneDigitsAlways;
  bool m_cloneClustersAlways;

  bool m_cloneMCLinks; ///< Flag to turn on cloning of links to MCParticles

  /// Type of MCParticle cloner
  std::string m_mcpClonerName;

  /// MCParticle Cloner
  ICloneMCParticle* m_mcPcloner = nullptr;

  /// Select the cluster to save based on type
  bool m_selClusters;
};
