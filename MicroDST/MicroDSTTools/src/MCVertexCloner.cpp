/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files

// From LHCb
#include <Event/MCVertex.h>
// local
#include "MCVertexCloner.h"

//-----------------------------------------------------------------------------
// Implementation file for class : MCVertexCloner
//
// 2007-11-30 : Juan PALACIOS
//-----------------------------------------------------------------------------

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
MCVertexCloner::MCVertexCloner( const std::string& type, const std::string& name, const IInterface* parent )
    : base_class( type, name, parent ) {
  declareProperty( "ICloneMCParticle", m_particleClonerName = "MCParticleCloner" );
  // setProperty( "OutputLevel", 2 );
}

//=============================================================================

StatusCode MCVertexCloner::initialize() {
  const StatusCode sc = base_class::initialize();
  if ( sc.isFailure() ) return sc;

  m_particleCloner = tool<ICloneMCParticle>( m_particleClonerName, this->parent() );

  return sc;
}

//=============================================================================

LHCb::MCVertex* MCVertexCloner::operator()( const LHCb::MCVertex* vertex ) {
  if ( !vertex ) return nullptr;

  LHCb::MCVertex* clone = getStoredClone<LHCb::MCVertex>( vertex );

  const auto nProd      = vertex->products().size();
  const auto nCloneProd = ( clone ? clone->products().size() : 0 );

  return ( clone && ( nProd == nCloneProd ) ? clone : this->clone( vertex ) );
}

//=============================================================================

LHCb::MCVertex* MCVertexCloner::clone( const LHCb::MCVertex* vertex ) {
  LHCb::MCVertex* clone = nullptr;
  if ( vertex ) {
    if ( msgLevel( MSG::DEBUG ) )
      debug() << "Cloning MCVertex key=" << vertex->key() << " " << objectLocation( vertex ) << endmsg;
    clone = cloneKeyedContainerItem<BasicCloner>( vertex );
    if ( clone ) {
      // Clone and set the mother for this vertex
      clone->setMother( ( *m_particleCloner )( vertex->mother() ) );
      // clone daugthers
      cloneDecayProducts( vertex->products(), clone );
    }
  }
  return clone;
}

//=============================================================================

void MCVertexCloner::cloneDecayProducts( const SmartRefVector<LHCb::MCParticle>& products,
                                         LHCb::MCVertex*                         clonedVertex ) {
  // Clear the current products
  clonedVertex->clearProducts();

  // loop over the products and clone
  for ( const auto& prod : products ) {
    if ( msgLevel( MSG::DEBUG ) )
      debug() << " -> Cloning MCParticle key=" << prod->key() << " " << objectLocation( prod ) << endmsg;

    LHCb::MCParticle* productClone = ( *m_particleCloner )( prod );
    if ( productClone ) {
      // set origin vertexfor the cloned product particle
      productClone->setOriginVertex( clonedVertex );
      // if not already present, add to vertex products list
      const bool found = std::any_of(
          clonedVertex->products().begin(), clonedVertex->products().end(),
          [&productClone]( const SmartRef<LHCb::MCParticle>& mcP ) { return mcP.target() == productClone; } );
      if ( !found ) { clonedVertex->addToProducts( productClone ); }
      // else
      //{ warning() << "Attempt add a duplicate MCParticle product SmartRef" << endmsg; }
    }
  }
}

//=============================================================================

// Declaration of the Tool Factory
DECLARE_COMPONENT( MCVertexCloner )

//=============================================================================
