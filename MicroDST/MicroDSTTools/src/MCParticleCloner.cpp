/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// STL
#include <algorithm>

// LHCb
#include "Event/MCParticle.h"

// MicroDST
#include <MicroDST/Functors.hpp>
// local
#include "MCParticleCloner.h"

//-----------------------------------------------------------------------------
// Implementation file for class : MCParticleCloner
//
// 2007-11-30 : Juan PALACIOS
//-----------------------------------------------------------------------------

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
MCParticleCloner::MCParticleCloner( const std::string& type, const std::string& name, const IInterface* parent )
    : base_class( type, name, parent ) {
  declareProperty( "ICloneMCVertex", m_vertexClonerName = "MCVertexCloner" );
  // setProperty( "OutputLevel", 2 );
}

//=============================================================================

StatusCode MCParticleCloner::initialize() {
  const StatusCode sc = base_class::initialize();
  if ( sc.isFailure() ) return sc;

  m_vertexCloner =
      ( m_vertexClonerName == "NONE" ? nullptr : this->tool<ICloneMCVertex>( m_vertexClonerName, this->parent() ) );

  return sc;
}

//=============================================================================

LHCb::MCParticle* MCParticleCloner::clone( const LHCb::MCParticle* mcp ) {
  if ( !mcp ) return nullptr;

  if ( msgLevel( MSG::DEBUG ) ) debug() << "clone() called for " << *mcp << endmsg;

  // Clone the MCParticle
  LHCb::MCParticle* clone = cloneKeyedContainerItem<BasicMCPCloner>( mcp );

  // Original origin vertex
  const LHCb::MCVertex* originVertex = mcp->originVertex();

  // Should we clone the origin vertex ?
  if ( cloneOriginVertex( originVertex ) ) {

    // Has it already been cloned
    LHCb::MCVertex* originVertexClone = getStoredClone<LHCb::MCVertex>( originVertex );
    if ( !originVertexClone ) {
      if ( msgLevel( MSG::DEBUG ) ) debug() << "Cloning origin vertex " << *originVertex << endmsg;

      // make a clone
      originVertexClone = cloneKeyedContainerItem<BasicVtxCloner>( originVertex );
      if ( msgLevel( MSG::DEBUG ) ) debug() << "Cloned vertex " << *originVertexClone << endmsg;

      // Clear the current list of products in the cloned vertex
      originVertexClone->clearProducts();

      // Clone the origin vertex mother
      const auto* mother      = mcp->mother();
      auto*       motherClone = ( mother ? ( *this )( mother ) : nullptr );
      if ( motherClone && msgLevel( MSG::DEBUG ) ) debug() << "Cloned mother " << *motherClone << endmsg;
      originVertexClone->setMother( motherClone );
    }

    // Add the cloned origin vertex to the cloned MCP
    clone->setOriginVertex( originVertexClone );

    // Add the cloned MCP to the cloned origin vertex, if not already there
    const bool found =
        std::any_of( originVertexClone->products().begin(), originVertexClone->products().end(),
                     [&clone]( const SmartRef<LHCb::MCParticle>& mcP ) { return mcP.target() == clone; } );
    if ( !found ) { originVertexClone->addToProducts( clone ); }
    // else
    //{ warning() << "Attempt add a duplicate MCParticle product SmartRef" << endmsg; }

  } else {
    clone->setOriginVertex( nullptr );
  }

  // Clone the end vertices
  clone->clearEndVertices();
  cloneDecayVertices( mcp->endVertices(), clone );

  return clone;
}

//=============================================================================

void MCParticleCloner::cloneDecayVertices( const SmartRefVector<LHCb::MCVertex>& endVertices,
                                           LHCb::MCParticle*                     clonedParticle ) {
  for ( const auto& endVtx : endVertices ) {
    if ( endVtx->isDecay() && !( endVtx->products().empty() ) ) {
      if ( m_vertexCloner ) {
        if ( msgLevel( MSG::VERBOSE ) )
          verbose() << "Cloning Decay Vertex " << *endVtx << " with " << endVtx->products().size() << " products!"
                    << endmsg;
        clonedParticle->addToEndVertices( ( *m_vertexCloner )( endVtx ) );
      } else {
        if ( msgLevel( MSG::VERBOSE ) ) verbose() << "Copying decay vertex SmartRefs" << endmsg;
        clonedParticle->addToEndVertices( endVtx );
      }
    }
  }
}

//=============================================================================

LHCb::MCParticle* MCParticleCloner::operator()( const LHCb::MCParticle* mcp ) {
  if ( !mcp ) return nullptr;
  LHCb::MCParticle* clone = getStoredClone<LHCb::MCParticle>( mcp );
  return ( clone ? clone : this->clone( mcp ) );
}

//=============================================================================

// Declaration of the Tool Factory
DECLARE_COMPONENT( MCParticleCloner )

//=============================================================================
