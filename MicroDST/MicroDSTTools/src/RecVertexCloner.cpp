/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files

// from LHCb
#include "Event/RecVertex.h"

// local
#include "RecVertexCloner.h"

//-----------------------------------------------------------------------------
// Implementation file for class : RecVertexCloner
//
// 2007-12-05 : Juan PALACIOS
//-----------------------------------------------------------------------------

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
RecVertexCloner::RecVertexCloner( const std::string& type, const std::string& name, const IInterface* parent )
    : base_class( type, name, parent ) {
  // setProperty( "OutputLevel", 2 );
}

//=============================================================================

LHCb::RecVertex* RecVertexCloner::operator()( const LHCb::RecVertex* vertex ) { return this->clone( vertex ); }

//=============================================================================

LHCb::RecVertex* RecVertexCloner::clone( const LHCb::RecVertex* vertex ) {
  LHCb::RecVertex* clone_v = NULL;

  const bool veto = isVetoed( vertex );

  if ( veto ) {
    clone_v = const_cast<LHCb::RecVertex*>( vertex );
  } else {
    if ( msgLevel( MSG::VERBOSE ) ) verbose() << "Cloning RecVertex at " << tesLocation( vertex ) << endmsg;
    clone_v = cloneKeyedContainerItem<PVCloner>( vertex );
  }

  return clone_v;
}

//=============================================================================

// Declaration of the Tool Factory
DECLARE_COMPONENT( RecVertexCloner )
