/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef MICRODST_CALOADCCLONER_H
#define MICRODST_CALOADCCLONER_H 1

#include "ObjectClonerBase.h"

#include <MicroDST/Functors.hpp>
#include <MicroDST/ICloneCaloAdc.h> // Interface

// from LHCb
#include "Event/CaloAdc.h"

/** @class CaloAdcCloner CaloAdcCloner.h src/CaloAdcCloner.h
 *
 *  Clone an LHCb::CaloAdc.
 *
 *  @author Ricardo Vazquez Gomez
 *  @date   2017-06-15
 */

class CaloAdcCloner : public extends<ObjectClonerBase, ICloneCaloAdc> {

public:
  /// Standard constructor
  CaloAdcCloner( const std::string& type, const std::string& name, const IInterface* parent );

  LHCb::CaloAdc* operator()( const LHCb::CaloAdc* adc ) override;

private:
  LHCb::CaloAdc* clone( const LHCb::CaloAdc* adc );

private:
  typedef MicroDST::BasicCopy<LHCb::CaloAdc> BasicCaloAdcCloner;
};

#endif // MICRODST_CALOADCCLONER_H
