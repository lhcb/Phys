/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "ParticleCloner.h"

//-----------------------------------------------------------------------------
// Implementation file for class : ParticleCloner
//
// 2007-11-30 : Juan PALACIOS
//-----------------------------------------------------------------------------

//=============================================================================

ParticleCloner::ParticleCloner( const std::string& type, const std::string& name, const IInterface* parent )
    : base_class( type, name, parent ) {
  declareProperty( "ICloneVertex", m_vertexClonerName = "VertexCloner" );
  declareProperty( "ICloneProtoParticle", m_ppClonerName = "ProtoParticleCloner" );
  // setProperty( "OutputLevel", 1 );
}

//=============================================================================

StatusCode ParticleCloner::initialize() {
  const StatusCode sc = base_class::initialize();
  if ( sc.isFailure() ) return sc;

  if ( msgLevel( MSG::DEBUG ) ) { debug() << "Going to initialise ICloneProtoParticle and ICloneVertex" << endmsg; }

  m_vertexCloner =
      ( m_vertexClonerName != "NONE" ? tool<ICloneVertex>( m_vertexClonerName, this->parent() ) : nullptr );

  m_ppCloner = ( m_ppClonerName != "NONE" ? tool<ICloneProtoParticle>( m_ppClonerName, this->parent() ) : nullptr );

  if ( msgLevel( MSG::DEBUG ) ) {
    if ( m_ppCloner ) {
      debug() << "Found ICloneProtoParticle " << m_ppClonerName << endmsg;
    } else {
      debug() << "Did not find ICloneProtoParticle '" << m_ppClonerName << "'. ProtoParticle cloning de-activated."
              << endmsg;
    }
  }

  return sc;
}

//=============================================================================

LHCb::Particle* ParticleCloner::operator()( const LHCb::Particle* particle ) { return this->clone( particle ); }

//=============================================================================

LHCb::Particle* ParticleCloner::clone( const LHCb::Particle* particle ) {
  if ( !particle ) {
    if ( msgLevel( MSG::DEBUG ) ) { debug() << "Particle pointer is NULL !" << endmsg; }
    return nullptr;
  }

  LHCb::Particle* particleClone = cloneKeyedContainerItem<BasicParticleCloner>( particle );

  if ( particleClone ) {
    // vertex
    if ( m_vertexCloner ) {
      particleClone->setEndVertex( ( *m_vertexCloner )( particle->endVertex() ) );
    } else {
      particleClone->setEndVertex( SmartRef<LHCb::Vertex>( particle->endVertex() ) );
    }

    // Daughters
    storeDaughters( particleClone, particle->daughters() );

    // ProtoParticle
    const LHCb::ProtoParticle* protoToSet = nullptr;
    if ( m_ppCloner && particle->proto() ) { protoToSet = m_ppCloner->clone( particle->proto(), particle ); }
    if ( protoToSet ) { particleClone->setProto( protoToSet ); }
  }

  return particleClone;
}

//=============================================================================

void ParticleCloner::storeDaughters( LHCb::Particle* particleClone, const SmartRefVector<LHCb::Particle>& daughters ) {
  particleClone->clearDaughters();

  for ( auto dau : daughters ) {
    LHCb::Particle* daughterClone = this->clone( dau );
    if ( daughterClone ) { particleClone->addToDaughters( daughterClone ); }
  }
}

//=============================================================================

// Declaration of the Tool Factory
DECLARE_COMPONENT( ParticleCloner )

//=============================================================================
