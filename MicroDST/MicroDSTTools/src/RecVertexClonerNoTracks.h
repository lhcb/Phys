/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef RecVertexClonerNoTracks_H
#define RecVertexClonerNoTracks_H 1

// local
#include "RecVertexCloner.h"

/** @class RecVertexClonerNoTracks RecVertexClonerNoTracks.h
 *
 *  MicroDSTTool that clones an LHCb::RecVertex for storage on the MicroDST.
 *  The LHCb::RecVertex's constituent LHCb::Tracks are not cloned, and the
 *  SmartRef vector is cleared.
 *
 *  @author Juan PALACIOS
 *  @date   2007-12-05
 */
class RecVertexClonerNoTracks : public RecVertexCloner {

public:
  /// Standard constructor
  RecVertexClonerNoTracks( const std::string& type, const std::string& name, const IInterface* parent );

protected:
  LHCb::RecVertex* clone( const LHCb::RecVertex* vertex ) override;
};

#endif // RecVertexClonerNoTracks_H
