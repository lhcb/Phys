/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "CaloAdcCloner.h"

//-----------------------------------------------------------------------------
// Implementation file for class : CaloAdcCloner
//
// 2017-06-15 : Ricardo Vazquez Gomez
//-----------------------------------------------------------------------------

//=============================================================================

CaloAdcCloner::CaloAdcCloner( const std::string& type, const std::string& name, const IInterface* parent )
    : base_class( type, name, parent ) {
  // setProperty( "OutputLevel", 1 );
}

//=============================================================================

LHCb::CaloAdc* CaloAdcCloner::operator()( const LHCb::CaloAdc* adc ) { return this->clone( adc ); }

//=============================================================================

LHCb::CaloAdc* CaloAdcCloner::clone( const LHCb::CaloAdc* adc ) {
  if ( !adc ) {
    if ( msgLevel( MSG::DEBUG ) ) debug() << "CaloAdc pointer is NULL !" << endmsg;
    return NULL;
  }

  if ( !adc->parent() ) {
    this->Warning( "Cannot clone a CaloAdc with no parent !" ).ignore();
    return NULL;
  }

  // Is this object in the veto list ?
  if ( isVetoed( adc ) ) { return const_cast<LHCb::CaloAdc*>( adc ); }

  LHCb::CaloAdc* clone = cloneKeyedContainerItem<BasicCaloAdcCloner>( adc );
  if ( !clone ) return clone;

  return clone;
}

//=============================================================================

// Declaration of the Tool Factory
DECLARE_COMPONENT( CaloAdcCloner )

//=============================================================================
