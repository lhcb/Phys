/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files

// LHCb
#include "Event/FlavourTag.h"

// local
#include "FlavourTagCloner.h"

//-----------------------------------------------------------------------------
// Implementation file for class : FlavourTagCloner
//
// 2008-08-08 : Juan PALACIOS
//-----------------------------------------------------------------------------

//=============================================================================

LHCb::FlavourTag* FlavourTagCloner::operator()( const LHCb::FlavourTag* tag ) { return this->clone( tag ); }

//=============================================================================

LHCb::FlavourTag* FlavourTagCloner::clone( const LHCb::FlavourTag* tag ) {
  // Clone the FT object
  LHCb::FlavourTag* tmp = cloneKeyedContainerItem<BasicFTCopy>( tag );

  // Update the Particle SmartRef
  tmp->setTaggedB( getStoredClone<LHCb::Particle>( tag->taggedB() ) );

  // Clear the taggers vector
  tmp->setTaggers( std::vector<LHCb::Tagger>() );

  // return
  return tmp;
}
//=============================================================================

// Declaration of the Tool Factory
DECLARE_COMPONENT( FlavourTagCloner )
