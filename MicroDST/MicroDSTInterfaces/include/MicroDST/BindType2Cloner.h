/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: BindType2Cloner.h,v 1.3 2008-09-01 17:06:18 jpalac Exp $
#ifndef MICRODST_BINDTYPE2CLONER_H
#define MICRODST_BINDTYPE2CLONER_H 1

#include "MicroDST/BindType2ClonerDef.h"
/** @class BindType2Cloner BindType2Cloner.h MicroDST/BindType2Cloner.h
 *
 *
 *  @author Juan PALACIOS
 *  @date   2008-09-01
 */

// template <typename T> struct BindType2Cloner
// {
//   typedef T type;
//   typedef MicroDST::BasicCopy<T> cloner;
// };

#endif // MICRODST_BINDTYPE2CLONER_H
