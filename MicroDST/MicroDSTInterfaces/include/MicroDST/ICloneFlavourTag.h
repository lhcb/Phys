/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: ICloneFlavourTag.h,v 1.3 2009-07-29 21:29:01 jpalac Exp $
#ifndef MICRODST_ICLONEFLAVOURTAG_H
#define MICRODST_ICLONEFLAVOURTAG_H 1

#include "MicroDST/ICloner.h"
namespace LHCb {
  class FlavourTag;
}

/** @class ICloneFlavourTag MicroDST/ICloneFlavourTag.h
 *
 *
 *  @author Juan PALACIOS
 *  @date   2008-08-08
 */
class GAUDI_API ICloneFlavourTag : virtual public MicroDST::ICloner<LHCb::FlavourTag> {

public:
  /// Interface ID
  DeclareInterfaceID( ICloneFlavourTag, 2, 0 );

  /// Destructor
  virtual ~ICloneFlavourTag() {}
};

#endif // MICRODST_ICLONEFLAVOURTAG_H
