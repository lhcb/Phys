/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: ICloneParticle.h,v 1.3 2009-07-29 21:29:01 jpalac Exp $
#ifndef MICRODST_ICLONEPARTICLE_H
#define MICRODST_ICLONEPARTICLE_H 1

// Include files
// from STL
#include <string>

// from MicroDST
#include "MicroDST/ICloner.h"
namespace LHCb {
  class Particle;
}

/** @class ICloneParticle MicroDST/ICloneParticle.h
 *
 *
 *  @author Juan PALACIOS
 *  @date   2007-11-30
 */

class GAUDI_API ICloneParticle : virtual public MicroDST::ICloner<LHCb::Particle> {

public:
  /// Interface ID
  DeclareInterfaceID( ICloneParticle, 2, 0 );

  /// Destructor
  virtual ~ICloneParticle() {}
};

#endif // MICRODST_ICLONEPARTICLE_H
