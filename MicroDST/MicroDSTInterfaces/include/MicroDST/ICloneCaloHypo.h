/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: ICloneCaloHypo.h,v 1.1 2010-08-12 15:54:53 jpalac Exp $
#ifndef MICRODST_ICLONECALOHYPO_H
#define MICRODST_ICLONECALOHYPO_H 1

// from MicroDST
#include "MicroDST/ICloner.h"

// Forward declarations
namespace LHCb {
  class CaloHypo;
  class Particle;
} // namespace LHCb

/** @class ICloneCaloHypo MicroDST/ICloneCaloHypo.h
 *
 *
 *  @author Juan PALACIOS
 *  @date   2010-08-12
 */
class GAUDI_API ICloneCaloHypo : virtual public MicroDST::ICloner<LHCb::CaloHypo> {

public:
  /// Interface ID
  DeclareInterfaceID( ICloneCaloHypo, 1, 0 );

  /// Destructor
  virtual ~ICloneCaloHypo() {}

public:
  /// Clone a Hypo
  virtual LHCb::CaloHypo* clone( const LHCb::CaloHypo* hypo, const LHCb::Particle* parent = nullptr ) = 0;
};

#endif // MICRODST_ICLONECALOHYPO_H
