/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: ICloneRecVertex.h,v 1.6 2010-08-11 12:45:22 jpalac Exp $
#ifndef MICRODST_ICLONERECVERTEX_H
#define MICRODST_ICLONERECVERTEX_H 1

// from MicroDST
#include "MicroDST/ICloner.h"

// Forward declarations
namespace LHCb {
  class RecVertex;
}

/** @class ICloneRecVertex MicroDST/ICloneRecVertex.h
 *
 *
 *  @author Juan PALACIOS
 *  @date   2007-12-05
 */
class GAUDI_API ICloneRecVertex : virtual public MicroDST::ICloner<LHCb::RecVertex> {

public:
  /// Interface ID
  DeclareInterfaceID( ICloneRecVertex, 3, 0 );

  /// Destructor
  virtual ~ICloneRecVertex() {}
};

#endif // MICRODST_ICLONERECVERTEX_H
