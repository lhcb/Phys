/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: ICloneCaloAdc.h,v 1.1 2017-06-15 11:25:05 rvazquez Exp $
#ifndef MICRODST_ICLONECALOADC_H
#define MICRODST_ICLONECALOADC_H 1

// from MicroDST
#include "MicroDST/ICloner.h"

// Forward declarations
namespace LHCb {
  class CaloAdc;
}

/** @class ICloneCaloAdc MicroDST/ICloneCaloAdc.h
 *
 *
 *  @author Ricardo Vazquez Gomez
 *  @date   2017-06-15
 */
class GAUDI_API ICloneCaloAdc : virtual public MicroDST::ICloner<LHCb::CaloAdc> {

public:
  /// Interface ID
  DeclareInterfaceID( ICloneCaloAdc, 1, 0 );

  /// Destructor
  virtual ~ICloneCaloAdc() {}
};

#endif // MICRODST_ICLONECALOADC_H
