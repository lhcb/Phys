/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

// from STL
#include <string>

// from Gaudi
#include "GaudiKernel/IAlgTool.h"

namespace MicroDST {

  /** @class ICloner ICloner.h MicroDST/ICloner.h
   *
   *
   *  @author Juan Palacios
   *  @date   2009-07-29
   */

  template <class T>
  class ICloner : virtual public IAlgTool {

  public:
    /// Type typedef
    typedef T Type;

  public:
    /// Clone operator
    virtual T* operator()( const T* source ) = 0;
  };

} // namespace MicroDST
