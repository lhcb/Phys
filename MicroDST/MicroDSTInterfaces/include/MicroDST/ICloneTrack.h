/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: ICloneTrack.h,v 1.3 2009-07-29 21:29:01 jpalac Exp $
#ifndef MICRODST_ICLONETRACK_H
#define MICRODST_ICLONETRACK_H 1
// from MicroDST
#include "MicroDST/ICloner.h"

// Forward declarations
namespace LHCb {
  class Track;
}

/** @class ICloneTrack MicroDST/ICloneTrack.h
 *
 *
 *  @author Juan PALACIOS
 *  @date   2007-12-05
 */
class GAUDI_API ICloneTrack : virtual public MicroDST::ICloner<LHCb::Track> {

public:
  /// Interface ID
  DeclareInterfaceID( ICloneTrack, 2, 0 );

  /// Destructor
  virtual ~ICloneTrack() {}
};

#endif // MICRODST_ICLONETRACK_H
