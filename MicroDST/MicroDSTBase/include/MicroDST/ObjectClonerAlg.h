/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef MICRODST_MICRODSTOBJECTCLONERALG_H
#define MICRODST_MICRODSTOBJECTCLONERALG_H 1

// Include files
// From MicroDST
#include "MicroDST/BindType2ClonerDef.h"
#include "MicroDST/Defaults.h"
#include "MicroDST/MicroDSTAlgorithm.h"

namespace MicroDST {

  /** @class ObjectClonerAlg ObjectClonerAlg.h MicroDST/ObjectClonerAlg.h
   *
   *
   *  @author Juan PALACIOS
   *  @date   2008-08-27
   */

  template <typename T>
  class ObjectClonerAlg : public MicroDSTAlgorithm {

  private:
    typedef Location<T>                         LOCATION;
    typedef typename BindType2Cloner<T>::Cloner CLONER;

  public:
    /// Standard constructor
    ObjectClonerAlg( const std::string& name, ISvcLocator* pSvcLocator ) : MicroDSTAlgorithm( name, pSvcLocator ) {}

    StatusCode initialize() override {
      const StatusCode sc = MicroDSTAlgorithm::initialize();
      if ( sc.isFailure() ) return sc;

      if ( inputTESLocation().empty() ) { setInputTESLocation( LOCATION::Default ); }
      if ( msgLevel( MSG::VERBOSE ) ) verbose() << "inputTESLocation() is " << inputTESLocation() << endmsg;

      return sc;
    }

    StatusCode execute() override {
      for ( const auto& loc : this->inputTESLocations() ) {
        const std::string inputLocation  = niceLocationName( loc );
        const std::string outputLocation = this->outputTESLocation( inputLocation );

        if ( msgLevel( MSG::VERBOSE ) )
          verbose() << "Going to clone KeyedContainer from " << inputLocation << " into " << outputLocation << endmsg;

        const T* cont = copyAndStoreObject<CLONER>( inputLocation, outputLocation );

        if ( !cont ) {
          Warning( "Unable to clone or get object from " + inputLocation, StatusCode::FAILURE, 0 ).ignore();
        }
      }

      setFilterPassed( true );

      return StatusCode::SUCCESS;
    }
  };

} // namespace MicroDST

#endif // MICRODST_MICRODSTOBJECTCLONERALG_H
