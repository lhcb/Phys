/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: BindType2ClonerDef.h,v 1.3 2010-08-11 13:37:52 jpalac Exp $
#ifndef MICRODST_BINDTYPE2CLONERDEF_H
#define MICRODST_BINDTYPE2CLONERDEF_H 1

// Include files
#include <MicroDST/Functors.hpp>

/** @class BindType2Cloner BindType2ClonerDef.h MicroDST/BindType2ClonerDef.h
 *
 *
 *  @author Juan PALACIOS
 *  @date   2008-09-01
 */
template <typename T>
struct BindType2Cloner {
  typedef T                      Type;
  typedef MicroDST::BasicCopy<T> Cloner;
};

#endif // MICRODST_BINDTYPE2CLONERDEF_H
