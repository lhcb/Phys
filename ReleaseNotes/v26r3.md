2019-10-10 Phys v26r3
=====================

Release for use with Run 1 or Run 2 data, prepared on the run2-patches branch.
It is based on Gaudi v32r2, LHCb v45r2, Lbcom v23r0p2 and Rec v24r2, and uses LCG_96b with ROOT 6.18.04.

Version identical to Phys v26r3 except for the dependencies update.