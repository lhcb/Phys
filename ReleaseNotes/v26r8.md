2021-07-04 Phys v26r8
===

This version uses Rec v24r7, Lbcom v23r0p7, LHCb v45r7, Gaudi v36r0 and LCG100 with ROOT 6.24.00.

This version is released on `run2-patches` branch and is intended for use with Run1 or Run2 data. For Run3, use a version released on `master` branch

Built relative to Phys [v26r7](../-/tags/v26r7), with the following changes:

### New features ~"new feature"

- ~"Flavour tagging" | Added inclusive tagger version 140521, !947 (@vjevtic)


### Fixes ~"bug fix" ~workaround

- ~Jets | Fix memory link in particle flow algorithms, !963 (@sfarry)
- ~Build | Extend the unknown-attributes warning workaround to ROOT 6.24, !958 (@cattanem)
- JIRA-LHCBPS-1886, !914 (@ibelyaev) [LHCBPS-1886]
- Bug fix: correct typo "*" -> "**" in Hlt2TISSelection, !911 (@mwilkins)
- Cherry pick StdLooseDetachedTau changes (run2-patches), !898 (@masmith)


### Enhancements ~enhancement

- Add NoPID upstream muons (cherry pick for run2-patches), !897 (@masmith)


### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- Fix or ignore untested StatusCodes exposed by Gaudi v36 and clang11, !960 (@cattanem)
- Remove usage of errorsPrint() method, to follow gaudi/Gaudi!1140, !946 (@cattanem)
- Fixes for LCG 99 (run2-patches), !915 (@masmith)


### Other

- ~Functors | BPVMASSMOMCONSTR, !892 (@decianm)
- Adapt to drop of StatusCode check via StatusCodeSvc, !952 (@clemenci)
