/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef FILTEREVENTLIST_H
#define FILTEREVENTLIST_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiAlgorithm.h"
#include <algorithm>
#include <utility>
#include <vector>

/** @class FilterEventList FilterEventList.h
 *
 *  Algorithm that selects events with a given list
 *  of (event#,run#)
 *
 *  OPTIONS:
 *  - EventRunList: list of pairs (Event,Run) to look for
 *  - SetFilterPassed: What to do with events in this list?
 *                     Pass=true? or Fail=false? Default true
 *
 *  @author Rob Lambert
 *  @date   2009-08-14
 */
class FilterEventList : public GaudiAlgorithm {
public:
  /// Standard constructor
  FilterEventList( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode initialize() override; ///< Algorithm initialization
  StatusCode execute() override;    ///< Algorithm execution
  StatusCode finalize() override;   ///< Algorithm finalization

protected:
private:
  typedef std::pair<int, int>              EventRunPair;
  typedef std::vector<std::pair<int, int>> EventRunList;

  EventRunList m_list;
  bool         m_pass;
};
#endif // FILTEREVENTLIST_H
