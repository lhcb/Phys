/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
#ifndef SUBSTITUTEPIDTOOL_H
#define SUBSTITUTEPIDTOOL_H 1
// ============================================================================
// Include files
// ============================================================================
// from Gaudi
// ============================================================================
#include "GaudiAlg/GaudiTool.h"
// ============================================================================
// Interfaces
// ============================================================================
#include "Kernel/ISubstitutePID.h" // Interface
// ============================================================================
/** @class SubstitutePIDTool SubstitutePIDTool.h
 *
 *  Tool to substitute PID of a DecayChain.
 *
 *  All Code is extracted from SubstitutePID Algorithm by Vanya.
 *  See this algorithm for configuration. The options are the same.
 *
 *  @see SubstitutePID
 *
 *  @author Patrick Koppenburg
 *  @date   2011-12-07
 *  @author  Vanya BELYAEV   Ivan.Belyaev@cern.ch
 *  @date 2011-05-22
 */
class SubstitutePIDTool : public GaudiTool, virtual public ISubstitutePID {
public:
  // ==========================================================================
  /// Standard constructor
  SubstitutePIDTool( const std::string& type, const std::string& name, const IInterface* parent );
  /// destructor
  virtual ~SubstitutePIDTool();
  // ==========================================================================
private:
  // ==========================================================================
  struct Substitution {
    // ========================================================================
    Substitution()
        : m_finder( Decays::Trees::Types_<const LHCb::Particle*>::Invalid() )
        , m_pid( 0 )
        , m_used( 0 )
        , m_decay()
        , m_particle()

    {}
    Substitution( Decays::IDecay::iTree& tree, const LHCb::ParticleID& pid, const std::string& decay,
                  const std::string& particle )
        : m_finder( tree ), m_pid( pid ), m_used( 0 ), m_decay( decay ), m_particle( particle ) {}
    // ========================================================================
    Decays::IDecay::Finder m_finder;   //                 the decay finder
    LHCb::ParticleID       m_pid;      //                              PID
    unsigned long long     m_used;     //                            #used
    std::string            m_decay;    // the decay string
    std::string            m_particle; // the particle name
    // ========================================================================
  };
  // ==========================================================================
  typedef std::vector<Substitution> Substitutions;
  // ==========================================================================
  /// mapping : { 'decay-component' : "new-pid" } (property)
  SubstitutionMap m_map; // mapping : { 'decay-component' : "new-pid" }
  /// the actual substitution engine
  mutable Substitutions m_subs; // the actual substitution engine
  /// update property
  bool m_to_be_updated1; // update property
  /// statistics ?
  bool m_stat; // run statistics
  // ==========================================================================
public:
  // ==========================================================================
  /// initialize
  StatusCode initialize() override;
  /// initialize
  StatusCode finalize() override;
  /// decode the substitution code
  StatusCode decodeCode( const SubstitutionMap& newMap ) override;
  /// loop over particles
  StatusCode substitute( const LHCb::Particle::ConstVector& input, LHCb::Particle::ConstVector& output ) override;
  /// perform the actual substitution
  unsigned int substitute( LHCb::Particle* p ) override;
  //
  void updateHandler( Gaudi::Details::PropertyBase& p ); ///< update properties
  // ==========================================================================
private:
  // ==========================================================================
  /// perform the recursive 4-momentum correction
  unsigned int correctP4( LHCb::Particle* p );
  // ==========================================================================
  /// get the statistics
  typedef std::map<std::pair<std::string, std::string>, unsigned long long> MAP;
  mutable MAP                                                               m_final;
  mutable MAP                                                               m_single;
  // ==========================================================================
};
// ============================================================================
//                                                                      The END
// ============================================================================
#endif // SUBSTITUTEPIDTOOL_H
