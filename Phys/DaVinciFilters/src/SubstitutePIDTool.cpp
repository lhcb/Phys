/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
// Include files
// ============================================================================
// STL
// ============================================================================
#include <cmath>
#include <tuple>
#include <vector>
// ============================================================================
// from Gaudi
// ============================================================================
#include "GaudiKernel/StatEntity.h"
#include "GaudiKernel/ToStream.h"
// ============================================================================
// LoKi
// ============================================================================
#include "LoKi/Decays.h"
#include "LoKi/IDecay.h"
#include "LoKi/ParticleProperties.h"
#include "LoKi/PrintDecay.h"
// ============================================================================
#include "Kernel/DecayTree.h"
#include "Kernel/IParticlePropertySvc.h"
#include "Kernel/ParticleProperty.h"
// ============================================================================
// Boost
// ============================================================================
#include "boost/format.hpp"
// ============================================================================
// local
// ============================================================================
#include "SubstitutePIDTool.h"
// ============================================================================
/** @file
 *  Implementation file for class : SubstitutePIDTool
 *  @date 2011-12-07
 *  @author Patrick Koppenburg
 */
// ============================================================================
namespace {
  // ==========================================================================
  // remove excessive blanks
  inline std::string _process_( std::string s ) {
    auto pos = s.find( "  " );
    while ( std::string::npos != pos ) {
      s.erase( pos, 1 );
      pos = s.find( "  " );
    }
    pos = s.find( "( " );
    while ( std::string::npos != pos ) {
      s.erase( pos + 1, 1 );
      pos = s.find( "( " );
    }
    pos = s.find( " )" );
    while ( std::string::npos != pos ) {
      s.erase( pos, 1 );
      pos = s.find( " )" );
    }
    return s;
  }
  // ==========================================================================
} // namespace
// ============================================================================
// Standard constructor, initializes variables
// ============================================================================
SubstitutePIDTool::SubstitutePIDTool( const std::string& type, const std::string& name, const IInterface* parent )
    : GaudiTool( type, name, parent ), m_map(), m_subs(), m_to_be_updated1( true ), m_stat( false ) {
  declareInterface<ISubstitutePID>( this );
  declareProperty( "Substitutions", m_map, "PID-substitutions :  { ' decay-component' : 'new-pid' }" )
      ->declareUpdateHandler( &SubstitutePIDTool::updateHandler, this );
  declareProperty( "RunStatistics", m_stat, "Run debug-statistics" );
}
// ============================================================================
// Destructor
// ============================================================================
SubstitutePIDTool::~SubstitutePIDTool() {}
// ============================================================================
// Initialize
// ============================================================================
StatusCode SubstitutePIDTool::initialize() {
  StatusCode sc = GaudiTool::initialize();
  if ( sc.isFailure() ) { return sc; }
  //
  if ( "ToolSvc.SubstitutePIDTool" == name() ) {
    return Error( "Running this tool as a public tool without defining an instance name is dangerous. Quitting." );
  }
  sc = decodeCode( m_map ); /// try if it's non empty
  //
  svc<IService>( "LoKiSvc", true );
  // Assert ( s , "LoKi-service is not available!" ) ;
  //
  m_stat = m_stat || msgLevel( MSG::DEBUG );
  //
  return sc;
}
// ============================================================================
// Finalize
// ============================================================================
StatusCode SubstitutePIDTool::finalize() {
  //
  typedef std::tuple<std::string, std::string, unsigned long long> Entry;
  typedef std::vector<Entry>                                       Entries;
  //
  Entries entries;
  for ( Substitutions::const_iterator isub = m_subs.begin(); m_subs.end() != isub; ++isub ) {
    entries.emplace_back( isub->m_particle, isub->m_decay, isub->m_used );
  }
  unsigned long maxlen1 = 0;
  unsigned long maxlen2 = 0;
  for ( const auto& e : entries ) {
    maxlen1 = std::max( maxlen1, std::get<0>( e ).size() );
    maxlen2 = std::max( maxlen2, std::get<1>( e ).size() );
  }
  //
  // report problematic substitutions
  //
  const std::string strfmt1 =
      "NO substitutions %|" + Gaudi::Utils::toString( maxlen1 ) + "s| : %|-" + Gaudi::Utils::toString( maxlen2 ) + "s|";
  for ( const auto& e : entries ) {
    if ( 0 == std::get<2>( e ) ) {
      boost::format fmt( strfmt1 );
      fmt % std::get<0>( e ) % std::get<1>( e );
      Error( fmt.str() ).ignore();
    }
  }
  //
  // report all substitutions
  //
  const std::string strfmt2 = " # substitutions %|" + Gaudi::Utils::toString( maxlen1 ) + "s| : %|-" +
                              Gaudi::Utils::toString( maxlen2 ) + "s| / %|d|";
  for ( const auto& e : entries ) {
    boost::format fmt( strfmt2 );
    fmt % std::get<0>( e ) % std::get<1>( e ) % std::get<2>( e );
    info() << fmt.str() << endmsg;
  }
  //
  // single&final transforms
  //
  Entries entries2;
  for ( const auto& e : m_single ) {
    entries2.emplace_back( _process_( e.first.first ), _process_( e.first.second ), e.second );
  }
  Entries entries3;
  for ( const auto& e : m_final ) {
    entries3.emplace_back( _process_( e.first.first ), _process_( e.first.second ), e.second );
  }
  maxlen1 = 0;
  maxlen2 = 0;
  for ( const auto& e : entries2 ) {
    maxlen1 = std::max( maxlen1, std::get<0>( e ).size() );
    maxlen2 = std::max( maxlen2, std::get<1>( e ).size() );
  }
  for ( const auto& e : entries3 ) {
    maxlen1 = std::max( maxlen1, std::get<0>( e ).size() );
    maxlen2 = std::max( maxlen2, std::get<1>( e ).size() );
  }
  const std::string strfmt3 = "Single transform: %|" + Gaudi::Utils::toString( maxlen1 ) + "s| : %|-" +
                              Gaudi::Utils::toString( maxlen2 ) + "s| / %|d|";
  for ( const auto& e : entries2 ) {
    boost::format fmt( strfmt3 );
    fmt % std::get<0>( e ) % std::get<1>( e ) % std::get<2>( e );
    info() << fmt.str() << endmsg;
  }
  const std::string strfmt4 = "Final  transform: %|" + Gaudi::Utils::toString( maxlen1 ) + "s| : %|-" +
                              Gaudi::Utils::toString( maxlen2 ) + "s| / %|d|";
  for ( const auto& e : entries3 ) {
    boost::format fmt( strfmt4 );
    fmt % std::get<0>( e ) % std::get<1>( e ) % std::get<2>( e );
    info() << fmt.str() << endmsg;
  }
  //
  // finalize the base class, e.g. counters
  return GaudiTool::finalize();
}
// ============================================================================
// decode the code
// ============================================================================
StatusCode SubstitutePIDTool::decodeCode( const SubstitutionMap& newMap ) {
  //
  if ( m_map.empty() && !newMap.empty() ) {                          /* OK */
  } else if ( !m_map.empty() && !newMap.empty() && m_map != newMap ) // replace
  {
    Warning( "Replace Substitutions: " + Gaudi::Utils::toString( m_map ) +
             " with: " + Gaudi::Utils::toString( newMap ) )
        .ignore();
  } else if ( newMap.empty() && !m_to_be_updated1 ) {
    return Warning( "Empty new Substitution map is provided, no action", StatusCode::SUCCESS );
  }
  //
  else if ( newMap == m_map && !m_to_be_updated1 ) {
    return StatusCode::SUCCESS;
  }
  //
  m_map = newMap;
  //
  // get the factory
  Decays::IDecay* factory = tool<Decays::IDecay>( "LoKi::Decay" );
  //
  m_subs.clear();
  LHCb::IParticlePropertySvc* ppSvc = svc<LHCb::IParticlePropertySvc>( "LHCb::ParticlePropertySvc", true );
  //
  for ( SubstitutionMap::const_iterator item = m_map.begin(); m_map.end() != item; ++item ) {
    /// construct the tree
    Decays::IDecay::Tree tree = factory->tree( item->first );
    if ( !tree ) {
      StatusCode sc = tree.validate( ppSvc );
      if ( sc.isFailure() ) {
        return Error( "Unable to validate the tree '" + tree.toString() + "' built from the descriptor '" +
                          item->first + "'",
                      sc );
      }
    }
    // get ParticleID
    const LHCb::ParticleProperty* pp = ppSvc->find( item->second );
    if ( 0 == pp ) { return Error( "Unable to find ParticleID for '" + item->second + "'" ); }
    //
    m_subs.emplace_back( tree, pp->particleID(), _process_( tree.toString() ), pp->name() );
  }
  //
  if ( m_subs.size() != m_map.size() ) { return Error( "Mismatch in decoded substitution container" ); }
  //
  m_to_be_updated1 = false;
  //
  // release services & tools
  releaseTool( factory ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  //
  return StatusCode::SUCCESS;
}

// ============================================================================
// loop over particles
// ============================================================================
StatusCode SubstitutePIDTool::substitute( const LHCb::Particle::ConstVector& input,
                                          LHCb::Particle::ConstVector&       output ) {
  // substitute
  if ( m_to_be_updated1 ) {
    StatusCode sc = decodeCode( m_map );
    if ( sc.isFailure() ) { return Error( "Can't decode the substitution map", sc ); }
  } else if ( m_subs.empty() ) {
    return Error( "No substitutions are defined!" );
  }

  //
  StatEntity& cnt = counter( "#substituted" );
  for ( const LHCb::Particle* p : input ) {
    if ( 0 == p ) { continue; }
    //
    // clone the whole decay tree
    //
    LHCb::DecayTree tree( *p );
    //
    cnt += substitute( tree.head() );
    output.push_back( tree.release() );
  }
  return StatusCode::SUCCESS;
}
// ============================================================================
// perform the actual substitution
// ============================================================================
unsigned int SubstitutePIDTool::substitute( LHCb::Particle* p ) {
  if ( 0 == p ) { return 0; }
  //
  std::string original = "";
  if ( m_stat ) { original = _process_( LoKi::PrintPhys::printDecay( p ) ); }
  //
  unsigned int substituted = 0;
  //
  std::string before = "";
  //
  for ( auto& sub : m_subs ) {
    LHCb::Particle** _p = &p;
    //
    LHCb::Particle::ConstVector found;
    if ( 0 == sub.m_finder.findDecay( _p, _p + 1, found ) ) { continue; }
    //
    for ( LHCb::Particle::ConstVector::const_iterator ip = found.begin(); found.end() != ip; ++ip ) {
      //
      if ( m_stat ) {
        before = _process_( LoKi::PrintPhys::printDecay( p ) );
      } else {
        before.clear();
      }
      //
      const LHCb::Particle* pf = *ip;
      if ( 0 == pf ) { continue; }
      LHCb::Particle* pf_ = const_cast<LHCb::Particle*>( pf );
      if ( 0 == pf_ ) { continue; }
      pf_->setParticleID( sub.m_pid );
      //
      if ( m_stat ) {
        std::string decay = _process_( LoKi::PrintPhys::printDecay( p ) );
        info() << "Single transform: " << before << " to " << decay << endmsg;
        m_single[std::make_pair( before, decay )] += 1;
      }
      ++substituted;
      ++( sub.m_used );
    }
  }
  //
  counter( "Substitutions" ) += substituted;
  counter( "Substituted" ) += ( 0 < substituted );
  //
  if ( 0 < substituted ) {
    //
    correctP4( p );
    //
    if ( m_stat ) {
      std::string decay = _process_( LoKi::PrintPhys::printDecay( p ) );
      info() << "Final  transform: " << original << " to " << decay << endmsg;
      m_final[std::make_pair( original, decay )] += 1;
    }
  }
  //
  return substituted;
  //
}
// ============================================================================
// perform the recursive 4-momentum correction
// ============================================================================
unsigned int SubstitutePIDTool::correctP4( LHCb::Particle* p ) {
  if ( 0 == p ) { return 0; } // RETURN
  //
  if ( p->isBasicParticle() ) {
    const Gaudi::LorentzVector& oldMom = p->momentum();
    //
    const double newMass   = LoKi::Particles::massFromPID( p->particleID() );
    const double newEnergy = std::sqrt( oldMom.P2() + newMass * newMass );
    //
    p->setMomentum( Gaudi::LorentzVector( oldMom.Px(), oldMom.Py(), oldMom.Pz(), newEnergy ) );
    p->setMeasuredMass( newMass );
    return 1;
  }
  //
  typedef SmartRefVector<LHCb::Particle> DAUGHTERS;
  const DAUGHTERS&                       daughters = p->daughters();
  //
  unsigned int num = 0;
  //
  Gaudi::LorentzVector sum;
  for ( DAUGHTERS::const_iterator idau = daughters.begin(); daughters.end() != idau; ++idau ) {
    const LHCb::Particle* dau = *idau;
    if ( 0 == dau ) { continue; } // CONTINUE
    //
    num += correctP4( const_cast<LHCb::Particle*>( dau ) );
    sum += dau->momentum();
  }
  //
  if ( 0 != num ) {
    p->setMomentum( sum );
    p->setMeasuredMass( sum.M() );
  }
  //
  return num;
}
// ============================================================================
void SubstitutePIDTool::updateHandler( Gaudi::Details::PropertyBase& p ) {
  // no action if not initialized yet:
  if ( Gaudi::StateMachine::INITIALIZED > FSMState() ) { return; }
  m_to_be_updated1 = true;
  /// mark as "to-be-updated"
  Warning( "The structural property '" + p.name() + "' is updated. It will take effect at the next call",
           StatusCode::SUCCESS )
      .ignore();
  if ( msgLevel( MSG::DEBUG ) ) { debug() << "The updated property is: " << p << endmsg; }
}
// ============================================================================
// Declaration of the Tool Factory
DECLARE_COMPONENT( SubstitutePIDTool )
//=============================================================================
