###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Phys/DaVinciFilters
-------------------
#]=======================================================================]

gaudi_add_module(DaVinciFilters
    SOURCES
        src/ConeJetProxy.cpp
        src/DVFilter.cpp
        src/FilterDecays.cpp
        src/FilterDesktop.cpp
        src/FilterEventList.cpp
        src/FilterInTrees.cpp
        src/FilterStream.cpp
        src/FilterUnique.cpp
        src/FitDecayTrees.cpp
        src/ParticleVeto.cpp
        src/Pi0Veto.cpp
        src/SubPIDMMFilter.cpp
        src/SubstitutePID.cpp
        src/SubstitutePIDTool.cpp
    LINK
        Boost::headers
        Gaudi::GaudiAlgLib
        Gaudi::GaudiKernel
        LHCb::CaloUtils
        LHCb::DAQEventLib
        LHCb::LoKiCoreLib
        LHCb::PartPropLib
        LHCb::PhysEvent
        Phys::DaVinciInterfacesLib
        Phys::DaVinciKernelLib
        Phys::DaVinciTypesLib
        Phys::DecayTreeFitter
        Phys::LoKiArrayFunctorsLib
        Phys::LoKiPhysLib
        Rec::TrackInterfacesLib
)
