/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
// Include files
// ============================================================================
// GaudiKernel
// ============================================================================
#include "GaudiKernel/IInterface.h"
// ============================================================================
// Kernel ?
// ============================================================================
#include "Kernel/IPV2MC.h"
// ============================================================================
/** @file
 *
 * Implementation file for class IPV2MC
 *
 *  This file is a part of LoKi project -
 *    "C++ ToolKit  for Smart and Friendly Physics Analysis"
 *
 *  The package has been designed with the kind help from
 *  Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
 *  contributions and advices from G.Raven, J.van Tilburg,
 *  A.Golutvin, P.Koppenburg have been used in the design.
 *
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date 2006-01-23
 */
// ============================================================================
namespace {
  // ==========================================================================
  /** @var IID_IPV2MC
   *  unique static identifier of IPV2MC interface
   *  @see IPV2MC
   *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
   *  @date 2006-03-18
   */
  // ==========================================================================
  const InterfaceID IID_IPV2MC( "IPV2MC", 1, 0 );
  // ==========================================================================
} // namespace
// ============================================================================
/// Return the unique interface identifier
// ============================================================================
const InterfaceID& IPV2MC::interfaceID() { return IID_IPV2MC; }
// ============================================================================
/// destructor
// ============================================================================
IPV2MC::~IPV2MC() {}
// ============================================================================
// The END
// ============================================================================
