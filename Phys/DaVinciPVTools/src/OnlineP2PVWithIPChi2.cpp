/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id$
/** @class OnlineP2PVWithIPChi2 OnlineP2PVWithIPChi2.cpp
 *
 *
 *  @author Juan PALACIOS
 *  @date   2008-10-20
 */
// Include files
// Local
#include "DistanceCalculatorNames.h"
#include "GenericParticle2PVRelator.h"
#include "P2PVLogic.h"

/** @class P2PVWithIP P2PVWithIP.cpp
 *
 *
 *  @author Juan PALACIOS
 *  @date   2008-10-16
 */
typedef GenericParticle2PVRelator<_p2PVWithIPChi2, OnlineDistanceCalculatorName> OnlineP2PVWithIPChi2;
// Declaration of the Tool Factory
DECLARE_COMPONENT( OnlineP2PVWithIPChi2 )
