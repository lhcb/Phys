###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Phys/DaVinciPVTools
-------------------
#]=======================================================================]

gaudi_add_module(DaVinciPVTools
    SOURCES
        src/BestPVAlg.cpp
        src/BestPVAlg2.cpp
        src/CheckPV.cpp
        src/DistanceCalculatorNames.cpp
        src/OnlineP2PVWithIP.cpp
        src/OnlineP2PVWithIPChi2.cpp
        src/P2PVWithIP.cpp
        src/P2PVWithIPChi2.cpp
        src/PVRelatorAlg.cpp
    LINK
        Gaudi::GaudiAlgLib
        LHCb::MicroDstLib
        LHCb::PhysEvent
        LHCb::RecEvent
        LHCb::RelationsLib
        Phys::DaVinciInterfacesLib
        Phys::DaVinciKernelLib
        Phys::DaVinciTypesLib
)

gaudi_install(PYTHON)
