###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Gaudi.Configuration import *
from Configurables import GenericParticle2PVRelator__p2PVWithIP_OfflineDistanceCalculatorName_ as P2PVWithIP
from Configurables import GenericParticle2PVRelator__p2PVWithIPChi2_OfflineDistanceCalculatorName_ as P2PVWithIPChi2
from Configurables import GenericParticle2PVRelator__p2PVWithIP_OnlineDistanceCalculatorName_ as OnlineP2PVWithIP
from Configurables import GenericParticle2PVRelator__p2PVWithIPChi2_OnlineDistanceCalculatorName_ as OnlineP2PVWithIPChi2
