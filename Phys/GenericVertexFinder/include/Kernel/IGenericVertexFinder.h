/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef GENERICVERTEXFINDER_IGenericVertexFinder_H
#define GENERICVERTEXFINDER_IGenericVertexFinder_H 1

// Include files
// -------------
#include "Event/Particle.h"
#include "Event/RecVertex.h"
#include "Event/Track.h"
#include "Event/Vertex.h"
#include "GaudiKernel/IAlgTool.h"
#include "Kernel/STLExtensions.h"

struct IGenericVertexFinder : extend_interfaces<IAlgTool> {
public:
  /// Declare interface ID
  DeclareInterfaceID( IGenericVertexFinder, 1, 0 );

  /// typedefs, subclasses, etc.
  struct ParticleWithVertex {
    std::unique_ptr<LHCb::Particle> particle;
    std::unique_ptr<LHCb::Vertex>   vertex;
  };
  using ParticleContainer  = std::vector<ParticleWithVertex>;
  using RecVertexContainer = std::vector<std::unique_ptr<LHCb::RecVertex>>;

  /// returns a vector with particles and associated vertex
  virtual ParticleContainer findVertices( const LHCb::Particle::ConstVector& particles ) const = 0;

  /// returns a vector of recvertices
  virtual RecVertexContainer findVertices( const LHCb::Track::Range& tracks ) const = 0;
};

#endif // TRACKINTERFACES_ITRACKCHI2CALCULATOR_H
