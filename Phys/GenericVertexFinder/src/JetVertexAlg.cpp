/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Gaudi
#include "Event/Particle.h"
#include "GaudiAlg/GaudiAlgorithm.h"
#include "GaudiKernel/ToolHandle.h"
#include "Kernel/IGenericVertexFinder.h"
#include "ParticleLocation.h"
#include "VeloDet/DeVelo.h"

//#include <string>

/** @class JetVertexAlg JetVertexAlg.cpp
 *
 * GaudiAlgorithm that looks for vertices inside the particles in the
 * input container. Use case: finding vertices inside a jet.
 *
 * Consider an input particle A with daughters d_1, d_2, d_3 ... d_N.
 *
 * The algorithm searches for vertices in the list d_1 ... d_N. For
 * every found vertex a new LHCb::Particle D is created. Its daughters
 * are the particles {d_i ... d_j} that formed the vertex.  In the
 * particle A, the daughters {d_i ... d_j} are replaced by the new
 * daughter particle D.  Consequently, the output particle A' will
 * have the same number of daughters as the input particle, or
 * smaller.
 *
 * For memory management, the new daughters must be stored in the
 * event store, at the location specified by DaughterOutput.
 *
 * The new particles A' are stored in the location specified by
 * Output. If the output container is not specified, the particles in
 * the input container are modified.
 *
 *  @author Wouter HULSBERGEN
 **/

class JetVertexAlg : public GaudiAlgorithm {

public:
  // Constructors and destructor
  using GaudiAlgorithm::GaudiAlgorithm;
  StatusCode execute() override;
  StatusCode initialize() override;

private:
  ToolHandle<IGenericVertexFinder>          m_vertexfinder{this, "VertexFinder", "GenericVertexFinder"};
  Gaudi::Property<bool>                     m_removeJetsWithoutVertex{this, "RemoveJetsWithoutVertex", false};
  Gaudi::Property<Kernel::ParticleLocation> m_inputLocation{this, "Input"};
  Gaudi::Property<Kernel::ParticleLocation> m_outputLocation{this, "Output"};
  Gaudi::Property<Kernel::ParticleLocation> m_outputDaughterLocation{this, "DaughterOutput"};

  DeVelo* m_velodet{nullptr};
};

DECLARE_COMPONENT( JetVertexAlg )

StatusCode JetVertexAlg::initialize() {
  StatusCode sc = GaudiAlgorithm::initialize();
  m_velodet     = getDet<DeVelo>( DeVeloLocation::Default );
  info() << "InputParticleLocation: '" << m_inputLocation.value().particles() << "'" << std::endl
         << "OutputParticleLocation: '" << m_outputLocation.value().particles() << "'" << endmsg;
  return sc;
}

namespace {
  double sqr( double x ) { return x * x; }
} // namespace

StatusCode JetVertexAlg::execute() {
  // Get the beamline
  auto beamline = 0.5 * ( Gaudi::XYZVector{m_velodet->halfBoxOffset( DeVelo::LeftHalf )} +
                          Gaudi::XYZVector{m_velodet->halfBoxOffset( DeVelo::RightHalf )} );

  // get the particles
  LHCb::Particle::Range input = get<LHCb::Particle::Range>( m_inputLocation.value().particles() );
  // if the outputlocation is not set, we overwrite the existing jets
  LHCb::Particles* outputparticles{nullptr};
  LHCb::Vertices*  outputvertices{nullptr};

  // containers that keep the modified jets. If not set, modify the
  // jets in the input container.
  if ( !m_outputLocation.value().empty() ) {
    outputparticles = new LHCb::Particles();
    outputvertices  = new LHCb::Vertices();
    put( outputparticles, m_outputLocation.value().particles() );
    put( outputvertices, m_outputLocation.value().vertices() );
  }

  // container that keeps daughter particles that we create. these
  // should be put somewhere in the event store.
  Kernel::ParticleLocation daughteroutput{m_outputDaughterLocation.value()};
  if ( daughteroutput.empty() )
    daughteroutput = Kernel::ParticleLocation{m_outputLocation.value().name() + "Daughters"};

  LHCb::Particles* outputsubparticles = new LHCb::Particles();
  LHCb::Vertices*  outputsubvertices  = new LHCb::Vertices();
  put( outputsubparticles, daughteroutput.particles() );
  put( outputsubvertices, daughteroutput.vertices() );

  for ( const LHCb::Particle* orig : input ) {
    // this looks a bit cumbersome in terms of vertex ownership, but
    // that's because I cannot make up my mind yet who should actually
    // create it. at the moment, the tool also makes the vertex if it
    // isn't there yet..

    // call the generic vertex finder on the daughters
    auto compositeparticles = m_vertexfinder->findVertices( orig->daughtersVector() );

    // create the output
    if ( ( !m_removeJetsWithoutVertex || compositeparticles.size() > 0 ) ) {

      LHCb::Particle* jet{nullptr};
      LHCb::Vertex*   jetvertex{nullptr};
      if ( outputparticles ) {
        jet = orig->clone();
        outputparticles->add( jet );
        jetvertex = new LHCb::Vertex{};
        outputvertices->add( jetvertex );
        jet->setEndVertex( jetvertex );
      } else {
        jet       = const_cast<LHCb::Particle*>( orig );
        jetvertex = jet->endVertex();
      }

      // assign the new daughters. first add all the composites. make
      // sure to store the new composite daughter particles in the
      // event store. since we release the unique_ptrs here, but still
      // need the pointers, we make a temporary vector.
      std::vector<LHCb::Particle*> compositedaughters;
      compositedaughters.reserve( compositeparticles.size() );
      std::for_each( compositeparticles.begin(), compositeparticles.end(), [&]( auto& p ) {
        auto particle = p.particle.release();
        outputsubparticles->add( particle );
        outputsubvertices->add( p.vertex.release() );
        compositedaughters.push_back( particle );
      } );
      SmartRefVector<LHCb::Particle> daughters{compositedaughters.begin(), compositedaughters.end()};

      // then add particles in the jet that were not used in any
      // vertex.
      auto not_present_in_composites = [&]( const auto& p ) {
        return std::none_of( compositedaughters.begin(), compositedaughters.end(), [&]( const auto* vtx ) {
          const auto& d = vtx->daughters();
          return std::find( d.begin(), d.end(), p ) != d.end();
        } );
      };
      const auto& od = orig->daughters();
      std::copy_if( od.begin(), od.end(), std::back_inserter( daughters ), not_present_in_composites );

      jet->setDaughters( daughters );

      // choose a vertex to set as the vertex of the mother
      const LHCb::Vertex* bestvertex{nullptr};
      double              bestR2{999999};
      unsigned int        bestN{0};
      for ( const auto& dau : compositedaughters ) {
        auto         iver = dau->endVertex();
        double       R2   = sqr( iver->position().x() - beamline.x() ) + sqr( iver->position().y() - beamline.y() );
        unsigned int N    = dau->daughters().size();
        if ( !bestvertex || ( N > bestN || ( N == bestN && R2 < bestR2 ) ) ) {
          bestvertex = &( *iver );
          bestR2     = R2;
          bestN      = N;
        }
      }
      if ( bestvertex )
        *jetvertex = *bestvertex;
      else {
        // assign the beamline
        jetvertex->clearOutgoingParticles();
        jetvertex->setChi2AndDoF( 0.0, 0 );
        jetvertex->setPosition( Gaudi::XYZPoint{beamline.x(), beamline.y(), 0.0} );
      }
    }
  }

  return StatusCode::SUCCESS;
}
