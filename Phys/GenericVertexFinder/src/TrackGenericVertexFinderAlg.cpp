/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/Track.h"
#include "GaudiAlg/GaudiAlgorithm.h"
#include "Kernel/IGenericVertexFinder.h"

/** @class TrackGenericVertexFinderAlg TrackGenericVertexFinderAlg.cpp
 *
 * GaudiAlgorithm that takes looks for vertices in a list of input
 * tracks. Use case: secondary vertex finding in Rec/Track/Best.
 *
 * The algorithm searches for vertices in the list LHCb::Track
 * container {t_1 ... t_N}. For every found vertex a new
 * LHCb::RecVertex V is created. Its daughters are the tracks {t_i
 * ... t_j} that formed the vertex. The vertices V stored in the
 * location specified by OutputVertexLocation.
 *
 *  @author Wouter HULSBERGEN
 **/

class TrackGenericVertexFinderAlg : public GaudiAlgorithm {
public:
  using GaudiAlgorithm::GaudiAlgorithm;
  StatusCode execute() override;

private:
  Gaudi::Property<std::string>     m_tracklocation{this, "InputTrackLocation"}; // Input Tracks container location
  Gaudi::Property<std::string>     m_vertexlocation{this, "OutputVertexLocation"};
  ToolHandle<IGenericVertexFinder> m_vertexfinder{this, "VertexFinder", "GenericVertexFinder"};
};

DECLARE_COMPONENT( TrackGenericVertexFinderAlg )

//=============================================================================
// ::execute()
//=============================================================================
StatusCode TrackGenericVertexFinderAlg::execute() {
  auto                     tracks = get<LHCb::Track::Range>( m_tracklocation );
  LHCb::Track::ConstVector seltracks;
  seltracks.reserve( tracks.size() );
  std::copy_if( tracks.begin(), tracks.end(), std::back_inserter( seltracks ),
                []( const auto& tr ) { return tr->hasVelo() || tr->hasTT(); } );
  // call the vertex finder
  auto vertices = m_vertexfinder->findVertices( seltracks );
  // create the output. transfer ownership to event store.
  // this is a transform, but the container misses 'push_back'.
  auto recvertices = new LHCb::RecVertex::Container{};
  for ( auto& v : vertices ) recvertices->add( v.release() );
  put( recvertices, m_vertexlocation );

  if ( msgLevel( MSG::DEBUG ) )
    debug() << "Number of tracks and vertices: " << seltracks.size() << " " << vertices.size() << endmsg;
  return StatusCode::SUCCESS;
}
