/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef LHCB_TRAJPARTICLE_H
#define LHCB_TRAJPARTICLE_H

#include "Event/Particle.h"
#include "Event/Track.h"
#include "Kernel/TaggedBool.h"
#include "TrackKernel/TrackStateVertex.h"
#include "TrackKernel/TrackVertexUtils.h"
#include "TrackKernel/ZTrajectory.h"

/** @class TrajParticle TrajPartice.h
 *
 * Helper class for GenericVertexer: stores information on the
 * 'trajectory' of a particle (either a track, or a vertex) that goes
 * into vertex.
 *
 *  @author Wouter Hulsbergen
 **/

namespace LHCb {

  class Particle;
  struct TrajParticleVertex;

  /** @class unique_ptr_optional_ownership
   *
   * Template class that holds a pointer to an object with optional
   * ownership. If the object is owner, the pointer is deallocated in
   * the destructor. Has appropriate move constructor and assignment
   * operators.
   **/

  template <class T>
  class unique_ptr_optional_ownership {
  private:
    T*   m_pointer{nullptr};
    bool m_isowner{false};

  public:
    // constructor
    unique_ptr_optional_ownership() = default;
    unique_ptr_optional_ownership( T* p, bool isowner = false ) : m_pointer( p ), m_isowner( isowner ) {}
    // move constructor
    unique_ptr_optional_ownership( unique_ptr_optional_ownership&& source )
        : m_pointer{std::exchange( source.m_pointer, nullptr )}, m_isowner{std::exchange( source.m_isowner, false )} {}
    // move assignment
    unique_ptr_optional_ownership& operator=( unique_ptr_optional_ownership&& source ) {
      m_pointer = std::exchange( source.m_pointer, nullptr );
      m_isowner = std::exchange( source.m_isowner, false );
      return *this;
    }
    // destructor
    ~unique_ptr_optional_ownership() {
      if ( m_isowner ) delete m_pointer;
    }
    // dereference
    T& operator*() const { return *m_pointer; }
    T* operator->() const { return m_pointer; }

    // hide things that do now work?
    unique_ptr_optional_ownership( const unique_ptr_optional_ownership& source ) = delete;
    unique_ptr_optional_ownership& operator=( unique_ptr_optional_ownership& source ) = delete;
  };

  /// Class that holds a trajectory for a particle.
  class TrajParticle {
  public:
    using ZTrajectory = LHCb::ZTrajectory;
    using OwnsTraj    = xplicit::tagged_bool<struct OwnsTraj_tag>;

    TrajParticle() = default;

    // constructor for a particle without a track
    TrajParticle( const LHCb::Particle& p, const ZTrajectory& traj, OwnsTraj ownstraj );
    // constructor for a particle with a track
    TrajParticle( const LHCb::Particle& p, const LHCb::Track& track, const ZTrajectory& traj, OwnsTraj ownstraj );
    // constructor for a track without a particle
    TrajParticle( const LHCb::Track& track, const ZTrajectory& traj, OwnsTraj ownstraj );

    // set the particle. (need to get rid of this)
    void setParticle( const LHCb::Particle& p ) { m_particle = &p; }
    // return the trajectory
    const ZTrajectory& trajectory() const { return *m_traj; }
    // return a state (cached)
    const LHCb::State& state( double z, double tolerance = 10 * Gaudi::Units::mm ) const {
      if ( std::abs( m_cachedstate.z() - z ) > tolerance ) m_cachedstate = m_traj->state( z );
      return m_cachedstate;
    }
    // return the cached state, whatever it is
    const LHCb::State& state() const { return m_cachedstate; }
    // compute the delta-chi2 to a vertex
    double chi2( const LHCb::TrackStateVertex& vertex ) const {
      return LHCb::TrackVertexUtils::vertexChi2( state( vertex.position().z() ), vertex.position(),
                                                 vertex.covMatrix() );
    }
    // return the type used for sorting
    int typeForSorting() const { return m_typeForSorting; }
    // return the start of the trajectory
    double beginZ() const { return m_beginZ; }
    // return the density
    double density() const { return m_density; }
    // return the pT
    double pT() const { return m_pT; }
    // return the particle
    const LHCb::Particle* particle() const { return m_particle; }
    // return the track, if any
    const LHCb::Track* track() const { return m_track; }
    // return of this particle was used in a vertex
    bool used() const { return m_mother != nullptr; }
    // return the mother
    LHCb::TrajParticleVertex* mother() const { return m_mother; }
    // set the mother
    void setMother( LHCb::TrajParticleVertex* m ) const { m_mother = m; }
    // return the number of lhcbids this track has in common with another track
    size_t nCommonLhcbIDs( const TrajParticle& rhs ) const {
      const LHCb::Track* lhstrk = track();
      const LHCb::Track* rhstrk = rhs.track();
      return lhstrk && rhstrk ? lhstrk->nCommonLhcbIDs( *rhstrk ) : 0;
    }
    // return true if particle has track with velo hits
    bool hasVeloHits() const { return m_hasVeloHits; }

    // return estimate of z position at state closest to beam
    double zBeam() const;

  protected:
    // hide the copy constructor until we have implemented it
    // TrajParticle(const TrajParticle&) {}
  private:
    const LHCb::Particle*                            m_particle{nullptr};
    const LHCb::Track*                               m_track{nullptr};
    unique_ptr_optional_ownership<const ZTrajectory> m_traj;
    bool                                             m_ownstraj{false};
    double                                           m_beginZ{999};
    double                                           m_beginZCov{0};
    double                                           m_density{0};
    double                                           m_pT{0};
    bool                                             m_hasVeloHits{true};
    mutable LHCb::State                              m_cachedstate;
    mutable LHCb::TrajParticleVertex*                m_mother{nullptr};
    int                                              m_typeForSorting{0};
  };
} // namespace LHCb
#endif
