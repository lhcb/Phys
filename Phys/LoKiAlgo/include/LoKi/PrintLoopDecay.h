/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
#ifndef LOKI_PRINTLOOPDECAY_H
#define LOKI_PRINTLOOPDECAY_H 1
// ============================================================================
// Include files
// ============================================================================
// STD & STL
// ===========================================================================
#include <iostream>
#include <limits>
#include <string>
// ===========================================================================
// LoKi
// ===========================================================================
#include "LoKi/PhysTypes.h"
// ===========================================================================
// forward declarations
// ===========================================================================
class MsgStream;
// ===========================================================================
/** @file
 *
 *  This file is a part of LoKi project -
 *    "C++ ToolKit  for Smart and Friendly Physics Analysis"
 *
 *  The package has been designed with the kind help from
 *  Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
 *  contributions and advices from G.Raven, J.van Tilburg,
 *  A.Golutvin, P.Koppenburg have been used in the design.
 *
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date 2006-05-26
 */
// ============================================================================
namespace LoKi {
  // ==========================================================================
  class Loop;
  // ==========================================================================
  namespace PrintLoop {
    // ========================================================================
    /// the maximal recursion level
    const int s_maxLevel4 = std::numeric_limits<int>::max();
    // ========================================================================
    /** Simple function to print decay in more or less "readable" format
     *
     *  @code
     *
     *  Loop B = ... ;
     *
     *  printDecay( B , std::cout ) ;
     *
     *  @endcode
     *
     *  @param particle pointer to particle to be printed
     *  @param stream   stream to be used
     *  @param cut      condition
     *  @param level    recursion level
     *  @param blank    to be printed instead of cutted particles
     *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
     *  @date   2006-01-18
     */
    GAUDI_API
    std::ostream& printDecay( const LoKi::Loop& particle, std::ostream& stream, const LoKi::Types::Cuts& cut,
                              const int level = s_maxLevel4, const std::string& blank = "<cut>" );
    // ========================================================================
    /** Simple function to print decay in more or less "readable" format
     *
     *  @code
     *
     *  Loop B = ... ;
     *
     *  printDecay ( B , std::cout ) ;
     *
     *  @endcode
     *
     *  @param particle pointer to particle to be printed
     *  @param stream   stream to be used
     *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
     *  @date   2006-01-18
     */
    GAUDI_API
    std::ostream& printDecay( const LoKi::Loop& particle, std::ostream& stream );
    // ========================================================================
    /** Simple function to print decay in more or less "readable" format
     *
     *  @code
     *
     *  Loop B = ... ;
     *
     *  printDecay ( B , always() ) ;
     *
     *  @endcode
     *
     *  @param particle pointer to Particle to be printed
     *  @param stream   stream to be used
     *  @param cut      condition
     *  @param level    recursion level
     *  @param blank    to be printed instead of cutted particles
     *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
     *  @date   2006-01-18
     */
    GAUDI_API
    MsgStream& printDecay( const LoKi::Loop& particle, MsgStream& stream, const LoKi::Types::Cuts& cut,
                           const int level = s_maxLevel4, const std::string& blank = "<cut>" );
    // ========================================================================
    /** Simple function to print decay in more or less "readable" format
     *
     *  @code
     *
     *  Loop B = ... ;
     *
     *  printDecay( B , always() ) ;
     *
     *  @endcode
     *
     *  @param particle pointer to Particle to be printed
     *  @param stream   stream to be used
     *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
     *  @date   2006-01-18
     */
    GAUDI_API
    MsgStream& printDecay( const LoKi::Loop& particle, MsgStream& stream );
    // ========================================================================
    /** Simple function to print decay in more or less "readable" format
     *
     *  @code
     *
     *  Loop B = ... ;
     *
     *  info() << printDecay( p ) ;
     *
     *  @endcode
     *
     *  @param particle pointer to Particle to be printed
     *  @param cut      condition
     *  @param level    recursion level
     *  @param blank    to be printed instead of cutted particles
     *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
     *  @date   2006-01-18
     */
    GAUDI_API
    std::string printDecay( const LoKi::Loop& particle, const LoKi::Types::Cuts& cut, const int level = s_maxLevel4,
                            const std::string& blank = "<cut>" );
    // ========================================================================
    /** Simple function to print decay in more or less "readable" format
     *
     *  @code
     *
     *  Loop B = ... ;
     *
     *  info() << printDecay( B ) ;
     *
     *  @endcode
     *
     *  @param particle pointer to Particle to be printed
     *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
     *  @date   2006-01-18
     */
    GAUDI_API
    std::string printDecay( const LoKi::Loop& particle );
    // ========================================================================
  } // namespace PrintLoop
  // ==========================================================================
  namespace Print {
    // ========================================================================
    using namespace LoKi::PrintLoop;
    // ========================================================================
  } // namespace Print
  // ==========================================================================
} //                                                      end of namespace LoKi
// ============================================================================
//                                                                      The END
// ============================================================================
#endif // LOKI_PRINTLOOPDECAY_H
