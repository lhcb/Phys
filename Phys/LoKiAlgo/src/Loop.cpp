/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id$
// ============================================================================
// Include files
// ============================================================================
// LoKiCore
// ============================================================================
#include "LoKi/Interface.h"
// ============================================================================
// LoKiPhys
// ============================================================================
#include "LoKi/Algo.h"
#include "LoKi/Loop.h"
#include "LoKi/LoopObj.h"
// ============================================================================
/** @file
 *
 *  Implementation file for class LoKi::Loop
 *
 *  This file is a part of LoKi project -
 *    "C++ ToolKit  for Smart and Friendly Physics Analysis"
 *
 *  The package has been designed with the kind help from
 *  Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
 *  contributions and advices from G.Raven, J.van Tilburg,
 *  A.Golutvin, P.Koppenburg have been used in the design.
 *
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date 2006-03-14
 */
// ============================================================================
LoKi::Loop::Loop( const LoKi::LoopObj* obj ) : LoKi::Interface<LoKi::LoopObj>( obj ) {}
// ============================================================================
LoKi::Loop::Loop( const LoKi::Loop& obj ) : LoKi::Interface<LoKi::LoopObj>( obj ) {}
// ============================================================================
LoKi::Loop::~Loop() {}
// ============================================================================
const LHCb::Particle* LoKi::Loop::particle() const {
  if ( !validPointer() ) { return 0; }
  return *getObject();
}
// ============================================================================
const LHCb::Vertex* LoKi::Loop::vertex() const {
  if ( !validPointer() ) { return 0; }
  return *getObject();
}
// ============================================================================
const LHCb::Particle* LoKi::Loop::child( const size_t index ) const {
  if ( !validPointer() ) { return 0; }
  return getObject()->child( index );
}
// ============================================================================

// ============================================================================
// The END
// ============================================================================
