/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Pipeline.h"

Pipeline::Pipeline() { syncOutputLevel(); }

void Pipeline::setPipeline( const vector<vector<string>>& selectionPipeline ) {
  m_mergedFeatureCache.clear();

  for ( auto vec : selectionPipeline ) { addPipelineElement( vec ); }
  mergePipeline();
  BOOST_LOG_TRIVIAL( debug ) << "[Pipeline::setPipeline] pipeline contains " << m_pipeline.size() << " generators.";
}

void Pipeline::setAliases( const map<string, string>& aliasMap ) { m_aliasMap = aliasMap; }

void Pipeline::addPipelineElement( const vector<string>& element ) {
  auto sp = make_unique<SelectionParser>();
  sp->setFeatureAliases( m_aliasMap );
  sp->parseSelections( element );
  auto featureNames = sp->getFeaturesNames();

  auto commonFeatureIndices = filterCommonFeatures( featureNames );
  for ( auto& link : commonFeatureIndices ) {
    auto rawName                  = featureNames.at( link.first );
    auto adjustedName             = FeatureGenerator::m_manualFeaturePrefix + rawName;
    featureNames.at( link.first ) = adjustedName;
  }

  auto fg = make_unique<FeatureGenerator>();
  addToolsToGenerator( fg.get() );
  fg->setFeatureAliases( m_aliasMap );
  fg->setFeatureNames( featureNames );

  // reserve some space for caching
  m_pipeline.push_back( make_tuple( std::move( fg ), std::move( sp ), commonFeatureIndices,
                                    vector<double>( featureNames.size() ), false ) );
}

unordered_map<size_t, pair<size_t, size_t>> Pipeline::filterCommonFeatures( const vector<string>& e ) {
  unordered_map<size_t, pair<size_t, size_t>> result( {} );

  // iterate names
  for ( size_t i = 0; i < e.size(); i++ ) {
    auto name = e.at( i );

    // check links of previous pipeline elements
    for ( size_t j = 0; j < m_pipeline.size(); j++ ) {
      auto& prevFG           = get<FG>( m_pipeline.at( j ) );
      auto& prevLink         = get<LINK>( m_pipeline.at( j ) );
      auto  prevFeatureNames = prevFG->getFeatureNames();

      for ( size_t k = 0; k < prevFeatureNames.size(); k++ ) {
        auto prevName = prevFeatureNames.at( k );
        // insert link to previous element, if the name or its alias is present
        // and the previous element is not already a link
        bool equalFullNames    = name == prevName;
        bool equalAliasedNames = m_aliasMap.count( name ) && ( m_aliasMap.at( name ) == prevName );
        if ( ( equalFullNames || equalAliasedNames ) && prevLink.count( i ) == 0 ) result.insert( {i, {j, k}} );
      }
    }
  }

  // @TODO write a test for this function.

  return result;
}

void Pipeline::setToolProvider( const GaudiTool* provider ) {
  m_toolProvider = provider;
  for ( auto& g : m_pipeline ) get<FG>( g )->setToolProvider( provider );
  syncOutputLevel();
}

void Pipeline::addToolsToGenerator( FeatureGenerator* generator ) const {
  if ( m_toolProvider ) {
    generator->setToolProvider( m_toolProvider );
    generator->setTool<ILoKiWrapper>( m_toolProvider->tool<ILoKiWrapper>( "LoKiWrapper", m_toolProvider ) );
    generator->setTool<ITaggingUtils>( m_toolProvider->tool<ITaggingUtils>( "TaggingUtils", m_toolProvider ) );
    generator->setTool<IMonteCarloFeatures>(
        m_toolProvider->tool<IMonteCarloFeatures>( "MonteCarloFeatures", m_toolProvider ) );
    generator->setTool<ICaloElectron>( m_toolProvider->tool<ICaloElectron>( "CaloElectron", m_toolProvider ) );
    generator->setTool<IParticleDescendants>(
        m_toolProvider->tool<IParticleDescendants>( "ParticleDescendants", m_toolProvider ) );
    generator->setTool<IDistanceCalculator>(
        m_toolProvider->tool<IDistanceCalculator>( "LoKi::DistanceCalculator:PUBLIC" ) );
    generator->setTool<IDVAlgorithm>( Gaudi::Utils::getIDVAlgorithm( m_toolProvider->contextSvc(), m_toolProvider ) );
  } else {
    throw std::logic_error( "The pipeline cannot be initialized without "
                            "providing a tool provider. Pass a `GaudiTool` to `setToolsProvider`"
                            "before initilizing the pipeline." );
  }
}

vector<const LHCb::Particle*> Pipeline::applySelection( const vector<const LHCb::Particle*>& candidates ) {
  setTaggingParticles( candidates );
  // first apply selection to every particle and fill cache
  for ( auto& particle : candidates ) applySelection( particle );

  // return the ordered, selected particles
  vector<const LHCb::Particle*> result;
  result.reserve( candidates.size() );
  for ( auto& cachePtr : m_mergedFeatureCache ) result.push_back( m_mergedParticleCacheReverseMap.at( cachePtr ) );

  return result;
}

vector<const LHCb::Particle*> Pipeline::selectN( const vector<const LHCb::Particle*>& candidates, const size_t n ) {
  auto selectedCandidates = applySelection( candidates );
  selectedCandidates.resize( n );
  return selectedCandidates;
}

const LHCb::Particle* Pipeline::selectOne( const vector<const LHCb::Particle*>& candidates ) {
  applySelection( candidates );
  if ( m_mergedFeatureCache.empty() )
    return nullptr;
  else
    return m_mergedParticleCacheReverseMap.at( m_mergedFeatureCache.front() );
}

bool Pipeline::applySelection( const LHCb::Particle* candidate ) {
  bool passing = true;
  setTaggingParticle( candidate );
  for ( auto& g : m_pipeline ) {
    calculateAndCheckFeatures( g, passing );
    if ( !passing ) return false;
  }
  mergedFeatures();
  return passing;
}

vector<double>& Pipeline::generatePipelineElementFeatures( PipelineElement& e ) {
  auto& cacheIsUpToDate = get<STATE>( e );
  auto& cache           = get<CACHE>( e );
  if ( !cacheIsUpToDate ) {
    auto& fg    = get<FG>( e );
    auto& links = get<LINK>( e );
    applyLinks( cache, links );
    fg->updateFeatures( cache );
    cacheIsUpToDate = true;
  }
  return cache;
}

void Pipeline::calculateAndCheckFeatures( PipelineElement& e, bool& passing ) {
  auto& cacheIsUpToDate = get<STATE>( e );
  auto& values          = get<CACHE>( e );
  auto& sp              = get<SP>( e );
  auto& fg              = get<FG>( e );
  auto& links           = get<LINK>( e );
  for ( size_t i = 0; i < values.size(); ++i ) {
    auto& value = values.at( i );
    if ( links.count( i ) ) { value = get<CACHE>( m_pipeline.at( links.at( i ).first ) ).at( links.at( i ).second ); }
    fg->updateFeatureAtPosition( i, value );
    passing &= sp->checkFeatureAtPosition( value, i );
    if ( !passing ) break;
  }
  cacheIsUpToDate = passing;
}

void Pipeline::applyLinks( vector<double>& features, const unordered_map<size_t, pair<size_t, size_t>>& links ) {
  for ( const auto& link : links ) {
    features.at( link.first ) = get<CACHE>( m_pipeline.at( link.second.first ) ).at( link.second.second );
  }
}

string Pipeline::featureLog( vector<string> names, vector<double> values ) {
  stringstream ss;
  for ( auto tup : boost::combine( names, values ) ) {
    string name;
    double value;
    boost::tie( name, value ) = tup;
    ss << name << " " << value << "\n";
  }
  return ss.str();
}

void Pipeline::setSignalCandidate( const LHCb::Particle* signalCandidate ) {
  for ( auto& g : m_pipeline ) {
    get<FG>( g )->setSignalCandidate( signalCandidate );
    get<STATE>( g ) = false;
  }
  resetCache();
}

void Pipeline::setReconstructionVertex( const LHCb::RecVertex* vertex ) {
  for ( auto& g : m_pipeline ) {
    get<FG>( g )->setReconstructionVertex( vertex );
    get<STATE>( g ) = false;
  }
  resetCache();
}

void Pipeline::setTaggingParticles( const vector<const LHCb::Particle*>& particles ) {
  for ( auto& g : m_pipeline ) {
    get<FG>( g )->setTaggingParticleVector( particles );
    get<STATE>( g ) = false;
  }
  resetCache();
}

void Pipeline::setPileUpVertices( const vector<const LHCb::RecVertex*>& pileUpVertices ) {
  for ( auto& g : m_pipeline ) {
    get<FG>( g )->setPileUpVertices( pileUpVertices );
    get<STATE>( g ) = false;
  }
  resetCache();
}

void Pipeline::setTaggingParticle( const LHCb::Particle* particle ) {
  if ( particle != m_taggingParticle ) {
    for ( auto& g : m_pipeline ) {
      get<FG>( g )->setTaggingParticle( particle );
      get<STATE>( g ) = false;
    }
    m_taggingParticle = particle;
  }
}

vector<vector<double>> Pipeline::features( const LHCb::Particle* candidate ) {
  setTaggingParticle( candidate );
  return features();
}

vector<vector<double>> Pipeline::features() {
  vector<vector<double>> returnVector;
  returnVector.reserve( m_pipeline.size() );
  for ( auto& e : m_pipeline ) returnVector.push_back( generatePipelineElementFeatures( e ) );
  return returnVector;
}

vector<vector<string>> Pipeline::featureNames() {
  vector<vector<string>> nameVec;
  for ( auto& g : m_pipeline ) nameVec.push_back( get<SP>( g )->getFeaturesNames() );
  return nameVec;
}

vector<double> Pipeline::mergedFeatures() {
  if ( m_mergedFeatureIndices.empty() ) mergePipeline();

  // special case: no tagging particles
  if ( !m_taggingParticle ) return {};

  // only calculate features once for each tagging particle
  if ( !m_mergedParticleCacheMap.count( m_taggingParticle ) ) {
    auto allFeatures     = features();
    auto currentFeatures = make_shared<vector<double>>();
    currentFeatures->reserve( m_mergedFeatureIndices.size() );
    for ( auto& indices : m_mergedFeatureIndices ) {
      currentFeatures->push_back( allFeatures.at( indices.first ).at( indices.second ) );
    }
    auto pos =
        find_if( m_mergedFeatureCache.begin(), m_mergedFeatureCache.end(), [&]( shared_ptr<vector<double>> e ) -> bool {
          return e->at( m_sortMergedIndex ) < currentFeatures->at( m_sortMergedIndex );
        } );
    m_mergedFeatureCache.insert( pos, currentFeatures );
    m_mergedParticleCacheMap.insert( {m_taggingParticle, currentFeatures} );
    m_mergedParticleCacheReverseMap.insert( {currentFeatures, m_taggingParticle} );
  }
  return *( m_mergedParticleCacheMap.at( m_taggingParticle ) );
}

vector<vector<double>> Pipeline::mergedFeatureMatrix() {
  vector<vector<double>> transposedMatrix( m_mergedFeatureNames.size() );
  for ( auto& features : m_mergedFeatureCache ) {
    for ( size_t i = 0; i < features->size(); i++ ) { transposedMatrix.at( i ).push_back( features->at( i ) ); }
  }
  return transposedMatrix;
}
vector<vector<double>> Pipeline::mergedFeatureMatrixTransposed() {
  vector<vector<double>> transposedMatrix( m_mergedFeatureCache.size() );
  for ( std::size_t j = 0; j < m_mergedFeatureCache.size(); ++j ) {
    transposedMatrix[j].resize( m_mergedFeatureNames.size() );
    std::copy( m_mergedFeatureCache[j]->begin(), m_mergedFeatureCache[j]->end(), transposedMatrix[j].begin() );
  }
  return transposedMatrix;
}

vector<string> Pipeline::mergedFeatureNames() {
  if ( m_mergedFeatureNames.empty() ) mergePipeline();
  return m_mergedFeatureNames;
}

void Pipeline::mergePipeline() {
  // @TODO maybe an unordered set?
  vector<string> unionNames;
  auto           nameVec = featureNames();
  for ( auto nameVecIt = nameVec.begin(); nameVecIt != nameVec.end(); nameVecIt++ ) {
    for ( auto nameIt = ( *nameVecIt ).begin(); nameIt != ( *nameVecIt ).end(); nameIt++ ) {
      // if the feature is not already contained, add it and store the
      // corresponding feature index
      if ( find( unionNames.begin(), unionNames.end(), *nameIt ) == unionNames.end() ) {
        unionNames.push_back( *nameIt );
        auto vecIndex = nameVecIt - nameVec.begin(), featureIndex = nameIt - ( *nameVecIt ).begin();
        m_mergedFeatureIndices.push_back( make_pair( vecIndex, featureIndex ) );
      }
    }
  }
  m_mergedFeatureNames = unionNames;

  auto sortIndexIt = find( m_mergedFeatureNames.begin(), m_mergedFeatureNames.end(), m_sortFeatureName );
  if ( sortIndexIt != m_mergedFeatureNames.end() ) {
    m_sortMergedIndex = sortIndexIt - m_mergedFeatureNames.begin();
  } else {
    m_sortMergedIndex = 0;
  }

  for ( auto nameIt = m_mergedFeatureNames.begin(); nameIt < m_mergedFeatureNames.end(); ++nameIt ) {
    if ( FeatureGenerator::featureIsMVAFeature( *nameIt ) ||
         ( m_aliasMap.count( *nameIt ) && FeatureGenerator::featureIsMVAFeature( m_aliasMap.at( *nameIt ) ) ) )
      m_lastMVAFeature = nameIt - m_mergedFeatureNames.begin();
  }
}

void Pipeline::syncOutputLevel() {
  using namespace boost::log;
  auto level = trivial::severity_level::fatal;
  if ( m_toolProvider ) {
    auto outputLevelProperty =
        static_cast<Gaudi::Property<int>*>( Gaudi::Utils::getProperty( m_toolProvider, "OutputLevel" ) );
    if ( outputLevelProperty ) {
      // the outputLevel property of MSG::Level can be found in the IMessageSvc.h
      // class it starts at nil and thus needs to be substracted by 1 to match
      // with boost
      auto propLevel = static_cast<int>( outputLevelProperty->value() ) - 1;
      if ( propLevel >= static_cast<int>( trivial::severity_level::trace ) &&
           propLevel <= static_cast<int>( trivial::severity_level::fatal ) ) {
        level = static_cast<trivial::severity_level>( propLevel );
      }
    }
  }
  core::get()->set_filter( trivial::severity >= level );
}

void Pipeline::resetCache() noexcept {
  m_taggingParticle = nullptr;
  m_mergedFeatureCache.clear();
  m_mergedParticleCacheMap.clear();
  m_mergedParticleCacheReverseMap.clear();
}

void Pipeline::setSortingFeature( const string& name ) {
  m_sortFeatureName = name;
  mergePipeline();
}

double Pipeline::getLastMVAValue( const LHCb::Particle* particle ) {
  setTaggingParticle( particle );
  return mergedFeatures().at( m_lastMVAFeature );
}
