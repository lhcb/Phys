/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "SelectionParser.h"

/* @brief This regex allows a pattern with one of ["==", ">", "<", "!="] + a
 * numercial value + an optional string, consisting of characters in the ranges
 * [A-Za-z0-9]. Each part may be separated by spaces.
 *
 * See https://regex101.com/r/C1jiCy/12 for a parsed, explained version
 */
const boost::regex SelectionParser::m_regex_pattern(
    R"(^([\w\s\(\)\[\]'\*-\/]*)?\s*(([><]=?|==|\!=)\s*(-?\d+(\.\d+)?)\s*(\*\s*(\w+))?)?$)" );

const std::map<std::string, double>
    SelectionParser::m_units( {{"keV", 1e-3}, {"MeV", 1}, {"GeV", 1e3}, {"TeV", 1e6}, {"PeV", 1e9}} );

void SelectionParser::parseSelections( const std::vector<std::string>& selections ) {
  std::vector<std::pair<std::string, std::string>> pairs;
  m_featureNames.clear();
  m_selectorIndicesOfFeatures.clear();
  for ( size_t selIndex = 0; selIndex < selections.size(); ++selIndex ) {
    auto p = parseFeatureSelectionString( selections.at( selIndex ) );
    // only store feature if not already present
    auto nameIndex = static_cast<size_t>(
        std::distance( m_featureNames.begin(), std::find( m_featureNames.begin(), m_featureNames.end(), p.first ) ) );
    if ( nameIndex >= m_featureNames.size() ) {
      m_featureNames.push_back( p.first );
      m_selectorIndicesOfFeatures.push_back( {} );
    }
    m_selectorIndicesOfFeatures.at( nameIndex ).push_back( selIndex );
    pairs.push_back( p );
  }

  // reset selectors
  m_selectors.clear();
  m_featureIndicesOfSelectors.clear();

  for ( auto& p : pairs ) {
    auto position = std::find( m_featureNames.begin(), m_featureNames.end(), p.first );
    if ( position != m_featureNames.end() ) {
      m_selectors.push_back( parseSelectionString( p.second ) );
      m_featureIndicesOfSelectors.push_back( position - m_featureNames.begin() );
      BOOST_LOG_TRIVIAL( debug ) << "[SelectionParser::parseSelections] added selection " << p.first << " " << p.second
                                 << " pointing to feature index " << position - m_featureNames.begin() << " at index "
                                 << m_featureIndicesOfSelectors.size() - 1;
    } else {
      std::cout << "[SelectionParser.parseSelections] given selection " << p.first << ", " << p.second
                << " not in feature vector.\n";
      throw std::invalid_argument( "[SelectionParser.parseSelections] "
                                   "given selection not in feature vector." );
    }
  }
}

std::pair<std::string, std::string> SelectionParser::parseFeatureSelectionString( const std::string& s ) {
  BOOST_LOG_TRIVIAL( debug ) << "[SelectionParser::parseFeatureSelectionString]"
                             << " matching regex with `" << s << "`";
  auto matches = TaggingHelpers::matchRegex( s, m_regex_pattern );
  if ( matches.empty() ) {
    std::cerr << "[SelectionParser::parseFeatureSelectionString] tried to parse "
              << " selection `" << s << "` but did not find any matches with regular "
              << " expression." << std::endl;
    throw std::invalid_argument( "[SelectionParser::parseFeatureSelectionString] "
                                 "string could not be parsed." );
  }
  auto feature_str = matches.at( 1 );
  if ( m_reverseAliasMap.count( feature_str ) ) feature_str = m_reverseAliasMap.at( feature_str );
  BOOST_LOG_TRIVIAL( debug ) << "[SelectionParser::parseFeatureSelectionString]"
                             << " found feature `" << feature_str << "` in string `" << s << "`";

  // selection might be empty
  std::string selection_str = "";
  if ( matches.size() > 1 ) selection_str = matches.at( 2 );
  BOOST_LOG_TRIVIAL( debug ) << "[SelectionParser::parseFeatureSelectionString]"
                             << " found selection `" << selection_str << "` in string `" << s << "`";

  return std::make_pair( feature_str, selection_str );
}

Func SelectionParser::parseSelectionString( const std::string& selectionString ) {
  // always true if selection string is empty (aka no selection)
  if ( selectionString.empty() ) {
    return []( double ) -> bool { return true; };
  }

  auto        matches    = TaggingHelpers::matchRegex( selectionString, m_regex_pattern );
  std::string number_str = matches.at( 4 ), unit_str = matches.at( 7 ), comparison_str = matches.at( 3 );

  char*  number_end;
  double number = std::strtod( number_str.c_str(), &number_end );

  if ( unit_str.size() ) {
    auto unit_str_it = m_units.find( unit_str );
    if ( unit_str_it != m_units.end() ) {
      number *= unit_str_it->second;
    } else {
      BOOST_LOG_TRIVIAL( error ) << "[SelectionParser::parseSelectionString] "
                                 << "given Unit" << unit_str << " is malformed.";
    }
  }

  Func selector;
  if ( comparison_str == ">" ) {
    selector = [number]( double val ) -> bool { return val > number; };
  } else if ( comparison_str == ">=" ) {
    selector = [number]( double val ) -> bool { return val >= number; };
  } else if ( comparison_str == "<" ) {
    selector = [number]( double val ) -> bool { return val < number; };
  } else if ( comparison_str == "<=" ) {
    selector = [number]( double val ) -> bool { return val <= number; };
  } else if ( comparison_str == "==" ) {
    selector = [number]( double val ) -> bool { return val == number; };
  } else if ( comparison_str == "!=" ) {
    selector = [number]( double val ) -> bool { return val != number; };
  } else {
    BOOST_LOG_TRIVIAL( error ) << "[SelectionParser::parseSelectionString] "
                               << "comparison " << comparison_str << " could not be parsed";
  }
  return selector;
}

bool SelectionParser::checkFeatures( const std::vector<double>& features ) {
  assert( m_featureIndicesOfSelectors.size() == m_selectors.size() );
  // Use boost::make_zip_operator as proposed here
  // http://stackoverflow.com/questions/12552277/whats-the-best-way-to-iterate-over-two-or-more-containers-simultaneously
  BOOST_LOG_TRIVIAL( trace ) << "[SelectionParser::checkFeatures] "
                             << "Checking features";
  for ( size_t i = 0; i < m_featureIndicesOfSelectors.size(); i++ ) {
    auto featureIndex = m_featureIndicesOfSelectors.at( i );
    if ( ( featureIndex < features.size() ) && ( i < m_selectors.size() ) ) {
      auto value = features.at( featureIndex );
      auto func  = m_selectors.at( i );
      BOOST_LOG_TRIVIAL( trace ) << "                                 " << m_featureNames.at( featureIndex ) << ": "
                                 << value << " will " << ( !func( value ) ? "not" : "" ) << " pass";
      if ( !func( value ) ) return false;
    } else {
      BOOST_LOG_TRIVIAL( warning ) << "[SelectionParser::checkFeatures] "
                                   << "featureIndex is out of range!"
                                   << " (featureIndex==" << featureIndex << ") < (features.size()==" << features.size()
                                   << ") or (i==" << i << ") < (m_selectors.size()==" << m_selectors.size() << ")\n"
                                   << " Did you set the featureNames of your featureGenerator and "
                                   << "the SelectionParser?";
      return false;
    }
  }
  BOOST_LOG_TRIVIAL( trace ) << "                                 "
                             << "All Features passed!";
  return true;
}

std::vector<std::string> SelectionParser::getFeaturesNames() const { return m_featureNames; }

void SelectionParser::setFeatureAliases( const std::map<std::string, std::string>& aliases ) {
  m_aliasMap = aliases;
  m_reverseAliasMap.clear();
  for ( auto& alias : aliases ) { m_reverseAliasMap.insert( std::make_pair( alias.second, alias.first ) ); }
}

bool SelectionParser::checkFeatureAtPosition( double& value, size_t& pos ) {
  auto passing = true;
  for ( auto& i : m_selectorIndicesOfFeatures.at( pos ) ) passing &= m_selectors.at( i )( value );
  return passing;
}
