/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: INNetTool.h,v 1.6 2010-02-02 19:29:08 musy Exp $
#ifndef NNETTOOL_INNETTOOL_H
#define NNETTOOL_INNETTOOL_H 1

// Include files
// from STL
#include <TROOT.h>
#include <string>
// from Gaudi
#include "GaudiKernel/IAlgTool.h"

static const InterfaceID IID_INNetTool( "INNetTool", 1, 0 );

/** @class INNetTool INNetTool.h
 *
 *  v1.2
 *  @author Marco Musy (Milano)
 *  @date   2004-12-14
 */
class INNetTool : virtual public IAlgTool {
public:
  /// Retrieve interface ID
  static const InterfaceID& interfaceID() { return IID_INNetTool; };

  virtual double MLPm( std::vector<double>& )   = 0;
  virtual double MLPe( std::vector<double>& )   = 0;
  virtual double MLPk( std::vector<double>& )   = 0;
  virtual double MLPkS( std::vector<double>& )  = 0;
  virtual double MLPpS( std::vector<double>& )  = 0;
  virtual double MLPvtx( std::vector<double>& ) = 0;
};
#endif // NNETTOOL_INNETTOOL_H
