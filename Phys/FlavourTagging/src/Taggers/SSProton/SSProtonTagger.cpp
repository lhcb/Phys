/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "SSProtonTagger.h"

// from Gaudi

// from Phys

// from local

DECLARE_COMPONENT( SSProtonTagger )

using namespace std;
using namespace LHCb;

SSProtonTagger::SSProtonTagger( const std::string& type, const std::string& name, const IInterface* parent )
    : GaudiTool( type, name, parent ) {
  declareInterface<ITagger>( this );
}

StatusCode SSProtonTagger::initialize() {
  auto sc = GaudiTool::initialize();
  if ( sc.isFailure() ) return sc;

  m_pipeline = make_unique<Pipeline>();
  m_pipeline->setSortingFeature( m_sortingFeature );
  m_pipeline->setToolProvider( this );
  m_pipeline->setAliases( m_featureAliases );
  m_pipeline->setPipeline( m_selectionPipeline );

  return StatusCode::SUCCESS;
}

Tagger SSProtonTagger::tag( const Particle* sigPart, const RecVertex* assocVtx, const int,
                            Particle::ConstVector& tagParts ) {
  Tagger tagObject;

  m_pipeline->setReconstructionVertex( assocVtx );
  m_pipeline->setSignalCandidate( sigPart );

  auto selectedTaggingProton = m_pipeline->selectOne( tagParts );

  if ( selectedTaggingProton ) {
    if ( msgLevel( MSG::DEBUG ) ) {
      m_pipeline->setTaggingParticle( selectedTaggingProton );
      debug() << "[SSProtonTagger::tag] Found Tagging Proton!" << endmsg;
      debug() << "\n"
              << TaggingHelpers::printFeatureVector( m_pipeline->mergedFeatureNames(), m_pipeline->mergedFeatures(),
                                                     "\t[SSProtonTagger::tag]" )
              << endmsg;
    }

    // this is correct only if the last feature within the whole pipeline
    // actually is the final mva (see SSProtonTaggerConf.py)
    auto mvaValue = m_pipeline->features( selectedTaggingProton ).back().back();

    if ( msgLevel( MSG::DEBUG ) ) debug() << "[SSProtonTagger::tag] MVA prediction: " << mvaValue << endmsg;

    // if the mva predicts, that the tag will be *in*correct, the sign
    // needs to be flipped
    int  decision = selectedTaggingProton->charge();
    auto omega    = 1 - mvaValue;
    if ( mvaValue < 0.5 ) {
      decision *= -1;
      omega = mvaValue;
    }

    if ( msgLevel( MSG::DEBUG ) ) debug() << "[SSProtonTagger] tagged " << decision << endmsg;

    // omega (actually eta) is the mistag estimate, this needs to be calibrated
    // later (@TODO: or at this point via a calibration tool?)
    tagObject.setOmega( omega );
    tagObject.setDecision( decision );
    tagObject.setType( taggerType() );
    tagObject.addToTaggerParts( selectedTaggingProton );
    tagObject.setMvaValue( mvaValue );
    tagObject.setCharge( selectedTaggingProton->charge() );
  } else if ( msgLevel( MSG::DEBUG ) ) {
    debug() << "[SSProtonTagger::tag] No tagging Proton found!" << endmsg;
  }

  return tagObject;
}

Tagger SSProtonTagger::tag( const Particle* sigPart, const RecVertex* assocVtx, RecVertex::ConstVector& puVtxs,
                            Particle::ConstVector& tagParts ) {
  m_pipeline->setPileUpVertices( puVtxs );
  return tag( sigPart, assocVtx, puVtxs.size(), tagParts );
}

std::vector<std::string> SSProtonTagger::featureNames() const { return m_pipeline->mergedFeatureNames(); }

std::vector<double> SSProtonTagger::featureValues() const { return m_pipeline->mergedFeatures(); }

std::vector<std::string> SSProtonTagger::featureNamesTagParts() const { return m_pipeline->mergedFeatureNames(); }

std::vector<std::vector<double>> SSProtonTagger::featureValuesTagParts() const {
  return m_pipeline->mergedFeatureMatrix();
}
