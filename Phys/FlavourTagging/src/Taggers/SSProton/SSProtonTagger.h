/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

// from Gaudi
#include "GaudiAlg/GaudiTool.h"

// from LHCb
#include "Event/FlavourTag.h"

// from Phys
#include "Kernel/ITagger.h"

// local
#include "src/Selection/Pipeline.h"

/**
 * @brief      Tags a candidate using a myon from the opposite-side decay
 *
 * @authors    Marco Musy, Stefania Vecchi, Kevin Heinicke, Julian Wishahi
 * @date       2005-2017
 */
class SSProtonTagger : public GaudiTool, virtual public ITagger {
public:
  SSProtonTagger( const std::string& type, const std::string& name, const IInterface* parent );

  StatusCode initialize() override;

  LHCb::Tagger::TaggerType taggerType() const override { return LHCb::Tagger::TaggerType::SSProton; }

  LHCb::Tagger tag( const LHCb::Particle* sigPart, const LHCb::RecVertex* assocVtx, const int nPUVtxs,
                    LHCb::Particle::ConstVector& ) override;

  LHCb::Tagger tag( const LHCb::Particle* sigPart, const LHCb::RecVertex* assocVtx,
                    LHCb::RecVertex::ConstVector& puVtxs, LHCb::Particle::ConstVector& tagParts ) override;

  std::vector<std::string> featureNames() const override;

  std::vector<double> featureValues() const override;

  std::vector<std::string> featureNamesTagParts() const override;

  std::vector<std::vector<double>> featureValuesTagParts() const override;

private:
  Gaudi::Property<std::vector<std::vector<std::string>>> m_selectionPipeline{this, "SelectionPipeline", {{}}, ""};
  Gaudi::Property<std::map<std::string, std::string>>    m_featureAliases{this, "Aliases", {}, ""};
  std::unique_ptr<Pipeline>                              m_pipeline       = nullptr;
  std::string                                            m_sortingFeature = "mva";
};
