/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include <vector>

#include "../SSProton_Data_Run1_All_Bd2Dpi_TMVA_BDT_v1r1.h"

/* @brief a BDT implementation, returning the sum of all tree weights given
 * a feature vector
 */
double SSProton_Data_Run1_All_Bd2Dpi_TMVA_BDT_v1r1::tree_9( const std::vector<double>& features ) const {
  double sum = 0;

  // tree 2700
  if ( features[6] < 2.32779 ) {
    if ( features[8] < 3.05694 ) {
      sum += -0.000103067;
    } else {
      sum += 0.000103067;
    }
  } else {
    if ( features[0] < 1.77191 ) {
      sum += -0.000103067;
    } else {
      sum += 0.000103067;
    }
  }
  // tree 2701
  if ( features[8] < 2.38156 ) {
    if ( features[3] < 0.390309 ) {
      sum += -5.69588e-05;
    } else {
      sum += 5.69588e-05;
    }
  } else {
    if ( features[3] < 0.380456 ) {
      sum += 5.69588e-05;
    } else {
      sum += -5.69588e-05;
    }
  }
  // tree 2702
  if ( features[8] < 2.38156 ) {
    if ( features[1] < -0.162004 ) {
      sum += 5.8407e-05;
    } else {
      sum += -5.8407e-05;
    }
  } else {
    if ( features[4] < -2.7239 ) {
      sum += -5.8407e-05;
    } else {
      sum += 5.8407e-05;
    }
  }
  // tree 2703
  if ( features[8] < 2.38156 ) {
    if ( features[1] < 0.0265351 ) {
      sum += -5.83203e-05;
    } else {
      sum += 5.83203e-05;
    }
  } else {
    if ( features[7] < 4.57139 ) {
      sum += 5.83203e-05;
    } else {
      sum += -5.83203e-05;
    }
  }
  // tree 2704
  if ( features[0] < 1.93071 ) {
    if ( features[1] < -0.581424 ) {
      sum += 9.64513e-05;
    } else {
      sum += -9.64513e-05;
    }
  } else {
    if ( features[5] < 0.751479 ) {
      sum += 9.64513e-05;
    } else {
      sum += -9.64513e-05;
    }
  }
  // tree 2705
  if ( features[8] < 2.38156 ) {
    if ( features[3] < 0.390309 ) {
      sum += -5.67457e-05;
    } else {
      sum += 5.67457e-05;
    }
  } else {
    if ( features[4] < -1.10944 ) {
      sum += 5.67457e-05;
    } else {
      sum += -5.67457e-05;
    }
  }
  // tree 2706
  if ( features[7] < 3.73601 ) {
    sum += -4.86945e-05;
  } else {
    if ( features[6] < 2.32779 ) {
      sum += -4.86945e-05;
    } else {
      sum += 4.86945e-05;
    }
  }
  // tree 2707
  if ( features[8] < 2.38156 ) {
    if ( features[1] < 0.0265351 ) {
      sum += -5.77944e-05;
    } else {
      sum += 5.77944e-05;
    }
  } else {
    if ( features[1] < -0.185621 ) {
      sum += -5.77944e-05;
    } else {
      sum += 5.77944e-05;
    }
  }
  // tree 2708
  if ( features[6] < 2.32779 ) {
    if ( features[8] < 3.05694 ) {
      sum += -0.000102413;
    } else {
      sum += 0.000102413;
    }
  } else {
    if ( features[0] < 1.77191 ) {
      sum += -0.000102413;
    } else {
      sum += 0.000102413;
    }
  }
  // tree 2709
  if ( features[8] < 2.38156 ) {
    if ( features[3] < 0.390309 ) {
      sum += -5.68966e-05;
    } else {
      sum += 5.68966e-05;
    }
  } else {
    if ( features[4] < -2.7239 ) {
      sum += -5.68966e-05;
    } else {
      sum += 5.68966e-05;
    }
  }
  // tree 2710
  if ( features[8] < 2.38156 ) {
    if ( features[3] < 0.390309 ) {
      sum += -5.67076e-05;
    } else {
      sum += 5.67076e-05;
    }
  } else {
    if ( features[3] < 0.380456 ) {
      sum += 5.67076e-05;
    } else {
      sum += -5.67076e-05;
    }
  }
  // tree 2711
  if ( features[5] < 0.245271 ) {
    if ( features[7] < 4.64755 ) {
      sum += 9.67048e-05;
    } else {
      sum += -9.67048e-05;
    }
  } else {
    if ( features[7] < 4.33271 ) {
      sum += -9.67048e-05;
    } else {
      sum += 9.67048e-05;
    }
  }
  // tree 2712
  if ( features[8] < 2.38156 ) {
    if ( features[3] < 0.390309 ) {
      sum += -5.64499e-05;
    } else {
      sum += 5.64499e-05;
    }
  } else {
    if ( features[8] < 3.45532 ) {
      sum += 5.64499e-05;
    } else {
      sum += -5.64499e-05;
    }
  }
  // tree 2713
  if ( features[5] < 0.245271 ) {
    if ( features[8] < 2.38187 ) {
      sum += -7.88625e-05;
    } else {
      sum += 7.88625e-05;
    }
  } else {
    if ( features[7] < 4.33271 ) {
      sum += -7.88625e-05;
    } else {
      sum += 7.88625e-05;
    }
  }
  // tree 2714
  if ( features[5] < 0.245271 ) {
    if ( features[4] < -1.43121 ) {
      sum += 7.70306e-05;
    } else {
      sum += -7.70306e-05;
    }
  } else {
    if ( features[8] < 3.18029 ) {
      sum += -7.70306e-05;
    } else {
      sum += 7.70306e-05;
    }
  }
  // tree 2715
  if ( features[5] < 0.245271 ) {
    if ( features[4] < -1.43121 ) {
      sum += 7.89724e-05;
    } else {
      sum += -7.89724e-05;
    }
  } else {
    if ( features[4] < -0.463655 ) {
      sum += -7.89724e-05;
    } else {
      sum += 7.89724e-05;
    }
  }
  // tree 2716
  if ( features[5] < 0.245271 ) {
    if ( features[7] < 4.64755 ) {
      sum += 9.59088e-05;
    } else {
      sum += -9.59088e-05;
    }
  } else {
    if ( features[7] < 4.33271 ) {
      sum += -9.59088e-05;
    } else {
      sum += 9.59088e-05;
    }
  }
  // tree 2717
  if ( features[8] < 3.15342 ) {
    if ( features[3] < 0.714531 ) {
      sum += -0.000121909;
    } else {
      sum += 0.000121909;
    }
  } else {
    if ( features[3] < 0.721502 ) {
      sum += 0.000121909;
    } else {
      sum += -0.000121909;
    }
  }
  // tree 2718
  if ( features[5] < 0.245271 ) {
    if ( features[7] < 4.64755 ) {
      sum += 9.59236e-05;
    } else {
      sum += -9.59236e-05;
    }
  } else {
    if ( features[0] < 3.44297 ) {
      sum += -9.59236e-05;
    } else {
      sum += 9.59236e-05;
    }
  }
  // tree 2719
  if ( features[8] < 2.38156 ) {
    if ( features[3] < 0.390309 ) {
      sum += -5.6348e-05;
    } else {
      sum += 5.6348e-05;
    }
  } else {
    if ( features[3] < 0.380456 ) {
      sum += 5.6348e-05;
    } else {
      sum += -5.6348e-05;
    }
  }
  // tree 2720
  if ( features[6] < 2.32779 ) {
    if ( features[8] < 3.05694 ) {
      sum += -9.79273e-05;
    } else {
      sum += 9.79273e-05;
    }
  } else {
    if ( features[3] < 0.951513 ) {
      sum += 9.79273e-05;
    } else {
      sum += -9.79273e-05;
    }
  }
  // tree 2721
  if ( features[8] < 2.38156 ) {
    if ( features[1] < -0.162004 ) {
      sum += 5.83072e-05;
    } else {
      sum += -5.83072e-05;
    }
  } else {
    if ( features[4] < -2.7239 ) {
      sum += -5.83072e-05;
    } else {
      sum += 5.83072e-05;
    }
  }
  // tree 2722
  if ( features[0] < 3.03054 ) {
    if ( features[6] < 2.67893 ) {
      sum += -7.6104e-05;
    } else {
      sum += 7.6104e-05;
    }
  } else {
    if ( features[5] < 0.712418 ) {
      sum += 7.6104e-05;
    } else {
      sum += -7.6104e-05;
    }
  }
  // tree 2723
  if ( features[0] < 1.93071 ) {
    if ( features[5] < 0.883423 ) {
      sum += -7.86704e-05;
    } else {
      sum += 7.86704e-05;
    }
  } else {
    if ( features[5] < 0.751479 ) {
      sum += 7.86704e-05;
    } else {
      sum += -7.86704e-05;
    }
  }
  // tree 2724
  if ( features[8] < 2.38156 ) {
    if ( features[1] < -0.162004 ) {
      sum += 5.83286e-05;
    } else {
      sum += -5.83286e-05;
    }
  } else {
    if ( features[7] < 4.57139 ) {
      sum += 5.83286e-05;
    } else {
      sum += -5.83286e-05;
    }
  }
  // tree 2725
  if ( features[5] < 0.329645 ) {
    if ( features[5] < 0.253431 ) {
      sum += 9.80743e-05;
    } else {
      sum += -9.80743e-05;
    }
  } else {
    if ( features[7] < 4.69073 ) {
      sum += -9.80743e-05;
    } else {
      sum += 9.80743e-05;
    }
  }
  // tree 2726
  if ( features[8] < 2.38156 ) {
    if ( features[1] < 0.0265351 ) {
      sum += -5.83083e-05;
    } else {
      sum += 5.83083e-05;
    }
  } else {
    if ( features[7] < 4.57139 ) {
      sum += 5.83083e-05;
    } else {
      sum += -5.83083e-05;
    }
  }
  // tree 2727
  if ( features[8] < 2.38156 ) {
    if ( features[3] < 0.390309 ) {
      sum += -5.65088e-05;
    } else {
      sum += 5.65088e-05;
    }
  } else {
    if ( features[8] < 3.45532 ) {
      sum += 5.65088e-05;
    } else {
      sum += -5.65088e-05;
    }
  }
  // tree 2728
  if ( features[8] < 2.38156 ) {
    if ( features[4] < -1.41151 ) {
      sum += -5.55512e-05;
    } else {
      sum += 5.55512e-05;
    }
  } else {
    if ( features[7] < 4.57139 ) {
      sum += 5.55512e-05;
    } else {
      sum += -5.55512e-05;
    }
  }
  // tree 2729
  if ( features[6] < 2.32779 ) {
    if ( features[6] < 1.61417 ) {
      sum += 9.69725e-05;
    } else {
      sum += -9.69725e-05;
    }
  } else {
    if ( features[0] < 1.77191 ) {
      sum += -9.69725e-05;
    } else {
      sum += 9.69725e-05;
    }
  }
  // tree 2730
  if ( features[5] < 0.329645 ) {
    if ( features[7] < 4.64755 ) {
      sum += 0.000101385;
    } else {
      sum += -0.000101385;
    }
  } else {
    if ( features[6] < 2.67895 ) {
      sum += -0.000101385;
    } else {
      sum += 0.000101385;
    }
  }
  // tree 2731
  if ( features[5] < 0.329645 ) {
    if ( features[0] < 2.82292 ) {
      sum += -8.90519e-05;
    } else {
      sum += 8.90519e-05;
    }
  } else {
    if ( features[4] < -0.463655 ) {
      sum += -8.90519e-05;
    } else {
      sum += 8.90519e-05;
    }
  }
  // tree 2732
  if ( features[7] < 3.73601 ) {
    sum += -5.71221e-05;
  } else {
    if ( features[3] < 1.14936 ) {
      sum += 5.71221e-05;
    } else {
      sum += -5.71221e-05;
    }
  }
  // tree 2733
  if ( features[6] < 2.32779 ) {
    if ( features[2] < 0.313175 ) {
      sum += 7.59719e-05;
    } else {
      sum += -7.59719e-05;
    }
  } else {
    sum += 7.59719e-05;
  }
  // tree 2734
  if ( features[2] < 0.742337 ) {
    if ( features[8] < 2.86397 ) {
      sum += -0.000103577;
    } else {
      sum += 0.000103577;
    }
  } else {
    if ( features[3] < 0.879452 ) {
      sum += 0.000103577;
    } else {
      sum += -0.000103577;
    }
  }
  // tree 2735
  if ( features[5] < 0.329645 ) {
    if ( features[7] < 4.64755 ) {
      sum += 0.000114793;
    } else {
      sum += -0.000114793;
    }
  } else {
    if ( features[4] < -0.463655 ) {
      sum += -0.000114793;
    } else {
      sum += 0.000114793;
    }
  }
  // tree 2736
  if ( features[5] < 0.329645 ) {
    if ( features[7] < 4.53009 ) {
      sum += 0.000102397;
    } else {
      sum += -0.000102397;
    }
  } else {
    if ( features[1] < -0.53912 ) {
      sum += 0.000102397;
    } else {
      sum += -0.000102397;
    }
  }
  // tree 2737
  if ( features[8] < 2.09211 ) {
    if ( features[3] < 0.710516 ) {
      sum += -8.36418e-05;
    } else {
      sum += 8.36418e-05;
    }
  } else {
    if ( features[2] < 0.527756 ) {
      sum += 8.36418e-05;
    } else {
      sum += -8.36418e-05;
    }
  }
  // tree 2738
  if ( features[8] < 2.09211 ) {
    if ( features[3] < 0.710516 ) {
      sum += -8.31948e-05;
    } else {
      sum += 8.31948e-05;
    }
  } else {
    if ( features[2] < 0.527756 ) {
      sum += 8.31948e-05;
    } else {
      sum += -8.31948e-05;
    }
  }
  // tree 2739
  if ( features[0] < 3.03054 ) {
    if ( features[4] < -1.10944 ) {
      sum += 7.71519e-05;
    } else {
      sum += -7.71519e-05;
    }
  } else {
    if ( features[1] < 0.0281889 ) {
      sum += -7.71519e-05;
    } else {
      sum += 7.71519e-05;
    }
  }
  // tree 2740
  if ( features[7] < 3.73601 ) {
    sum += -6.14537e-05;
  } else {
    if ( features[4] < -3.0468 ) {
      sum += -6.14537e-05;
    } else {
      sum += 6.14537e-05;
    }
  }
  // tree 2741
  if ( features[8] < 2.38156 ) {
    if ( features[1] < -0.162004 ) {
      sum += 5.81408e-05;
    } else {
      sum += -5.81408e-05;
    }
  } else {
    if ( features[8] < 3.45532 ) {
      sum += 5.81408e-05;
    } else {
      sum += -5.81408e-05;
    }
  }
  // tree 2742
  if ( features[6] < 2.32779 ) {
    if ( features[8] < 3.05694 ) {
      sum += -0.000101154;
    } else {
      sum += 0.000101154;
    }
  } else {
    if ( features[0] < 1.77191 ) {
      sum += -0.000101154;
    } else {
      sum += 0.000101154;
    }
  }
  // tree 2743
  if ( features[8] < 2.38156 ) {
    if ( features[0] < 2.20567 ) {
      sum += 5.59841e-05;
    } else {
      sum += -5.59841e-05;
    }
  } else {
    if ( features[4] < -2.7239 ) {
      sum += -5.59841e-05;
    } else {
      sum += 5.59841e-05;
    }
  }
  // tree 2744
  if ( features[1] < 0.309319 ) {
    if ( features[3] < 0.823237 ) {
      sum += -9.2045e-05;
    } else {
      sum += 9.2045e-05;
    }
  } else {
    if ( features[4] < -0.774054 ) {
      sum += 9.2045e-05;
    } else {
      sum += -9.2045e-05;
    }
  }
  // tree 2745
  if ( features[8] < 2.38156 ) {
    if ( features[3] < 0.390309 ) {
      sum += -5.61325e-05;
    } else {
      sum += 5.61325e-05;
    }
  } else {
    if ( features[4] < -1.10944 ) {
      sum += 5.61325e-05;
    } else {
      sum += -5.61325e-05;
    }
  }
  // tree 2746
  if ( features[8] < 2.38156 ) {
    if ( features[1] < -0.162004 ) {
      sum += 5.7802e-05;
    } else {
      sum += -5.7802e-05;
    }
  } else {
    if ( features[3] < 0.380456 ) {
      sum += 5.7802e-05;
    } else {
      sum += -5.7802e-05;
    }
  }
  // tree 2747
  if ( features[8] < 2.38156 ) {
    if ( features[7] < 4.29516 ) {
      sum += -5.56905e-05;
    } else {
      sum += 5.56905e-05;
    }
  } else {
    if ( features[8] < 3.45532 ) {
      sum += 5.56905e-05;
    } else {
      sum += -5.56905e-05;
    }
  }
  // tree 2748
  if ( features[8] < 2.38156 ) {
    if ( features[1] < 0.0265351 ) {
      sum += -5.75503e-05;
    } else {
      sum += 5.75503e-05;
    }
  } else {
    if ( features[5] < 0.248817 ) {
      sum += 5.75503e-05;
    } else {
      sum += -5.75503e-05;
    }
  }
  // tree 2749
  if ( features[8] < 2.38156 ) {
    if ( features[1] < 0.0265351 ) {
      sum += -5.78268e-05;
    } else {
      sum += 5.78268e-05;
    }
  } else {
    if ( features[8] < 3.45532 ) {
      sum += 5.78268e-05;
    } else {
      sum += -5.78268e-05;
    }
  }
  // tree 2750
  if ( features[8] < 2.38156 ) {
    if ( features[1] < -0.162004 ) {
      sum += 5.7702e-05;
    } else {
      sum += -5.7702e-05;
    }
  } else {
    if ( features[4] < -2.7239 ) {
      sum += -5.7702e-05;
    } else {
      sum += 5.7702e-05;
    }
  }
  // tree 2751
  if ( features[5] < 0.245271 ) {
    if ( features[7] < 4.64755 ) {
      sum += 9.50919e-05;
    } else {
      sum += -9.50919e-05;
    }
  } else {
    if ( features[7] < 4.33271 ) {
      sum += -9.50919e-05;
    } else {
      sum += 9.50919e-05;
    }
  }
  // tree 2752
  if ( features[8] < 3.15342 ) {
    if ( features[3] < 0.714531 ) {
      sum += -0.000117432;
    } else {
      sum += 0.000117432;
    }
  } else {
    if ( features[5] < 0.795254 ) {
      sum += 0.000117432;
    } else {
      sum += -0.000117432;
    }
  }
  // tree 2753
  if ( features[8] < 3.15342 ) {
    if ( features[3] < 0.714531 ) {
      sum += -0.000116805;
    } else {
      sum += 0.000116805;
    }
  } else {
    if ( features[5] < 0.795254 ) {
      sum += 0.000116805;
    } else {
      sum += -0.000116805;
    }
  }
  // tree 2754
  if ( features[8] < 3.15342 ) {
    if ( features[3] < 0.714531 ) {
      sum += -0.000114298;
    } else {
      sum += 0.000114298;
    }
  } else {
    if ( features[7] < 4.70473 ) {
      sum += 0.000114298;
    } else {
      sum += -0.000114298;
    }
  }
  // tree 2755
  if ( features[8] < 3.15342 ) {
    if ( features[5] < 0.878077 ) {
      sum += -9.9051e-05;
    } else {
      sum += 9.9051e-05;
    }
  } else {
    if ( features[4] < -0.709333 ) {
      sum += 9.9051e-05;
    } else {
      sum += -9.9051e-05;
    }
  }
  // tree 2756
  if ( features[8] < 3.15342 ) {
    if ( features[6] < 1.53772 ) {
      sum += 8.48666e-05;
    } else {
      sum += -8.48666e-05;
    }
  } else {
    sum += 8.48666e-05;
  }
  // tree 2757
  if ( features[8] < 3.15342 ) {
    if ( features[5] < 0.878077 ) {
      sum += -9.84077e-05;
    } else {
      sum += 9.84077e-05;
    }
  } else {
    if ( features[4] < -0.709333 ) {
      sum += 9.84077e-05;
    } else {
      sum += -9.84077e-05;
    }
  }
  // tree 2758
  if ( features[8] < 2.09211 ) {
    if ( features[6] < 2.13557 ) {
      sum += 0.000102408;
    } else {
      sum += -0.000102408;
    }
  } else {
    if ( features[6] < 2.15222 ) {
      sum += -0.000102408;
    } else {
      sum += 0.000102408;
    }
  }
  // tree 2759
  if ( features[5] < 0.329645 ) {
    if ( features[7] < 4.64755 ) {
      sum += 0.00010138;
    } else {
      sum += -0.00010138;
    }
  } else {
    if ( features[3] < 0.662954 ) {
      sum += -0.00010138;
    } else {
      sum += 0.00010138;
    }
  }
  // tree 2760
  if ( features[8] < 2.38156 ) {
    if ( features[3] < 0.390309 ) {
      sum += -5.61757e-05;
    } else {
      sum += 5.61757e-05;
    }
  } else {
    if ( features[3] < 0.380456 ) {
      sum += 5.61757e-05;
    } else {
      sum += -5.61757e-05;
    }
  }
  // tree 2761
  if ( features[8] < 2.38156 ) {
    if ( features[4] < -1.41151 ) {
      sum += -5.50882e-05;
    } else {
      sum += 5.50882e-05;
    }
  } else {
    if ( features[3] < 0.380456 ) {
      sum += 5.50882e-05;
    } else {
      sum += -5.50882e-05;
    }
  }
  // tree 2762
  if ( features[8] < 2.38156 ) {
    if ( features[1] < 0.0265351 ) {
      sum += -5.79603e-05;
    } else {
      sum += 5.79603e-05;
    }
  } else {
    if ( features[3] < 0.380456 ) {
      sum += 5.79603e-05;
    } else {
      sum += -5.79603e-05;
    }
  }
  // tree 2763
  if ( features[3] < 1.04065 ) {
    if ( features[8] < 2.67103 ) {
      sum += -0.000109372;
    } else {
      sum += 0.000109372;
    }
  } else {
    if ( features[6] < 1.65196 ) {
      sum += 0.000109372;
    } else {
      sum += -0.000109372;
    }
  }
  // tree 2764
  if ( features[6] < 2.32779 ) {
    if ( features[8] < 3.05694 ) {
      sum += -0.000100224;
    } else {
      sum += 0.000100224;
    }
  } else {
    if ( features[0] < 1.77191 ) {
      sum += -0.000100224;
    } else {
      sum += 0.000100224;
    }
  }
  // tree 2765
  if ( features[8] < 2.09211 ) {
    if ( features[1] < -0.539051 ) {
      sum += 8.55499e-05;
    } else {
      sum += -8.55499e-05;
    }
  } else {
    if ( features[6] < 2.15222 ) {
      sum += -8.55499e-05;
    } else {
      sum += 8.55499e-05;
    }
  }
  // tree 2766
  if ( features[8] < 2.09211 ) {
    if ( features[6] < 2.13557 ) {
      sum += 0.000101367;
    } else {
      sum += -0.000101367;
    }
  } else {
    if ( features[6] < 2.15222 ) {
      sum += -0.000101367;
    } else {
      sum += 0.000101367;
    }
  }
  // tree 2767
  if ( features[8] < 2.09211 ) {
    if ( features[5] < 0.754281 ) {
      sum += 9.18713e-05;
    } else {
      sum += -9.18713e-05;
    }
  } else {
    if ( features[6] < 2.15222 ) {
      sum += -9.18713e-05;
    } else {
      sum += 9.18713e-05;
    }
  }
  // tree 2768
  if ( features[8] < 2.09211 ) {
    if ( features[6] < 2.13557 ) {
      sum += 8.04721e-05;
    } else {
      sum += -8.04721e-05;
    }
  } else {
    if ( features[1] < -0.17959 ) {
      sum += -8.04721e-05;
    } else {
      sum += 8.04721e-05;
    }
  }
  // tree 2769
  if ( features[0] < 3.03054 ) {
    if ( features[0] < 2.12578 ) {
      sum += 8.12952e-05;
    } else {
      sum += -8.12952e-05;
    }
  } else {
    if ( features[5] < 0.712418 ) {
      sum += 8.12952e-05;
    } else {
      sum += -8.12952e-05;
    }
  }
  // tree 2770
  if ( features[8] < 2.09211 ) {
    if ( features[6] < 2.13557 ) {
      sum += 0.000100317;
    } else {
      sum += -0.000100317;
    }
  } else {
    if ( features[6] < 2.15222 ) {
      sum += -0.000100317;
    } else {
      sum += 0.000100317;
    }
  }
  // tree 2771
  if ( features[8] < 2.09211 ) {
    if ( features[7] < 4.76501 ) {
      sum += 8.10459e-05;
    } else {
      sum += -8.10459e-05;
    }
  } else {
    if ( features[2] < 0.527756 ) {
      sum += 8.10459e-05;
    } else {
      sum += -8.10459e-05;
    }
  }
  // tree 2772
  if ( features[8] < 2.09211 ) {
    if ( features[2] < 0.394494 ) {
      sum += -8.45729e-05;
    } else {
      sum += 8.45729e-05;
    }
  } else {
    if ( features[6] < 2.15222 ) {
      sum += -8.45729e-05;
    } else {
      sum += 8.45729e-05;
    }
  }
  // tree 2773
  if ( features[8] < 2.09211 ) {
    if ( features[5] < 0.754281 ) {
      sum += 8.59582e-05;
    } else {
      sum += -8.59582e-05;
    }
  } else {
    if ( features[2] < 0.527756 ) {
      sum += 8.59582e-05;
    } else {
      sum += -8.59582e-05;
    }
  }
  // tree 2774
  if ( features[8] < 2.09211 ) {
    if ( features[5] < 0.754281 ) {
      sum += 6.98417e-05;
    } else {
      sum += -6.98417e-05;
    }
  } else {
    if ( features[0] < 1.65576 ) {
      sum += -6.98417e-05;
    } else {
      sum += 6.98417e-05;
    }
  }
  // tree 2775
  if ( features[5] < 0.329645 ) {
    if ( features[3] < 0.421425 ) {
      sum += 7.8304e-05;
    } else {
      sum += -7.8304e-05;
    }
  } else {
    if ( features[5] < 0.893056 ) {
      sum += -7.8304e-05;
    } else {
      sum += 7.8304e-05;
    }
  }
  // tree 2776
  if ( features[8] < 2.09211 ) {
    if ( features[6] < 2.13557 ) {
      sum += 9.93569e-05;
    } else {
      sum += -9.93569e-05;
    }
  } else {
    if ( features[6] < 2.15222 ) {
      sum += -9.93569e-05;
    } else {
      sum += 9.93569e-05;
    }
  }
  // tree 2777
  if ( features[0] < 3.03054 ) {
    if ( features[0] < 2.12578 ) {
      sum += 8.10402e-05;
    } else {
      sum += -8.10402e-05;
    }
  } else {
    if ( features[5] < 0.712418 ) {
      sum += 8.10402e-05;
    } else {
      sum += -8.10402e-05;
    }
  }
  // tree 2778
  if ( features[8] < 2.09211 ) {
    if ( features[5] < 0.754281 ) {
      sum += 8.99721e-05;
    } else {
      sum += -8.99721e-05;
    }
  } else {
    if ( features[6] < 2.15222 ) {
      sum += -8.99721e-05;
    } else {
      sum += 8.99721e-05;
    }
  }
  // tree 2779
  if ( features[8] < 2.09211 ) {
    if ( features[5] < 0.754281 ) {
      sum += 8.94933e-05;
    } else {
      sum += -8.94933e-05;
    }
  } else {
    if ( features[6] < 2.15222 ) {
      sum += -8.94933e-05;
    } else {
      sum += 8.94933e-05;
    }
  }
  // tree 2780
  if ( features[5] < 0.329645 ) {
    if ( features[7] < 4.64755 ) {
      sum += 0.00010228;
    } else {
      sum += -0.00010228;
    }
  } else {
    if ( features[1] < -0.53912 ) {
      sum += 0.00010228;
    } else {
      sum += -0.00010228;
    }
  }
  // tree 2781
  if ( features[8] < 2.09211 ) {
    if ( features[6] < 2.13557 ) {
      sum += 9.8039e-05;
    } else {
      sum += -9.8039e-05;
    }
  } else {
    if ( features[6] < 2.15222 ) {
      sum += -9.8039e-05;
    } else {
      sum += 9.8039e-05;
    }
  }
  // tree 2782
  if ( features[8] < 2.09211 ) {
    if ( features[1] < -0.539051 ) {
      sum += 8.18266e-05;
    } else {
      sum += -8.18266e-05;
    }
  } else {
    if ( features[6] < 2.15222 ) {
      sum += -8.18266e-05;
    } else {
      sum += 8.18266e-05;
    }
  }
  // tree 2783
  if ( features[3] < 1.04065 ) {
    if ( features[6] < 2.24 ) {
      sum += -0.000109338;
    } else {
      sum += 0.000109338;
    }
  } else {
    if ( features[6] < 1.65196 ) {
      sum += 0.000109338;
    } else {
      sum += -0.000109338;
    }
  }
  // tree 2784
  if ( features[8] < 2.09211 ) {
    if ( features[6] < 2.13557 ) {
      sum += 9.69533e-05;
    } else {
      sum += -9.69533e-05;
    }
  } else {
    if ( features[6] < 2.15222 ) {
      sum += -9.69533e-05;
    } else {
      sum += 9.69533e-05;
    }
  }
  // tree 2785
  if ( features[0] < 1.93071 ) {
    if ( features[5] < 0.883423 ) {
      sum += -7.8761e-05;
    } else {
      sum += 7.8761e-05;
    }
  } else {
    if ( features[5] < 0.751479 ) {
      sum += 7.8761e-05;
    } else {
      sum += -7.8761e-05;
    }
  }
  // tree 2786
  if ( features[5] < 0.329645 ) {
    if ( features[4] < -1.32703 ) {
      sum += 9.82141e-05;
    } else {
      sum += -9.82141e-05;
    }
  } else {
    if ( features[7] < 4.69073 ) {
      sum += -9.82141e-05;
    } else {
      sum += 9.82141e-05;
    }
  }
  // tree 2787
  if ( features[0] < 1.93071 ) {
    if ( features[1] < -0.581424 ) {
      sum += 9.63645e-05;
    } else {
      sum += -9.63645e-05;
    }
  } else {
    if ( features[5] < 0.751479 ) {
      sum += 9.63645e-05;
    } else {
      sum += -9.63645e-05;
    }
  }
  // tree 2788
  if ( features[8] < 2.09211 ) {
    if ( features[5] < 0.754281 ) {
      sum += 8.75955e-05;
    } else {
      sum += -8.75955e-05;
    }
  } else {
    if ( features[6] < 2.15222 ) {
      sum += -8.75955e-05;
    } else {
      sum += 8.75955e-05;
    }
  }
  // tree 2789
  if ( features[0] < 3.03054 ) {
    if ( features[5] < 0.920264 ) {
      sum += -7.62809e-05;
    } else {
      sum += 7.62809e-05;
    }
  } else {
    if ( features[5] < 0.712418 ) {
      sum += 7.62809e-05;
    } else {
      sum += -7.62809e-05;
    }
  }
  // tree 2790
  if ( features[8] < 2.09211 ) {
    if ( features[6] < 2.13557 ) {
      sum += 9.41645e-05;
    } else {
      sum += -9.41645e-05;
    }
  } else {
    if ( features[2] < 0.527756 ) {
      sum += 9.41645e-05;
    } else {
      sum += -9.41645e-05;
    }
  }
  // tree 2791
  if ( features[1] < 0.309319 ) {
    if ( features[3] < 0.823237 ) {
      sum += -9.06216e-05;
    } else {
      sum += 9.06216e-05;
    }
  } else {
    if ( features[4] < -0.774054 ) {
      sum += 9.06216e-05;
    } else {
      sum += -9.06216e-05;
    }
  }
  // tree 2792
  if ( features[8] < 2.09211 ) {
    if ( features[5] < 0.754281 ) {
      sum += 8.47797e-05;
    } else {
      sum += -8.47797e-05;
    }
  } else {
    if ( features[2] < 0.527756 ) {
      sum += 8.47797e-05;
    } else {
      sum += -8.47797e-05;
    }
  }
  // tree 2793
  if ( features[3] < 1.04065 ) {
    if ( features[6] < 2.24 ) {
      sum += -0.00010857;
    } else {
      sum += 0.00010857;
    }
  } else {
    if ( features[6] < 1.65196 ) {
      sum += 0.00010857;
    } else {
      sum += -0.00010857;
    }
  }
  // tree 2794
  if ( features[8] < 2.09211 ) {
    if ( features[6] < 2.13557 ) {
      sum += 7.83493e-05;
    } else {
      sum += -7.83493e-05;
    }
  } else {
    if ( features[0] < 1.65576 ) {
      sum += -7.83493e-05;
    } else {
      sum += 7.83493e-05;
    }
  }
  // tree 2795
  if ( features[2] < -0.974311 ) {
    sum += -5.01781e-05;
  } else {
    if ( features[6] < 2.32779 ) {
      sum += -5.01781e-05;
    } else {
      sum += 5.01781e-05;
    }
  }
  // tree 2796
  if ( features[8] < 2.09211 ) {
    if ( features[6] < 2.13557 ) {
      sum += 9.32635e-05;
    } else {
      sum += -9.32635e-05;
    }
  } else {
    if ( features[2] < 0.527756 ) {
      sum += 9.32635e-05;
    } else {
      sum += -9.32635e-05;
    }
  }
  // tree 2797
  if ( features[0] < 3.03054 ) {
    if ( features[7] < 4.33271 ) {
      sum += -6.25112e-05;
    } else {
      sum += 6.25112e-05;
    }
  } else {
    sum += 6.25112e-05;
  }
  // tree 2798
  if ( features[5] < 0.329645 ) {
    if ( features[7] < 4.53009 ) {
      sum += 0.000114234;
    } else {
      sum += -0.000114234;
    }
  } else {
    if ( features[4] < -0.463655 ) {
      sum += -0.000114234;
    } else {
      sum += 0.000114234;
    }
  }
  // tree 2799
  if ( features[8] < 2.09211 ) {
    if ( features[3] < 0.710516 ) {
      sum += -8.01074e-05;
    } else {
      sum += 8.01074e-05;
    }
  } else {
    if ( features[2] < 0.527756 ) {
      sum += 8.01074e-05;
    } else {
      sum += -8.01074e-05;
    }
  }
  // tree 2800
  if ( features[5] < 0.329645 ) {
    if ( features[7] < 4.64755 ) {
      sum += 0.000101838;
    } else {
      sum += -0.000101838;
    }
  } else {
    if ( features[1] < -0.53912 ) {
      sum += 0.000101838;
    } else {
      sum += -0.000101838;
    }
  }
  // tree 2801
  if ( features[5] < 0.329645 ) {
    if ( features[4] < -1.32703 ) {
      sum += 9.75793e-05;
    } else {
      sum += -9.75793e-05;
    }
  } else {
    if ( features[4] < -0.463655 ) {
      sum += -9.75793e-05;
    } else {
      sum += 9.75793e-05;
    }
  }
  // tree 2802
  if ( features[8] < 2.09211 ) {
    if ( features[6] < 2.13557 ) {
      sum += 9.56146e-05;
    } else {
      sum += -9.56146e-05;
    }
  } else {
    if ( features[6] < 2.15222 ) {
      sum += -9.56146e-05;
    } else {
      sum += 9.56146e-05;
    }
  }
  // tree 2803
  if ( features[5] < 0.329645 ) {
    if ( features[5] < 0.253431 ) {
      sum += 9.75647e-05;
    } else {
      sum += -9.75647e-05;
    }
  } else {
    if ( features[7] < 4.69073 ) {
      sum += -9.75647e-05;
    } else {
      sum += 9.75647e-05;
    }
  }
  // tree 2804
  if ( features[8] < 2.09211 ) {
    if ( features[5] < 0.754281 ) {
      sum += 8.64576e-05;
    } else {
      sum += -8.64576e-05;
    }
  } else {
    if ( features[6] < 2.15222 ) {
      sum += -8.64576e-05;
    } else {
      sum += 8.64576e-05;
    }
  }
  // tree 2805
  if ( features[5] < 0.329645 ) {
    if ( features[5] < 0.253431 ) {
      sum += 9.71262e-05;
    } else {
      sum += -9.71262e-05;
    }
  } else {
    if ( features[4] < -0.463655 ) {
      sum += -9.71262e-05;
    } else {
      sum += 9.71262e-05;
    }
  }
  // tree 2806
  if ( features[7] < 3.73601 ) {
    sum += -5.47259e-05;
  } else {
    if ( features[7] < 4.7945 ) {
      sum += 5.47259e-05;
    } else {
      sum += -5.47259e-05;
    }
  }
  // tree 2807
  if ( features[8] < 2.09211 ) {
    if ( features[6] < 2.13557 ) {
      sum += 8.45624e-05;
    } else {
      sum += -8.45624e-05;
    }
  } else {
    if ( features[4] < -3.0468 ) {
      sum += -8.45624e-05;
    } else {
      sum += 8.45624e-05;
    }
  }
  // tree 2808
  if ( features[5] < 0.329645 ) {
    if ( features[7] < 4.53009 ) {
      sum += 0.000101344;
    } else {
      sum += -0.000101344;
    }
  } else {
    if ( features[1] < -0.53912 ) {
      sum += 0.000101344;
    } else {
      sum += -0.000101344;
    }
  }
  // tree 2809
  if ( features[8] < 2.09211 ) {
    if ( features[5] < 0.754281 ) {
      sum += 8.35108e-05;
    } else {
      sum += -8.35108e-05;
    }
  } else {
    if ( features[2] < 0.527756 ) {
      sum += 8.35108e-05;
    } else {
      sum += -8.35108e-05;
    }
  }
  // tree 2810
  if ( features[3] < 1.04065 ) {
    if ( features[8] < 2.67103 ) {
      sum += -9.99624e-05;
    } else {
      sum += 9.99624e-05;
    }
  } else {
    if ( features[4] < -0.357317 ) {
      sum += 9.99624e-05;
    } else {
      sum += -9.99624e-05;
    }
  }
  // tree 2811
  if ( features[8] < 2.09211 ) {
    if ( features[5] < 0.754281 ) {
      sum += 7.54342e-05;
    } else {
      sum += -7.54342e-05;
    }
  } else {
    if ( features[4] < -3.0468 ) {
      sum += -7.54342e-05;
    } else {
      sum += 7.54342e-05;
    }
  }
  // tree 2812
  if ( features[8] < 2.09211 ) {
    if ( features[6] < 2.13557 ) {
      sum += 8.37794e-05;
    } else {
      sum += -8.37794e-05;
    }
  } else {
    if ( features[4] < -3.0468 ) {
      sum += -8.37794e-05;
    } else {
      sum += 8.37794e-05;
    }
  }
  // tree 2813
  if ( features[8] < 2.09211 ) {
    if ( features[7] < 4.76501 ) {
      sum += 6.49009e-05;
    } else {
      sum += -6.49009e-05;
    }
  } else {
    if ( features[8] < 3.22507 ) {
      sum += -6.49009e-05;
    } else {
      sum += 6.49009e-05;
    }
  }
  // tree 2814
  if ( features[5] < 0.329645 ) {
    if ( features[7] < 4.53009 ) {
      sum += 0.000113258;
    } else {
      sum += -0.000113258;
    }
  } else {
    if ( features[7] < 4.69073 ) {
      sum += -0.000113258;
    } else {
      sum += 0.000113258;
    }
  }
  // tree 2815
  if ( features[8] < 2.09211 ) {
    if ( features[6] < 2.13557 ) {
      sum += 9.15189e-05;
    } else {
      sum += -9.15189e-05;
    }
  } else {
    if ( features[2] < 0.527756 ) {
      sum += 9.15189e-05;
    } else {
      sum += -9.15189e-05;
    }
  }
  // tree 2816
  if ( features[8] < 2.09211 ) {
    if ( features[6] < 2.13557 ) {
      sum += 9.45855e-05;
    } else {
      sum += -9.45855e-05;
    }
  } else {
    if ( features[6] < 2.15222 ) {
      sum += -9.45855e-05;
    } else {
      sum += 9.45855e-05;
    }
  }
  // tree 2817
  if ( features[5] < 0.329645 ) {
    if ( features[7] < 4.53009 ) {
      sum += 0.000112683;
    } else {
      sum += -0.000112683;
    }
  } else {
    if ( features[7] < 4.69073 ) {
      sum += -0.000112683;
    } else {
      sum += 0.000112683;
    }
  }
  // tree 2818
  if ( features[8] < 2.09211 ) {
    if ( features[7] < 4.76501 ) {
      sum += 8.13145e-05;
    } else {
      sum += -8.13145e-05;
    }
  } else {
    if ( features[6] < 2.15222 ) {
      sum += -8.13145e-05;
    } else {
      sum += 8.13145e-05;
    }
  }
  // tree 2819
  if ( features[8] < 2.09211 ) {
    if ( features[6] < 2.13557 ) {
      sum += 9.37399e-05;
    } else {
      sum += -9.37399e-05;
    }
  } else {
    if ( features[6] < 2.15222 ) {
      sum += -9.37399e-05;
    } else {
      sum += 9.37399e-05;
    }
  }
  // tree 2820
  if ( features[8] < 2.09211 ) {
    if ( features[3] < 0.710516 ) {
      sum += -7.88014e-05;
    } else {
      sum += 7.88014e-05;
    }
  } else {
    if ( features[2] < 0.527756 ) {
      sum += 7.88014e-05;
    } else {
      sum += -7.88014e-05;
    }
  }
  // tree 2821
  if ( features[8] < 2.09211 ) {
    if ( features[6] < 2.13557 ) {
      sum += 9.32312e-05;
    } else {
      sum += -9.32312e-05;
    }
  } else {
    if ( features[6] < 2.15222 ) {
      sum += -9.32312e-05;
    } else {
      sum += 9.32312e-05;
    }
  }
  // tree 2822
  if ( features[1] < 0.309319 ) {
    if ( features[3] < 0.823237 ) {
      sum += -8.23855e-05;
    } else {
      sum += 8.23855e-05;
    }
  } else {
    if ( features[6] < 2.24033 ) {
      sum += -8.23855e-05;
    } else {
      sum += 8.23855e-05;
    }
  }
  // tree 2823
  if ( features[1] < 0.309319 ) {
    if ( features[3] < 0.823237 ) {
      sum += -8.91353e-05;
    } else {
      sum += 8.91353e-05;
    }
  } else {
    if ( features[8] < 2.67159 ) {
      sum += -8.91353e-05;
    } else {
      sum += 8.91353e-05;
    }
  }
  // tree 2824
  if ( features[8] < 2.09211 ) {
    if ( features[5] < 0.754281 ) {
      sum += 8.44601e-05;
    } else {
      sum += -8.44601e-05;
    }
  } else {
    if ( features[6] < 2.15222 ) {
      sum += -8.44601e-05;
    } else {
      sum += 8.44601e-05;
    }
  }
  // tree 2825
  if ( features[8] < 2.09211 ) {
    if ( features[5] < 0.754281 ) {
      sum += 6.87202e-05;
    } else {
      sum += -6.87202e-05;
    }
  } else {
    if ( features[8] < 3.22507 ) {
      sum += -6.87202e-05;
    } else {
      sum += 6.87202e-05;
    }
  }
  // tree 2826
  if ( features[4] < -3.0468 ) {
    sum += -5.48858e-05;
  } else {
    if ( features[5] < 1.13208 ) {
      sum += 5.48858e-05;
    } else {
      sum += -5.48858e-05;
    }
  }
  // tree 2827
  if ( features[8] < 2.09211 ) {
    if ( features[6] < 2.13557 ) {
      sum += 9.03567e-05;
    } else {
      sum += -9.03567e-05;
    }
  } else {
    if ( features[2] < 0.527756 ) {
      sum += 9.03567e-05;
    } else {
      sum += -9.03567e-05;
    }
  }
  // tree 2828
  if ( features[0] < 3.03054 ) {
    if ( features[4] < -1.10944 ) {
      sum += 7.65545e-05;
    } else {
      sum += -7.65545e-05;
    }
  } else {
    if ( features[5] < 0.46068 ) {
      sum += 7.65545e-05;
    } else {
      sum += -7.65545e-05;
    }
  }
  // tree 2829
  if ( features[8] < 2.09211 ) {
    if ( features[6] < 2.13557 ) {
      sum += 9.22135e-05;
    } else {
      sum += -9.22135e-05;
    }
  } else {
    if ( features[6] < 2.15222 ) {
      sum += -9.22135e-05;
    } else {
      sum += 9.22135e-05;
    }
  }
  // tree 2830
  if ( features[0] < 3.03054 ) {
    if ( features[0] < 2.12578 ) {
      sum += 8.11223e-05;
    } else {
      sum += -8.11223e-05;
    }
  } else {
    if ( features[1] < 0.0281889 ) {
      sum += -8.11223e-05;
    } else {
      sum += 8.11223e-05;
    }
  }
  // tree 2831
  if ( features[8] < 2.09211 ) {
    if ( features[6] < 2.13557 ) {
      sum += 9.17272e-05;
    } else {
      sum += -9.17272e-05;
    }
  } else {
    if ( features[6] < 2.15222 ) {
      sum += -9.17272e-05;
    } else {
      sum += 9.17272e-05;
    }
  }
  // tree 2832
  if ( features[8] < 2.09211 ) {
    if ( features[6] < 2.13557 ) {
      sum += 9.12386e-05;
    } else {
      sum += -9.12386e-05;
    }
  } else {
    if ( features[6] < 2.15222 ) {
      sum += -9.12386e-05;
    } else {
      sum += 9.12386e-05;
    }
  }
  // tree 2833
  if ( features[8] < 2.09211 ) {
    if ( features[1] < -0.539051 ) {
      sum += 7.53442e-05;
    } else {
      sum += -7.53442e-05;
    }
  } else {
    if ( features[2] < 0.527756 ) {
      sum += 7.53442e-05;
    } else {
      sum += -7.53442e-05;
    }
  }
  // tree 2834
  if ( features[8] < 2.09211 ) {
    if ( features[2] < 0.394494 ) {
      sum += -7.74639e-05;
    } else {
      sum += 7.74639e-05;
    }
  } else {
    if ( features[6] < 2.15222 ) {
      sum += -7.74639e-05;
    } else {
      sum += 7.74639e-05;
    }
  }
  // tree 2835
  if ( features[4] < -3.0468 ) {
    sum += -5.5327e-05;
  } else {
    if ( features[8] < 1.99563 ) {
      sum += -5.5327e-05;
    } else {
      sum += 5.5327e-05;
    }
  }
  // tree 2836
  if ( features[8] < 2.09211 ) {
    if ( features[5] < 0.754281 ) {
      sum += 8.23722e-05;
    } else {
      sum += -8.23722e-05;
    }
  } else {
    if ( features[6] < 2.15222 ) {
      sum += -8.23722e-05;
    } else {
      sum += 8.23722e-05;
    }
  }
  // tree 2837
  if ( features[8] < 2.09211 ) {
    if ( features[6] < 2.13557 ) {
      sum += 8.25177e-05;
    } else {
      sum += -8.25177e-05;
    }
  } else {
    if ( features[4] < -3.0468 ) {
      sum += -8.25177e-05;
    } else {
      sum += 8.25177e-05;
    }
  }
  // tree 2838
  if ( features[5] < 0.329645 ) {
    if ( features[7] < 4.64755 ) {
      sum += 0.000113114;
    } else {
      sum += -0.000113114;
    }
  } else {
    if ( features[4] < -0.463655 ) {
      sum += -0.000113114;
    } else {
      sum += 0.000113114;
    }
  }
  // tree 2839
  if ( features[8] < 2.09211 ) {
    if ( features[6] < 2.13557 ) {
      sum += 9.00977e-05;
    } else {
      sum += -9.00977e-05;
    }
  } else {
    if ( features[6] < 2.15222 ) {
      sum += -9.00977e-05;
    } else {
      sum += 9.00977e-05;
    }
  }
  // tree 2840
  if ( features[0] < 3.03054 ) {
    if ( features[2] < 0.956816 ) {
      sum += -7.89397e-05;
    } else {
      sum += 7.89397e-05;
    }
  } else {
    if ( features[5] < 0.46068 ) {
      sum += 7.89397e-05;
    } else {
      sum += -7.89397e-05;
    }
  }
  // tree 2841
  if ( features[8] < 2.09211 ) {
    if ( features[6] < 2.13557 ) {
      sum += 8.96057e-05;
    } else {
      sum += -8.96057e-05;
    }
  } else {
    if ( features[6] < 2.15222 ) {
      sum += -8.96057e-05;
    } else {
      sum += 8.96057e-05;
    }
  }
  // tree 2842
  if ( features[5] < 0.329645 ) {
    if ( features[7] < 4.64755 ) {
      sum += 0.000112545;
    } else {
      sum += -0.000112545;
    }
  } else {
    if ( features[4] < -0.463655 ) {
      sum += -0.000112545;
    } else {
      sum += 0.000112545;
    }
  }
  // tree 2843
  if ( features[8] < 2.09211 ) {
    if ( features[6] < 2.13557 ) {
      sum += 8.9194e-05;
    } else {
      sum += -8.9194e-05;
    }
  } else {
    if ( features[6] < 2.15222 ) {
      sum += -8.9194e-05;
    } else {
      sum += 8.9194e-05;
    }
  }
  // tree 2844
  if ( features[5] < 0.329645 ) {
    if ( features[5] < 0.253431 ) {
      sum += 9.66869e-05;
    } else {
      sum += -9.66869e-05;
    }
  } else {
    if ( features[4] < -0.463655 ) {
      sum += -9.66869e-05;
    } else {
      sum += 9.66869e-05;
    }
  }
  // tree 2845
  if ( features[8] < 2.09211 ) {
    if ( features[3] < 0.710516 ) {
      sum += -7.7543e-05;
    } else {
      sum += 7.7543e-05;
    }
  } else {
    if ( features[2] < 0.527756 ) {
      sum += 7.7543e-05;
    } else {
      sum += -7.7543e-05;
    }
  }
  // tree 2846
  if ( features[8] < 2.09211 ) {
    if ( features[0] < 2.72769 ) {
      sum += -7.50326e-05;
    } else {
      sum += 7.50326e-05;
    }
  } else {
    if ( features[2] < 0.527756 ) {
      sum += 7.50326e-05;
    } else {
      sum += -7.50326e-05;
    }
  }
  // tree 2847
  if ( features[5] < 0.329645 ) {
    if ( features[7] < 4.53009 ) {
      sum += 0.000111973;
    } else {
      sum += -0.000111973;
    }
  } else {
    if ( features[7] < 4.69073 ) {
      sum += -0.000111973;
    } else {
      sum += 0.000111973;
    }
  }
  // tree 2848
  if ( features[0] < 3.03054 ) {
    if ( features[0] < 2.12578 ) {
      sum += 8.09663e-05;
    } else {
      sum += -8.09663e-05;
    }
  } else {
    if ( features[1] < 0.0281889 ) {
      sum += -8.09663e-05;
    } else {
      sum += 8.09663e-05;
    }
  }
  // tree 2849
  if ( features[8] < 2.09211 ) {
    if ( features[6] < 2.13557 ) {
      sum += 8.88003e-05;
    } else {
      sum += -8.88003e-05;
    }
  } else {
    if ( features[6] < 2.15222 ) {
      sum += -8.88003e-05;
    } else {
      sum += 8.88003e-05;
    }
  }
  // tree 2850
  if ( features[8] < 2.09211 ) {
    if ( features[2] < 0.394494 ) {
      sum += -6.92164e-05;
    } else {
      sum += 6.92164e-05;
    }
  } else {
    if ( features[4] < -3.0468 ) {
      sum += -6.92164e-05;
    } else {
      sum += 6.92164e-05;
    }
  }
  // tree 2851
  if ( features[5] < 0.329645 ) {
    if ( features[7] < 4.64755 ) {
      sum += 8.46258e-05;
    } else {
      sum += -8.46258e-05;
    }
  } else {
    if ( features[0] < 3.30549 ) {
      sum += -8.46258e-05;
    } else {
      sum += 8.46258e-05;
    }
  }
  // tree 2852
  if ( features[5] < 0.329645 ) {
    if ( features[5] < 0.253431 ) {
      sum += 9.61977e-05;
    } else {
      sum += -9.61977e-05;
    }
  } else {
    if ( features[4] < -0.463655 ) {
      sum += -9.61977e-05;
    } else {
      sum += 9.61977e-05;
    }
  }
  // tree 2853
  if ( features[8] < 2.09211 ) {
    if ( features[3] < 0.710516 ) {
      sum += -7.71183e-05;
    } else {
      sum += 7.71183e-05;
    }
  } else {
    if ( features[6] < 2.15222 ) {
      sum += -7.71183e-05;
    } else {
      sum += 7.71183e-05;
    }
  }
  // tree 2854
  if ( features[8] < 2.09211 ) {
    if ( features[6] < 2.13557 ) {
      sum += 8.81466e-05;
    } else {
      sum += -8.81466e-05;
    }
  } else {
    if ( features[2] < 0.527756 ) {
      sum += 8.81466e-05;
    } else {
      sum += -8.81466e-05;
    }
  }
  // tree 2855
  if ( features[8] < 2.09211 ) {
    if ( features[6] < 2.13557 ) {
      sum += 7.56994e-05;
    } else {
      sum += -7.56994e-05;
    }
  } else {
    if ( features[8] < 3.22507 ) {
      sum += -7.56994e-05;
    } else {
      sum += 7.56994e-05;
    }
  }
  // tree 2856
  if ( features[8] < 2.09211 ) {
    if ( features[6] < 2.13557 ) {
      sum += 8.18493e-05;
    } else {
      sum += -8.18493e-05;
    }
  } else {
    if ( features[4] < -3.0468 ) {
      sum += -8.18493e-05;
    } else {
      sum += 8.18493e-05;
    }
  }
  // tree 2857
  if ( features[5] < 0.329645 ) {
    if ( features[7] < 4.53009 ) {
      sum += 0.00011131;
    } else {
      sum += -0.00011131;
    }
  } else {
    if ( features[7] < 4.69073 ) {
      sum += -0.00011131;
    } else {
      sum += 0.00011131;
    }
  }
  // tree 2858
  if ( features[1] < 0.309319 ) {
    if ( features[3] < 0.823237 ) {
      sum += -8.83762e-05;
    } else {
      sum += 8.83762e-05;
    }
  } else {
    if ( features[8] < 2.67159 ) {
      sum += -8.83762e-05;
    } else {
      sum += 8.83762e-05;
    }
  }
  // tree 2859
  if ( features[1] < 0.309319 ) {
    if ( features[1] < -0.797617 ) {
      sum += 8.20805e-05;
    } else {
      sum += -8.20805e-05;
    }
  } else {
    if ( features[6] < 2.24033 ) {
      sum += -8.20805e-05;
    } else {
      sum += 8.20805e-05;
    }
  }
  // tree 2860
  if ( features[0] < 3.03054 ) {
    if ( features[0] < 2.12578 ) {
      sum += 6.83826e-05;
    } else {
      sum += -6.83826e-05;
    }
  } else {
    sum += 6.83826e-05;
  }
  // tree 2861
  if ( features[5] < 0.329645 ) {
    if ( features[8] < 2.96068 ) {
      sum += -8.92016e-05;
    } else {
      sum += 8.92016e-05;
    }
  } else {
    if ( features[4] < -0.463655 ) {
      sum += -8.92016e-05;
    } else {
      sum += 8.92016e-05;
    }
  }
  // tree 2862
  if ( features[8] < 2.09211 ) {
    if ( features[6] < 2.13557 ) {
      sum += 8.75808e-05;
    } else {
      sum += -8.75808e-05;
    }
  } else {
    if ( features[2] < 0.527756 ) {
      sum += 8.75808e-05;
    } else {
      sum += -8.75808e-05;
    }
  }
  // tree 2863
  if ( features[1] < 0.309319 ) {
    if ( features[1] < -0.797617 ) {
      sum += 8.66071e-05;
    } else {
      sum += -8.66071e-05;
    }
  } else {
    if ( features[0] < 2.98766 ) {
      sum += -8.66071e-05;
    } else {
      sum += 8.66071e-05;
    }
  }
  // tree 2864
  if ( features[8] < 2.09211 ) {
    if ( features[6] < 2.13557 ) {
      sum += 8.77685e-05;
    } else {
      sum += -8.77685e-05;
    }
  } else {
    if ( features[6] < 2.15222 ) {
      sum += -8.77685e-05;
    } else {
      sum += 8.77685e-05;
    }
  }
  // tree 2865
  if ( features[8] < 2.09211 ) {
    if ( features[6] < 2.13557 ) {
      sum += 8.71137e-05;
    } else {
      sum += -8.71137e-05;
    }
  } else {
    if ( features[2] < 0.527756 ) {
      sum += 8.71137e-05;
    } else {
      sum += -8.71137e-05;
    }
  }
  // tree 2866
  if ( features[5] < 0.329645 ) {
    if ( features[7] < 4.64755 ) {
      sum += 0.000110706;
    } else {
      sum += -0.000110706;
    }
  } else {
    if ( features[7] < 4.69073 ) {
      sum += -0.000110706;
    } else {
      sum += 0.000110706;
    }
  }
  // tree 2867
  if ( features[8] < 2.09211 ) {
    if ( features[6] < 2.13557 ) {
      sum += 8.72243e-05;
    } else {
      sum += -8.72243e-05;
    }
  } else {
    if ( features[6] < 2.15222 ) {
      sum += -8.72243e-05;
    } else {
      sum += 8.72243e-05;
    }
  }
  // tree 2868
  if ( features[8] < 2.09211 ) {
    if ( features[7] < 4.76501 ) {
      sum += 7.61777e-05;
    } else {
      sum += -7.61777e-05;
    }
  } else {
    if ( features[6] < 2.15222 ) {
      sum += -7.61777e-05;
    } else {
      sum += 7.61777e-05;
    }
  }
  // tree 2869
  if ( features[2] < -0.330568 ) {
    if ( features[8] < 2.86438 ) {
      sum += -0.000100929;
    } else {
      sum += 0.000100929;
    }
  } else {
    if ( features[2] < 0.6662 ) {
      sum += 0.000100929;
    } else {
      sum += -0.000100929;
    }
  }
  // tree 2870
  if ( features[8] < 2.09211 ) {
    if ( features[4] < -1.64968 ) {
      sum += -6.22583e-05;
    } else {
      sum += 6.22583e-05;
    }
  } else {
    if ( features[2] < 0.527756 ) {
      sum += 6.22583e-05;
    } else {
      sum += -6.22583e-05;
    }
  }
  // tree 2871
  if ( features[8] < 2.09211 ) {
    if ( features[0] < 2.72769 ) {
      sum += -6.17288e-05;
    } else {
      sum += 6.17288e-05;
    }
  } else {
    if ( features[0] < 1.65576 ) {
      sum += -6.17288e-05;
    } else {
      sum += 6.17288e-05;
    }
  }
  // tree 2872
  if ( features[8] < 2.09211 ) {
    if ( features[5] < 0.754281 ) {
      sum += 7.94628e-05;
    } else {
      sum += -7.94628e-05;
    }
  } else {
    if ( features[6] < 2.15222 ) {
      sum += -7.94628e-05;
    } else {
      sum += 7.94628e-05;
    }
  }
  // tree 2873
  if ( features[1] < 0.309319 ) {
    if ( features[1] < -0.797617 ) {
      sum += 8.93664e-05;
    } else {
      sum += -8.93664e-05;
    }
  } else {
    if ( features[4] < -0.774054 ) {
      sum += 8.93664e-05;
    } else {
      sum += -8.93664e-05;
    }
  }
  // tree 2874
  if ( features[5] < 0.329645 ) {
    if ( features[7] < 4.64755 ) {
      sum += 0.000110411;
    } else {
      sum += -0.000110411;
    }
  } else {
    if ( features[4] < -0.463655 ) {
      sum += -0.000110411;
    } else {
      sum += 0.000110411;
    }
  }
  // tree 2875
  if ( features[2] < -0.330568 ) {
    if ( features[8] < 2.86438 ) {
      sum += -8.87718e-05;
    } else {
      sum += 8.87718e-05;
    }
  } else {
    if ( features[4] < -1.10944 ) {
      sum += 8.87718e-05;
    } else {
      sum += -8.87718e-05;
    }
  }
  // tree 2876
  if ( features[8] < 2.09211 ) {
    if ( features[5] < 0.754281 ) {
      sum += 6.56361e-05;
    } else {
      sum += -6.56361e-05;
    }
  } else {
    if ( features[3] < 1.14936 ) {
      sum += 6.56361e-05;
    } else {
      sum += -6.56361e-05;
    }
  }
  // tree 2877
  if ( features[8] < 2.09211 ) {
    if ( features[5] < 0.754281 ) {
      sum += 7.89245e-05;
    } else {
      sum += -7.89245e-05;
    }
  } else {
    if ( features[2] < 0.527756 ) {
      sum += 7.89245e-05;
    } else {
      sum += -7.89245e-05;
    }
  }
  // tree 2878
  if ( features[0] < 3.03054 ) {
    if ( features[0] < 2.12578 ) {
      sum += 6.81803e-05;
    } else {
      sum += -6.81803e-05;
    }
  } else {
    sum += 6.81803e-05;
  }
  // tree 2879
  if ( features[8] < 2.09211 ) {
    if ( features[6] < 2.13557 ) {
      sum += 8.60424e-05;
    } else {
      sum += -8.60424e-05;
    }
  } else {
    if ( features[6] < 2.15222 ) {
      sum += -8.60424e-05;
    } else {
      sum += 8.60424e-05;
    }
  }
  // tree 2880
  if ( features[1] < 0.309319 ) {
    if ( features[1] < -0.797617 ) {
      sum += 8.89268e-05;
    } else {
      sum += -8.89268e-05;
    }
  } else {
    if ( features[4] < -0.774054 ) {
      sum += 8.89268e-05;
    } else {
      sum += -8.89268e-05;
    }
  }
  // tree 2881
  if ( features[5] < 0.329645 ) {
    if ( features[7] < 4.64755 ) {
      sum += 0.000110024;
    } else {
      sum += -0.000110024;
    }
  } else {
    if ( features[7] < 4.69073 ) {
      sum += -0.000110024;
    } else {
      sum += 0.000110024;
    }
  }
  // tree 2882
  if ( features[8] < 2.09211 ) {
    if ( features[6] < 2.13557 ) {
      sum += 8.56482e-05;
    } else {
      sum += -8.56482e-05;
    }
  } else {
    if ( features[2] < 0.527756 ) {
      sum += 8.56482e-05;
    } else {
      sum += -8.56482e-05;
    }
  }
  // tree 2883
  if ( features[8] < 2.09211 ) {
    if ( features[6] < 2.13557 ) {
      sum += 8.54741e-05;
    } else {
      sum += -8.54741e-05;
    }
  } else {
    if ( features[6] < 2.15222 ) {
      sum += -8.54741e-05;
    } else {
      sum += 8.54741e-05;
    }
  }
  // tree 2884
  if ( features[8] < 2.09211 ) {
    if ( features[2] < 0.394494 ) {
      sum += -7.32779e-05;
    } else {
      sum += 7.32779e-05;
    }
  } else {
    if ( features[2] < 0.527756 ) {
      sum += 7.32779e-05;
    } else {
      sum += -7.32779e-05;
    }
  }
  // tree 2885
  if ( features[8] < 2.09211 ) {
    if ( features[6] < 2.13557 ) {
      sum += 8.50271e-05;
    } else {
      sum += -8.50271e-05;
    }
  } else {
    if ( features[6] < 2.15222 ) {
      sum += -8.50271e-05;
    } else {
      sum += 8.50271e-05;
    }
  }
  // tree 2886
  if ( features[5] < 0.329645 ) {
    if ( features[7] < 4.53009 ) {
      sum += 0.000110074;
    } else {
      sum += -0.000110074;
    }
  } else {
    if ( features[4] < -0.463655 ) {
      sum += -0.000110074;
    } else {
      sum += 0.000110074;
    }
  }
  // tree 2887
  if ( features[8] < 2.09211 ) {
    if ( features[6] < 2.13557 ) {
      sum += 8.47241e-05;
    } else {
      sum += -8.47241e-05;
    }
  } else {
    if ( features[2] < 0.527756 ) {
      sum += 8.47241e-05;
    } else {
      sum += -8.47241e-05;
    }
  }
  // tree 2888
  if ( features[7] < 3.73601 ) {
    sum += -5.48703e-05;
  } else {
    if ( features[7] < 4.7945 ) {
      sum += 5.48703e-05;
    } else {
      sum += -5.48703e-05;
    }
  }
  // tree 2889
  if ( features[8] < 2.09211 ) {
    if ( features[5] < 0.754281 ) {
      sum += 7.78131e-05;
    } else {
      sum += -7.78131e-05;
    }
  } else {
    if ( features[6] < 2.15222 ) {
      sum += -7.78131e-05;
    } else {
      sum += 7.78131e-05;
    }
  }
  // tree 2890
  if ( features[8] < 2.09211 ) {
    if ( features[6] < 2.13557 ) {
      sum += 8.41957e-05;
    } else {
      sum += -8.41957e-05;
    }
  } else {
    if ( features[6] < 2.15222 ) {
      sum += -8.41957e-05;
    } else {
      sum += 8.41957e-05;
    }
  }
  // tree 2891
  if ( features[8] < 2.09211 ) {
    if ( features[6] < 2.13557 ) {
      sum += 7.37684e-05;
    } else {
      sum += -7.37684e-05;
    }
  } else {
    if ( features[8] < 3.22507 ) {
      sum += -7.37684e-05;
    } else {
      sum += 7.37684e-05;
    }
  }
  // tree 2892
  if ( features[8] < 2.09211 ) {
    if ( features[7] < 4.76501 ) {
      sum += 7.37577e-05;
    } else {
      sum += -7.37577e-05;
    }
  } else {
    if ( features[6] < 2.15222 ) {
      sum += -7.37577e-05;
    } else {
      sum += 7.37577e-05;
    }
  }
  // tree 2893
  if ( features[5] < 0.329645 ) {
    if ( features[5] < 0.253431 ) {
      sum += 9.51935e-05;
    } else {
      sum += -9.51935e-05;
    }
  } else {
    if ( features[4] < -0.463655 ) {
      sum += -9.51935e-05;
    } else {
      sum += 9.51935e-05;
    }
  }
  // tree 2894
  if ( features[8] < 2.09211 ) {
    if ( features[6] < 2.13557 ) {
      sum += 8.41246e-05;
    } else {
      sum += -8.41246e-05;
    }
  } else {
    if ( features[2] < 0.527756 ) {
      sum += 8.41246e-05;
    } else {
      sum += -8.41246e-05;
    }
  }
  // tree 2895
  if ( features[7] < 3.73601 ) {
    sum += -6.08724e-05;
  } else {
    if ( features[4] < -3.0468 ) {
      sum += -6.08724e-05;
    } else {
      sum += 6.08724e-05;
    }
  }
  // tree 2896
  if ( features[5] < 0.329645 ) {
    if ( features[3] < 0.421425 ) {
      sum += 7.70459e-05;
    } else {
      sum += -7.70459e-05;
    }
  } else {
    if ( features[3] < 0.662954 ) {
      sum += -7.70459e-05;
    } else {
      sum += 7.70459e-05;
    }
  }
  // tree 2897
  if ( features[8] < 2.09211 ) {
    if ( features[6] < 2.13557 ) {
      sum += 8.33576e-05;
    } else {
      sum += -8.33576e-05;
    }
  } else {
    if ( features[6] < 2.15222 ) {
      sum += -8.33576e-05;
    } else {
      sum += 8.33576e-05;
    }
  }
  // tree 2898
  if ( features[8] < 2.09211 ) {
    if ( features[6] < 2.13557 ) {
      sum += 8.3476e-05;
    } else {
      sum += -8.3476e-05;
    }
  } else {
    if ( features[2] < 0.527756 ) {
      sum += 8.3476e-05;
    } else {
      sum += -8.3476e-05;
    }
  }
  // tree 2899
  if ( features[8] < 2.09211 ) {
    if ( features[6] < 2.13557 ) {
      sum += 8.30301e-05;
    } else {
      sum += -8.30301e-05;
    }
  } else {
    if ( features[2] < 0.527756 ) {
      sum += 8.30301e-05;
    } else {
      sum += -8.30301e-05;
    }
  }
  // tree 2900
  if ( features[1] < 0.309319 ) {
    if ( features[6] < 2.68053 ) {
      sum += -8.5761e-05;
    } else {
      sum += 8.5761e-05;
    }
  } else {
    if ( features[4] < -0.774054 ) {
      sum += 8.5761e-05;
    } else {
      sum += -8.5761e-05;
    }
  }
  // tree 2901
  if ( features[0] < 1.93071 ) {
    if ( features[5] < 0.883423 ) {
      sum += -7.85976e-05;
    } else {
      sum += 7.85976e-05;
    }
  } else {
    if ( features[5] < 0.751479 ) {
      sum += 7.85976e-05;
    } else {
      sum += -7.85976e-05;
    }
  }
  // tree 2902
  if ( features[8] < 2.09211 ) {
    if ( features[6] < 2.13557 ) {
      sum += 8.26448e-05;
    } else {
      sum += -8.26448e-05;
    }
  } else {
    if ( features[2] < 0.527756 ) {
      sum += 8.26448e-05;
    } else {
      sum += -8.26448e-05;
    }
  }
  // tree 2903
  if ( features[5] < 0.329645 ) {
    if ( features[7] < 4.53009 ) {
      sum += 0.000109586;
    } else {
      sum += -0.000109586;
    }
  } else {
    if ( features[7] < 4.69073 ) {
      sum += -0.000109586;
    } else {
      sum += 0.000109586;
    }
  }
  // tree 2904
  if ( features[7] < 3.73601 ) {
    sum += -5.7179e-05;
  } else {
    if ( features[3] < 1.04065 ) {
      sum += 5.7179e-05;
    } else {
      sum += -5.7179e-05;
    }
  }
  // tree 2905
  if ( features[5] < 0.329645 ) {
    if ( features[7] < 4.53009 ) {
      sum += 0.000109221;
    } else {
      sum += -0.000109221;
    }
  } else {
    if ( features[4] < -0.463655 ) {
      sum += -0.000109221;
    } else {
      sum += 0.000109221;
    }
  }
  // tree 2906
  if ( features[8] < 2.09211 ) {
    if ( features[6] < 2.13557 ) {
      sum += 8.22157e-05;
    } else {
      sum += -8.22157e-05;
    }
  } else {
    if ( features[2] < 0.527756 ) {
      sum += 8.22157e-05;
    } else {
      sum += -8.22157e-05;
    }
  }
  // tree 2907
  if ( features[8] < 2.09211 ) {
    if ( features[6] < 2.13557 ) {
      sum += 8.2591e-05;
    } else {
      sum += -8.2591e-05;
    }
  } else {
    if ( features[6] < 2.15222 ) {
      sum += -8.2591e-05;
    } else {
      sum += 8.2591e-05;
    }
  }
  // tree 2908
  if ( features[8] < 2.09211 ) {
    if ( features[7] < 4.76501 ) {
      sum += 7.24705e-05;
    } else {
      sum += -7.24705e-05;
    }
  } else {
    if ( features[2] < 0.527756 ) {
      sum += 7.24705e-05;
    } else {
      sum += -7.24705e-05;
    }
  }
  // tree 2909
  if ( features[5] < 0.329645 ) {
    if ( features[7] < 4.53009 ) {
      sum += 0.000108935;
    } else {
      sum += -0.000108935;
    }
  } else {
    if ( features[7] < 4.69073 ) {
      sum += -0.000108935;
    } else {
      sum += 0.000108935;
    }
  }
  // tree 2910
  if ( features[5] < 0.329645 ) {
    if ( features[8] < 2.96068 ) {
      sum += -7.88748e-05;
    } else {
      sum += 7.88748e-05;
    }
  } else {
    if ( features[1] < -0.53912 ) {
      sum += 7.88748e-05;
    } else {
      sum += -7.88748e-05;
    }
  }
  // tree 2911
  if ( features[8] < 2.09211 ) {
    if ( features[6] < 2.13557 ) {
      sum += 7.98691e-05;
    } else {
      sum += -7.98691e-05;
    }
  } else {
    if ( features[4] < -3.0468 ) {
      sum += -7.98691e-05;
    } else {
      sum += 7.98691e-05;
    }
  }
  // tree 2912
  if ( features[8] < 2.09211 ) {
    if ( features[7] < 4.76501 ) {
      sum += 6.46155e-05;
    } else {
      sum += -6.46155e-05;
    }
  } else {
    if ( features[1] < -0.17959 ) {
      sum += -6.46155e-05;
    } else {
      sum += 6.46155e-05;
    }
  }
  // tree 2913
  if ( features[0] < 3.03054 ) {
    if ( features[2] < 0.956816 ) {
      sum += -7.94323e-05;
    } else {
      sum += 7.94323e-05;
    }
  } else {
    if ( features[5] < 0.46068 ) {
      sum += 7.94323e-05;
    } else {
      sum += -7.94323e-05;
    }
  }
  // tree 2914
  if ( features[1] < 0.309319 ) {
    if ( features[1] < -0.797617 ) {
      sum += 8.84971e-05;
    } else {
      sum += -8.84971e-05;
    }
  } else {
    if ( features[4] < -0.774054 ) {
      sum += 8.84971e-05;
    } else {
      sum += -8.84971e-05;
    }
  }
  // tree 2915
  if ( features[7] < 3.73601 ) {
    sum += -6.05457e-05;
  } else {
    if ( features[4] < -3.0468 ) {
      sum += -6.05457e-05;
    } else {
      sum += 6.05457e-05;
    }
  }
  // tree 2916
  if ( features[5] < 0.329645 ) {
    if ( features[5] < 0.253431 ) {
      sum += 9.42734e-05;
    } else {
      sum += -9.42734e-05;
    }
  } else {
    if ( features[4] < -0.463655 ) {
      sum += -9.42734e-05;
    } else {
      sum += 9.42734e-05;
    }
  }
  // tree 2917
  if ( features[8] < 2.09211 ) {
    if ( features[6] < 2.13557 ) {
      sum += 8.21383e-05;
    } else {
      sum += -8.21383e-05;
    }
  } else {
    if ( features[6] < 2.15222 ) {
      sum += -8.21383e-05;
    } else {
      sum += 8.21383e-05;
    }
  }
  // tree 2918
  if ( features[8] < 2.09211 ) {
    if ( features[6] < 2.13557 ) {
      sum += 8.13559e-05;
    } else {
      sum += -8.13559e-05;
    }
  } else {
    if ( features[2] < 0.527756 ) {
      sum += 8.13559e-05;
    } else {
      sum += -8.13559e-05;
    }
  }
  // tree 2919
  if ( features[8] < 2.09211 ) {
    if ( features[6] < 2.13557 ) {
      sum += 8.1619e-05;
    } else {
      sum += -8.1619e-05;
    }
  } else {
    if ( features[6] < 2.15222 ) {
      sum += -8.1619e-05;
    } else {
      sum += 8.1619e-05;
    }
  }
  // tree 2920
  if ( features[8] < 2.09211 ) {
    if ( features[6] < 2.13557 ) {
      sum += 8.11845e-05;
    } else {
      sum += -8.11845e-05;
    }
  } else {
    if ( features[6] < 2.15222 ) {
      sum += -8.11845e-05;
    } else {
      sum += 8.11845e-05;
    }
  }
  // tree 2921
  if ( features[8] < 2.09211 ) {
    if ( features[6] < 2.13557 ) {
      sum += 8.07524e-05;
    } else {
      sum += -8.07524e-05;
    }
  } else {
    if ( features[6] < 2.15222 ) {
      sum += -8.07524e-05;
    } else {
      sum += 8.07524e-05;
    }
  }
  // tree 2922
  if ( features[8] < 2.09211 ) {
    if ( features[6] < 2.13557 ) {
      sum += 8.06717e-05;
    } else {
      sum += -8.06717e-05;
    }
  } else {
    if ( features[2] < 0.527756 ) {
      sum += 8.06717e-05;
    } else {
      sum += -8.06717e-05;
    }
  }
  // tree 2923
  if ( features[8] < 2.09211 ) {
    if ( features[2] < 0.394494 ) {
      sum += -7.0077e-05;
    } else {
      sum += 7.0077e-05;
    }
  } else {
    if ( features[6] < 2.15222 ) {
      sum += -7.0077e-05;
    } else {
      sum += 7.0077e-05;
    }
  }
  // tree 2924
  if ( features[8] < 2.09211 ) {
    if ( features[7] < 4.76501 ) {
      sum += 7.13289e-05;
    } else {
      sum += -7.13289e-05;
    }
  } else {
    if ( features[6] < 2.15222 ) {
      sum += -7.13289e-05;
    } else {
      sum += 7.13289e-05;
    }
  }
  // tree 2925
  if ( features[8] < 2.09211 ) {
    if ( features[6] < 2.13557 ) {
      sum += 7.91113e-05;
    } else {
      sum += -7.91113e-05;
    }
  } else {
    if ( features[4] < -3.0468 ) {
      sum += -7.91113e-05;
    } else {
      sum += 7.91113e-05;
    }
  }
  // tree 2926
  if ( features[5] < 0.329645 ) {
    if ( features[4] < -1.32703 ) {
      sum += 8.25955e-05;
    } else {
      sum += -8.25955e-05;
    }
  } else {
    if ( features[6] < 2.67895 ) {
      sum += -8.25955e-05;
    } else {
      sum += 8.25955e-05;
    }
  }
  // tree 2927
  if ( features[1] < 0.309319 ) {
    if ( features[1] < -0.797617 ) {
      sum += 7.94287e-05;
    } else {
      sum += -7.94287e-05;
    }
  } else {
    sum += 7.94287e-05;
  }
  // tree 2928
  if ( features[8] < 2.09211 ) {
    if ( features[7] < 4.76501 ) {
      sum += 6.39725e-05;
    } else {
      sum += -6.39725e-05;
    }
  } else {
    if ( features[1] < -0.17959 ) {
      sum += -6.39725e-05;
    } else {
      sum += 6.39725e-05;
    }
  }
  // tree 2929
  if ( features[8] < 2.09211 ) {
    if ( features[7] < 4.76501 ) {
      sum += 7.08599e-05;
    } else {
      sum += -7.08599e-05;
    }
  } else {
    if ( features[6] < 2.15222 ) {
      sum += -7.08599e-05;
    } else {
      sum += 7.08599e-05;
    }
  }
  // tree 2930
  if ( features[7] < 3.73601 ) {
    sum += -6.03339e-05;
  } else {
    if ( features[4] < -3.0468 ) {
      sum += -6.03339e-05;
    } else {
      sum += 6.03339e-05;
    }
  }
  // tree 2931
  if ( features[8] < 2.09211 ) {
    if ( features[5] < 0.754281 ) {
      sum += 7.36652e-05;
    } else {
      sum += -7.36652e-05;
    }
  } else {
    if ( features[6] < 2.15222 ) {
      sum += -7.36652e-05;
    } else {
      sum += 7.36652e-05;
    }
  }
  // tree 2932
  if ( features[5] < 0.329645 ) {
    if ( features[7] < 4.64755 ) {
      sum += 0.000108524;
    } else {
      sum += -0.000108524;
    }
  } else {
    if ( features[4] < -0.463655 ) {
      sum += -0.000108524;
    } else {
      sum += 0.000108524;
    }
  }
  // tree 2933
  if ( features[5] < 0.329645 ) {
    if ( features[7] < 4.53009 ) {
      sum += 0.000107973;
    } else {
      sum += -0.000107973;
    }
  } else {
    if ( features[4] < -0.463655 ) {
      sum += -0.000107973;
    } else {
      sum += 0.000107973;
    }
  }
  // tree 2934
  if ( features[8] < 2.09211 ) {
    if ( features[6] < 2.13557 ) {
      sum += 8.01314e-05;
    } else {
      sum += -8.01314e-05;
    }
  } else {
    if ( features[2] < 0.527756 ) {
      sum += 8.01314e-05;
    } else {
      sum += -8.01314e-05;
    }
  }
  // tree 2935
  if ( features[8] < 2.09211 ) {
    if ( features[5] < 0.754281 ) {
      sum += 6.71443e-05;
    } else {
      sum += -6.71443e-05;
    }
  } else {
    if ( features[5] < 0.624955 ) {
      sum += 6.71443e-05;
    } else {
      sum += -6.71443e-05;
    }
  }
  // tree 2936
  if ( features[7] < 3.73601 ) {
    sum += -5.41524e-05;
  } else {
    if ( features[7] < 4.7945 ) {
      sum += 5.41524e-05;
    } else {
      sum += -5.41524e-05;
    }
  }
  // tree 2937
  if ( features[8] < 2.09211 ) {
    if ( features[3] < 0.710516 ) {
      sum += -6.99291e-05;
    } else {
      sum += 6.99291e-05;
    }
  } else {
    if ( features[6] < 2.15222 ) {
      sum += -6.99291e-05;
    } else {
      sum += 6.99291e-05;
    }
  }
  // tree 2938
  if ( features[8] < 2.09211 ) {
    if ( features[7] < 4.76501 ) {
      sum += 7.10197e-05;
    } else {
      sum += -7.10197e-05;
    }
  } else {
    if ( features[2] < 0.527756 ) {
      sum += 7.10197e-05;
    } else {
      sum += -7.10197e-05;
    }
  }
  // tree 2939
  if ( features[8] < 2.09211 ) {
    if ( features[0] < 2.72769 ) {
      sum += -6.0906e-05;
    } else {
      sum += 6.0906e-05;
    }
  } else {
    if ( features[8] < 3.22507 ) {
      sum += -6.0906e-05;
    } else {
      sum += 6.0906e-05;
    }
  }
  // tree 2940
  if ( features[5] < 0.329645 ) {
    if ( features[4] < -1.32703 ) {
      sum += 9.31624e-05;
    } else {
      sum += -9.31624e-05;
    }
  } else {
    if ( features[4] < -0.463655 ) {
      sum += -9.31624e-05;
    } else {
      sum += 9.31624e-05;
    }
  }
  // tree 2941
  if ( features[8] < 2.09211 ) {
    if ( features[6] < 2.13557 ) {
      sum += 7.94126e-05;
    } else {
      sum += -7.94126e-05;
    }
  } else {
    if ( features[2] < 0.527756 ) {
      sum += 7.94126e-05;
    } else {
      sum += -7.94126e-05;
    }
  }
  // tree 2942
  if ( features[8] < 2.09211 ) {
    if ( features[6] < 2.13557 ) {
      sum += 7.89886e-05;
    } else {
      sum += -7.89886e-05;
    }
  } else {
    if ( features[2] < 0.527756 ) {
      sum += 7.89886e-05;
    } else {
      sum += -7.89886e-05;
    }
  }
  // tree 2943
  if ( features[5] < 0.329645 ) {
    if ( features[7] < 4.53009 ) {
      sum += 9.91042e-05;
    } else {
      sum += -9.91042e-05;
    }
  } else {
    if ( features[1] < -0.53912 ) {
      sum += 9.91042e-05;
    } else {
      sum += -9.91042e-05;
    }
  }
  // tree 2944
  if ( features[5] < 0.329645 ) {
    if ( features[7] < 4.53009 ) {
      sum += 0.000106881;
    } else {
      sum += -0.000106881;
    }
  } else {
    if ( features[4] < -0.463655 ) {
      sum += -0.000106881;
    } else {
      sum += 0.000106881;
    }
  }
  // tree 2945
  if ( features[8] < 2.09211 ) {
    if ( features[6] < 2.13557 ) {
      sum += 7.19156e-05;
    } else {
      sum += -7.19156e-05;
    }
  } else {
    if ( features[1] < -0.17959 ) {
      sum += -7.19156e-05;
    } else {
      sum += 7.19156e-05;
    }
  }
  // tree 2946
  if ( features[8] < 2.09211 ) {
    if ( features[3] < 0.710516 ) {
      sum += -6.96217e-05;
    } else {
      sum += 6.96217e-05;
    }
  } else {
    if ( features[2] < 0.527756 ) {
      sum += 6.96217e-05;
    } else {
      sum += -6.96217e-05;
    }
  }
  // tree 2947
  if ( features[1] < 0.309319 ) {
    if ( features[1] < -0.797617 ) {
      sum += 8.78096e-05;
    } else {
      sum += -8.78096e-05;
    }
  } else {
    if ( features[4] < -0.774054 ) {
      sum += 8.78096e-05;
    } else {
      sum += -8.78096e-05;
    }
  }
  // tree 2948
  if ( features[7] < 3.73601 ) {
    sum += -5.37968e-05;
  } else {
    if ( features[7] < 4.7945 ) {
      sum += 5.37968e-05;
    } else {
      sum += -5.37968e-05;
    }
  }
  // tree 2949
  if ( features[0] < 1.51828 ) {
    if ( features[1] < -0.538022 ) {
      sum += 8.53171e-05;
    } else {
      sum += -8.53171e-05;
    }
  } else {
    if ( features[4] < -1.10944 ) {
      sum += 8.53171e-05;
    } else {
      sum += -8.53171e-05;
    }
  }
  // tree 2950
  if ( features[0] < 1.51828 ) {
    if ( features[3] < 0.825361 ) {
      sum += -6.19257e-05;
    } else {
      sum += 6.19257e-05;
    }
  } else {
    if ( features[2] < -0.330568 ) {
      sum += -6.19257e-05;
    } else {
      sum += 6.19257e-05;
    }
  }
  // tree 2951
  if ( features[5] < 0.329645 ) {
    if ( features[7] < 4.53009 ) {
      sum += 0.000108164;
    } else {
      sum += -0.000108164;
    }
  } else {
    if ( features[7] < 4.69073 ) {
      sum += -0.000108164;
    } else {
      sum += 0.000108164;
    }
  }
  // tree 2952
  if ( features[8] < 2.09211 ) {
    if ( features[7] < 4.76501 ) {
      sum += 7.03733e-05;
    } else {
      sum += -7.03733e-05;
    }
  } else {
    if ( features[4] < -3.0468 ) {
      sum += -7.03733e-05;
    } else {
      sum += 7.03733e-05;
    }
  }
  // tree 2953
  if ( features[5] < 0.329645 ) {
    if ( features[8] < 2.96068 ) {
      sum += -7.73028e-05;
    } else {
      sum += 7.73028e-05;
    }
  } else {
    if ( features[3] < 0.662954 ) {
      sum += -7.73028e-05;
    } else {
      sum += 7.73028e-05;
    }
  }
  // tree 2954
  if ( features[7] < 3.73601 ) {
    sum += -6.00162e-05;
  } else {
    if ( features[4] < -3.0468 ) {
      sum += -6.00162e-05;
    } else {
      sum += 6.00162e-05;
    }
  }
  // tree 2955
  if ( features[5] < 0.329645 ) {
    if ( features[7] < 4.64755 ) {
      sum += 9.68648e-05;
    } else {
      sum += -9.68648e-05;
    }
  } else {
    if ( features[3] < 0.662954 ) {
      sum += -9.68648e-05;
    } else {
      sum += 9.68648e-05;
    }
  }
  // tree 2956
  if ( features[5] < 0.329645 ) {
    if ( features[8] < 2.96068 ) {
      sum += -8.76692e-05;
    } else {
      sum += 8.76692e-05;
    }
  } else {
    if ( features[7] < 4.69073 ) {
      sum += -8.76692e-05;
    } else {
      sum += 8.76692e-05;
    }
  }
  // tree 2957
  if ( features[7] < 3.73601 ) {
    sum += -5.95982e-05;
  } else {
    if ( features[4] < -3.0468 ) {
      sum += -5.95982e-05;
    } else {
      sum += 5.95982e-05;
    }
  }
  // tree 2958
  if ( features[6] < 2.32779 ) {
    if ( features[8] < 3.05694 ) {
      sum += -9.42931e-05;
    } else {
      sum += 9.42931e-05;
    }
  } else {
    if ( features[0] < 1.77191 ) {
      sum += -9.42931e-05;
    } else {
      sum += 9.42931e-05;
    }
  }
  // tree 2959
  if ( features[0] < 3.03054 ) {
    if ( features[0] < 2.12578 ) {
      sum += 6.82284e-05;
    } else {
      sum += -6.82284e-05;
    }
  } else {
    sum += 6.82284e-05;
  }
  // tree 2960
  if ( features[7] < 3.73601 ) {
    sum += -5.93543e-05;
  } else {
    if ( features[4] < -3.0468 ) {
      sum += -5.93543e-05;
    } else {
      sum += 5.93543e-05;
    }
  }
  // tree 2961
  if ( features[8] < 2.09211 ) {
    if ( features[5] < 0.754281 ) {
      sum += 7.33472e-05;
    } else {
      sum += -7.33472e-05;
    }
  } else {
    if ( features[6] < 2.15222 ) {
      sum += -7.33472e-05;
    } else {
      sum += 7.33472e-05;
    }
  }
  // tree 2962
  if ( features[8] < 2.09211 ) {
    if ( features[5] < 0.754281 ) {
      sum += 6.70535e-05;
    } else {
      sum += -6.70535e-05;
    }
  } else {
    if ( features[5] < 0.624955 ) {
      sum += 6.70535e-05;
    } else {
      sum += -6.70535e-05;
    }
  }
  // tree 2963
  if ( features[0] < 3.03054 ) {
    if ( features[2] < 0.956816 ) {
      sum += -7.97819e-05;
    } else {
      sum += 7.97819e-05;
    }
  } else {
    if ( features[5] < 0.46068 ) {
      sum += 7.97819e-05;
    } else {
      sum += -7.97819e-05;
    }
  }
  // tree 2964
  if ( features[8] < 2.09211 ) {
    if ( features[6] < 2.13557 ) {
      sum += 6.91342e-05;
    } else {
      sum += -6.91342e-05;
    }
  } else {
    if ( features[3] < 1.14936 ) {
      sum += 6.91342e-05;
    } else {
      sum += -6.91342e-05;
    }
  }
  // tree 2965
  if ( features[7] < 3.73601 ) {
    sum += -4.54673e-05;
  } else {
    if ( features[8] < 1.88686 ) {
      sum += -4.54673e-05;
    } else {
      sum += 4.54673e-05;
    }
  }
  // tree 2966
  if ( features[5] < 0.329645 ) {
    if ( features[7] < 4.53009 ) {
      sum += 0.000107299;
    } else {
      sum += -0.000107299;
    }
  } else {
    if ( features[7] < 4.69073 ) {
      sum += -0.000107299;
    } else {
      sum += 0.000107299;
    }
  }
  // tree 2967
  if ( features[8] < 2.09211 ) {
    if ( features[7] < 4.76501 ) {
      sum += 6.99447e-05;
    } else {
      sum += -6.99447e-05;
    }
  } else {
    if ( features[6] < 2.15222 ) {
      sum += -6.99447e-05;
    } else {
      sum += 6.99447e-05;
    }
  }
  // tree 2968
  if ( features[1] < 0.309319 ) {
    if ( features[1] < -0.797617 ) {
      sum += 8.77398e-05;
    } else {
      sum += -8.77398e-05;
    }
  } else {
    if ( features[8] < 2.67159 ) {
      sum += -8.77398e-05;
    } else {
      sum += 8.77398e-05;
    }
  }
  // tree 2969
  if ( features[0] < 3.03054 ) {
    if ( features[0] < 2.12578 ) {
      sum += 8.08534e-05;
    } else {
      sum += -8.08534e-05;
    }
  } else {
    if ( features[5] < 0.46068 ) {
      sum += 8.08534e-05;
    } else {
      sum += -8.08534e-05;
    }
  }
  // tree 2970
  if ( features[8] < 2.09211 ) {
    if ( features[7] < 4.76501 ) {
      sum += 6.95828e-05;
    } else {
      sum += -6.95828e-05;
    }
  } else {
    if ( features[6] < 2.15222 ) {
      sum += -6.95828e-05;
    } else {
      sum += 6.95828e-05;
    }
  }
  // tree 2971
  if ( features[7] < 3.73601 ) {
    sum += -5.3493e-05;
  } else {
    if ( features[7] < 4.7945 ) {
      sum += 5.3493e-05;
    } else {
      sum += -5.3493e-05;
    }
  }
  // tree 2972
  if ( features[8] < 2.09211 ) {
    if ( features[3] < 0.710516 ) {
      sum += -6.30286e-05;
    } else {
      sum += 6.30286e-05;
    }
  } else {
    if ( features[5] < 0.624955 ) {
      sum += 6.30286e-05;
    } else {
      sum += -6.30286e-05;
    }
  }
  // tree 2973
  if ( features[5] < 0.329645 ) {
    if ( features[7] < 4.53009 ) {
      sum += 9.82456e-05;
    } else {
      sum += -9.82456e-05;
    }
  } else {
    if ( features[1] < -0.53912 ) {
      sum += 9.82456e-05;
    } else {
      sum += -9.82456e-05;
    }
  }
  // tree 2974
  if ( features[8] < 2.09211 ) {
    if ( features[6] < 2.13557 ) {
      sum += 7.80883e-05;
    } else {
      sum += -7.80883e-05;
    }
  } else {
    if ( features[2] < 0.527756 ) {
      sum += 7.80883e-05;
    } else {
      sum += -7.80883e-05;
    }
  }
  // tree 2975
  if ( features[8] < 2.09211 ) {
    if ( features[7] < 4.76501 ) {
      sum += 6.94057e-05;
    } else {
      sum += -6.94057e-05;
    }
  } else {
    if ( features[4] < -3.0468 ) {
      sum += -6.94057e-05;
    } else {
      sum += 6.94057e-05;
    }
  }
  // tree 2976
  if ( features[5] < 0.329645 ) {
    if ( features[4] < -1.32703 ) {
      sum += 9.31739e-05;
    } else {
      sum += -9.31739e-05;
    }
  } else {
    if ( features[7] < 4.69073 ) {
      sum += -9.31739e-05;
    } else {
      sum += 9.31739e-05;
    }
  }
  // tree 2977
  if ( features[0] < 3.03054 ) {
    if ( features[0] < 2.12578 ) {
      sum += 8.03808e-05;
    } else {
      sum += -8.03808e-05;
    }
  } else {
    if ( features[5] < 0.46068 ) {
      sum += 8.03808e-05;
    } else {
      sum += -8.03808e-05;
    }
  }
  // tree 2978
  if ( features[8] < 2.09211 ) {
    if ( features[6] < 2.13557 ) {
      sum += 7.76179e-05;
    } else {
      sum += -7.76179e-05;
    }
  } else {
    if ( features[2] < 0.527756 ) {
      sum += 7.76179e-05;
    } else {
      sum += -7.76179e-05;
    }
  }
  // tree 2979
  if ( features[0] < 3.03054 ) {
    if ( features[0] < 2.12578 ) {
      sum += 7.99142e-05;
    } else {
      sum += -7.99142e-05;
    }
  } else {
    if ( features[1] < 0.0281889 ) {
      sum += -7.99142e-05;
    } else {
      sum += 7.99142e-05;
    }
  }
  // tree 2980
  if ( features[0] < 1.93071 ) {
    if ( features[1] < -0.581424 ) {
      sum += 9.59513e-05;
    } else {
      sum += -9.59513e-05;
    }
  } else {
    if ( features[5] < 0.751479 ) {
      sum += 9.59513e-05;
    } else {
      sum += -9.59513e-05;
    }
  }
  // tree 2981
  if ( features[5] < 0.329645 ) {
    if ( features[8] < 2.96068 ) {
      sum += -8.67366e-05;
    } else {
      sum += 8.67366e-05;
    }
  } else {
    if ( features[7] < 4.69073 ) {
      sum += -8.67366e-05;
    } else {
      sum += 8.67366e-05;
    }
  }
  // tree 2982
  if ( features[0] < 1.93071 ) {
    if ( features[1] < -0.581424 ) {
      sum += 8.92746e-05;
    } else {
      sum += -8.92746e-05;
    }
  } else {
    if ( features[1] < -0.633391 ) {
      sum += -8.92746e-05;
    } else {
      sum += 8.92746e-05;
    }
  }
  // tree 2983
  if ( features[8] < 2.09211 ) {
    if ( features[5] < 0.754281 ) {
      sum += 7.2515e-05;
    } else {
      sum += -7.2515e-05;
    }
  } else {
    if ( features[6] < 2.15222 ) {
      sum += -7.2515e-05;
    } else {
      sum += 7.2515e-05;
    }
  }
  // tree 2984
  if ( features[8] < 2.09211 ) {
    if ( features[6] < 2.13557 ) {
      sum += 7.71899e-05;
    } else {
      sum += -7.71899e-05;
    }
  } else {
    if ( features[4] < -3.0468 ) {
      sum += -7.71899e-05;
    } else {
      sum += 7.71899e-05;
    }
  }
  // tree 2985
  if ( features[0] < 3.03054 ) {
    if ( features[7] < 4.33271 ) {
      sum += -6.10701e-05;
    } else {
      sum += 6.10701e-05;
    }
  } else {
    sum += 6.10701e-05;
  }
  // tree 2986
  if ( features[8] < 2.09211 ) {
    if ( features[6] < 2.13557 ) {
      sum += 7.72153e-05;
    } else {
      sum += -7.72153e-05;
    }
  } else {
    if ( features[6] < 2.15222 ) {
      sum += -7.72153e-05;
    } else {
      sum += 7.72153e-05;
    }
  }
  // tree 2987
  if ( features[8] < 2.09211 ) {
    if ( features[0] < 2.72769 ) {
      sum += -6.12187e-05;
    } else {
      sum += 6.12187e-05;
    }
  } else {
    if ( features[5] < 0.624955 ) {
      sum += 6.12187e-05;
    } else {
      sum += -6.12187e-05;
    }
  }
  // tree 2988
  if ( features[8] < 2.09211 ) {
    if ( features[5] < 0.754281 ) {
      sum += 7.18601e-05;
    } else {
      sum += -7.18601e-05;
    }
  } else {
    if ( features[6] < 2.15222 ) {
      sum += -7.18601e-05;
    } else {
      sum += 7.18601e-05;
    }
  }
  // tree 2989
  if ( features[1] < 0.309319 ) {
    if ( features[1] < -0.797617 ) {
      sum += 8.79351e-05;
    } else {
      sum += -8.79351e-05;
    }
  } else {
    if ( features[4] < -0.774054 ) {
      sum += 8.79351e-05;
    } else {
      sum += -8.79351e-05;
    }
  }
  // tree 2990
  if ( features[0] < 3.03054 ) {
    if ( features[0] < 2.12578 ) {
      sum += 7.98044e-05;
    } else {
      sum += -7.98044e-05;
    }
  } else {
    if ( features[1] < 0.0281889 ) {
      sum += -7.98044e-05;
    } else {
      sum += 7.98044e-05;
    }
  }
  // tree 2991
  if ( features[8] < 2.09211 ) {
    if ( features[6] < 2.13557 ) {
      sum += 7.68644e-05;
    } else {
      sum += -7.68644e-05;
    }
  } else {
    if ( features[4] < -3.0468 ) {
      sum += -7.68644e-05;
    } else {
      sum += 7.68644e-05;
    }
  }
  // tree 2992
  if ( features[0] < 3.03054 ) {
    if ( features[0] < 2.12578 ) {
      sum += 7.96685e-05;
    } else {
      sum += -7.96685e-05;
    }
  } else {
    if ( features[5] < 0.46068 ) {
      sum += 7.96685e-05;
    } else {
      sum += -7.96685e-05;
    }
  }
  // tree 2993
  if ( features[1] < 0.309319 ) {
    if ( features[1] < -0.797617 ) {
      sum += 8.75688e-05;
    } else {
      sum += -8.75688e-05;
    }
  } else {
    if ( features[4] < -0.774054 ) {
      sum += 8.75688e-05;
    } else {
      sum += -8.75688e-05;
    }
  }
  // tree 2994
  if ( features[5] < 0.329645 ) {
    if ( features[7] < 4.53009 ) {
      sum += 0.000106122;
    } else {
      sum += -0.000106122;
    }
  } else {
    if ( features[7] < 4.69073 ) {
      sum += -0.000106122;
    } else {
      sum += 0.000106122;
    }
  }
  // tree 2995
  if ( features[0] < 1.93071 ) {
    if ( features[5] < 0.883423 ) {
      sum += -7.69362e-05;
    } else {
      sum += 7.69362e-05;
    }
  } else {
    if ( features[2] < -0.330568 ) {
      sum += -7.69362e-05;
    } else {
      sum += 7.69362e-05;
    }
  }
  // tree 2996
  if ( features[6] < 2.32779 ) {
    if ( features[8] < 3.05694 ) {
      sum += -9.39989e-05;
    } else {
      sum += 9.39989e-05;
    }
  } else {
    if ( features[0] < 1.77191 ) {
      sum += -9.39989e-05;
    } else {
      sum += 9.39989e-05;
    }
  }
  // tree 2997
  if ( features[8] < 2.09211 ) {
    if ( features[7] < 4.76501 ) {
      sum += 6.90541e-05;
    } else {
      sum += -6.90541e-05;
    }
  } else {
    if ( features[2] < 0.527756 ) {
      sum += 6.90541e-05;
    } else {
      sum += -6.90541e-05;
    }
  }
  // tree 2998
  if ( features[8] < 2.09211 ) {
    if ( features[5] < 0.754281 ) {
      sum += 7.17112e-05;
    } else {
      sum += -7.17112e-05;
    }
  } else {
    if ( features[4] < -3.0468 ) {
      sum += -7.17112e-05;
    } else {
      sum += 7.17112e-05;
    }
  }
  // tree 2999
  if ( features[8] < 2.09211 ) {
    if ( features[6] < 2.13557 ) {
      sum += 7.64806e-05;
    } else {
      sum += -7.64806e-05;
    }
  } else {
    if ( features[2] < 0.527756 ) {
      sum += 7.64806e-05;
    } else {
      sum += -7.64806e-05;
    }
  }
  return sum;
}
