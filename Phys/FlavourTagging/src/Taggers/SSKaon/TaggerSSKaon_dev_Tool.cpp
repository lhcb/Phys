/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files
#include "TaggerSSKaon_dev_Tool.h"
#include "src/Utils/TaggingHelpers.h"

//--------------------------------------------------------------------
// Implementation file for class : TaggerSSKaon_dev_Tool
//
// Basem Khanji following TaggerNEWKaonSameTool.cpp as example
// 2017-08-01
// Follow old framework for now, new framework neeeds more improvemnts to accomodate SSkaon features
//--------------------------------------------------------------------

using namespace LHCb;
using namespace Gaudi::Units;

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( TaggerSSKaon_dev_Tool )

//====================================================================
TaggerSSKaon_dev_Tool::TaggerSSKaon_dev_Tool( const std::string& type, const std::string& name,
                                              const IInterface* parent )
    : GaudiTool( type, name, parent ) {
  declareInterface<ITagger>( this );

  declareProperty( "CombTech", m_CombinationTechnique = "BDTG" );
  declareProperty( "BDTName", m_BDTName = "BDTTool_GBDT" );

  // preselection  for Reco14
  declareProperty( "Kaon_PIDk_cut", m_PID_k_cut = 0.75 );
  declareProperty( "Kaon_PIDkp_cut", m_PIDkp_cut = -8.5 );
  declareProperty( "Kaon_ghost_cut", m_ghost_cut = 10. ); // no cut Reco 12 & Reco14
  declareProperty( "Kaon_distPhi_cut", m_distPhi_cut_kaon = 0.005 );
  declareProperty( "m_ips_cut_kaon", m_ips_cut_kaon = 14 );

  declareProperty( "Kaon_P0_Cal", m_P0_Cal_kaon = 0.025841 );  // 0.408 Reco12
  declareProperty( "Kaon_P1_Cal", m_P1_Cal_kaon = -0.21766 );  // 0.86 Reco12
  declareProperty( "Kaon_AverageEta", m_AverageEta = 0.3949 ); // 0.4302 Reco12
  declareProperty( "Kaon_ProbMin", m_ProbMin_kaon = 0.5 );
  declareProperty( "Kaon_ProbMax", m_ProbMax_kaon = 0.5 );

  // declareProperty( "Kaon_ipPU_cut", m_ipPU_cut_kaon      = 7.5 );
  declareProperty( "Kaon_BDTseltracks_cut", m_BDTseltracks_cut_kaon = 0.72 ); // Reco12 & Reco14

  m_BDTseltracks = 0.;
  m_BDTeta       = 0.;
  m_BDTeta_cc    = 0.;

  m_util    = 0;
  m_descend = 0;
}

TaggerSSKaon_dev_Tool::~TaggerSSKaon_dev_Tool() {}

//=====================================================================
StatusCode TaggerSSKaon_dev_Tool::initialize() {
  const StatusCode sc = GaudiTool::initialize();
  if ( sc.isFailure() ) return sc;

  if ( msgLevel( MSG::DEBUG ) )
    debug() << "BDT KSS_dev calib ctt: P0_Cal " << m_P0_Cal_kaon << ", P1_Cal " << m_P1_Cal_kaon << endmsg;

  m_util = tool<ITaggingUtils>( "TaggingUtils", this );

  m_descend = tool<IParticleDescendants>( "ParticleDescendants", this );

  std::vector<std::string> variable_names;
  variable_names.push_back( "log(Tr_P)" );
  variable_names.push_back( "log(Tr_PT)" );
  variable_names.push_back( "log(B_PT)" );
  variable_names.push_back( "log(abs(deltaPhi))" );
  variable_names.push_back( "log(abs(deltaEta))" );
  variable_names.push_back( "log(pTrel)" );
  variable_names.push_back( "log(Tr_BPVIP)" );
  variable_names.push_back( "log(sqrt(Tr_BPVIPCHI2))" );
  variable_names.push_back( "log(Tr_TRCHI2DOF)" );
  variable_names.push_back( "log(nTracks_presel)" );
  variable_names.push_back( "nPV" );
  variable_names.push_back( "Tr_PROBNNk" );
  variable_names.push_back( "Tr_PROBNNpi" );
  variable_names.push_back( "Tr_PROBNNp" );

  myBDTseltracks_reader = new BDTseltracksReaderCompileWrapper( variable_names );
  variable_names.clear();

  variable_names.push_back( "log(B_PT)" );
  variable_names.push_back( "nTracks_presel_BDTGcut" );
  variable_names.push_back( "nPV" );
  variable_names.push_back( "(Tr_Charge*Tr_BDTG_selBestTracks_14vars)" );
  variable_names.push_back( "(Tr_Charge_2nd*Tr_BDTG_selBestTracks_14vars_2nd)" );
  variable_names.push_back( "(Tr_Charge_3rd*Tr_BDTG_selBestTracks_14vars_3rd)" );

  myBDTeta_reader = new BDTetaReaderCompileWrapper( variable_names );
  variable_names.clear();

  return sc;
}

//=====================================================================
Tagger TaggerSSKaon_dev_Tool::tag( const Particle* AXB0, const RecVertex* RecVert, const int nPV,
                                   Particle::ConstVector& vtags ) {

  Tagger tkaon_dev;
  if ( !RecVert ) return tkaon_dev;

  if ( msgLevel( MSG::VERBOSE ) ) verbose() << "-- BDT_SSKaon_dev Tagger--" << endmsg;

  const double         B_Pt   = AXB0->pt();
  const int            no_vtx = nPV;
  Gaudi::LorentzVector ptotB  = AXB0->momentum();

  // fill auxdaugh for distphi
  double                distphi;
  Particle::ConstVector axdaugh = m_descend->descendants( AXB0 );
  axdaugh.push_back( AXB0 );
  // select kaon opposite tagger(s)
  Particle const*                       ikaon = 0;
  Particle::ConstVector::const_iterator ipart;
  // Particle::ConstVector::iterator ipart;

  Particle::ConstVector vtags_sel;

  for ( ipart = vtags.begin(); ipart != vtags.end(); ipart++ )

  { // presel before BDTseltrac cut

    if ( m_util->isInTree( *ipart, axdaugh, distphi ) ) continue; // exclude signal
    if ( distphi < m_distPhi_cut_kaon ) continue;
    const int type = ( *ipart )->proto()->track()->type();
    if ( type != 3 && type != 4 ) continue;
    double IP, IPerr;
    m_util->calcIP( *ipart, RecVert, IP, IPerr ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    IP                 = fabs( IP );
    const double IPsig = fabs( IP / IPerr );
    if ( IPsig > m_ips_cut_kaon ) continue;
    const double P = ( *ipart )->p();
    if ( P > 200000 || P < 2000 ) continue;
    const double Pt = ( *ipart )->pt();
    if ( Pt > 10000 ) continue;
    ////////////////////////////////
    vtags_sel.push_back( *ipart ); // store presel tagger candidate
    ////////////////////////////////
  }

  int cands = vtags_sel.size();

  double cands_BDTseltracks = cands;

  typedef std::pair<double, int>                                   myPair;
  typedef std::pair<double, Particle::ConstVector::const_iterator> myPair_ev;
  std::vector<myPair>                                              myMap;
  std::vector<myPair_ev>                                           event_map;

  const int max_tracks = 50;

  std::tr1::array<double, max_tracks> pre_sign_tag;
  std::tr1::array<double, max_tracks> pre_rBDT_opp;
  std::tr1::array<double, max_tracks> pre_pidk;

  std::tr1::array<double, max_tracks> pos_sign_tag;
  std::tr1::array<double, max_tracks> pos_rBDT_opp;
  // std::tr1::array<double, max_tracks> pos_pidk;

  int count = 0;

  for ( ipart = vtags_sel.begin(); ipart != vtags_sel.end(); ipart++ ) { // do BDTseltrack selection

    Gaudi::LorentzVector ptotT = ( *ipart )->momentum();

    const double Pt             = ( *ipart )->pt();
    const double P              = ( *ipart )->p();
    const Track* track          = ( *ipart )->proto()->track();
    const double tr_chi2perndof = track->chi2PerDoF();
    const double pidk           = ( *ipart )->proto()->info( ProtoParticle::ProbNNk, -1000.0 );
    const double pidpi          = ( *ipart )->proto()->info( ProtoParticle::ProbNNpi, -1000.0 );
    const double pidp           = ( *ipart )->proto()->info( ProtoParticle::ProbNNp, -1000.0 );
    const double pLrel = ( ptotB.Px() * ptotT.Px() + ptotB.Py() * ptotT.Py() + ptotB.Pz() * ptotT.Pz() ) / ptotB.P();
    const double pTrel = sqrt( ptotT.P() * ptotT.P() - pLrel * pLrel );
    double       IP, IPerr;
    m_util->calcIP( *ipart, RecVert, IP, IPerr ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    IP = fabs( IP );
    // std::cout<<"Hi I am in the SSKaonDeV before IPerr check"<<std::endl;
    if ( !IPerr ) continue;
    const double IPsig = fabs( IP / IPerr );
    // std::cout<<"Hi I am in the SSKaonDeV after IPerr check"<<std::endl;
    const double diff_eta = fabs( log( tan( ptotB.Theta() / 2. ) / tan( asin( Pt / P ) / 2. ) ) );
    double       diff_phi = fabs( ( *ipart )->momentum().Phi() - ptotB.Phi() );
    if ( diff_phi > 3.1416 ) diff_phi = 6.2832 - diff_phi;

    std::vector<double> values;
    values.push_back( log( P ) );
    values.push_back( log( Pt ) );
    values.push_back( log( B_Pt ) );
    values.push_back( log( diff_phi ) );
    values.push_back( log( diff_eta ) );
    values.push_back( log( pTrel ) );
    values.push_back( log( IP ) );
    values.push_back( log( IPsig ) );
    values.push_back( log( tr_chi2perndof ) );
    values.push_back( log( cands_BDTseltracks ) );
    values.push_back( no_vtx );
    values.push_back( pidk );
    values.push_back( pidpi );
    values.push_back( pidp );

    m_BDTseltracks = myBDTseltracks_reader->GetMvaValue( values );

    // for(unsigned int i=0; i<values.size(); ++i) std::cout << values.at(i)<<" " <<std::endl;
    if ( msgLevel( MSG::DEBUG ) ) {
      debug() << " TaggerBDTSSK(Dev) BDTseltracks =" << m_BDTseltracks << " BDT inputs:";
      for ( unsigned int i = 0; i < values.size(); ++i ) debug() << values.at( i ) << " ";
      debug() << endmsg;
    }

    // if(ippu < m_ipPU_cut_kaon)  continue;
    // std::cout<<"Hi I am in the SSKaonDeV before BDT cut "<<std::endl;
    if ( m_BDTseltracks < m_BDTseltracks_cut_kaon ) continue;
    // std::cout<<"Hi I am in the SSKaonDeV after BDT cut "<<std::endl;
    const myPair pair = std::make_pair( m_BDTseltracks, count ); // sort tracks by BDTseltracks output, highest first
    myMap.push_back( pair );
    if ( msgLevel( MSG::DEBUG ) ) debug() << " " << pair.first << " " << pair.second << endmsg;

    pre_sign_tag.at( count ) = ( *ipart )->charge();
    pre_rBDT_opp.at( count ) = m_BDTseltracks;
    pre_pidk.at( count )     = pidk;

    ++count;

    const myPair_ev ev_pair = std::make_pair( m_BDTseltracks, ipart ); // find highest nn1 track
    event_map.push_back( ev_pair );
  }
  if ( msgLevel( MSG::DEBUG ) )
    debug() << " vtags_sel.size()=" << vtags_sel.size() << " myMap.size()" << myMap.size() << endmsg;

  count = 0;

  cands               = myMap.size();
  double cands_BDTeta = cands;

  if ( msgLevel( MSG::DEBUG ) ) debug() << " after BDTseltracks cut " << cands << endmsg;

  int    my_ss_k_dec = 0;
  double my_ss_k_eta = -0.1;

  if ( cands == 0 )
    return tkaon_dev;

  else {
    std::stable_sort( myMap.begin(), myMap.end(), std::greater<myPair>() );
    for ( unsigned i = 0; i < (unsigned)cands; i++ ) {
      // assert(i < pos_rnet_opp.size());
      // assert(i < pre_rnet_opp.size());
      // assert(i < pos_sign_tag.size());
      // assert(i < pre_sign_tag.size());
      // assert(i < pos_pidk.size());
      // assert(i < pre_pidk.size());
      // std::cout<<"Hi I am in the SSKaonDeV when ncand is not zero "<<std::endl;
      if ( msgLevel( MSG::DEBUG ) )
        debug() << " <map 1> " << myMap[i].first << " <map 2> " << myMap[i].second << endmsg;
      pos_rBDT_opp.at( i ) = pre_rBDT_opp.at( myMap[i].second );
      pos_sign_tag.at( i ) = pre_sign_tag.at( myMap[i].second );
      // pos_pidk.at(i)     = pre_pidk.at(myMap[i].second);
    }

    if ( cands < max_tracks ) {
      for ( int i = 0; i < ( max_tracks - cands ); ++i ) {
        pos_rBDT_opp.at( cands + i ) = 0.;
        pos_sign_tag.at( cands + i ) = 0.;
        // pos_pidk.at(cands+i)     = 0.;
      }
    }

    std::vector<double> values;
    values.push_back( log( B_Pt ) );
    values.push_back( cands_BDTeta );
    values.push_back( no_vtx );
    values.push_back( pos_sign_tag.at( 0 ) * pos_rBDT_opp.at( 0 ) );
    values.push_back( pos_sign_tag.at( 1 ) * pos_rBDT_opp.at( 1 ) );
    values.push_back( pos_sign_tag.at( 2 ) * pos_rBDT_opp.at( 2 ) );
    double BDTeta = myBDTeta_reader->GetMvaValue( values );
    m_BDTeta      = ( BDTeta / 0.71 + 1. ) * 0.5;
    //
    std::vector<double> valuescc;
    valuescc.push_back( log( B_Pt ) );
    valuescc.push_back( cands_BDTeta );
    valuescc.push_back( no_vtx );
    valuescc.push_back( -1. * pos_sign_tag.at( 0 ) * pos_rBDT_opp.at( 0 ) );
    valuescc.push_back( -1. * pos_sign_tag.at( 1 ) * pos_rBDT_opp.at( 1 ) );
    valuescc.push_back( -1. * pos_sign_tag.at( 2 ) * pos_rBDT_opp.at( 2 ) );
    double BDTeta_cc = myBDTeta_reader->GetMvaValue( valuescc );
    m_BDTeta_cc      = ( BDTeta_cc / 0.71 + 1. ) * 0.5;
    my_ss_k_eta      = ( m_BDTeta + ( 1. - m_BDTeta_cc ) ) * 0.5;
    // std::cout<<"Here is Eta value : "<< my_ss_k_eta <<std::endl;
    if ( msgLevel( MSG::DEBUG ) ) debug() << " BDTeta  " << BDTeta << endmsg;

    myMap.clear();
    // event_map is a vector, so let's sort it descending...
    std::stable_sort( event_map.rbegin(), event_map.rend() );
    // assert(event_map.front().first >= event_map.back().first);
    ikaon = *event_map[0].second;

    event_map.clear();
  }

  if ( !ikaon ) return tkaon_dev;

  if ( my_ss_k_eta >= m_ProbMax_kaon ) my_ss_k_dec = 1;

  if ( my_ss_k_eta < m_ProbMin_kaon ) my_ss_k_dec = -1;

  if ( my_ss_k_eta >= 0.5 ) my_ss_k_eta = 1. - my_ss_k_eta;

  my_ss_k_eta = ( m_P0_Cal_kaon + m_AverageEta ) + ( 1 + m_P1_Cal_kaon ) * ( my_ss_k_eta - m_AverageEta );

  tkaon_dev.setOmega( my_ss_k_eta );
  tkaon_dev.setDecision( my_ss_k_dec );
  tkaon_dev.setMvaValue( my_ss_k_eta );
  tkaon_dev.setType( taggerType() );
  // tkaon.addToTaggerParts(ikaon);
  for ( int i = 0; i < cands_BDTeta; i++ ) { tkaon_dev.addToTaggerParts( *event_map[i].second ); }

  if ( msgLevel( MSG::DEBUG ) ) debug() << " NNetSSK decision=" << my_ss_k_dec << " omega=" << my_ss_k_eta << endmsg;

  return tkaon_dev;
}
