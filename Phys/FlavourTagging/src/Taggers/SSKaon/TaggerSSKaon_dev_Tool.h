/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: TaggerSSKaon_dev_Tool.h,v 1.0 2013-01-23 $
#ifndef USER_TAGGERSSKAON_DEV_TOOL_H
#define USER_TAGGERSSKAON_DEV_TOOL_H 1

// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/AlgTool.h"
// from Event
#include "Event/FlavourTag.h"
#include "Kernel/ITagger.h"

// from local
//#include "ITMVANNetTool.h"
#include "FlavourTagging/ITaggingUtils.h"
#include "Kernel/IParticleDescendants.h"

#include <tr1/array>

#include "Classifiers/TMVA/SSKaon_BDTeta_dev.h"
#include "Classifiers/TMVA/SSKaon_BDTseltracks_dev.h"

/** @class TaggerSSKaon_dev_Tool TaggerSSKaon_dev_Tool.h
 *
 *  Tool to tag the B flavour with the new KaonSame Tagger
 *
 *  @author Basem Khanji
 *  @date   01/08/2017
 */

class TaggerSSKaon_dev_Tool : public GaudiTool, virtual public ITagger {

public:
  /// Standard constructor
  TaggerSSKaon_dev_Tool( const std::string& type, const std::string& name, const IInterface* parent );
  ~TaggerSSKaon_dev_Tool();                       ///< Destructor
  StatusCode               initialize() override; ///<  initialization
  LHCb::Tagger::TaggerType taggerType() const override { return LHCb::Tagger::TaggerType::SSKaonLatest; }

  //-------------------------------------------------------------
  using ITagger::tag;
  LHCb::Tagger tag( const LHCb::Particle*, const LHCb::RecVertex*, const int, LHCb::Particle::ConstVector& ) override;
  //-------------------------------------------------------------

private:
  ITaggingUtils*        m_util;
  IParticleDescendants* m_descend;
  std::string           m_CombinationTechnique, m_BDTName;

  BDTseltracksReaderCompileWrapper* myBDTseltracks_reader;
  BDTetaReaderCompileWrapper*       myBDTeta_reader;

  double m_PID_k_cut;
  double m_PIDkp_cut;
  double m_ghost_cut;
  double m_distPhi_cut_kaon;
  double m_ips_cut_kaon;

  double m_AverageEta;
  double m_ProbMin_kaon;
  double m_ProbMax_kaon;
  double m_P0_Cal_kaon;
  double m_P1_Cal_kaon;

  // double m_ipPU_cut_kaon;
  double m_BDTseltracks_cut_kaon;
  double m_BDTseltracks;
  double m_BDTeta;
  double m_BDTeta_cc;
};

//===============================================================//
#endif // USER_TaggerSSKaon_dev_Tool_H
