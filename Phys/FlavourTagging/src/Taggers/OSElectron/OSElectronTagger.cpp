/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "OSElectronTagger.h"

// from Gaudi

// from Phys

// from local

DECLARE_COMPONENT( OSElectronTagger )

using namespace std;
using namespace LHCb;

OSElectronTagger::OSElectronTagger( const std::string& type, const std::string& name, const IInterface* parent )
    : GaudiTool( type, name, parent ) {
  declareInterface<ITagger>( this );
}

StatusCode OSElectronTagger::initialize() {
  auto sc = GaudiTool::initialize();
  if ( sc.isFailure() ) return sc;

  m_pipeline = make_unique<Pipeline>();
  m_pipeline->setToolProvider( this );
  m_pipeline->setAliases( m_featureAliases );
  m_pipeline->setPipeline( m_selectionPipeline );

  return StatusCode::SUCCESS;
}

Tagger OSElectronTagger::tag( const Particle* sigPart, const RecVertex* assocVtx, const int,
                              Particle::ConstVector& tagParts ) {
  Tagger tagObject;

  m_pipeline->setReconstructionVertex( assocVtx );
  m_pipeline->setSignalCandidate( sigPart );

  auto selectedTaggingElectron = m_pipeline->selectOne( tagParts );

  if ( selectedTaggingElectron ) {
    if ( msgLevel( MSG::DEBUG ) ) {
      m_pipeline->setTaggingParticle( selectedTaggingElectron );
      debug() << "[OSElectronTagger::tag] Found Tagging Electron!" << endmsg;
      debug() << "\n"
              << TaggingHelpers::printFeatureVector( m_pipeline->mergedFeatureNames(), m_pipeline->mergedFeatures(),
                                                     "\t[OSElectronTagger::tag]" )
              << endmsg;
    }

    // this is correct only if the last feature within the whole pipeline
    // actually is the final mva (see OSElectronTaggerConf.py)
    auto mvaValue = m_pipeline->features( selectedTaggingElectron ).back().back();

    if ( msgLevel( MSG::DEBUG ) ) debug() << "[OSElectronTagger::tag] MVA prediction: " << mvaValue << endmsg;

    // if the mva predicts, that the tag will be *in*correct, the sign
    // needs to be flipped
    int  decision = -1 * selectedTaggingElectron->charge();
    auto omega    = 1 - mvaValue;
    if ( mvaValue < 0.5 ) {
      decision *= -1;
      omega = mvaValue;
    }

    if ( msgLevel( MSG::DEBUG ) ) debug() << "[OSElectronTagger] tagged " << decision << endmsg;

    // omega (actually eta) is the mistag estimate, this needs to be calibrated
    // later (@TODO: or at this point via a calibration tool?)
    tagObject.setOmega( omega );
    tagObject.setDecision( decision );
    tagObject.setType( taggerType() );
    tagObject.addToTaggerParts( selectedTaggingElectron );
    tagObject.setMvaValue( mvaValue );
    tagObject.setCharge( selectedTaggingElectron->charge() );
  } else if ( msgLevel( MSG::DEBUG ) ) {
    debug() << "[OSElectronTagger::tag] No tagging Electron found!" << endmsg;
  }

  return tagObject;
}

Tagger OSElectronTagger::tag( const Particle* sigPart, const RecVertex* assocVtx, RecVertex::ConstVector& puVtxs,
                              Particle::ConstVector& tagParts ) {
  m_pipeline->setPileUpVertices( puVtxs );
  return tag( sigPart, assocVtx, puVtxs.size(), tagParts );
}

std::vector<std::string> OSElectronTagger::featureNames() const { return m_pipeline->mergedFeatureNames(); }

std::vector<double> OSElectronTagger::featureValues() const { return m_pipeline->mergedFeatures(); }

std::vector<std::string> OSElectronTagger::featureNamesTagParts() const { return m_pipeline->mergedFeatureNames(); }

std::vector<std::vector<double>> OSElectronTagger::featureValuesTagParts() const {
  return m_pipeline->mergedFeatureMatrix();
}
