/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef PHYS_PHYS_FLAVOURTAGGING_OSELECTRONCLASSIFIERFACTORY_H
#define PHYS_PHYS_FLAVOURTAGGING_OSELECTRONCLASSIFIERFACTORY_H 1

// from Gaudi
#include "GaudiAlg/GaudiTool.h"

// from STL
#include <functional>

// local
#include "src/Classification/ITaggingClassifierFactory.h"
#include "src/Classification/TaggingClassifierTMVA.h"
#include "src/Classification/TaggingClassifierXGB.h"

class OSElectronClassifierFactory : public GaudiTool, virtual public ITaggingClassifierFactory {
public:
  OSElectronClassifierFactory( const std::string& type, const std::string& name, const IInterface* parent );

  StatusCode initialize() override;

  StatusCode finalize() override { return GaudiTool::finalize(); }

  std::unique_ptr<ITaggingClassifier> taggingClassifier() override;

  virtual std::unique_ptr<ITaggingClassifier> taggingClassifier( const std::string& type,
                                                                 const std::string& name ) override;

private:
  Gaudi::Property<std::string> m_classifierType{this, "ClassifierType", "TMVA",
                                                "Type of classifier (can be TMVA, XGB)"};
  Gaudi::Property<std::string> m_classifierName{this, "ClassifierName",
                                                "OSElectron_Data_Run1_All_Bu2JpsiK_TMVA_MLP_v1r0",
                                                "Name to identify the classifier. Can be also be a path to a binary."};
  Gaudi::Property<bool>        m_nameIsPath{this, "NameIsPath", false, "Interpret name as path to binary."};

  std::map<std::string, std::function<std::unique_ptr<TaggingClassifierTMVA>()>> m_classifierMapTMVA;

  template <class T>
  void addTMVAClassifier( const std::string& classifierName ) {
    m_classifierMapTMVA.emplace( classifierName, &std::make_unique<T> );
  }
};

#endif // PHYS_PHYS_FLAVOURTAGGING_OSELECTRONCLASSIFIERFACTORY_H
