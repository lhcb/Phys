/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "MCElectronOSWrapper.h"

// hack, otherwise: redefinitions...
//
namespace MyMCElectronOSSpace {
#ifndef SKIP_TMVA
#  include "weights/ele__eleMLPBNN_MC.class.C"
#endif
} // namespace MyMCElectronOSSpace

MCElectronOSWrapper::MCElectronOSWrapper( std::vector<std::string>& names ) {
#ifdef SKIP_TMVA
  int size = names.size();
  if ( size == 0 ) std::cout << "WARNING: NO VALUES PASSED" << std::endl;
#else
  reader = new MyMCElectronOSSpace::Read_eleMLPBNN_MC( names );
#endif
}

MCElectronOSWrapper::~MCElectronOSWrapper() {
#ifndef SKIP_TMVA
  delete reader;
#endif
}

double MCElectronOSWrapper::GetMvaValue( std::vector<double> const& values ) {
#ifdef SKIP_TMVA
  int size = values.size();
  if ( size == 0 ) std::cout << "WARNING: NO VALUES PASSED" << std::endl;
  return 0.0;
#else
  return reader->GetMvaValue( values );
#endif
  return 0.0;
}
