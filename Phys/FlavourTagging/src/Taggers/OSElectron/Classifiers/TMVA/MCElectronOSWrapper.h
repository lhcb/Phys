/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef MCELECTRONOSWRAPPER_H
#define MCELECTRONOSWRAPPER_H 1

#include "src/TMVAWrapper.h"

namespace MyMCElectronOSSpace {
  class Read_eleMLPBNN_MC;
}

class MCElectronOSWrapper : public TMVAWrapper {
public:
  MCElectronOSWrapper( std::vector<std::string>& );
  ~MCElectronOSWrapper();
  double GetMvaValue( std::vector<double> const& ) override;

private:
  MyMCElectronOSSpace::Read_eleMLPBNN_MC* reader;
};

#endif
