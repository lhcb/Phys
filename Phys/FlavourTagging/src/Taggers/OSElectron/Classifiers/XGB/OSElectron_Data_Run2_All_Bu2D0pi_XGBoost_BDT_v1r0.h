/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include <cmath>

#include "src/Classification/TaggingClassifierTMVA.h"

class OSElectron_Data_Run2_All_Bu2D0pi_XGBoost_BDT_v1r0 : public TaggingClassifierTMVA {
public:
  double GetMvaValue( const std::vector<double>& featureValues ) const override;

private:
  double evaluateEnsemble( const std::vector<double>& featureValues ) const;

  /* @brief sigmoid transformation */
  double sigmoid( double value ) const;
};
