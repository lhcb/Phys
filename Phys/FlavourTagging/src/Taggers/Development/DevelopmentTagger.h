/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

// from Gaudi
#include "GaudiAlg/GaudiTool.h"

// from LHCb
#include "Event/FlavourTag.h"

// from Phys
#include "Kernel/ITagger.h"

// local
#include "src/Selection/Pipeline.h"

#include "LoKi/Operators.h"
#include "LoKi/Particles11.h"
#include "LoKi/Particles8.h"
#include "LoKi/Particles9.h"
#include "LoKi/PhysTypes.h"

/** @brief Pseudto-Tagger to produce optimization tuples.
 *
 * @authors    Kevin Heinicke, Julian Wishahi
 * @date       2017
 */
class DevelopmentTagger : public GaudiTool, virtual public ITagger {

public:
  DevelopmentTagger( const std::string& type, const std::string& name, const IInterface* parent );

  StatusCode initialize() override;

  LHCb::Tagger::TaggerType taggerType() const override { return LHCb::Tagger::TaggerType::unknown; }

  LHCb::Tagger tag( const LHCb::Particle* sigPart, const LHCb::RecVertex* assocVtx, const int nPUVtxs,
                    LHCb::Particle::ConstVector& ) override;

  LHCb::Tagger tag( const LHCb::Particle* sigPart, const LHCb::RecVertex* assocVtx,
                    LHCb::RecVertex::ConstVector& puVtxs, LHCb::Particle::ConstVector& tagParts ) override;

  std::vector<std::string> featureNames() const override;

  std::vector<double> featureValues() const override;

  std::vector<std::string> featureNamesTagParts() const override;

  std::vector<std::vector<double>> featureValuesTagParts() const override;

private:
  Gaudi::Property<std::vector<std::vector<std::string>>> m_selectionPipeline{this, "SelectionPipeline", {{}}, ""};
  Gaudi::Property<std::map<std::string, std::string>>    m_featureAliases{this, "Aliases", {}, ""};
  Gaudi::Property<std::string> m_iFTTagPartsLocation{this, "IFTTagPartsLocation", "Phys/TaggingIFTTracks/Particles",
                                                     "iFT-specific tagging particle location, configured in "
                                                     "FlavourTagging/python/Configuration.py"};
  std::unique_ptr<Pipeline>    m_pipeline = nullptr;
};
