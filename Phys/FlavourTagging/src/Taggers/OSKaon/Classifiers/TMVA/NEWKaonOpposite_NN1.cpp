/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "NEWKaonOpposite_NN1.h"

// hack, otherwise: redefinitions...
//
namespace MyMCSpace {
#include "weights/NN1_OSK.dat/TMVAClassification_MLPBNN.class.C"
}

MCReaderCompileWrapper::MCReaderCompileWrapper( std::vector<std::string>& names )
    : mcreader( new MyMCSpace::ReadMLPBNN( names ) ) {}

MCReaderCompileWrapper::~MCReaderCompileWrapper() { delete mcreader; }

double MCReaderCompileWrapper::GetMvaValue( std::vector<double> const& values ) {
  return mcreader->GetMvaValue( values );
}
