/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef MCKAONOSWRAPPER_H
#define MCKAONOSWRAPPER_H 1

#include "src/TMVAWrapper.h"

namespace MyMCKaonOSSpace {
  class Read_kaonMLPBNN_MC;
}

class MCKaonOSWrapper : public TMVAWrapper {
public:
  MCKaonOSWrapper( std::vector<std::string>& );
  ~MCKaonOSWrapper();
  double GetMvaValue( std::vector<double> const& ) override;

private:
  MyMCKaonOSSpace::Read_kaonMLPBNN_MC* reader;
};

#endif
