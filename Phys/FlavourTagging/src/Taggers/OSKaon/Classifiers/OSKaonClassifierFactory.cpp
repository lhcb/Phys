/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "OSKaonClassifierFactory.h"

// ITaggingClassifier implementations
#include "TMVA/OSKaon_Data_Run1_All_Bu2JpsiK_TMVA_MLP_v1r0.h"
#include "XGBoost/OSKaon_Data_Run2_All_Bu2D0pi_XGBoost_BDT_v1r0.h"
#include "XGBoost/OSKaon_Data_Run2_All_Bu2JpsiK_XGBoost_BDT_v1r0.h"
#include "XGBoost/OSKaon_Data_Run2_All_Bu2JpsiK_XGBoost_BDT_v2r0.h"

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( OSKaonClassifierFactory )

OSKaonClassifierFactory::OSKaonClassifierFactory( const std::string& type, const std::string& name,
                                                  const IInterface* parent )
    : GaudiTool( type, name, parent ) {
  declareInterface<ITaggingClassifierFactory>( this );
}

StatusCode OSKaonClassifierFactory::initialize() {
  auto sc = GaudiTool::initialize();
  if ( sc.isFailure() ) return sc;

  addTMVAClassifier<OSKaon_Data_Run1_All_Bu2JpsiK_TMVA_MLP_v1r0>( "OSKaon_Data_Run1_All_Bu2JpsiK_TMVA_MLP_v1r0" );
  addTMVAClassifier<OSKaon_Data_Run2_All_Bu2JpsiK_XGBoost_BDT_v1r0>( "OSKaon_Data_Run2_All_Bu2JpsiK_XGBoost_BDT_v1r0" );
  addTMVAClassifier<OSKaon_Data_Run2_All_Bu2D0pi_XGBoost_BDT_v1r0>( "OSKaon_Data_Run2_All_Bu2D0pi_XGBoost_BDT_v1r0" );
  addTMVAClassifier<OSKaon_Data_Run2_All_Bu2JpsiK_XGBoost_BDT_v2r0>( "OSKaon_Data_Run2_All_Bu2JpsiK_XGBoost_BDT_v2r0" );

  return sc;
}

std::unique_ptr<ITaggingClassifier> OSKaonClassifierFactory::taggingClassifier() {
  return m_classifierMapTMVA[m_classifierName]();
}

std::unique_ptr<ITaggingClassifier> OSKaonClassifierFactory::taggingClassifier( const std::string& type,
                                                                                const std::string& name ) {
  if ( type == "TMVA" ) {
    return m_classifierMapTMVA[name]();
  } else {
    error() << "Classifier of type " << type << " unknown!" << endmsg;
    return nullptr;
  }
}
