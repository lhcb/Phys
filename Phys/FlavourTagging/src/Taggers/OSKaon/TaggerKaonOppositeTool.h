/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: TaggerKaonOppositeTool.h,v 1.11 2010-06-17 17:46:03 mgrabalo Exp $
#ifndef USER_TAGGERKAONOPPOSITETOOL_H
#define USER_TAGGERKAONOPPOSITETOOL_H 1

// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/AlgTool.h"
// from Event
#include "Event/FlavourTag.h"
#include "Kernel/ITagger.h"
// from ROOT
#include <TROOT.h>

// from local
#include "FlavourTagging/ITaggingUtils.h"
#include "Kernel/IParticleDescendants.h"
#include "src/INNetTool.h"
#include <list>
#include <string>
#include <utility>

// forward declarations
class ITaggingClassifier;
class ITaggingClassifierFactory;

/** @class TaggerKaonOppositeTool TaggerKaonOppositeTool.h
 *
 *  Tool to tag the B flavour with a KaonOpposite Tagger
 *
 *  @author Marco Musy
 *  @date   30/06/2005
 */

class TaggerKaonOppositeTool : public GaudiTool, virtual public ITagger {

public:
  /// Standard constructor
  TaggerKaonOppositeTool( const std::string& type, const std::string& name, const IInterface* parent );
  virtual ~TaggerKaonOppositeTool(); ///< Destructor
  StatusCode initialize() override;  ///<  initialization
  StatusCode finalize() override;

  LHCb::Tagger::TaggerType taggerType() const override { return LHCb::Tagger::TaggerType::OS_Kaon; }

  //-------------------------------------------------------------
  using ITagger::tag;
  LHCb::Tagger tag( const LHCb::Particle*, const LHCb::RecVertex*, const int, LHCb::Particle::ConstVector& ) override;

  //-------------------------------------------------------------

private:
  std::unique_ptr<ITaggingClassifier> m_classifier        = nullptr;
  ITaggingClassifierFactory*          m_classifierFactory = nullptr;

  Gaudi::Property<std::string> m_classifierFactoryName{this, "ClassifierFactoryName", "OSKaonClassifierFactory",
                                                       "Name of the factory that creates the classifier."};

  ITaggingUtils*        m_util    = nullptr;
  IParticleDescendants* m_descend = nullptr;
  std::string           m_CombinationTechnique;

  // properties
  double m_Pt_cut_kaon;
  double m_P_cut_kaon;
  double m_IP_cut_kaon;
  double m_IPs_cut_kaon;
  double m_lcs_kaon;
  double m_AverageOmega;
  double m_ghost_cut;
  double m_PIDk_cut;
  double m_PIDkp_cut;
  double m_ipPU_cut_kaon;
  double m_distPhi_cut_kaon;

  double m_PIDNNm_cut_kaon;
  double m_PIDNNe_cut_kaon;
  double m_PIDNNk_cut_kaon;
  double m_PIDNNpi_cut_kaon;
  double m_PIDNNp_cut_kaon;
  double m_PIDNNkp_cut_kaon;
  double m_PIDNNkpi_cut_kaon;
  double m_ghostProb_kaon;
  double m_ProbMin_kaon;
  double m_P0_Cal_kaon;
  double m_P1_Cal_kaon;
  double m_Eta_Cal_kaon;
  int    m_isMonteCarlo;
  double m_P0k, m_P1k, m_P2k, m_P3k;
};

//===============================================================//
#endif // USER_TAGGERKAONOPPOSITETOOL_H
