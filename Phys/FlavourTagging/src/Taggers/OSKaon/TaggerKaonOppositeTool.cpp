/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files
#include "TaggerKaonOppositeTool.h"
#include "src/Classification/ITaggingClassifierFactory.h"
#include "src/Utils/TaggingHelpers.h"

//--------------------------------------------------------------------
// Implementation file for class : TaggerKaonOppositeTool
//
// Author: Marco Musy
//--------------------------------------------------------------------

using namespace LHCb;
using namespace Gaudi::Units;

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( TaggerKaonOppositeTool )

//====================================================================
TaggerKaonOppositeTool::TaggerKaonOppositeTool( const std::string& type, const std::string& name,
                                                const IInterface* parent )
    : GaudiTool( type, name, parent )
    , m_util( NULL )

{
  declareInterface<ITagger>( this );

  declareProperty( "CombTech", m_CombinationTechnique = "NNet" );

  declareProperty( "Kaon_Pt_cut", m_Pt_cut_kaon = 0.7 * GeV );
  declareProperty( "Kaon_P_cut", m_P_cut_kaon = 2.0 * GeV );
  declareProperty( "Kaon_IPs_cut", m_IPs_cut_kaon = 4.0 );
  declareProperty( "Kaon_IP_cut", m_IP_cut_kaon = 1.6 );
  declareProperty( "Kaon_lcs_cut", m_lcs_kaon = 3.0 );
  declareProperty( "Kaon_PIDk_cut", m_PIDk_cut = -100. );
  declareProperty( "Kaon_PIDkp_cut", m_PIDkp_cut = -300. );
  declareProperty( "Kaon_ghost_cut", m_ghost_cut = -999.0 );
  declareProperty( "Kaon_ipPU_cut", m_ipPU_cut_kaon = 6.0 );
  declareProperty( "Kaon_distPhi_cut", m_distPhi_cut_kaon = 0.005 );
  declareProperty( "Kaon_PIDNNk_cut", m_PIDNNk_cut_kaon = 0.25 );
  declareProperty( "Kaon_PIDNNm_cut", m_PIDNNm_cut_kaon = 0.8 );
  declareProperty( "Kaon_PIDNNe_cut", m_PIDNNe_cut_kaon = 0.8 );
  declareProperty( "Kaon_PIDNNpi_cut", m_PIDNNpi_cut_kaon = 0.8 );
  declareProperty( "Kaon_PIDNNp_cut", m_PIDNNp_cut_kaon = 0.8 );
  declareProperty( "Kaon_PIDNNkp_cut", m_PIDNNkp_cut_kaon = 0. );
  declareProperty( "Kaon_PIDNNkpi_cut", m_PIDNNkpi_cut_kaon = -0.6 );
  declareProperty( "Kaon_ghostProb_cut", m_ghostProb_kaon = 0.35 );
  declareProperty( "Kaon_P0_Cal", m_P0_Cal_kaon = 0. );
  declareProperty( "Kaon_P1_Cal", m_P1_Cal_kaon = 1. );
  declareProperty( "Kaon_Eta_Cal", m_Eta_Cal_kaon = 0. );
  declareProperty( "Kaon_AverageOmega", m_AverageOmega = 0.33 );
  declareProperty( "Kaon_ProbMin", m_ProbMin_kaon = 0. ); // no cut
  declareProperty( "isMonteCarlo", m_isMonteCarlo = 0 );
  // k scaleX=-5.12122 scaleY=1.21427 offsetY=0.088854 pivotX=0.573943
  declareProperty( "P0_k_scale", m_P0k = -5.12122 );
  declareProperty( "P1_k_scale", m_P1k = 1.21427 );
  declareProperty( "P2_k_scale", m_P2k = 0.088854 );
  declareProperty( "P3_k_scale", m_P3k = 0.573943 );

  m_descend = 0;
}

TaggerKaonOppositeTool::~TaggerKaonOppositeTool() {}

//=====================================================================
StatusCode TaggerKaonOppositeTool::initialize() {
  const StatusCode sc = GaudiTool::initialize();
  if ( sc.isFailure() ) return sc;

  if ( msgLevel( MSG::DEBUG ) )
    debug() << "KOS calib ctt: P0_Cal " << m_P0_Cal_kaon << ", P1_Cal " << m_P1_Cal_kaon << endmsg;

  m_util = tool<ITaggingUtils>( "TaggingUtils", this );

  m_descend = tool<IParticleDescendants>( "ParticleDescendants", this );

  m_classifierFactory = tool<ITaggingClassifierFactory>( m_classifierFactoryName, this );
  if ( m_classifierFactory == nullptr ) {
    error() << "Could not load the TaggingClassifierFactory " << m_classifierFactoryName << endmsg;
    return StatusCode::FAILURE;
  } else {
    m_classifier = m_classifierFactory->taggingClassifier();
  }

  return sc;
}
//================================================================================
StatusCode TaggerKaonOppositeTool::finalize() { return GaudiTool::finalize(); }

//==========================================================================
Tagger TaggerKaonOppositeTool::tag( const Particle* AXB0, const RecVertex* RecVert, const int nPV,
                                    Particle::ConstVector& vtags ) {
  Tagger tkaon;
  if ( !RecVert ) return tkaon;

  if ( msgLevel( MSG::VERBOSE ) ) verbose() << "--Kaon OppositeSide Tagger--" << endmsg;

  // fill auxdaugh for distphi
  double                distphi;
  Particle::ConstVector axdaugh = m_descend->descendants( AXB0 );
  axdaugh.push_back( AXB0 );
  // select kaon opposite tagger(s)
  // if more than one satisfies cuts, take the highest Pt one
  const Particle* ikaon  = 0;
  double          ptmaxk = -99.0, save_IPs = -10,
         save_IPPU = -10., // save_PIDk=-10., save_PIDpk = -10,
      save_PIDNNp = -10., save_PIDNNpi = -10., save_PIDNNk = -10.;
  for ( Particle::ConstVector::const_iterator ipart = vtags.begin(); ipart != vtags.end(); ++ipart ) {
    const Particle*      axp   = ( *ipart );
    const ProtoParticle* proto = ( *ipart )->proto();
    const Track*         track = ( *ipart )->proto()->track();

    if ( track->type() != Track::Types::Long ) continue;
    const double lcs = track->chi2PerDoF();
    if ( lcs > m_lcs_kaon ) continue;

    if ( axp->info( LHCb::Particle::additionalInfo::FlavourTaggingTaggerID, -1. ) > 0 )
      continue; // already tagger by muon   // new
    if ( proto->muonPID() )
      if ( proto->muonPID()->IsMuon() != 0 ) continue; // new

    const double tsa = track->likelihood();
    if ( tsa < m_ghost_cut ) continue;

    if ( track->ghostProbability() > m_ghostProb_kaon ) continue;

    const double pidk = proto->info( ProtoParticle::additionalInfo::CombDLLk, -1000.0 );
    if ( pidk < m_PIDk_cut ) continue;
    if ( pidk == 0 || pidk == -1000.0 ) continue;

    const double pidproton = proto->info( ProtoParticle::additionalInfo::CombDLLp, -1000.0 );
    if ( pidk - pidproton < m_PIDkp_cut ) continue;

    const double PIDNNm = proto->info( ProtoParticle::additionalInfo::ProbNNmu, -1000.0 );
    if ( PIDNNm > m_PIDNNm_cut_kaon ) continue;

    const double PIDNNe = proto->info( ProtoParticle::additionalInfo::ProbNNe, -1000.0 );
    if ( PIDNNe > m_PIDNNe_cut_kaon ) continue;

    const double PIDNNpi = proto->info( ProtoParticle::additionalInfo::ProbNNpi, -1000.0 );
    if ( PIDNNpi > m_PIDNNpi_cut_kaon ) continue;

    const double PIDNNp = proto->info( ProtoParticle::additionalInfo::ProbNNp, -1000.0 );
    if ( PIDNNp > m_PIDNNp_cut_kaon ) continue;

    const double PIDNNk = proto->info( ProtoParticle::additionalInfo::ProbNNk, -1000.0 );
    if ( PIDNNk < m_PIDNNk_cut_kaon ) continue;

    if ( PIDNNk - PIDNNp < m_PIDNNkp_cut_kaon ) continue;
    if ( PIDNNk - PIDNNpi < m_PIDNNkpi_cut_kaon ) continue;

    const double Pt = axp->pt();
    if ( Pt < m_Pt_cut_kaon ) continue;

    const double P = axp->p();
    if ( P < m_P_cut_kaon ) continue;

    double IP( 0. ), IPerr( 0. );
    m_util->calcIP( axp, RecVert, IP, IPerr ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    if ( !IPerr ) continue;
    const double IPsig = fabs( IP / IPerr );
    if ( fabs( IP ) > m_IP_cut_kaon ) continue;
    if ( IPsig < m_IPs_cut_kaon ) continue;

    const double ippu = axp->info( LHCb::Particle::additionalInfo::FlavourTaggingIPPUs, 10000. );
    if ( ippu < m_ipPU_cut_kaon ) continue;

    if ( msgLevel( MSG::VERBOSE ) )
      verbose() << " Kaon IPs=" << IPsig << " IP=" << fabs( IP ) << " IPPU=" << ippu << endmsg;

    // distphi
    if ( m_util->isInTree( axp, axdaugh, distphi ) ) continue; // exclude signal
    if ( distphi < m_distPhi_cut_kaon ) continue;

    Particle* c = const_cast<Particle*>( *ipart );
    c->addInfo( LHCb::Particle::additionalInfo::FlavourTaggingTaggerID,
                321 ); // store the information that the kaon tagger is found    // new

    if ( Pt > ptmaxk ) {
      ikaon     = axp;
      ptmaxk    = Pt;
      save_IPs  = IPsig;
      save_IPPU = ippu;
      // save_PIDpk = pidproton - pidk;
      // save_PIDk = pidk;
      save_PIDNNp  = PIDNNp;
      save_PIDNNpi = PIDNNpi;
      save_PIDNNk  = PIDNNk;
    }
  }
  if ( !ikaon ) return tkaon;

  // calculate omega
  double pn   = 1 - m_AverageOmega;
  double sign = 1.;
  double rnet = -999.;

  if ( m_CombinationTechnique == "NNet" ) {

    std::vector<double> inputVals = {static_cast<double>( m_util->countTracks( vtags ) ),
                                     log( ikaon->p() / GeV ),
                                     log( ikaon->pt() / GeV ),
                                     static_cast<double>( nPV ),
                                     log( AXB0->pt() / GeV ),
                                     log( std::abs( save_IPs ) ),
                                     log( ikaon->proto()->track()->chi2PerDoF() ),
                                     save_PIDNNk,
                                     save_PIDNNpi,
                                     save_PIDNNp,
                                     log( ikaon->proto()->track()->ghostProbability() ),
                                     log( save_IPPU )};

    rnet = m_classifier->getClassifierValue( inputVals );

    if ( rnet >= 0 && rnet <= 1 ) {
      pn = 1.0 - TaggingHelpers::funcNN( rnet, m_P0k, m_P1k, m_P2k, m_P3k );
    } else {
      if ( msgLevel( MSG::DEBUG ) ) debug() << "**********************BAD TRAINING Kaon" << rnet << endmsg;
      pn = -1.;
    }
    if ( msgLevel( MSG::VERBOSE ) ) verbose() << " Kaon pn=" << pn << endmsg;

    // Calibration (w=1-pn) w' = p0 + p1(w-eta)
    pn = 1 - m_P0_Cal_kaon - m_P1_Cal_kaon * ( ( 1 - pn ) - m_Eta_Cal_kaon );
    debug() << " Kaon pn=" << pn << " w=" << 1 - pn << endmsg;
    if ( pn < 0 || pn > 1 ) {
      if ( msgLevel( MSG::DEBUG ) ) debug() << " ===> Something wrong with KAON Training " << pn << endmsg;
      return tkaon;
    }

    if ( pn < m_ProbMin_kaon ) return tkaon;
    if ( pn < 0.5 ) {
      pn   = 1. - pn;
      sign = -1.;
    }

    if ( msgLevel( MSG::DEBUG ) ) {

      debug() << " TaggerKaon: " << sign * ikaon->charge() << " omega=" << 1 - pn;
      for ( unsigned int iloop = 0; iloop < inputVals.size(); iloop++ ) { debug() << inputVals[iloop] << " "; }
      debug() << endmsg;
    }
  }

  tkaon.setOmega( 1 - pn );
  tkaon.setDecision( sign * ikaon->charge() > 0 ? -1 : 1 );
  tkaon.setType( taggerType() );
  tkaon.addToTaggerParts( ikaon );
  tkaon.setCharge( ikaon->charge() );
  tkaon.setMvaValue( rnet );

  return tkaon;
}
