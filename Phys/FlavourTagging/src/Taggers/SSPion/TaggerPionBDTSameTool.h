/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: TaggerPionBDTSameTool.h,v 1.9 2013-11-29 svecchi Exp $
#ifndef PHYS_PHYS_FLAVOURTAGGING_TAGGERPIONSAMETOOL_H
#define PHYS_PHYS_FLAVOURTAGGING_TAGGERPIONSAMETOOL_H 1

#include <cmath>
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/AlgTool.h"
// from Event
#include "Event/FlavourTag.h"
#include "Event/ProtoParticle.h"

#include "Kernel/ITagger.h"
#include "Kernel/IVertexFit.h"
#include "Math/Boost.h"

// from local
#include "FlavourTagging/ITaggingUtils.h"
#include "Kernel/IParticleDescendants.h"

// forward declarations
class ITaggingClassifier;
class ITaggingClassifierFactory;

/** @class TaggerPionBDTSameTool TaggerPionBDTSameTool.h
 *
 *  Tool to tag the B flavour with a PionBDTSame Tagger
 *
 *  @author Stefania Vecchi
 * (tool developed by Antonio Falabella, Marta Calvi and
 *  Vava Gligorov )
 *  @date   29/11/2013
 */

class TaggerPionBDTSameTool : public GaudiTool, virtual public ITagger {

public:
  /// Standard constructor
  TaggerPionBDTSameTool( const std::string& type, const std::string& name, const IInterface* parent );
  virtual ~TaggerPionBDTSameTool(); ///< Destructor

  StatusCode               initialize() override; ///<  initialization
  StatusCode               finalize() override;   ///<  finalization
  LHCb::Tagger::TaggerType taggerType() const override { return LHCb::Tagger::TaggerType::SS_PionBDT; }

  //-------------------------------------------------------------
  using ITagger::tag;
  LHCb::Tagger tag( const LHCb::Particle*, const LHCb::RecVertex*, const int, LHCb::Particle::ConstVector& ) override;
  //-------------------------------------------------------------

private:
  std::unique_ptr<ITaggingClassifier> m_classifier        = nullptr;
  ITaggingClassifierFactory*          m_classifierFactory = nullptr;

  Gaudi::Property<std::string> m_classifierFactoryName{this, "ClassifierFactoryName", "SSPionClassifierFactory",
                                                       "Name of the factory that creates the classifier."};

  //  INNetTool* m_nnet;
  const IVertexFit* m_fitter = nullptr;
  ITaggingUtils*    m_util   = nullptr;

  // properties
  double m_Pt_cut_pionS;
  double m_Bp_Pt_cut_pionS;
  double m_Bp_vtxChi2_cut_pionS;
  // double m_P_cut_pionS;
  double m_IPs_cut_pionS;
  double m_eta_cut_pionS;
  double m_phi_cut_pionS;
  // double m_dR_cut_pionS;
  double m_dQcut_pionS;
  //  double m_BDT_cut_pionS;
  double m_AverageOmega;
  double m_PionProbMin;
  double m_ghostprob_cut;
  double m_PIDp_cut_pionS;
  double m_PIDk_cut_pionS;

  double m_P0_pol_pionS;
  double m_P1_pol_pionS;
  double m_P2_pol_pionS;
  double m_P3_pol_pionS;
};
//===============================================================//
#endif // PHYS_PHYS_FLAVOURTAGGING_TAGGERPIONSAMETOOL_H
