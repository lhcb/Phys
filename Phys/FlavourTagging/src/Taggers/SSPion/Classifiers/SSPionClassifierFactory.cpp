/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "SSPionClassifierFactory.h"

// ITaggingClassifier implementations
// #include "TMVA/SSPion_Data_Run1_All_Bd2Dpi_TMVA_BDT_v1r0.h"
#include "TMVA/SSPion_Data_Run1_All_Bd2Dpi_TMVA_BDT_v1r1.h"

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( SSPionClassifierFactory )

SSPionClassifierFactory::SSPionClassifierFactory( const std::string& type, const std::string& name,
                                                  const IInterface* parent )
    : GaudiTool( type, name, parent ) {
  declareInterface<ITaggingClassifierFactory>( this );
}

StatusCode SSPionClassifierFactory::initialize() {
  auto sc = GaudiTool::initialize();
  if ( sc.isFailure() ) return sc;

  // This MVA should not be used
  // addTMVAClassifier<SSPion_Data_Run1_All_Bd2Dpi_TMVA_BDT_v1r0>("SSPion_Data_Run1_All_Bd2Dpi_TMVA_BDT_v1r0");
  addTMVAClassifier<SSPion_Data_Run1_All_Bd2Dpi_TMVA_BDT_v1r1>( "SSPion_Data_Run1_All_Bd2Dpi_TMVA_BDT_v1r1" );

  return StatusCode::SUCCESS;
}

std::unique_ptr<ITaggingClassifier> SSPionClassifierFactory::taggingClassifier() {
  return taggingClassifier( m_classifierType, m_classifierName );
}

std::unique_ptr<ITaggingClassifier> SSPionClassifierFactory::taggingClassifier( const std::string& type,
                                                                                const std::string& name ) {
  if ( type == "TMVA" ) {
    return m_classifierMapTMVA[name]();
  } else if ( type == "XGBoost" && m_nameIsPath ) {
    auto classifier = std::make_unique<TaggingClassifierXGB>();
    classifier->setPath( m_classifierName );
    return classifier;
  } else {
    error() << "Classifier of type " << type << " unknown!" << endmsg;
    return nullptr;
  }
}
