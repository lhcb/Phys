/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef MCPIONSSWRAPPER_H
#define MCPIONSSWRAPPER_H 1

#include "src/TMVAWrapper.h"

namespace MyMCPionSSSpace {
  class Read_pionSMLPBNN_MC;
}

class MCPionSSWrapper : public TMVAWrapper {
public:
  MCPionSSWrapper( std::vector<std::string>& );
  ~MCPionSSWrapper();
  double GetMvaValue( std::vector<double> const& ) override;

private:
  MyMCPionSSSpace::Read_pionSMLPBNN_MC* reader;
};

#endif
