/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "MCPionSSWrapper.h"

// hack, otherwise: redefinitions...
//
namespace MyMCPionSSSpace {
#ifndef SKIP_TMVA
#  include "weights/pionS__pionSMLPBNN_MC.class.C"
#endif
} // namespace MyMCPionSSSpace

MCPionSSWrapper::MCPionSSWrapper( std::vector<std::string>& names ) {
#ifdef SKIP_TMVA
  int size = names.size();
  if ( size == 0 ) std::cout << "WARNING: NO VALUES PASSED" << std::endl;
#else
  reader = new MyMCPionSSSpace::Read_pionSMLPBNN_MC( names );
#endif
}

MCPionSSWrapper::~MCPionSSWrapper() {
#ifndef SKIP_TMVA
  delete reader;
#endif
}

double MCPionSSWrapper::GetMvaValue( std::vector<double> const& values ) {
#ifdef SKIP_TMVA
  int size = values.size();
  if ( size == 0 ) std::cout << "WARNING: NO VALUES PASSED" << std::endl;
  return 0.0;
#else
  return reader->GetMvaValue( values );
#endif
  return 0.0;
}
