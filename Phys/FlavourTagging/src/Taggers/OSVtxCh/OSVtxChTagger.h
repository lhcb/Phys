/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef PHYS_PHYS_FLAVOURTAGGING_OSVTXCHTAGGER_H
#define PHYS_PHYS_FLAVOURTAGGING_OSVTXCHTAGGER_H 1

// from STL
#include <string>

// from Gaudi
#include "GaudiAlg/GaudiTool.h"

// from Phys
#include "Kernel/ITagger.h"

// forward declarations
class ITaggingUtils;
class ISecondaryVertexTool;
class ITaggingClassifierFactory;
class ITaggingClassifier;

/**
 * @brief      Opposite Side tagger using the weighted charge of a secondary Vertex
 *
 * The Opposite Side Vertex Charge Tagger (OSVtxCh) inclusively reconstructs the
 * secondary vertex of the opposite side b hadron (using a
 * ISecondaryVertexTool). The weighted sum of charges of the particles
 * contributing to the vertex is then used for tagging the signal b candidate. A
 * negative charge of the OS vertex (from a b->X decay, e.g. a B- decay) implies
 * a positive tag, i.e. a bbar quark as quark content of the signal b candidate.
 *
 * @class      OSVtxChTagger OSVtcChTagger.cpp OSVtxChTagger.h
 *
 * @authors    Marco Musy, Marc Grabalosa, Julian Wishahi
 * @date        2005-2017
 */

class OSVtxChTagger : public GaudiTool, virtual public ITagger {
public:
  OSVtxChTagger( const std::string& type, const std::string& name, const IInterface* parent );

  StatusCode initialize() override;
  StatusCode finalize() override;

  LHCb::Tagger::TaggerType taggerType() const override { return LHCb::Tagger::TaggerType::VtxCharge; }

  using ITagger::emptyTag;

  LHCb::Tagger tag( const LHCb::Particle* sigPart, const LHCb::RecVertex* assocVtx, const int nPUVtxs,
                    LHCb::Particle::ConstVector& tagParts ) override;

  LHCb::Tagger tag( const LHCb::Particle* sigPart, const LHCb::RecVertex* assocVtx,
                    LHCb::RecVertex::ConstVector& puVtxs, LHCb::Particle::ConstVector& tagParts ) override;

  std::vector<std::string> featureNames() const override { return m_featureNames; }

  std::vector<double> featureValues() const override { return m_featureValues; }

  std::vector<std::string> featureNamesTagParts() const override { return {}; };

  std::vector<std::vector<double>> featureValuesTagParts() const override { return {{}}; };

private:
  // tools and corresponding properties
  Gaudi::Property<std::string> m_classifierFactoryName{this, "ClassifierFactoryName", "OSVtxChClassifierFactory",
                                                       "Name of the factory that creates the classifier."};
  Gaudi::Property<std::string> m_secVtxToolName{this, "SecondaryVertexToolName", "SVertexOneSeedTool",
                                                "Name of the secondary vertex tool."};

  ITaggingUtils*                      m_tagUtils          = nullptr;
  ISecondaryVertexTool*               m_secVtxTool        = nullptr;
  ITaggingClassifierFactory*          m_classifierFactory = nullptr;
  std::unique_ptr<ITaggingClassifier> m_classifier        = nullptr;

  // properties related to selection requirements
  Gaudi::Property<double> m_vtxChWeightPowerK{this, "Vtx_WeightPowerK", 0.55,
                                              "Exponent k to the transverse momentum used as a weight for the charge"};
  Gaudi::Property<double> m_secVtxChMin{this, "Vtx_MinCharge", 0.2, "Minimum weighted charge of the vertex"};
  Gaudi::Property<double> m_secVtxMMin{this, "Vtx_MinMass", 0.6, "Minimum invariant mass of the vertex in GeV/c^2"};
  Gaudi::Property<double> m_secVtxPMin{this, "Vtx_MinP", 8., "Minimum absolute 3-momentum of the vertex in GeV/c"};
  Gaudi::Property<double> m_secVtxPtMin{
      this, "Vtx_MinPt", 0., "Minimum average transverse momentum of particles contributing to vertex in GeV/c"};
  Gaudi::Property<double> m_secVtxPtSumMin{
      this, "Vtx_MinPtSum", 2.2, "Minimum sum of transverse momenta of particles contributing to the vertex in GeV/c"};
  Gaudi::Property<double> m_secVtxIPsigSumMin{this, "Vtx_MinIPSigSum", 0.,
                                              "Minimum sum of IP significances of particles contributing to the vertex "
                                              "w.r.t. the associated Vertex of the B candidate"};
  Gaudi::Property<double> m_secVtxDocaSumMax{
      this, "Vtx_MaxDOCASum", 0.5,
      "Maximum sum of DOCA of particles contributing to the vertex w.r.t. the seed vertex"};
  Gaudi::Property<std::pair<double, double>> m_mvaValNegRange{
      this, "MVAValNegRange", {0.0, 0.5}, "MVA output range representing a negative classification."};
  Gaudi::Property<std::pair<double, double>> m_mvaValPosRange{
      this, "MVAValPosRange", {0.5, 1.0}, "MVA output range representing a positive classification."};

  // members for internal use
  const std::vector<std::string> m_featureNames{"nTracks",  "nPVs",        "logBpt",    "nTrackInVtx",
                                                "logVtxPt", "logVtxIPsig", "absVtxCh",  "logVtxM",
                                                "logVtxP",  "VtxDPhi",     "logVtxTau", "VtxDoca"};

  std::vector<double> m_featureValues{std::vector<double>( m_featureNames.size(), 0. )};

  const std::vector<std::string>   m_featureNamesTagParts{};
  std::vector<std::vector<double>> m_featureValuesTagParts{{}};
};

#endif // PHYS_PHYS_FLAVOURTAGGING_OSVTXCHTAGGER_H
