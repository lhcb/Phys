/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef MCVERTEXOSWRAPPER_H
#define MCVERTEXOSWRAPPER_H 1

#include "src/TMVAWrapper.h"

namespace MyMCVertexOSSpace {
  class Read_vtxMLPBNN_MC;
}

class MCVertexOSWrapper : public TMVAWrapper {
public:
  MCVertexOSWrapper( std::vector<std::string>& );
  ~MCVertexOSWrapper();
  double GetMvaValue( std::vector<double> const& ) override;

private:
  MyMCVertexOSSpace::Read_vtxMLPBNN_MC* reader;
};

#endif
