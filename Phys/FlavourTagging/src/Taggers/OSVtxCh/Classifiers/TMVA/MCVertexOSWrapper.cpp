/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "MCVertexOSWrapper.h"

// hack, otherwise: redefinitions...
//
namespace MyMCVertexOSSpace {
#ifndef SKIP_TMVA
#  include "weights/vtx__vtxMLPBNN_MC.class.C"
#endif
} // namespace MyMCVertexOSSpace

MCVertexOSWrapper::MCVertexOSWrapper( std::vector<std::string>& names ) {
#ifdef SKIP_TMVA
  int size = names.size();
  if ( size == 0 ) std::cout << "WARNING: NO VALUES PASSED" << std::endl;
#else
  reader = new MyMCVertexOSSpace::Read_vtxMLPBNN_MC( names );
#endif
}

MCVertexOSWrapper::~MCVertexOSWrapper() {
#ifndef SKIP_TMVA
  delete reader;
#endif
}

double MCVertexOSWrapper::GetMvaValue( std::vector<double> const& values ) {
#ifdef SKIP_TMVA
  int size = values.size();
  if ( size == 0 ) std::cout << "WARNING: NO VALUES PASSED" << std::endl;
  return 0.0;
#else
  return reader->GetMvaValue( values );
#endif
  return 0.0;
}
