/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files
#include "TaggerVertexChargeTool.h"
#include "src/Classification/ITaggingClassifierFactory.h"
#include "src/Utils/TaggingHelpers.h"

//--------------------------------------------------------------------
// Implementation file for class : TaggerVertexChargeTool
//
// Author: Marco Musy & Marc Grabalosa
//--------------------------------------------------------------------

using namespace LHCb;
using namespace Gaudi::Units;

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( TaggerVertexChargeTool )

//====================================================================
TaggerVertexChargeTool::TaggerVertexChargeTool( const std::string& type, const std::string& name,
                                                const IInterface* parent )
    : GaudiTool( type, name, parent ) {

  declareInterface<ITagger>( this );

  declareProperty( "SecondaryVertexName", m_SecondaryVertexToolName = "SVertexOneSeedTool" );
  declareProperty( "Vtx_AverageOmega", m_AverageOmega = 0.41 );

  // NNet - no bias, Probability - old with bias:
  declareProperty( "CombTech", m_CombinationTechnique = "NNet" );

  declareProperty( "Vtx_PowerK", m_PowerK = 0.55 );
  declareProperty( "Vtx_MinimumVCharge", m_MinimumVCharge = 0.2 );
  declareProperty( "Vtx_Ptsum", m_Ptsum_vtx = 2.2 );
  declareProperty( "Vtx_Ptmean", m_Ptmean_vtx = 0. );
  declareProperty( "Vtx_IPSsum", m_IPSsum_vtx = 0. ); // no cut
  declareProperty( "Vtx_DocaMaxsum", m_DocaMaxsum_vtx = 0.5 );
  declareProperty( "Vtx_Psum", m_Psum_vtx = 8. );
  declareProperty( "Vtx_Msum", m_Msum_vtx = 0.6 );

  declareProperty( "Vtx_ProbMin", m_ProbMin_vtx = 0. );
  declareProperty( "Vtx_P0_Cal", m_P0_Cal_vtx = 0. );
  declareProperty( "Vtx_P1_Cal", m_P1_Cal_vtx = 1. );
  declareProperty( "Vtx_Eta_Cal", m_Eta_Cal_vtx = 0. );

  // For CombinationTechnique: "Probability"
  declareProperty( "P0", m_P0 = 5.255669e-01 );
  declareProperty( "P1", m_P1 = -3.251661e-01 );
  declareProperty( "Gt075", m_Gt075 = 0.35 );
  declareProperty( "TracksEq2", m_wSameSign2 = 0.4141 );
  declareProperty( "TracksGt2", m_wSameSignMoreThan2 = 0.3250 );
  declareProperty( "isMonteCarlo", m_isMonteCarlo = 0 );
  // vtx scaleX=-5.77134 scaleY=1.11591 offsetY=0.113682 pivotX=0.570684
  declareProperty( "P0_vtx_scale", m_P0vtx = -5.77134 );
  declareProperty( "P1_vtx_scale", m_P1vtx = 1.11591 );
  declareProperty( "P2_vtx_scale", m_P2vtx = 0.113682 );
  declareProperty( "P3_vtx_scale", m_P3vtx = 0.570684 );
}

TaggerVertexChargeTool::~TaggerVertexChargeTool() {}

//=====================================================================
StatusCode TaggerVertexChargeTool::initialize() {
  const StatusCode sc = GaudiTool::initialize();
  if ( sc.isFailure() ) return sc;

  if ( msgLevel( MSG::DEBUG ) )
    debug() << "Vtx calib ctt: P0_Cal " << m_P0_Cal_vtx << ", P1_Cal " << m_P1_Cal_vtx << endmsg;

  m_svtool = tool<ISecondaryVertexTool>( "SVertexOneSeedTool", m_SecondaryVertexToolName, this );

  m_util = tool<ITaggingUtils>( "TaggingUtils", this );

  m_classifierFactory = tool<ITaggingClassifierFactory>( m_classifierFactoryName, this );
  if ( m_classifierFactory == nullptr ) {
    error() << "Could not load the TaggingClassifierFactory " << m_classifierFactoryName << endmsg;
    return StatusCode::FAILURE;
  } else {
    m_classifier = m_classifierFactory->taggingClassifier();
  }

  return sc;
}
//================================================================================
StatusCode TaggerVertexChargeTool::finalize() { return GaudiTool::finalize(); }

//=====================================================================
Tagger TaggerVertexChargeTool::tag( const Particle* AXB0, const RecVertex* RecVert, const int nPV,
                                    Particle::ConstVector& vtags ) {
  Tagger tVch;
  if ( !RecVert ) return tVch;
  if ( vtags.empty() ) return tVch;

  if ( msgLevel( MSG::VERBOSE ) ) verbose() << "--Vertex Tagger--" << endmsg;

  ///--- Inclusive Secondary Vertex ---
  // look for a secondary Vtx due to opposite B
  std::vector<Vertex> vvec( 0 );
  if ( m_svtool ) vvec = m_svtool->buildVertex( *RecVert, vtags );
  if ( vvec.empty() ) return tVch;
  const Particle::ConstVector& Pfit = vvec.at( 0 ).outgoingParticlesVector();

  // if Vertex does not contain any daughters, exit
  if ( Pfit.size() < 1 ) return tVch;
  if ( msgLevel( MSG::DEBUG ) )
    debug() << "--- SVTOOL buildVertex returns: " << vvec.size() << ", with " << Pfit.size() << "tracks" << endmsg;
  double maxprobf = vvec.at( 0 ).info( LHCb::Vertex::CreationMethod::LastGlobal + 1, 0.5 );
  if ( msgLevel( MSG::DEBUG ) ) debug() << " -- likelihood seed " << maxprobf << endmsg;
  Vertex seedvtx;

  const Gaudi::XYZPoint& BoppDir = vvec.at( 0 ).position(); // SV-PV !!!
  if ( msgLevel( MSG::DEBUG ) ) debug() << "BoppDir: " << BoppDir << endmsg;

  // calculate vertex charge and other variables for NN
  double               Vch = 0, norm = 0;
  double               Vptmean = 0, Vipsmean = 0, Vdocamax = 0;
  int                  vflagged = 0;
  Gaudi::LorentzVector SVmomentum;
  for ( Particle::ConstVector::const_iterator ip = Pfit.begin(); ip != Pfit.end(); ++ip ) {
    if ( msgLevel( MSG::DEBUG ) ) debug() << "SVTOOL  VtxCh, adding track pt= " << ( *ip )->pt() << endmsg;
    SVmomentum += ( *ip )->momentum();
    double a = std::pow( ( *ip )->pt() / GeV, m_PowerK );
    Vch += ( *ip )->charge() * a;
    norm += a;
    ++vflagged;
    Vptmean += ( *ip )->pt() / GeV;
    double minip( 0 ), miniperr( 0 );
    m_util->calcIP( *ip, RecVert, minip, miniperr ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    minip = fabs( minip );
    Vipsmean += minip / miniperr;
    double docaSV( 0 ), docaErrSV( 0 );
    m_util->calcDOCAmin( *ip, Pfit.at( 0 ), Pfit.at( 1 ), docaSV, docaErrSV )
        .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ ); // DOCA wrt the seeds
    Vdocamax += docaSV;
    if ( msgLevel( MSG::DEBUG ) ) debug() << "docaSV:" << docaSV << endmsg;
  }

  if ( norm ) {
    Vch /= norm;
    if ( fabs( Vch ) < m_MinimumVCharge ) Vch = 0;
    Vptmean /= vflagged;
    Vipsmean /= vflagged;
    Vdocamax /= vflagged;
  }
  if ( msgLevel( MSG::DEBUG ) ) debug() << "Vch: " << Vch << endmsg;
  if ( Vch == 0 ) return tVch;

  // Variables of the SV (Seed Vertex)
  double BDphiDir = TaggingHelpers::dphi( SVmomentum.phi(), BoppDir.Phi() ); // [-pi, pi]
  double SVP      = SVmomentum.P() / GeV;
  double SVM      = SVmomentum.M() / GeV;
  double SVGP     = SVP / ( 0.16 * SVM + 0.12 );
  double SVtau    = std::sqrt( BoppDir.Mag2() ) * 5.28 / SVGP / 0.299792458;
  if ( msgLevel( MSG::DEBUG ) )
    debug() << "BoppDir.Mag2: " << std::sqrt( BoppDir.Mag2() ) << ", SVGP: " << SVGP << ", SVtau: " << SVtau << endmsg;
  if ( Vptmean < m_Ptmean_vtx ) return tVch;
  if ( Vptmean * vflagged < m_Ptsum_vtx ) return tVch;
  if ( Vipsmean * vflagged < m_IPSsum_vtx ) return tVch;
  if ( Vdocamax * vflagged > m_DocaMaxsum_vtx ) return tVch;
  if ( SVP < m_Psum_vtx ) return tVch;
  if ( SVM < m_Msum_vtx ) return tVch;

  // calculate omega
  if ( msgLevel( MSG::DEBUG ) ) debug() << "calculate omega with " << m_CombinationTechnique << endmsg;
  double omega = m_AverageOmega;
  double pn    = 1 - omega;
  double sign  = 1.;

  if ( m_CombinationTechnique == "Probability" ) {
    if ( fabs( Vch ) < 0.75 ) omega = m_P0 + m_P1 * fabs( Vch );
    if ( fabs( Vch ) > 0.75 ) omega = m_Gt075;
    if ( fabs( Vch ) > 0.99 ) { // tracks have all the same charge
      if ( Pfit.size() == 2 ) omega = m_wSameSign2;
      if ( Pfit.size() > 2 ) omega = m_wSameSignMoreThan2;
    }
    pn = 1 - omega;
  }

  if ( m_CombinationTechnique == "NNet" ) {
    std::vector<double> inputVals;
    inputVals.push_back( (double)m_util->countTracks( vtags ) );
    inputVals.push_back( (double)nPV );
    inputVals.push_back( (double)log( AXB0->pt() / GeV ) );
    inputVals.push_back( (double)vflagged );
    inputVals.push_back( (double)log( Vptmean ) );
    inputVals.push_back( (double)log( Vipsmean ) );
    inputVals.push_back( (double)fabs( Vch ) );
    inputVals.push_back( (double)log( SVM ) );
    inputVals.push_back( (double)log( SVP ) );
    inputVals.push_back( (double)BDphiDir );
    inputVals.push_back( (double)log( SVtau ) );
    inputVals.push_back( (double)Vdocamax );

    double rnet = m_classifier->getClassifierValue( inputVals );

    if ( rnet >= 0 && rnet <= 1 ) {
      pn = 1.0 - TaggingHelpers::funcNN( rnet, m_P0vtx, m_P1vtx, m_P2vtx, m_P3vtx );
    } else {
      debug() << "**********************BAD TRAINING vtx" << rnet << endmsg;
      pn = -1.;
    }

    omega = 1. - pn;

    // Calibration (w=1-pn) w' = p0 + p1(w-eta)
    omega = m_P0_Cal_vtx + m_P1_Cal_vtx * ( omega - m_Eta_Cal_vtx );
    if ( msgLevel( MSG::DEBUG ) ) debug() << " Vtx pn=" << pn << " w=" << omega << endmsg;

    if ( omega < 0 || omega > 1 ) {
      if ( msgLevel( MSG::DEBUG ) ) debug() << "===> Something wrong with VTX Training " << omega << endmsg;
      return tVch;
    }

    if ( 1 - omega < m_ProbMin_vtx ) return tVch;

    if ( omega > 0.5 ) {
      omega = 1 - omega;
      sign  = -1;
    }
    pn = 1. - omega;

    if ( msgLevel( MSG::DEBUG ) ) {
      debug() << " TaggerVertex: " << sign * Vch << " omega=" << 1 - pn;
      for ( unsigned int iloop = 0; iloop < inputVals.size(); iloop++ ) { debug() << inputVals[iloop] << " "; }
      debug() << endmsg;
    }
  }

  tVch.setDecision( sign * Vch > 0 ? -1 : 1 );
  tVch.setOmega( omega );
  tVch.setType( taggerType() );
  for ( Particle::ConstVector::const_iterator ip = Pfit.begin(); ip != Pfit.end(); ++ip ) {
    tVch.addToTaggerParts( *ip );
  }

  return tVch;
}
