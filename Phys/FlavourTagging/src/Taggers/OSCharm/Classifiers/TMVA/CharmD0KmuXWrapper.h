/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef CHARMD0KMUXWRAPPER_H
#define CHARMD0KMUXWRAPPER_H 1

// Include files

// local
#include "src/TMVAWrapper.h"

namespace MyD0KmuXSpace {
  class ReadBDT;
  class PurityTable;
} // namespace MyD0KmuXSpace

/** @class CharmD0KmuXWrapper CharmD0KmuXWrapper.h
 *
 *  Wrapper for D0 -> KmuX classifier
 *
 *  @author Jack Timpson Wimberley
 *  @date   2014-02-18
 */
class CharmD0KmuXWrapper : public TMVAWrapper {
public:
  CharmD0KmuXWrapper( std::vector<std::string>& );
  ~CharmD0KmuXWrapper();
  double GetMvaValue( std::vector<double> const& ) override;

protected:
private:
  MyD0KmuXSpace::ReadBDT*     mcreader;
  MyD0KmuXSpace::PurityTable* purtable;
};
#endif // CHARMD0KMUXWRAPPER_H
