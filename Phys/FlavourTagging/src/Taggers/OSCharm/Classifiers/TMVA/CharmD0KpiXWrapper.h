/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef CHARMD0KPIXWRAPPER_H
#define CHARMD0KPIXWRAPPER_H 1

// Include files

// local
#include "src/TMVAWrapper.h"

namespace MyD0KpiXSpace {
  class ReadBDT;
  class PurityTable;
} // namespace MyD0KpiXSpace

/** @class CharmD0KpiXWrapper CharmD0KpiXWrapper.h
 *
 *  Wrapper for D0 -> KpiX classifier
 *
 *  @author Jack Timpson Wimberley
 *  @date   2014-02-18
 */
class CharmD0KpiXWrapper : public TMVAWrapper {
public:
  CharmD0KpiXWrapper( std::vector<std::string>& );
  ~CharmD0KpiXWrapper();
  double GetMvaValue( std::vector<double> const& ) override;

protected:
private:
  MyD0KpiXSpace::ReadBDT*     mcreader;
  MyD0KpiXSpace::PurityTable* purtable;
};
#endif // CHARMD0KPIXWRAPPER_H
