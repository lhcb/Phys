/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef MVA_CHARMTAGGER_RECO14_HISTOGRAM_H
#define MVA_CHARMTAGGER_RECO14_HISTOGRAM_H 1

class Histogram {
public:
  Histogram( int, double, double );
  ~Histogram();
  void   SetBinContent( int, double );
  int    FindBin( double );
  double GetBinContent( int );
  double GetPurityAtBDT( double );
  int    NumBins();

private:
  int     numBins;
  double  min;
  double  max;
  double* binLefts;
  double* vals;
};

#endif // MVA_CHARMTAGGER_RECO14_HISTOGRAM_H
