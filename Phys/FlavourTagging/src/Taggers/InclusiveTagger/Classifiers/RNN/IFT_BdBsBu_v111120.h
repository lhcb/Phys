/*****************************************************************************\
* (c) Copyright 2020-2021 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

/////////////////////////////////////////////////
//      AUTO-GENERATED CODE, DO NOT EDIT       //
// gitlab: gitlab.cern.ch/vjevtic/rnngenerator //
// simplenn: gitlab.cern.ch/mschille/simplenn  //
/////////////////////////////////////////////////

/* ================== File Information ==================
 | Timestamp            : 2020-11-12 14:56:46
 | RNNGenerator version : master @ f62e5af2c499c745dd9df017ec9140230af6baf9
 | simpleNN version     : 9820d364d5789ff5cfdc18e14bb1fcd97a4a6d24
 | RNNGenerator command : "python rnngenerator/generator.py -f IFT_Bd_11112020.json -filename IFT_BdBsBu_111120
 -classifiername IFT_BdBsBu_111120 -implpath src/Classification/simpleNN/include -inheritance public
 TaggingClassifierSimpleNN -jsonreaderpath .. -includeguard PHYS_FLAVOURTAGGING_IFT_BDBSBU_V111120"
 * ====================================================== */

#ifndef PHYS_FLAVOURTAGGING_IFT_BDBSBU_V111120_H
#define PHYS_FLAVOURTAGGING_IFT_BDBSBU_V111120_H

#include <algorithm>
#include <cmath>
#include <iostream>
#include <memory>
#include <numeric>
#include <string>
#include <vector>

#include "../JSONReader.h"
#include "simplenn/Dense.h"
#include "simplenn/Embedding.h"
#include "simplenn/GRU.h"
#include "simplenn/scattergather.h"
#include "simplenn/span.h"

namespace RNNClassifier {

  template <typename float_type = float>
  class IFT_BdBsBu_v111120 : public TaggingClassifierSimpleNN {
  public:
    IFT_BdBsBu_v111120( const std::string& archfile );

    template <typename input_type>
    float_type predict( const std::vector<std::vector<input_type>>& input_0 ) const;
    float_type predict( const std::vector<std::array<float_type, 18>>& input_0 ) const;

  private:
    IFT_BdBsBu_v111120( const JSONReader& );

    simpleNN::Dense<std::array<simpleNN::procs::ReLU, 24>, std::array<simpleNN::procs::linear<float_type>, 18>,
                    float_type>
                                                                                                              td_dense0;
    simpleNN::Dense<std::array<simpleNN::procs::ReLU, 24>, std::array<simpleNN::procs::id, 24>, float_type>   td_dense1;
    simpleNN::Dense<std::array<simpleNN::procs::ReLU, 24>, std::array<simpleNN::procs::id, 24>, float_type>   td_dense2;
    simpleNN::GRU<48, 24, simpleNN::procs::ReLU, simpleNN::procs::hard_sigmoid, float_type>                   noseq_gru;
    simpleNN::Dense<std::array<simpleNN::procs::sigmoid, 1>, std::array<simpleNN::procs::id, 48>, float_type> outputTag;
  };

  template <typename float_type>
  IFT_BdBsBu_v111120<float_type>::IFT_BdBsBu_v111120( const JSONReader& json_reader )
      : td_dense0(
            std::array<simpleNN::procs::ReLU, 24>(),
            {simpleNN::procs::linear<float_type>(
                 1.0 / json_reader.load_feature_scaling( 2, "B_IFT_OSMuonDev_TagPartsFeature_Tr_T_PROBNNe", "scale" ),
                 -json_reader.load_feature_scaling( 2, "B_IFT_OSMuonDev_TagPartsFeature_Tr_T_PROBNNe", "offset" ) /
                     json_reader.load_feature_scaling( 2, "B_IFT_OSMuonDev_TagPartsFeature_Tr_T_PROBNNe", "scale" ) ),
             simpleNN::procs::linear<float_type>(
                 1.0 /
                     json_reader.load_feature_scaling( 2, "B_IFT_OSMuonDev_TagPartsFeature_Tr_T_PROBNNghost", "scale" ),
                 -json_reader.load_feature_scaling( 2, "B_IFT_OSMuonDev_TagPartsFeature_Tr_T_PROBNNghost", "offset" ) /
                     json_reader.load_feature_scaling( 2, "B_IFT_OSMuonDev_TagPartsFeature_Tr_T_PROBNNghost",
                                                       "scale" ) ),
             simpleNN::procs::linear<float_type>(
                 1.0 / json_reader.load_feature_scaling( 2, "B_IFT_OSMuonDev_TagPartsFeature_Tr_T_PROBNNk", "scale" ),
                 -json_reader.load_feature_scaling( 2, "B_IFT_OSMuonDev_TagPartsFeature_Tr_T_PROBNNk", "offset" ) /
                     json_reader.load_feature_scaling( 2, "B_IFT_OSMuonDev_TagPartsFeature_Tr_T_PROBNNk", "scale" ) ),
             simpleNN::procs::linear<float_type>(
                 1.0 / json_reader.load_feature_scaling( 2, "B_IFT_OSMuonDev_TagPartsFeature_Tr_T_PROBNNmu", "scale" ),
                 -json_reader.load_feature_scaling( 2, "B_IFT_OSMuonDev_TagPartsFeature_Tr_T_PROBNNmu", "offset" ) /
                     json_reader.load_feature_scaling( 2, "B_IFT_OSMuonDev_TagPartsFeature_Tr_T_PROBNNmu", "scale" ) ),
             simpleNN::procs::linear<float_type>(
                 1.0 / json_reader.load_feature_scaling( 2, "B_IFT_OSMuonDev_TagPartsFeature_Tr_T_PROBNNp", "scale" ),
                 -json_reader.load_feature_scaling( 2, "B_IFT_OSMuonDev_TagPartsFeature_Tr_T_PROBNNp", "offset" ) /
                     json_reader.load_feature_scaling( 2, "B_IFT_OSMuonDev_TagPartsFeature_Tr_T_PROBNNp", "scale" ) ),
             simpleNN::procs::linear<float_type>(
                 1.0 / json_reader.load_feature_scaling( 2, "B_IFT_OSMuonDev_TagPartsFeature_Tr_T_PROBNNpi", "scale" ),
                 -json_reader.load_feature_scaling( 2, "B_IFT_OSMuonDev_TagPartsFeature_Tr_T_PROBNNpi", "offset" ) /
                     json_reader.load_feature_scaling( 2, "B_IFT_OSMuonDev_TagPartsFeature_Tr_T_PROBNNpi", "scale" ) ),
             simpleNN::procs::linear<float_type>(
                 1.0 / json_reader.load_feature_scaling( 2, "B_IFT_OSMuonDev_TagPartsFeature_Tr_T_P", "scale" ),
                 -json_reader.load_feature_scaling( 2, "B_IFT_OSMuonDev_TagPartsFeature_Tr_T_P", "offset" ) /
                     json_reader.load_feature_scaling( 2, "B_IFT_OSMuonDev_TagPartsFeature_Tr_T_P", "scale" ) ),
             simpleNN::procs::linear<float_type>(
                 1.0 / json_reader.load_feature_scaling( 2, "B_IFT_OSMuonDev_TagPartsFeature_Tr_T_PT", "scale" ),
                 -json_reader.load_feature_scaling( 2, "B_IFT_OSMuonDev_TagPartsFeature_Tr_T_PT", "offset" ) /
                     json_reader.load_feature_scaling( 2, "B_IFT_OSMuonDev_TagPartsFeature_Tr_T_PT", "scale" ) ),
             simpleNN::procs::linear<float_type>(
                 1.0 / json_reader.load_feature_scaling( 2, "B_IFT_OSMuonDev_TagPartsFeature_Tr_T_Charge", "scale" ),
                 -json_reader.load_feature_scaling( 2, "B_IFT_OSMuonDev_TagPartsFeature_Tr_T_Charge", "offset" ) /
                     json_reader.load_feature_scaling( 2, "B_IFT_OSMuonDev_TagPartsFeature_Tr_T_Charge", "scale" ) ),
             simpleNN::procs::linear<float_type>(
                 1.0 / json_reader.load_feature_scaling( 2, "B_IFT_OSMuonDev_TagPartsFeature_Tr_T_BPVIP", "scale" ),
                 -json_reader.load_feature_scaling( 2, "B_IFT_OSMuonDev_TagPartsFeature_Tr_T_BPVIP", "offset" ) /
                     json_reader.load_feature_scaling( 2, "B_IFT_OSMuonDev_TagPartsFeature_Tr_T_BPVIP", "scale" ) ),
             simpleNN::procs::linear<float_type>(
                 1.0 / json_reader.load_feature_scaling( 2, "B_IFT_OSMuonDev_TagPartsFeature_Tr_T_BPVIPCHI2", "scale" ),
                 -json_reader.load_feature_scaling( 2, "B_IFT_OSMuonDev_TagPartsFeature_Tr_T_BPVIPCHI2", "offset" ) /
                     json_reader.load_feature_scaling( 2, "B_IFT_OSMuonDev_TagPartsFeature_Tr_T_BPVIPCHI2", "scale" ) ),
             simpleNN::procs::linear<float_type>(
                 1.0 /
                     json_reader.load_feature_scaling( 2, "B_IFT_OSMuonDev_TagPartsFeature_Tr_T_SumBDT_ult", "scale" ),
                 -json_reader.load_feature_scaling( 2, "B_IFT_OSMuonDev_TagPartsFeature_Tr_T_SumBDT_ult", "offset" ) /
                     json_reader.load_feature_scaling( 2, "B_IFT_OSMuonDev_TagPartsFeature_Tr_T_SumBDT_ult",
                                                       "scale" ) ),
             simpleNN::procs::linear<float_type>(
                 1.0 /
                     json_reader.load_feature_scaling( 2, "B_IFT_OSMuonDev_TagPartsFeature_Tr_T_VeloCharge", "scale" ),
                 -json_reader.load_feature_scaling( 2, "B_IFT_OSMuonDev_TagPartsFeature_Tr_T_VeloCharge", "offset" ) /
                     json_reader.load_feature_scaling( 2, "B_IFT_OSMuonDev_TagPartsFeature_Tr_T_VeloCharge",
                                                       "scale" ) ),
             simpleNN::procs::linear<float_type>(
                 1.0 /
                     json_reader.load_feature_scaling( 2, "B_IFT_OSMuonDev_TagPartsFeature_Tr_T_IP_trMother", "scale" ),
                 -json_reader.load_feature_scaling( 2, "B_IFT_OSMuonDev_TagPartsFeature_Tr_T_IP_trMother", "offset" ) /
                     json_reader.load_feature_scaling( 2, "B_IFT_OSMuonDev_TagPartsFeature_Tr_T_IP_trMother",
                                                       "scale" ) ),
             simpleNN::procs::linear<float_type>(
                 1.0 / json_reader.load_feature_scaling( 2, "B_IFT_OSMuonDev_TagPartsFeature_diff_z", "scale" ),
                 -json_reader.load_feature_scaling( 2, "B_IFT_OSMuonDev_TagPartsFeature_diff_z", "offset" ) /
                     json_reader.load_feature_scaling( 2, "B_IFT_OSMuonDev_TagPartsFeature_diff_z", "scale" ) ),
             simpleNN::procs::linear<float_type>(
                 1.0 / json_reader.load_feature_scaling( 2, "B_IFT_OSMuonDev_TagPartsFeature_P_proj", "scale" ),
                 -json_reader.load_feature_scaling( 2, "B_IFT_OSMuonDev_TagPartsFeature_P_proj", "offset" ) /
                     json_reader.load_feature_scaling( 2, "B_IFT_OSMuonDev_TagPartsFeature_P_proj", "scale" ) ),
             simpleNN::procs::linear<float_type>(
                 1.0 / json_reader.load_feature_scaling( 2, "B_IFT_OSMuonDev_TagPartsFeature_cos_diff_phi", "scale" ),
                 -json_reader.load_feature_scaling( 2, "B_IFT_OSMuonDev_TagPartsFeature_cos_diff_phi", "offset" ) /
                     json_reader.load_feature_scaling( 2, "B_IFT_OSMuonDev_TagPartsFeature_cos_diff_phi", "scale" ) ),
             simpleNN::procs::linear<float_type>(
                 1.0 / json_reader.load_feature_scaling( 2, "B_IFT_OSMuonDev_TagPartsFeature_diff_eta", "scale" ),
                 -json_reader.load_feature_scaling( 2, "B_IFT_OSMuonDev_TagPartsFeature_diff_eta", "offset" ) /
                     json_reader.load_feature_scaling( 2, "B_IFT_OSMuonDev_TagPartsFeature_diff_eta", "scale" ) )},
            json_reader.load_weights<float_type>( 2, "bias_tensor" ),
            json_reader.load_weights<float_type>( 2, "kernel_tensor" ) )
      , td_dense1( std::array<simpleNN::procs::ReLU, 24>(), std::array<simpleNN::procs::id, 24>(),
                   json_reader.load_weights<float_type>( 3, "bias_tensor" ),
                   json_reader.load_weights<float_type>( 3, "kernel_tensor" ) )
      , td_dense2( std::array<simpleNN::procs::ReLU, 24>(), std::array<simpleNN::procs::id, 24>(),
                   json_reader.load_weights<float_type>( 4, "bias_tensor" ),
                   json_reader.load_weights<float_type>( 4, "kernel_tensor" ) )
      , noseq_gru( json_reader.load_weights<float_type>( 5, "bz" ), json_reader.load_weights<float_type>( 5, "Uz" ),
                   json_reader.load_weights<float_type>( 5, "Wz" ), json_reader.load_weights<float_type>( 5, "br" ),
                   json_reader.load_weights<float_type>( 5, "Ur" ), json_reader.load_weights<float_type>( 5, "Wr" ),
                   json_reader.load_weights<float_type>( 5, "bt" ), json_reader.load_weights<float_type>( 5, "Ut" ),
                   json_reader.load_weights<float_type>( 5, "Wt" ) )
      , outputTag( std::array<simpleNN::procs::sigmoid, 1>(), std::array<simpleNN::procs::id, 48>(),
                   json_reader.load_weights<float_type>( 7, "bias_tensor" ),
                   json_reader.load_weights<float_type>( 7, "kernel_tensor" ) ) {}

  template <typename float_type>
  IFT_BdBsBu_v111120<float_type>::IFT_BdBsBu_v111120( const std::string& archfile )
      : IFT_BdBsBu_v111120( JSONReader( archfile ) ) {}

  template <typename float_type>
  template <typename input_type>
  float_type IFT_BdBsBu_v111120<float_type>::predict( const std::vector<std::vector<input_type>>& input_0 ) const {
    std::vector<std::array<float_type, 18>> converted_0( input_0.size() );
    for ( std::size_t i = 0; i < input_0.size(); ++i ) {
      std::copy( input_0[i].begin(), input_0[i].end(), converted_0[i].begin() );
    }
    return predict( converted_0 );
  }

  template <typename float_type>
  float_type IFT_BdBsBu_v111120<float_type>::predict( const std::vector<std::array<float_type, 18>>& input_0 ) const {
    using namespace simpleNN;

    auto td_dense0_ = td_dense0.evaluator();
    auto td_dense1_ = td_dense1.evaluator();
    auto td_dense2_ = td_dense2.evaluator();
    auto noseq_gru_ = noseq_gru.evaluator();
    auto outputTag_ = outputTag.evaluator();

    for ( std::size_t idx = 0; idx < input_0.size(); ++idx ) {
      input_0[idx] >> td_dense0_ >> td_dense1_ >> td_dense2_ >> noseq_gru_;
    }
    noseq_gru_ >> outputTag_;
    return outputTag_[0];
  }

} // namespace RNNClassifier

#endif // PHYS_FLAVOURTAGGING_IFT_BDBSBU_V111120_H
