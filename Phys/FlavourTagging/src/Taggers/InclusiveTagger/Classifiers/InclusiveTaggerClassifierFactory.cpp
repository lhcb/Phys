/*****************************************************************************\
* (c) Copyright 2020-2021 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "InclusiveTaggerClassifierFactory.h"

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( InclusiveTaggerClassifierFactory )

InclusiveTaggerClassifierFactory::InclusiveTaggerClassifierFactory( const std::string& type, const std::string& name,
                                                                    const IInterface* parent )
    : GaudiTool( type, name, parent ) {
  declareInterface<ITaggingClassifierFactory>( this );
}

StatusCode InclusiveTaggerClassifierFactory::initialize() {
  auto sc = GaudiTool::initialize();
  if ( sc.isFailure() ) return sc; // set m_checked to true

  return StatusCode::SUCCESS;
}

std::unique_ptr<ITaggingClassifier>
InclusiveTaggerClassifierFactory::taggingClassifier( const std::string& version,
                                                     const std::string& weightfile_override ) {
  info() << "Instantiating inclusive tagger model version \"" << version << '\"' << endmsg;
  std::string weight_file = IFTVersionMap[version].second;
  if ( weightfile_override != "" ) {
    // Just in case the user wants to test an unofficial weight file
    // e.g. a set of weights which is not in an official ParamFiles release.
    // This is achieved by setting m_weightFileOverride in InclusiveTagger.h
    // to a certain file. The corresponding architecture needs to be supported.
    warning() << "InclusiveTaggerClassifierFactory: Using weight file override instead of official weights!" << endmsg;
    weight_file = weightfile_override;
  }

  switch ( IFTVersionMap[version].first ) {
  case IFTArchitecture::IFT_BdBsBu_v111120:
    return std::make_unique<RNNClassifier::IFT_BdBsBu_v111120<double>>( weight_file );
  case IFTArchitecture::IFT_BdBsBu_v031121:
    return std::make_unique<DeepSetClassifier::DeepSet_BdBsBu_v031121<double>>( weight_file );
  default:
    fatal() << "Factory cannot instantiate IFT architecture version \"" << version << "\" (missing case) !" << endmsg;
    return nullptr;
  }
}

std::unique_ptr<ITaggingClassifier> InclusiveTaggerClassifierFactory::taggingClassifier() {
  fatal() << "No valid inclusive tagger version provided!\n" << endmsg;
  return taggingClassifier( "InvalidUnavailableVersionName", "" );
}
