/*****************************************************************************\
* (c) Copyright 2020-2021 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#ifndef INCLUSIVETAGGER_LIBRARY_H
#define INCLUSIVETAGGER_LIBRARY_H

#include <cstdlib>
#include <map>
#include <memory>
#include <string>
#include <variant>

#include "src/Classification/TaggingClassifierSimpleNN.h"

// Include all TaggingClassifierSimpleNN derived types
#include "src/Taggers/InclusiveTagger/Classifiers/DeepSet/DeepSet_BdBsBu_v031121.h"
#include "src/Taggers/InclusiveTagger/Classifiers/RNN/IFT_BdBsBu_v111120.h"

// Every new IFT architecture version has to be added under a new
// 1. IFTArchitecture enum, 2. in the version map, 3. As another RNNClassifierType variant
// While preserving the ordering in the enum vs the variant!

enum IFTArchitecture {
  // Enumerates model architectures (not IFT versions) that are in use
  IFT_BdBsBu_v111120 = 1,
  IFT_BdBsBu_v031121 = 2,
};

using RNNClassifierType = std::variant<std::monostate, std::unique_ptr<RNNClassifier::IFT_BdBsBu_v111120<double>>,
                                       std::unique_ptr<DeepSetClassifier::DeepSet_BdBsBu_v031121<double>>>;

static const std::string weight_path = std::string( std::getenv( "PARAMFILESROOT" ) ) + "/data/InclusiveTagger/";

static std::map<std::string, std::pair<IFTArchitecture, std::string>> IFTVersionMap{
    // Enumerates IFT versions that are in use
    // { OPTION_FILE_NAME { ARCHITECTURE, WEIGHTFILE } }
    // RNN
    {"IFT_Bd_v111120", {IFTArchitecture::IFT_BdBsBu_v111120, weight_path + "IFT_Bd_111120.json"}},
    {"IFT_Bs_v111120", {IFTArchitecture::IFT_BdBsBu_v111120, weight_path + "IFT_Bs_111120.json"}},
    {"IFT_Bu_v111120", {IFTArchitecture::IFT_BdBsBu_v111120, weight_path + "IFT_Bu_111120.json"}},
    {"IFT_Bd_v140521", {IFTArchitecture::IFT_BdBsBu_v111120, weight_path + "IFT_Bd_140521.json"}},

    // Deep Sets
    {"IFT_Bd_v031121", {IFTArchitecture::IFT_BdBsBu_v031121, weight_path + "IFT_Bd_v031121.json"}},
    {"IFT_Bs_v031121", {IFTArchitecture::IFT_BdBsBu_v031121, weight_path + "IFT_Bs_v031121.json"}},
    {"IFT_Bu_v031121", {IFTArchitecture::IFT_BdBsBu_v031121, weight_path + "IFT_Bu_v031121.json"}},
};

template <typename TaggerType>
static std::unique_ptr<TaggerType> getIFTTaggerType( ITaggingClassifier* taggerPointer ) {
  return std::unique_ptr<TaggerType>( dynamic_cast<TaggerType*>( taggerPointer ) );
}

#endif // INCLUSIVETAGGER_LIBRARY_H
