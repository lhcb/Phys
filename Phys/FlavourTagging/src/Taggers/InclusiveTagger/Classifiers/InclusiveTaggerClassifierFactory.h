/*****************************************************************************\
* (c) Copyright 2020-2021 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef PHYS_PHYS_FLAVOURTAGGING_INCLUSIVETAGGERCLASSIFIERFACTORY_H
#define PHYS_PHYS_FLAVOURTAGGING_INCLUSIVETAGGERCLASSIFIERFACTORY_H 1

// from Gaudi
#include "GaudiAlg/GaudiTool.h"

// from STL
#include <algorithm>
#include <functional>
#include <memory>
#include <string>
#include <vector>

// local
#include "InclusiveTaggerLibrary.h"
#include "src/Classification/ITaggingClassifierFactory.h"
#include "src/Classification/TaggingClassifierSimpleNN.h"

class InclusiveTaggerClassifierFactory : public GaudiTool, virtual public ITaggingClassifierFactory {
public:
  InclusiveTaggerClassifierFactory( const std::string& type, const std::string& name, const IInterface* parent );

  StatusCode initialize() override;

  std::unique_ptr<ITaggingClassifier> taggingClassifier() override;
  std::unique_ptr<ITaggingClassifier> taggingClassifier( const std::string& type,
                                                         const std::string& name = "" ) override;

private:
  Gaudi::Property<std::string> m_classifierType{this, "ClassifierType", "RNN", "Recurrent neural network"};
  Gaudi::Property<std::string> m_classifierName{this, "ClassifierName", "RNNModelName",
                                                "Name of the RNN model type and weights file without postfix."};
  Gaudi::Property<bool>        m_nameIsPath{this, "NameIsPath", false, "Interpret name as path to binary."};
};

#endif // PHYS_PHYS_FLAVOURTAGGING_INCLUSIVETAGGERCLASSIFIERFACTORY_H
