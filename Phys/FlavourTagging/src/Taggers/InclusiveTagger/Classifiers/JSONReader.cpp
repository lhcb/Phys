/*****************************************************************************\
* (c) Copyright 2020-2021 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

/////////////////////////////////////////////////
//      AUTO-GENERATED CODE, DO NOT EDIT       //
// gitlab: gitlab.cern.ch/vjevtic/rnngenerator //
// simplenn: gitlab.cern.ch/mschille/simplenn  //
/////////////////////////////////////////////////

#include "JSONReader.h"

namespace RNNClassifier {

  JSONReader::JSONReader( const std::string& archfile ) { boost::property_tree::read_json( archfile, json_root ); }

  template <typename float_type>
  std::vector<float_type> JSONReader::load_weights( int layer_index, const std::string& tensor_name ) const {
    // Create iterator to layer
    auto entry = json_root.get_child( "config.layers" ).begin();
    std::advance( entry, layer_index );

    boost::property_tree::ptree pt_tensor;
    if ( entry->second.get<std::string>( "class_name" ) == "TimeDistributed" ) {
      pt_tensor = entry->second.get_child( "config.layer.config" ).get_child( tensor_name );
    } else {
      pt_tensor = entry->second.get_child( "config" ).get_child( tensor_name );
    }

    // Fill vector
    std::vector<float_type> M( pt_tensor.size() );

    int c = 0;
    for ( auto& elem : pt_tensor ) { M[c++] = elem.second.get_value<float_type>(); }
    return M;
  }

  template std::vector<float>  JSONReader::load_weights<float>( int, const std::string& ) const;
  template std::vector<double> JSONReader::load_weights<double>( int, const std::string& ) const;

  template <typename float_type>
  float_type JSONReader::load_feature_scaling( int layer_index, const std::string& feature,
                                               const std::string& param ) const {
    auto entry = json_root.get_child( "config.layers" ).begin();
    std::advance( entry, layer_index );

    boost::property_tree::ptree pt_scaling;
    if ( entry->second.get<std::string>( "class_name" ) == "TimeDistributed" ) {
      pt_scaling = entry->second.get_child( "config.layer.config.feature_scaling" );
    } else {
      pt_scaling = entry->second.get_child( "config.feature_scaling" );
    }

    for ( const auto& node : pt_scaling ) {
      if ( node.second.get<std::string>( "name" ) == feature ) { return node.second.get<float_type>( param ); }
    }
    std::cerr << "JSONReader error: Feature " << feature << " not found\n";
    return 0;
  }

  template float  JSONReader::load_feature_scaling<float>( int, const std::string&, const std::string& ) const;
  template double JSONReader::load_feature_scaling<double>( int, const std::string&, const std::string& ) const;

} // namespace RNNClassifier
