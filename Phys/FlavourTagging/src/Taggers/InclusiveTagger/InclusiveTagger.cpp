/*****************************************************************************\
* (c) Copyright 2020-2021 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "InclusiveTagger.h"

DECLARE_COMPONENT( InclusiveTagger )

InclusiveTagger::InclusiveTagger( const std::string& type, const std::string& name, const IInterface* parent )
    : GaudiTool( type, name, parent ) {
  declareInterface<ITagger>( this );
}

StatusCode InclusiveTagger::initialize() {
  auto sc = GaudiTool::initialize();
  if ( sc.isFailure() ) return sc;

  m_pipeline = std::make_unique<Pipeline>();
  m_pipeline->setSortingFeature( m_sortingFeature );
  m_pipeline->setToolProvider( this );
  m_pipeline->setAliases( m_featureAliases );
  m_pipeline->setPipeline( m_selectionPipeline );

  m_classifierFactory = tool<ITaggingClassifierFactory>( m_classifierFactoryName, this );

  if ( m_classifierFactory == nullptr ) {
    error() << "Could not initialize inclusive tagger factory " << m_classifierFactoryName << endmsg;
    return StatusCode::FAILURE;
  }

  IFTArchitecture tagger_version;
  try {
    tagger_version = IFTVersionMap[m_classifierVersion].first;
  } catch ( const std::out_of_range& versionError ) {
    fatal() << "Inclusive Tagger version \"" << m_classifierVersion << "\" has not been properly registered!" << endmsg;
    return StatusCode::FAILURE;
  }

  switch ( tagger_version ) {
  case IFTArchitecture::IFT_BdBsBu_v111120:
    m_simpleNNClassifier = getIFTTaggerType<RNNClassifier::IFT_BdBsBu_v111120<double>>(
        m_classifierFactory->taggingClassifier( m_classifierVersion, m_weightFileOverride ).release() );
    break;
  case IFTArchitecture::IFT_BdBsBu_v031121:
    m_simpleNNClassifier = getIFTTaggerType<DeepSetClassifier::DeepSet_BdBsBu_v031121<double>>(
        m_classifierFactory->taggingClassifier( m_classifierVersion, m_weightFileOverride ).release() );
    break;
  default:
    fatal() << "Could not instantiate inclusive tagger!" << endmsg;
    return StatusCode::FAILURE;
  }

  return StatusCode::SUCCESS;
}

LHCb::Tagger::TaggerType InclusiveTagger::taggerType() const { return LHCb::Tagger::TaggerType::InclusiveTagger; }

LHCb::Tagger InclusiveTagger::tag( const LHCb::Particle* sigPart, const LHCb::RecVertex* assocVtx, const int,
                                   LHCb::Particle::ConstVector& tagParts ) {
  m_pipeline->setSignalCandidate( sigPart );
  m_pipeline->setReconstructionVertex( assocVtx );
  m_pipeline->selectN( tagParts, m_maxNumTracks );

  std::vector<std::vector<double>> featureMatrix = m_pipeline->mergedFeatureMatrixTransposed();

  double graph_output = 0;

  switch ( m_simpleNNClassifier.index() ) {
  case IFTArchitecture::IFT_BdBsBu_v111120:
    graph_output = std::get<IFTArchitecture::IFT_BdBsBu_v111120>( m_simpleNNClassifier )->predict( featureMatrix );
    break;
  case IFTArchitecture::IFT_BdBsBu_v031121:
    graph_output = std::get<IFTArchitecture::IFT_BdBsBu_v031121>( m_simpleNNClassifier )->predict( featureMatrix );
    break;
  default:
    fatal() << "No predict method call found for inclusive tagger version " << m_simpleNNClassifier.index() << "!"
            << endmsg;
    break;
  }

  LHCb::Tagger tagObject;

  if ( graph_output >= 0.5 ) {
    tagObject.setOmega( 1.0 - graph_output );
    tagObject.setDecision( 1 );
  } else {
    tagObject.setOmega( graph_output );
    tagObject.setDecision( -1 );
  }
  tagObject.setMvaValue( graph_output );
  tagObject.setType( taggerType() );

  return tagObject;
}

LHCb::Tagger InclusiveTagger::tag( const LHCb::Particle* sigPart, const LHCb::RecVertex* assocVtx,
                                   LHCb::RecVertex::ConstVector& puVtxs, LHCb::Particle::ConstVector& tagParts ) {
  m_pipeline->setPileUpVertices( puVtxs );
  return tag( sigPart, assocVtx, puVtxs.size(), tagParts );
}

std::vector<std::string> InclusiveTagger::featureNames() const { return m_pipeline->mergedFeatureNames(); }

std::vector<double> InclusiveTagger::featureValues() const { return m_pipeline->mergedFeatures(); }

std::vector<std::string> InclusiveTagger::featureNamesTagParts() const { return m_pipeline->mergedFeatureNames(); }

std::vector<std::vector<double>> InclusiveTagger::featureValuesTagParts() const {
  return m_pipeline->mergedFeatureMatrix();
}
