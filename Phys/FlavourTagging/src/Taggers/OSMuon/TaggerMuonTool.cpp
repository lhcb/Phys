/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files
#include "TaggerMuonTool.h"
#include "src/Classification/ITaggingClassifierFactory.h"
#include "src/Utils/TaggingHelpers.h"

//--------------------------------------------------------------------
// Implementation file for class : TaggerMuonTool
//
// Author: Marco Musy
//--------------------------------------------------------------------

using namespace LHCb;
using namespace Gaudi::Units;

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( TaggerMuonTool )

//====================================================================
TaggerMuonTool::TaggerMuonTool( const std::string& type, const std::string& name, const IInterface* parent )
    : GaudiTool( type, name, parent ) {

  declareInterface<ITagger>( this );

  declareProperty( "CombTech", m_CombinationTechnique = "NNet" );

  declareProperty( "Muon_Pt_cut", m_Pt_cut_muon = 1.1 * GeV );
  declareProperty( "Muon_P_cut", m_P_cut_muon = 0.0 * GeV );
  declareProperty( "Muon_lcs_cut", m_lcs_cut_muon = 3. );
  declareProperty( "Muon_IPs_cut", m_IPs_cut_muon = 0.0 );
  declareProperty( "Muon_PIDm_cut", m_PIDm_cut = -100. );
  declareProperty( "Muon_ipPU_cut", m_ipPU_cut_muon = 3.0 );
  declareProperty( "Muon_distPhi_cut", m_distPhi_cut_muon = 0.005 );

  declareProperty( "Muon_ghostprob_cut", m_ghostprob_cut = 0.4 );
  declareProperty( "Muon_PIDNNm_cut", m_PIDNNm_cut_muon = 0.35 );
  declareProperty( "Muon_PIDNNpi_cut", m_PIDNNpi_cut_muon = 0.8 );
  declareProperty( "Muon_PIDNNe_cut", m_PIDNNe_cut_muon = 0.8 );
  declareProperty( "Muon_PIDNNk_cut", m_PIDNNk_cut_muon = 0.8 );
  declareProperty( "Muon_PIDNNp_cut", m_PIDNNp_cut_muon = 0.8 );

  declareProperty( "Muon_P0_Cal", m_P0_Cal_muon = 0. );
  declareProperty( "Muon_P1_Cal", m_P1_Cal_muon = 1. );
  declareProperty( "Muon_Eta_Cal", m_Eta_Cal_muon = 0. );
  declareProperty( "Muon_ProbMin", m_ProbMin_muon = 0. ); // no cut
  declareProperty( "Muon_AverageOmega", m_AverageOmega = 0.33 );
  declareProperty( "isMonteCarlo", m_isMonteCarlo = 0 );

  // mu scaleX=-5.47039 scaleY=1.23885 offsetY=-0.00793716 pivotX=0.647253
  declareProperty( "P0_mu_scale", m_P0mu = -5.47039 );
  declareProperty( "P1_mu_scale", m_P1mu = 1.23885 );
  declareProperty( "P2_mu_scale", m_P2mu = -0.00793716 );
  declareProperty( "P3_mu_scale", m_P3mu = 0.647253 );

  m_descend = 0;
}

TaggerMuonTool::~TaggerMuonTool() {}

//=====================================================================
StatusCode TaggerMuonTool::initialize() {
  StatusCode sc = GaudiTool::initialize();
  if ( sc.isFailure() ) return sc;

  if ( msgLevel( MSG::DEBUG ) )
    debug() << "Mu calib ctt: P0_Cal " << m_P0_Cal_muon << ", P1_Cal " << m_P1_Cal_muon << endmsg;

  m_util = tool<ITaggingUtils>( "TaggingUtils", this );

  m_descend = tool<IParticleDescendants>( "ParticleDescendants", this );

  m_classifierFactory = tool<ITaggingClassifierFactory>( m_classifierFactoryName, this );
  if ( m_classifierFactory == nullptr ) {
    error() << "Could not load the TaggingClassifierFactory " << m_classifierFactoryName << endmsg;
    return StatusCode::FAILURE;
  } else {
    m_classifier = m_classifierFactory->taggingClassifier();
  }

  return sc;
}
//================================================================================
StatusCode TaggerMuonTool::finalize() { return GaudiTool::finalize(); }

//================================================================================
Tagger TaggerMuonTool::tag( const Particle* AXB0, const RecVertex* RecVert, const int nPV,
                            Particle::ConstVector& vtags ) {
  Tagger tmu;
  if ( !RecVert ) return tmu;

  if ( msgLevel( MSG::VERBOSE ) ) verbose() << "--Muon Tagger--for Reco14" << endmsg;

  // fill auxdaugh for distphi
  double                distphi;
  Particle::ConstVector axdaugh = m_descend->descendants( AXB0 );
  axdaugh.push_back( AXB0 );
  // select muon tagger(s)
  // if more than one satisfies cuts, take the highest Pt one
  const Particle* imuon     = 0;
  double          ptmaxm    = -99.0;
  double          save_IPPU = -10, save_IPs = -10, save_PIDNNm = -10.;
  // save_pidm=-10,save_PIDNNpi=-10.;

  for ( Particle::ConstVector::const_iterator ipart = vtags.begin(); ipart != vtags.end(); ++ipart ) {
    const Particle*      axp   = ( *ipart );
    const ProtoParticle* proto = axp->proto();
    const Track*         track = ( *ipart )->proto()->track();

    if ( track->type() != LHCb::Track::Types::Long ) continue;

    int muonNSH = (int)proto->info( ProtoParticle::additionalInfo::MuonNShared, -1.0 );
    if ( muonNSH != 0 ) continue;

    if ( proto->muonPID() == 0 ) continue;
    if ( proto->muonPID()->IsMuon() == 0 ) continue;

    if ( !proto->info( ProtoParticle::additionalInfo::MuonPIDStatus, 0 ) ) continue;

    const double pidm = proto->info( ProtoParticle::additionalInfo::CombDLLmu, -1000.0 );
    if ( pidm < m_PIDm_cut ) continue;

    const double Pt = axp->pt();
    if ( Pt < m_Pt_cut_muon ) continue;

    const double P = axp->p();
    if ( P < m_P_cut_muon ) continue;

    const double lcs = track->chi2PerDoF();
    if ( lcs > m_lcs_cut_muon ) continue;

    if ( axp->info( LHCb::Particle::additionalInfo::FlavourTaggingTaggerID, -1. ) > 0 ) continue; // already tagged

    if ( track->ghostProbability() > m_ghostprob_cut ) continue;

    const double PIDNNm = proto->info( ProtoParticle::additionalInfo::ProbNNmu, -1000.0 );
    if ( PIDNNm < m_PIDNNm_cut_muon ) continue;
    const double PIDNNpi = proto->info( ProtoParticle::additionalInfo::ProbNNpi, -1000.0 );
    if ( PIDNNpi > m_PIDNNpi_cut_muon ) continue;
    const double PIDNNe = proto->info( ProtoParticle::additionalInfo::ProbNNe, -1000.0 );
    if ( PIDNNe > m_PIDNNe_cut_muon ) continue;
    const double PIDNNp = proto->info( ProtoParticle::additionalInfo::ProbNNp, -1000.0 );
    if ( PIDNNp > m_PIDNNp_cut_muon ) continue;
    const double PIDNNk = proto->info( ProtoParticle::additionalInfo::ProbNNk, -1000.0 );
    if ( PIDNNk > m_PIDNNk_cut_muon ) continue;

    // calculate signed IP wrt RecVert
    double IP( 0. ), IPerr( 0. );
    m_util->calcIP( *ipart, RecVert, IP, IPerr ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    if ( !IPerr ) continue;
    double IPsig = IP / IPerr;
    if ( fabs( IPsig ) < m_IPs_cut_muon ) continue;

    const double ippu = ( *ipart )->info( LHCb::Particle::additionalInfo::FlavourTaggingIPPUs, 10000. );
    if ( ippu < m_ipPU_cut_muon ) continue;
    // distphi
    if ( m_util->isInTree( *ipart, axdaugh, distphi ) ) continue;
    if ( distphi < m_distPhi_cut_muon ) continue;

    Particle* c = const_cast<Particle*>( *ipart );
    c->addInfo( LHCb::Particle::additionalInfo::FlavourTaggingTaggerID,
                13 ); // store the information that the muon tagger is found // new

    if ( Pt > ptmaxm ) { // Pt ordering
      imuon    = axp;
      ptmaxm   = Pt;
      save_IPs = IPsig;
      // save_pidm = pidm;
      save_IPPU   = ippu;
      save_PIDNNm = PIDNNm;
      // save_PIDNNpi = PIDNNpi;
    }
  }
  if ( !imuon ) return tmu;

  // calculate omega
  double pn   = 1. - m_AverageOmega;
  double rnet = -999.;
  double sign = 1.;

  if ( m_CombinationTechnique == "NNet" ) {
    if ( msgLevel( MSG::DEBUG ) ) debug() << nPV << endmsg;

    std::vector<double> inputVals = {static_cast<double>( m_util->countTracks( vtags ) ),
                                     log( imuon->p() / GeV ),
                                     log( imuon->pt() / GeV ),
                                     log( AXB0->pt() / GeV ),
                                     log( fabs( save_IPs ) ),
                                     log( imuon->proto()->track()->chi2PerDoF() ),
                                     save_PIDNNm,
                                     log( imuon->proto()->track()->ghostProbability() ),
                                     log( static_cast<double>( save_IPPU ) )};

    rnet = m_classifier->getClassifierValue( inputVals );

    if ( rnet >= 0 && rnet <= 1 ) {
      pn = 1.0 - TaggingHelpers::funcNN( rnet, m_P0mu, m_P1mu, m_P2mu, m_P3mu );
    } else {
      if ( msgLevel( MSG::DEBUG ) ) debug() << "**********************BAD TRAINING Muon" << rnet << endmsg;
      pn = -1.;
    }

    // Calibration (w=1-pn) w' = p0 + p1(w-eta)
    pn = 1. - m_P0_Cal_muon - m_P1_Cal_muon * ( ( 1. - pn ) - m_Eta_Cal_muon );

    if ( pn < 0 || pn > 1 ) {
      if ( msgLevel( MSG::INFO ) ) info() << " ===> Something wrong with MUON Training " << pn << endmsg;
      return tmu;
    }
    if ( pn < m_ProbMin_muon ) return tmu;

    if ( pn < 0.5 ) {
      pn   = 1. - pn;
      sign = -1.;
    }

    if ( msgLevel( MSG::DEBUG ) ) {
      debug() << " TaggerMuon: " << sign * imuon->charge() << " omega=" << 1 - pn << " ";
      for ( unsigned int iloop = 0; iloop < inputVals.size(); iloop++ ) { debug() << inputVals[iloop] << " "; }
      debug() << endmsg;
    }
  }

  tmu.setOmega( 1 - pn );
  tmu.setDecision( sign * imuon->charge() > 0 ? -1 : 1 );
  tmu.setType( taggerType() );
  tmu.addToTaggerParts( imuon );
  tmu.setMvaValue( rnet );
  tmu.setCharge( imuon->charge() );

  return tmu;
}
