/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "OSMuonClassifierFactory.h"

// ITaggingClassifier implementations
#include "TMVA/OSMuon_Data_Run1_All_Bu2JpsiK_TMVA_MLP_v1r0.h"
#include "XGB/OSMuon_Data_Run2_All_Bu2D0pi_XGBoost_BDT_v1r0.h"
#include "XGB/OSMuon_Data_Run2_All_Bu2JpsiK_XGB_BDT_v1r0.h"
#include "XGB/OSMuon_Data_Run2_All_Bu2JpsiK_XGB_BDT_v2r0.h"

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( OSMuonClassifierFactory )

OSMuonClassifierFactory::OSMuonClassifierFactory( const std::string& type, const std::string& name,
                                                  const IInterface* parent )
    : GaudiTool( type, name, parent ) {
  declareInterface<ITaggingClassifierFactory>( this );
}

StatusCode OSMuonClassifierFactory::initialize() {
  auto sc = GaudiTool::initialize();
  if ( sc.isFailure() ) return sc;

  addTMVAClassifier<OSMuon_Data_Run1_All_Bu2JpsiK_TMVA_MLP_v1r0>( "OSMuon_Data_Run1_All_Bu2JpsiK_TMVA_MLP_v1r0" );
  addTMVAClassifier<OSMuon_Data_Run2_All_Bu2JpsiK_XGB_BDT_v1r0>( "OSMuon_Data_Run2_All_Bu2JpsiK_XGB_BDT_v1r0" );
  addTMVAClassifier<OSMuon_Data_Run2_All_Bu2D0pi_XGBoost_BDT_v1r0>( "OSMuon_Data_Run2_All_Bu2D0pi_XGBoost_BDT_v1r0" );
  addTMVAClassifier<OSMuon_Data_Run2_All_Bu2JpsiK_XGB_BDT_v2r0>( "OSMuon_Data_Run2_All_Bu2JpsiK_XGB_BDT_v2r0" );

  return sc;
}

std::unique_ptr<ITaggingClassifier> OSMuonClassifierFactory::taggingClassifier() {
  return taggingClassifier( m_classifierType, m_classifierName );
}

std::unique_ptr<ITaggingClassifier> OSMuonClassifierFactory::taggingClassifier( const std::string& type,
                                                                                const std::string& name ) {
  if ( type == "TMVA" ) {
    return m_classifierMapTMVA[name]();
  } else if ( type == "XGBoost" && m_nameIsPath ) {
    auto classifier = std::make_unique<TaggingClassifierXGB>();
    classifier->setPath( m_classifierName );
    return classifier;
  } else {
    error() << "Classifier of type " << type << " unknown!" << endmsg;
    return nullptr;
  }
}
