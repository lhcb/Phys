/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef TAGGERS_OSMUON_CLASSIFIERS_TMVA_WRAPPERMLPBNNMC_H
#define TAGGERS_OSMUON_CLASSIFIERS_TMVA_WRAPPERMLPBNNMC_H 1

#include "src/TMVAWrapper.h"

namespace Taggers {
  namespace OSMuon {
    namespace Classifiers {
      namespace TMVA {

        // forward declaration
        class Read_muonMLPBNN_MC;

        class WrapperMLPBNNMC : public TMVAWrapper {
        public:
          WrapperMLPBNNMC( std::vector<std::string>& );
          ~WrapperMLPBNNMC();
          double GetMvaValue( std::vector<double> const& ) override;

        private:
          Read_muonMLPBNN_MC* reader;
        };
      } // namespace TMVA
    }   // namespace Classifiers
  }     // namespace OSMuon
} // namespace Taggers

#endif // TAGGERS_OSMUON_CLASSIFIERS_TMVA_WRAPPERMLPBNNMC_H
