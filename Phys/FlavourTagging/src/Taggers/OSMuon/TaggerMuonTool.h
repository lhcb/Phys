/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: TaggerMuonTool.h,v 1.9 2010-06-17 17:46:04 mgrabalo Exp $
#ifndef PHYS_PHYS_FLAVOURTAGGING_TAGGERMUONTOOL_H
#define PHYS_PHYS_FLAVOURTAGGING_TAGGERMUONTOOL_H 1

// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/AlgTool.h"

// from LHCb
#include "Event/FlavourTag.h"
#include "Kernel/ITagger.h"

// from ROOT
#include <TROOT.h>

// from Phys
#include "Kernel/IParticleDescendants.h"

// from local project
#include "FlavourTagging/ITaggingUtils.h"

// from STL
#include <list>
#include <string>
#include <utility>

// forward declarations
class ITaggingClassifier;
class ITaggingClassifierFactory;

/** @class TaggerMuonTool TaggerMuonTool.h
 *
 *  Tool to tag the B flavour with a Muon Tagger
 *
 *  @author Marco Musy
 *  @date   30/06/2005
 */

class TaggerMuonTool : public GaudiTool, virtual public ITagger {

public:
  /// Standard constructor
  TaggerMuonTool( const std::string& type, const std::string& name, const IInterface* parent );
  virtual ~TaggerMuonTool();        ///< Destructor
  StatusCode initialize() override; ///<  initialization
  StatusCode finalize() override;   ///<  finalization

  LHCb::Tagger::TaggerType taggerType() const override { return LHCb::Tagger::TaggerType::OS_Muon; }

  using ITagger::tag;
  LHCb::Tagger tag( const LHCb::Particle*, const LHCb::RecVertex*, const int, LHCb::Particle::ConstVector& ) override;

  //-------------------------------------------------------------

private:
  std::unique_ptr<ITaggingClassifier> m_classifier        = nullptr;
  ITaggingClassifierFactory*          m_classifierFactory = nullptr;

  Gaudi::Property<std::string> m_classifierFactoryName{this, "ClassifierFactoryName", "OSMuonClassifierFactory",
                                                       "Name of the factory that creates the classifier."};

  IParticleDescendants* m_descend = nullptr;
  ITaggingUtils*        m_util    = nullptr;

  // IDataProviderSvc* m_eventSvc;
  std::string m_CombinationTechnique;

  // properties
  double m_Pt_cut_muon;
  double m_P_cut_muon;
  double m_IPs_cut_muon;
  double m_lcs_cut_muon;
  double m_PIDm_cut;
  double m_ipPU_cut_muon;
  double m_distPhi_cut_muon;
  double m_ProbMin_muon;
  double m_ghostprob_cut;
  double m_PIDNNm_cut_muon;
  double m_PIDNNpi_cut_muon;
  double m_PIDNNe_cut_muon;
  double m_PIDNNk_cut_muon;
  double m_PIDNNp_cut_muon;
  double m_AverageOmega;
  double m_P0_Cal_muon;
  double m_P1_Cal_muon;
  double m_Eta_Cal_muon;
  int    m_isMonteCarlo;
  double m_P0mu, m_P1mu, m_P2mu, m_P3mu;
};

//===============================================================//
#endif // PHYS_PHYS_FLAVOURTAGGING_TAGGERMUONTOOL_H
