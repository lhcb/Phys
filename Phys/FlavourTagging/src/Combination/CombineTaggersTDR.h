/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: CombineTaggersTDR.h,v 1.4 2007-03-01 20:59:23 musy Exp $
#ifndef COMBINETAGGERSTDR_H
#define COMBINETAGGERSTDR_H 1
// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/AlgTool.h"

#include "ICombineTaggersTool.h" // Interface

/** @class CombineTaggersTDR CombineTaggersTDR.h CombineTaggersTDR.h
 *
 *  v1.3
 *  @author Marco Musy
 *  @date   2006-10-02
 */
class CombineTaggersTDR : public GaudiTool, virtual public ICombineTaggersTool {
public:
  /// Standard constructor
  CombineTaggersTDR( const std::string& type, const std::string& name, const IInterface* parent );

  ~CombineTaggersTDR(); ///< Destructor

  int combineTaggers( LHCb::FlavourTag& theTag, std::vector<LHCb::Tagger*>&, int, bool, bool ) override;
};
#endif // COMBINETAGGERSTDR_H
