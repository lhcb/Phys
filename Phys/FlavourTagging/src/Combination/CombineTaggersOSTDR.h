/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: CombineTaggersOSTDR.h,v 1.3 2006-10-24 10:16:44 jpalac Exp $
#ifndef COMBINETAGGERSOSTDR_H
#define COMBINETAGGERSOSTDR_H 1
// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/AlgTool.h"

#include "ICombineTaggersTool.h" // Interface

/** @class CombineTaggersOSTDR CombineTaggersOSTDR.h CombineTaggersOSTDR.h
 *
 *  Tool to do the combined OS tagging (independently from SS)
 *  @author Chris Barnes
 *  @date   2006-01-28
 */
class CombineTaggersOSTDR : public GaudiTool, virtual public ICombineTaggersTool {
public:
  /// Standard constructor
  CombineTaggersOSTDR( const std::string& type, const std::string& name, const IInterface* parent );

  ~CombineTaggersOSTDR(); ///< Destructor

  int combineTaggers( LHCb::FlavourTag& theTag, std::vector<LHCb::Tagger*>&, int, bool, bool ) override;

private:
};
#endif // COMBINETAGGERSOSTDR_H
