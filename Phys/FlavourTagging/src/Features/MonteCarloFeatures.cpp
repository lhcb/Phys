/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "MonteCarloFeatures.h"

DECLARE_COMPONENT( MonteCarloFeatures )

MonteCarloFeatures::MonteCarloFeatures( const std::string& type, const std::string& name, const IInterface* interface )
    : GaudiTool( type, name, interface ) {
  declareInterface<IMonteCarloFeatures>( this );
}

StatusCode MonteCarloFeatures::initialize() {
  auto sc = GaudiTool::initialize();
  if ( sc.isFailure() ) return sc;

  m_mcDecayFinder = tool<IMCDecayFinder>( "MCDecayFinder" );
  debug() << "[MonteCarloFeatures::initialize] setting decay" << endmsg;
  // @TODO set decay descriptor via options
  m_mcDecayFinder->setDecay( "[B+ -> (J/psi(1S) -> mu+ mu-) ^K+]cc" )
      .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );

  return StatusCode::SUCCESS;
}

std::vector<const LHCb::MCParticle*> MonteCarloFeatures::getMCParticles() { return m_mcParticles; }

const LHCb::MCParticle* MonteCarloFeatures::signalParticle() {
  // This check essentially prevents segfaults
  if ( m_mcParticles.size() && m_mcDecayFinder->hasDecay( m_mcParticles ) ) {
    // It seems like it has to be NULL
    const LHCb::MCParticle* signalParticle = nullptr;

    std::vector<const LHCb::MCParticle*> signalCandidates;
    while ( m_mcDecayFinder->findDecay( m_mcParticles, signalParticle ) ) {
      signalCandidates.push_back( signalParticle );
    }
    if ( signalCandidates.size() > 1 ) {
      warning() << "[MonteCarloFeatures::signalParticle] more than one signal decay found." << endmsg;
    }
    m_mcSignal = signalParticle;
  } else {
    m_mcSignal = nullptr;
  }
  return m_mcSignal;
}

void MonteCarloFeatures::readParticles() {
  auto mcParticleObjects = get<LHCb::MCParticles>( LHCb::MCParticleLocation::Default );
  m_mcParticles.clear();
  std::copy( mcParticleObjects->begin(), mcParticleObjects->end(), std::back_inserter( m_mcParticles ) );
}
