/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "CaloUtils/ICaloElectron.h"
#include "Event/MCParticle.h"
#include "Event/Particle.h"
#include "GaudiAlg/GaudiTool.h"
#include "Kernel/GetIDVAlgorithm.h"
#include "Kernel/IDVAlgorithm.h"
#include "Kernel/IDistanceCalculator.h"
#include "Kernel/IParticleDescendants.h"
#include "LoKi/IHybridFactory.h"
#include "LoKi/PhysTypes.h"
#include "RecInterfaces/IChargedProtoANNPIDTool.h"

#include <string>
#include <unordered_map>
#include <vector>

#include "boost/algorithm/string.hpp"
#include "boost/log/core.hpp"
#include "boost/log/expressions.hpp"
#include "boost/log/trivial.hpp"
#include "boost/regex.hpp"

#include "FeatureGeneratorCache.h"
#include "FeaturePreparation.h"
#include "FlavourTagging/ITaggingUtils.h"
#include "ILoKiWrapper.h"
#include "IMonteCarloFeatures.h"
#include "src/Classification/ITaggingClassifier.h"
#include "src/Utils/TaggingHelpers.h"

namespace logging = boost::log;

/**
 * @class FeatureGenerator
 *
 * Generate vectors of feature values for a given event. Prefer using a
 * Pipeline instance over using the FeatureGenerator directly.
 *
 * Each requested feature will be calculated by one std::function<double()>
 * (functor). These functors will be loaded in a specific order:
 * 1. Functors defined in functorDefinitions (see FeatureGenerator.cpp)
 * 2. Manual functors (preceded by `m_manualFeaturePrefix`) wont update any
 *    values, they can be set manually. These are mainly placeholders within
 *    the feature vector.
 * 3. ANNPID functors (e.g. PROBNNmu_MC15TuneV1) are calling an instance of
 *    ChargedProtoANNPIDTool manually implemented to allow for specific PID
 *    tuning application.
 * 4. MVA functors are named like e.g.
 *    `MVA[TaggingClassifierFactoryName](inputVar1, log(inputVar2))` and can
 *    use the previous features of the feature vector. Also manually defined
 *    features (without the `man_` prefix can be used here, but they need to be
 *    updated manually ;-)
 * 5. If no functor could have been activated in the above manner, a Loki
 *    functor is created.
 *
 * The above features (if not explicitly defined otherwise in the
 * functorDefinitions map) are applied to the tagging particles. If you use a
 * LoKi functor and want to apply it to the signal candidate, you can prefix
 * the loki feature by `Signal_`.
 */
class FeatureGenerator {
public:
  /** Set the vector of features, extracted when calling `generateFeatures`.
   * @param[in] features The vector of feature names. Each name can be a
   *   `LoKi` functor or implemented as a lambda function within the
   *   initializer list of this class . Custom functors will be used if both,
   *   `LoKi` and custom functors exist. An error is raised if the feature
   *   name is not implemented.
   */
  void setFeatureNames( const std::vector<std::string>& featureNames );
  void setFeatureNames( const std::vector<std::string>&           featureNames,
                        const std::map<std::string, std::string>& featureAliases );

  /** Needs to be called before setFeatureNames, to make aliases available
   * during configuration.
   * Use setFeatureNames(names, aliases) for a shortcut.
   */
  void setFeatureAliases( const std::map<std::string, std::string>& featureAliases );

  /** Call all previously defined (with setFeatureNames()) functors and return
   * the results.
   * @return A vector of doubles, corresponding to the previously defined
   *         vector of feature names.
   */
  std::vector<double> generateFeatures();
  std::vector<double> generateFeatures( const LHCb::Particle* taggingParticle );

  /** @brief apply the configured functors to the given feature vector
   * Manual features will be skipped.
   * @param[in,out] values feature values to be updated.
   */
  void updateFeatures( std::vector<double>& values );

  /** manually update a feature vector's value
   */
  void setFeatureValue( std::vector<double>& featureVector, const std::string& name, double value );

  /** Get the value of the given feature within the given featureVector.
   * @param[in] featureVector The feature vector
   * @param[in] name          The name of the feature
   */
  double getFeatureValue( const std::vector<double>& featureVector, const std::string& name );

  /** Update the information used to calculate all features
   */
  void setParticles( const LHCb::Particle* B0Candidate, const LHCb::Particle* taggingParticle,
                     const LHCb::RecVertex*                     reconstructionVertex,
                     const std::vector<const LHCb::Particle*>&  otherParticles,
                     const std::vector<const LHCb::RecVertex*>& pileUpVertices,
                     const LHCb::RecVertex::Range*              otherVertices );

  /** Set the tagging particle used for all following calculations
   */
  void setTaggingParticle( const LHCb::Particle* taggingParticle );

  void setTaggingParticleVector( const std::vector<const LHCb::Particle*>& otherParticles );

  /** Set the signal candidate used for all following calculations
   *
   * Usually this only needs to be done once per each Taggers tag()-function
   * event.
   */
  void setSignalCandidate( const LHCb::Particle* signalCandidate );

  /** Set the associated/reconstructed vertex used for all following
   * calculations
   *
   * Usually this only needs to be done once per each Taggers tag()-function
   * event.
   */
  void setReconstructionVertex( const LHCb::RecVertex*& reconstrucionVertex );

  /** Set a list of pile up vertices of this event, used for all following
   * calculations
   *
   * Usually this only needs to be done once per each Taggers tag()-function
   * event.
   */
  void setOtherVertices( const LHCb::RecVertex::Range* vertices );

  /** Set a list of pile up vertices of this event, used for all following
   * calculations
   *
   * Usually this only needs to be done once per each Taggers tag()-function
   * event.
   */
  void setPileUpVertices( const std::vector<const LHCb::RecVertex*>& vertices );

  /** The definition vector of all custom functors. If you want to define a
   * new variable, add a lambda-function with signature of
   * std::function<double()> to the initializer list of the default
   * constructor.
   */
  std::unordered_map<std::string, std::function<double()>> functorDefinitions = {
      {"diff_z",
       [this]() -> double {
         return m_taggingParticle->proto()->track()->position().z() - m_reconstructionVertex->position().z();
       }},
      {"P_proj", [this]() -> double { return m_signalCandidate->momentum().Dot( m_taggingParticle->momentum() ); }},
      {"cos_diff_phi",
       [this]() -> double {
         return std::cos(
             TaggingHelpers::dphi( m_signalCandidate->momentum().phi(), m_taggingParticle->momentum().phi() ) );
       }},
      {"diff_eta",
       [this]() -> double { // already implemented in etaDistance ( without sign)
         return m_signalCandidate->momentum().eta() - m_taggingParticle->momentum().eta();
       }},
      {"SumBDT_ult",
       [this]() -> double {
         Gaudi::XYZPoint         PosPV = m_reconstructionVertex->position();
         Gaudi::XYZPoint         PosSV( 0, 0, 200000 );
         const LHCb::VertexBase* endv = m_signalCandidate->endVertex();
         if ( endv ) { PosSV = m_signalCandidate->endVertex()->position(); }
         return m_utils->GetSumBDT_ult( m_taggingParticle, m_signalCandidate, PosPV, PosSV );
       }},
      {"IP_Mother",
       [this]() -> double {
         double recVertexIP( 0 ), recVertexIPerr( 0 );
         m_utils->calcIP( m_taggingParticle, m_signalCandidate->endVertex(), recVertexIP, recVertexIPerr ).ignore();
         return std::abs( recVertexIP ); // remove sign for compatibility with TupleToolIsoGeneric... do we want this?
       }},
      {"IP_Mother_Err",
       [this]() -> double {
         double recVertexIP( 0 ), recVertexIPerr( 0 );
         m_utils->calcIP( m_taggingParticle, m_signalCandidate->endVertex(), recVertexIP, recVertexIPerr ).ignore();
         return std::abs( recVertexIPerr ); // remove sign for compatibility with TupleToolIsoGeneric... do we want
                                            // this?
       }},
      {"IP_Mother_Sig",
       [this]() -> double {
         double recVertexIP( 0 ), recVertexIPerr( 0 );
         m_utils->calcIP( m_taggingParticle, m_signalCandidate->endVertex(), recVertexIP, recVertexIPerr ).ignore();
         return std::abs( recVertexIP / recVertexIPerr );
       }},
      {"IPPU",
       [this]() -> double {
         double minimumPileUpIP( 0 ), minimumPileUpIPerr( 0 );
         m_utils->calcIP( m_taggingParticle, getPileUpVertices(), minimumPileUpIP, minimumPileUpIPerr )
             .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
         if ( minimumPileUpIPerr == 0 ) return 10000;
         return minimumPileUpIP;
       }},
      {"IPPUErr",
       [this]() -> double {
         double minimumPileUpIP( 0 ), minimumPileUpIPerr( 0 );
         m_utils->calcIP( m_taggingParticle, getPileUpVertices(), minimumPileUpIP, minimumPileUpIPerr )
             .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
         return minimumPileUpIPerr;
       }},
      {"IPPUSig",
       [this]() -> double {
         double minimumPileUpIP( 0 ), minimumPileUpIPerr( 0 );
         m_utils->calcIP( m_taggingParticle, getPileUpVertices(), minimumPileUpIP, minimumPileUpIPerr )
             .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
         if ( minimumPileUpIPerr == 0 ) return 10000;
         return std::abs( minimumPileUpIP / minimumPileUpIPerr );
       }},
      {"IP",
       [this]() -> double {
         double recVertexIP( 0 ), recVertexIPerr( 0 );
         m_utils->calcIP( m_taggingParticle, m_reconstructionVertex, recVertexIP, recVertexIPerr )
             .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
         return recVertexIP;
       }},
      {"AbsIP",
       [this]() -> double {
         double recVertexIP( 0 ), recVertexIPerr( 0 );
         m_utils->calcIP( m_taggingParticle, m_reconstructionVertex, recVertexIP, recVertexIPerr )
             .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
         return std::abs( recVertexIP );
       }},
      {"IPErr",
       [this]() -> double {
         double recVertexIP( 0 ), recVertexIPerr( 0 );
         m_utils->calcIP( m_taggingParticle, m_reconstructionVertex, recVertexIP, recVertexIPerr )
             .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
         return recVertexIPerr;
       }},
      {"IPSig",
       [this]() -> double {
         double recVertexIP( 0 ), recVertexIPerr( 0 );
         m_utils->calcIP( m_taggingParticle, m_reconstructionVertex, recVertexIP, recVertexIPerr )
             .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
         return std::abs( recVertexIP / recVertexIPerr );
       }},
      {"eOverP",
       [this]() -> double {
         m_caloElectronTool->set( m_taggingParticle );
         double eOverP( -999.9 );
         eOverP = m_caloElectronTool->eOverP();
         return eOverP;
       }},
      {"MuonPIDIsMuon",
       [this]() -> double {
         auto protoParticle = m_taggingParticle->proto();
         if ( protoParticle && protoParticle->muonPID() ) {
           return (double)protoParticle->muonPID()->IsMuon();
         } else {
           return 0;
         }
       }},
      {"IsSignalDaughter",
       [this]() -> double {
         auto daughtersVector = m_descendantsTool->descendants( m_signalCandidate );
         daughtersVector.push_back( m_signalCandidate );
         return (double)m_utils->isInTree( m_taggingParticle, daughtersVector );
       }},
      {"MC_Signal_PID",
       [this]() -> double {
         auto mcParticle = getMCFeatures()->signalParticle();
         if ( mcParticle ) {
           return (double)mcParticle->particleID().pid();
         } else {
           return -1;
         }
       }},
      {"phiDistance",
       [this]() -> double {
         return std::fabs(
             TaggingHelpers::dphi( m_signalCandidate->momentum().phi(), m_taggingParticle->momentum().phi() ) );
       }},
      {"etaDistance",
       [this]() -> double {
         return std::fabs( m_signalCandidate->momentum().eta() - m_taggingParticle->momentum().eta() );
       }},
      {"DeltaR",
       [this]() -> double {
         double deta_2 = std::pow( m_signalCandidate->momentum().eta() - m_taggingParticle->momentum().eta(), 2 );
         double dphi_2 = std::pow(
             TaggingHelpers::dphi( m_signalCandidate->momentum().phi(), m_taggingParticle->momentum().phi() ), 2 );
         return std::sqrt( deta_2 + dphi_2 );
       }},
      {"DeltaQ",
       [this]() -> double {
         const double         mp      = m_taggingParticle->measuredMass();
         Gaudi::LorentzVector ptot_B  = m_signalCandidate->momentum();
         Gaudi::LorentzVector ptot_pS = m_taggingParticle->momentum();
         ptot_pS.SetE( std::sqrt( mp * mp + ptot_pS.P2() ) );
         return ( ptot_B + ptot_pS ).M() - ptot_B.M() - mp;
       }},
      {"Signal_TagPart_PT",
       [this]() -> double {
         const double         mp      = m_taggingParticle->measuredMass();
         Gaudi::LorentzVector ptot_B  = m_signalCandidate->momentum();
         Gaudi::LorentzVector ptot_pS = m_taggingParticle->momentum();
         ptot_pS.SetE( std::sqrt( mp * mp + ptot_pS.P2() ) );
         return ( ptot_B + ptot_pS ).Pt();
       }},
      {"minPhiDistance",
       [this]() -> double {
         auto signal_daughters = m_descendantsTool->descendants( m_signalCandidate );
         signal_daughters.push_back( m_signalCandidate );
         auto selectedParticle = *std::max_element(
             signal_daughters.begin(), signal_daughters.end(),
             [this]( const LHCb::Particle* p1, const LHCb::Particle* p2 ) {
               auto a = fabs( TaggingHelpers::dphi( m_taggingParticle->momentum().phi(), p1->momentum().phi() ) );
               auto b = fabs( TaggingHelpers::dphi( m_taggingParticle->momentum().phi(), p2->momentum().phi() ) );
               return ( a > b );
             } );
         return fabs( TaggingHelpers::dphi( m_taggingParticle->momentum().phi(), selectedParticle->momentum().phi() ) );
       }},
      {"nPV", [this]() -> double { return static_cast<double>( getOtherVertices()->size() ); }},
      {"Signal_TagPart_CHI2NDOF",
       [this]() -> double {
         double Vchindof( 0 );
         m_utils->GetVCHI2NDOF( m_signalCandidate, m_taggingParticle, Vchindof )
             .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
         return Vchindof;
       }},
      {"countTracks",
       [this]() -> double { return static_cast<double>( m_utils->countTracks( m_otherTaggingParticles ) ); }},
      {"TPVTAU", [this]() -> double { return m_utils->TPVTAU( m_taggingParticle, m_reconstructionVertex ); }},
      {"TPVDIRA", [this]() -> double { return m_utils->TPVDIRA( m_taggingParticle, m_reconstructionVertex ); }},
      {"TPVFD", [this]() -> double { return m_utils->TPVFD( m_taggingParticle, m_reconstructionVertex ); }},
      {"TPVFDCHI2", [this]() -> double { return m_utils->TPVFDCHI2( m_taggingParticle, m_reconstructionVertex ); }},
      {"TPVIPCHI2", [this]() -> double { return m_utils->TPVIPCHI2( m_taggingParticle, m_reconstructionVertex ); }},
      {"TPVIPCHI2_PI",
       [this]() -> double { return m_utils->TPVIPCHI2( m_taggingParticle, m_reconstructionVertex, "pi+" ); }},
      {"TPVIPCHI2_K",
       [this]() -> double { return m_utils->TPVIPCHI2( m_taggingParticle, m_reconstructionVertex, "K+" ); }},
      {"TPVIPCHI2_P",
       [this]() -> double { return m_utils->TPVIPCHI2( m_taggingParticle, m_reconstructionVertex, "p+" ); }},
      {"TPVIPCHI2_E",
       [this]() -> double { return m_utils->TPVIPCHI2( m_taggingParticle, m_reconstructionVertex, "e-" ); }},
      {"TPVIPCHI2_MU",
       [this]() -> double { return m_utils->TPVIPCHI2( m_taggingParticle, m_reconstructionVertex, "mu-" ); }},
      {"isBestPV",
       [this]() -> double {
         bool b = m_utils->isBestPV( m_taggingParticle, m_reconstructionVertex );
         if ( b )
           return 1;
         else
           return 0;
       }},
      {"ABSID", [this]() { return m_taggingParticle->particleID().abspid(); }},
      {"P", [this]() { return m_taggingParticle->p(); }},
      {"P/GeV", [this]() { return m_taggingParticle->p() / Gaudi::Units::GeV; }},
      {"PT", [this]() { return m_taggingParticle->pt(); }},
      {"PT/GeV", [this]() { return m_taggingParticle->pt() / Gaudi::Units::GeV; }},
      {"TRCHI2DOF", [this]() { return m_taggingParticle->proto()->track()->chi2PerDoF(); }},
      {"TRGHP", [this]() { return m_taggingParticle->proto()->track()->ghostProbability(); }},
      {"PP_InAccHcal", [this]() { return m_taggingParticle->proto()->info( LHCb::ProtoParticle::InAccHcal, -998 ); }},
      {"TRTYPE", [this]() { return m_taggingParticle->proto()->track()->type(); }},
      {"PIDe", [this]() { return m_taggingParticle->proto()->info( LHCb::ProtoParticle::CombDLLe, -998 ); }},
      {"PIDmu", [this]() { return m_taggingParticle->proto()->info( LHCb::ProtoParticle::CombDLLmu, -998 ); }},
      {"PP_VeloCharge", [this]() { return m_taggingParticle->proto()->info( LHCb::ProtoParticle::VeloCharge, -998 ); }},
      {"BPVIPCHI2",
       [this]() {
         // use same defaults as in DistanceCalculator docs
         double ip = -1 * Gaudi::Units::km, ipchi2 = -1000;
         auto   bestVertex = m_DVAlgorithm->bestVertex( m_taggingParticle );
         m_distanceCalculatorTool->distance( m_taggingParticle, bestVertex, ip, ipchi2 )
             .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
         return ipchi2;
       }},
      {"BPVIP",
       [this]() {
         // use same defaults as in DistanceCalculator docs
         double ip = -1 * Gaudi::Units::km, ipchi2 = -1000;
         auto   bestVertex = m_DVAlgorithm->bestVertex( m_taggingParticle );
         m_distanceCalculatorTool->distance( m_taggingParticle, bestVertex, ip, ipchi2 ).ignore();
         return ip;
       }},
      // @TODO dont hard-code these kind of simple calculations
      {"PROBNNe-PROBNNpi",
       [this]() {
         auto proto = m_taggingParticle->proto();
         return proto->info( LHCb::ProtoParticle::ProbNNe, -1 ) - proto->info( LHCb::ProtoParticle::ProbNNpi, -1 );
       }},
      {"PROBNNk-PROBNNp",
       [this]() {
         auto proto = m_taggingParticle->proto();
         return proto->info( LHCb::ProtoParticle::ProbNNk, -1 ) - proto->info( LHCb::ProtoParticle::ProbNNp, -1 );
       }},
      {"PROBNNk-PROBNNpi",
       [this]() {
         auto proto = m_taggingParticle->proto();
         return proto->info( LHCb::ProtoParticle::ProbNNk, -1 ) - proto->info( LHCb::ProtoParticle::ProbNNpi, -1 );
       }},
  };

  /** @brief set a GaudiTool which will be used for tool initialization
   * @param[in] provider the GaudiTool instance.
   */
  void setToolProvider( const GaudiTool* provider );

  /** @brief add a specific tool instance to the FeatureGenerator.
   * This is currently implemented for
   *  - ILokiWrapper
   *  - ITaggingUtils
   *  - IParticleDescendants
   *  - IMonteCarloFeatures
   *  - ICaloElectrons
   * @param[in] tool the tool instance.
   */
  template <class T>
  void setTool( T* tool );

  /** @brief receive a vector of feature names.
   * @return vector of feature names.
   */
  const std::vector<std::string>& getFeatureNames() const;

  /** Retrieve the full matrix of tagging particle features.
   * Make sure you called generateFeatures(), generateMVAFeautures(), or
   * generateSelectionFeatures() before invoking this function. Otherwise
   * the matrix might be empty or invalid.
   */
  const std::vector<std::vector<double>>& getFullFeatureMatrix();

  /* Parse an MVA feature of format `mva_*ITaggingClassifierToolName*(0,1,2)`
   * where *ITaggingClassifierToolName* is the name of a configured ITaggingClassifierTool which will be called with
   * features 0,1,2 of the current feature values.
   *
   * @param[in] fullName
   *   The full feature name (with prefix and indices)
   * @param[out] inputFeatureNames
   *   The indices contained in the featurename.
   * @param[out] mvaToolName
   *   The GaudiTool name which will be initialized to compute the MVA
   *   response. It is extracted from the full name and the tool will be
   *   added to the current m_toolProvider.
   */
  static void parseMVAFeatureName( const std::string& fullName, std::vector<std::string>& inputFeatureNames,
                                   std::string& mvaToolName );

  /** @brief extracts and strips transformation functions of feature names
   *
   * @param[in, out] input
   *   the vector of feature names, may contain transformations such as
   *   `log(X)`
   * @param[in] strip (default=true)
   *   whether or not to strip the transformation function from the feature
   *   name
   * @return
   *   the vector of transformation functions, e.g. `log` in the above
   *   example
   */
  static std::vector<std::string> stripMVAFeatureTransformations( std::vector<std::string>& input, bool strip = true );

  /* @brief Checks whether `featureName` can be interpreted as an MVA
   * feature
   */
  static bool featureIsMVAFeature( const std::string& featureName );

  /** @brief calculate and return the feature at the given position.
   * This function directly calculate the feature without any caching, etc.
   * @param[in] pos index of feature to update.
   * @param[in, out] value input/output value. Will be updated if it is a
   *   non-manual feature, otherwise the corresponding internal value will be
   *   set to the given value.
   */
  void updateFeatureAtPosition( const size_t& pos, double& value );

  /** prefix used for manual features
   */
  static const std::string m_manualFeaturePrefix;

private:
  /* @brief the list of all currently used functors (lambdas) */
  std::vector<std::function<double()>> m_activeFunctors;

  /* @brief the latest feature values */
  std::vector<double> m_features = {};

  /* @brief indices of the feature subsets within the feature vector.
   * The last index set `m_featureIndices` is just a dumb range.
   **/
  std::vector<size_t> m_featureIndices;

  std::vector<std::string>           m_activeFeatureNames, m_activeFeatureAliases;
  std::map<std::string, std::string> m_aliasMap, m_reverseAliasMap;

  /* @brief Store the index of a given feature name (or alias) for quick
   * access
   **/
  std::unordered_map<std::string, size_t> m_nameIndexMap;

  const LHCb::Particle *              m_signalCandidate = nullptr, *m_taggingParticle = nullptr;
  const LHCb::RecVertex*              m_reconstructionVertex  = nullptr;
  std::vector<const LHCb::Particle*>  m_otherTaggingParticles = {};
  std::vector<const LHCb::RecVertex*> m_pileUpVertices        = {};
  const LHCb::RecVertex::Range*       m_otherVertices         = nullptr;

  const static boost::regex m_manualFeatureRegexPattern, m_MVAParsingRegexPattern, m_MVATransformationRegexPattern,
      m_ANNPIDFeaturePattern;

  bool m_featuresValid = false;

  ILoKiWrapper*                          m_LoKiWrapper            = nullptr;
  ITaggingUtils*                         m_utils                  = nullptr;
  IParticleDescendants*                  m_descendantsTool        = nullptr;
  IMonteCarloFeatures*                   m_mcFeatures             = nullptr;
  ICaloElectron*                         m_caloElectronTool       = nullptr;
  ANNGlobalPID::IChargedProtoANNPIDTool* m_ANNPIDTool             = nullptr;
  IDistanceCalculator*                   m_distanceCalculatorTool = nullptr;
  IDVAlgorithm*                          m_DVAlgorithm            = nullptr;

  /* @brief will hold an instance of ITaggingClassifier as soon as a
   * MVA feature is initialized. See initilizeMVAFeature().
   */
  std::unique_ptr<ITaggingClassifier> m_classifier = nullptr;

  /* @brief hold a vector of preparators which transform features for
   * eventual mva feature inputs
   */
  std::vector<std::unique_ptr<FeaturePreparation>> m_preparators = {};

  void                                generateFeatures( const std::vector<size_t>& indices );
  size_t                              activateFunctor( std::string featureName );
  std::function<double()>             functorFromString( const std::string& featureName );
  bool                                matchRegexPrefix( const std::string& s, const boost::regex& ex );
  std::function<double()>             initializeMVAFeature( const std::string& featureName );
  std::function<double()>             initializeANNPIDFunctor( const std::string& featureName );
  IMonteCarloFeatures*                getMCFeatures();
  const LHCb::RecVertex::Range*       getOtherVertices();
  std::vector<const LHCb::RecVertex*> getPileUpVertices();
  void                                invalidateFeatures();
  void                                invalidateCache();
  std::vector<size_t>                 activateFeatureSet( const std::vector<std::string>& featureNames );

  // @brief A cache for all tagging particle features for one
  // Signal-Candidat + RecVertex pair
  std::vector<std::vector<double>> m_featureCache = {};

  // @brief add a vector of feautures to the cache
  // the order of this vector must match the order of feature names
  void addToCache( const std::vector<double>& values );

  const GaudiTool* m_toolProvider = nullptr;

  // store which features are manual
  std::unordered_map<size_t, bool> m_manualFeatureIndicesMap = {{}};

  // pid particle name resolution for ANNPID tool
  static const std::unordered_map<std::string, std::pair<LHCb::ParticleID, LHCb::ProtoParticle::additionalInfo>>
      m_pidNamingMap;
};
