/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "FeatureGenerator.h"
#include "src/Classification/ITaggingClassifierFactory.h"
#include "src/Utils/TaggingHelpers.h"

const boost::regex FeatureGenerator::m_manualFeatureRegexPattern = boost::regex( R"(^man_)", boost::regex::icase );

const std::string FeatureGenerator::m_manualFeaturePrefix = "man_";

// see https://regex101.com/r/jb1FfY/5 for regex details
const boost::regex FeatureGenerator::m_MVAParsingRegexPattern =
    boost::regex( R"(^mva\[([\w\/]+)\]\(([\w,\s\(\)]+)\))", boost::regex::icase );

// see https://regex101.com/r/1LzsLn/1 for details
const boost::regex FeatureGenerator::m_MVATransformationRegexPattern =
    boost::regex( R"(^(\w+)\(([\w\s\/]+)\))", boost::regex::icase );

// see https://regex101.com/r/jgyjlq/4 for details
const boost::regex FeatureGenerator::m_ANNPIDFeaturePattern =
    boost::regex( R"(^PROBNN([a-zA-Z]+)(_(\w+))?$)", boost::regex::icase );

const std::unordered_map<std::string, std::pair<LHCb::ParticleID, LHCb::ProtoParticle::additionalInfo>>
    FeatureGenerator::m_pidNamingMap = {
        {"e", {LHCb::ParticleID( 11 ), LHCb::ProtoParticle::additionalInfo::ProbNNe}},
        {"mu", {LHCb::ParticleID( 13 ), LHCb::ProtoParticle::additionalInfo::ProbNNmu}},
        {"pi", {LHCb::ParticleID( 211 ), LHCb::ProtoParticle::additionalInfo::ProbNNpi}},
        {"k", {LHCb::ParticleID( 321 ), LHCb::ProtoParticle::additionalInfo::ProbNNk}},
        {"p", {LHCb::ParticleID( 2212 ), LHCb::ProtoParticle::additionalInfo::ProbNNp}},
        {"ghost", {LHCb::ParticleID( 0 ), LHCb::ProtoParticle::additionalInfo::ProbNNghost}},
};

void FeatureGenerator::setParticles( const LHCb::Particle* B0Candidate, const LHCb::Particle* taggingParticle,
                                     const LHCb::RecVertex*                     reconstructionVertex,
                                     const std::vector<const LHCb::Particle*>&  otherParticles,
                                     const std::vector<const LHCb::RecVertex*>& pileUpVertices,
                                     const LHCb::RecVertex::Range*              otherVertices ) {
  invalidateFeatures();
  invalidateCache();
  m_signalCandidate       = B0Candidate;
  m_taggingParticle       = taggingParticle;
  m_reconstructionVertex  = reconstructionVertex;
  m_otherTaggingParticles = otherParticles;
  m_otherVertices         = otherVertices;
  m_pileUpVertices        = pileUpVertices;
}

void FeatureGenerator::setTaggingParticle( const LHCb::Particle* taggingParticle ) {
  invalidateFeatures();
  m_taggingParticle = taggingParticle;
}

void FeatureGenerator::setTaggingParticleVector( const std::vector<const LHCb::Particle*>& otherParticles ) {
  invalidateFeatures();
  invalidateCache();
  m_otherTaggingParticles = otherParticles;
}

void FeatureGenerator::setSignalCandidate( const LHCb::Particle* signalCandidate ) {
  invalidateFeatures();
  invalidateCache();
  m_signalCandidate = signalCandidate;

  // Temporary
  // @TODO: update the mctools particles somewhere else
  // just deactivate monte carlo truth information for now
  // if(m_mcFeatures) m_mcFeatures->readParticles();
}

void FeatureGenerator::setPileUpVertices( const std::vector<const LHCb::RecVertex*>& vertices ) {
  invalidateFeatures();
  invalidateCache();
  FeatureGeneratorCache::getInstance().currentPileUpVertices = vertices;
  m_pileUpVertices                                           = vertices;
}

void FeatureGenerator::setOtherVertices( const LHCb::RecVertex::Range* vertices ) {
  invalidateFeatures();
  invalidateCache();
  FeatureGeneratorCache::getInstance().currentOtherVertices = vertices;
  m_otherVertices                                           = vertices;
}

void FeatureGenerator::setFeatureNames( const std::vector<std::string>& featureNames ) {
  m_manualFeatureIndicesMap.clear();
  m_featureIndices = activateFeatureSet( featureNames );
}

void FeatureGenerator::setFeatureNames( const std::vector<std::string>&           featureNames,
                                        const std::map<std::string, std::string>& featureAliases ) {
  setFeatureAliases( featureAliases );
  setFeatureNames( featureNames );
}

void FeatureGenerator::setFeatureAliases( const std::map<std::string, std::string>& featureAliases ) {
  m_aliasMap = featureAliases;
  for ( auto& elem : m_aliasMap ) {
    m_reverseAliasMap.insert( std::pair<std::string, std::string>( elem.second, elem.first ) );
  }
}

std::vector<size_t> FeatureGenerator::activateFeatureSet( const std::vector<std::string>& featureNames ) {
  invalidateFeatures();

  std::vector<size_t> indices;
  indices.reserve( featureNames.size() );

  for ( auto& name : featureNames ) { indices.push_back( activateFunctor( name ) ); }

  return indices;
}

void FeatureGenerator::setFeatureValue( std::vector<double>& featureVector, const std::string& name, double value ) {
  featureVector.at( m_nameIndexMap.at( name ) ) = value;
  /* auto nameIterator = std::find(m_activeFeatureNames.begin(),
                                m_activeFeatureNames.end(),
                                name);
  if(nameIterator != m_activeFeatureNames.end()){
    featureVector.at(nameIterator - m_activeFeatureNames.begin()) = value;
  } else {
    auto nameIterator = std::find(m_activeFeatureAliases.begin(),
                                  m_activeFeatureAliases.end(),
                                  name);
    if(nameIterator != m_activeFeatureAliases.end()){
      featureVector.at(nameIterator - m_activeFeatureAliases.begin()) = value;
    }
  } */
}

double FeatureGenerator::getFeatureValue( const std::vector<double>& featureVector, const std::string& name ) {
  return featureVector.at( m_nameIndexMap.at( name ) );
}

std::vector<double> FeatureGenerator::generateFeatures() {
  if ( !m_featuresValid ) {
    generateFeatures( m_featureIndices );
    addToCache( m_features );
    m_featuresValid = true;
  }
  return m_features;
}

std::vector<double> FeatureGenerator::generateFeatures( const LHCb::Particle* taggingParticle ) {
  setTaggingParticle( taggingParticle );
  return generateFeatures();
}

void FeatureGenerator::generateFeatures( const std::vector<size_t>& indices ) {
  for ( auto& i : indices ) { m_features.at( i ) = m_activeFunctors.at( i )(); }
}

void FeatureGenerator::updateFeatures( std::vector<double>& values ) {
  assert( values.size() >= m_featureIndices.size() );
  size_t valueIndex = 0;
  for ( auto& i : m_featureIndices ) {
    if ( !m_manualFeatureIndicesMap.at( i ) ) { values.at( valueIndex ) = m_activeFunctors.at( i )(); }
    m_features.at( i ) = values.at( valueIndex++ );
  }
}

size_t FeatureGenerator::activateFunctor( std::string featureName ) {
  // Get the raw name and store the aliased one in the featureName variable
  auto fullName = featureName;
  if ( m_aliasMap.find( featureName ) != m_aliasMap.end() ) {
    // resolve alias, if there is one
    fullName = m_aliasMap.at( featureName );
    BOOST_LOG_TRIVIAL( debug ) << "[FeatureGenerator::activateFunctor] "
                               << "feature " << featureName << " has full name " << fullName;
  } else if ( m_reverseAliasMap.find( featureName ) != m_reverseAliasMap.end() ) {
    // also look if given feature has an alias
    featureName = m_reverseAliasMap.at( featureName );
  }

  BOOST_LOG_TRIVIAL( debug ) << "[FeatureGenerator::activateFunctor] "
                             << "activating functor " << fullName << " with alias " << featureName;

  // the return value
  size_t index;

  auto fNameIterator = std::find( m_activeFeatureNames.begin(), m_activeFeatureNames.end(), fullName );

  if ( fNameIterator != m_activeFeatureNames.end() ) {
    // if the functor is already active just store and return the index of it
    index = fNameIterator - m_activeFeatureNames.begin();
    BOOST_LOG_TRIVIAL( debug ) << "[FeatureGenerator::activateFunctor] functor " << fullName << " (" << featureName
                               << ")"
                               << " was already added at index " << index;
  } else {
    // otherwise, try to activate it
    m_activeFunctors.push_back( functorFromString( fullName ) );

    // and perform a lot of bookkeeping
    m_activeFeatureNames.push_back( fullName );
    m_activeFeatureAliases.push_back( featureName );
    m_features.push_back( 0 );
    index = m_features.size() - 1;
    m_manualFeatureIndicesMap.emplace( index, matchRegexPrefix( fullName, m_manualFeatureRegexPattern ) );
    if ( !m_nameIndexMap.count( featureName ) ) m_nameIndexMap.insert( {featureName, index} );
    if ( !m_nameIndexMap.count( fullName ) ) m_nameIndexMap.insert( {fullName, index} );
    BOOST_LOG_TRIVIAL( debug ) << "[FeatureGenerator::activateFunctor] functor " << fullName << " (" << featureName
                               << ")"
                               << " has been added at index " << index;
  }
  return index;
}

std::function<double()> FeatureGenerator::functorFromString( const std::string& fullName ) {
  auto functorIterator = functorDefinitions.find( fullName );
  if ( functorIterator != functorDefinitions.end() ) {
    return functorIterator->second;
  } else if ( matchRegexPrefix( fullName, m_manualFeatureRegexPattern ) ) {
    return []() -> double { return -1; };
  } else if ( matchRegexPrefix( fullName, m_ANNPIDFeaturePattern ) ) {
    return initializeANNPIDFunctor( fullName );
  } else if ( featureIsMVAFeature( fullName ) ) {
    BOOST_LOG_TRIVIAL( debug ) << "[FeatureGenerator::activateFunctor] `" << fullName
                               << " is mva feature, trying to activate now";
    return initializeMVAFeature( fullName );
  } else if ( m_LoKiWrapper ) {
    try {
      auto functor = m_LoKiWrapper->getFunctor( fullName );
      return functor;
    } catch ( const std::invalid_argument& e ) {
      // handle LoKiWrappers error here, if there are more options to
      // parse the featureName
      throw e;
    }
  } else {
    throw std::invalid_argument( "Requested feature not found. To define a "
                                 "manual feature, use prefix `" +
                                 m_manualFeaturePrefix +
                                 "`. "
                                 "You might also want to pass an instance of `LoKiWrapper` to this"
                                 "FeatureGenerator to look for LoKi functors." );
  }
}

bool FeatureGenerator::featureIsMVAFeature( const std::string& featureName ) {
  return !TaggingHelpers::matchRegex( featureName, m_MVAParsingRegexPattern ).empty();
}

bool FeatureGenerator::matchRegexPrefix( const std::string& s, const boost::regex& ex ) {
  std::string::const_iterator                       start( s.begin() ), end( s.end() );
  boost::match_results<std::string::const_iterator> what;
  return boost::regex_search( start, end, what, ex );
}

std::function<double()> FeatureGenerator::initializeMVAFeature( const std::string& featureName ) {
  if ( !m_toolProvider )
    throw std::invalid_argument( "You tried to access a MVA/Classifier "
                                 "variable, but the `m_toolProvider` tool is not initilized "
                                 "correctly. Check if you have called FeatureGenerator's"
                                 "`setToolProvider()` with a properly configured "
                                 "GaudiTool before initializing any MVA features." );

  // feature name parsing. See regex definition for details
  std::vector<std::string> inputFeatureNames;
  std::string              mvaToolName;
  parseMVAFeatureName( featureName, inputFeatureNames, mvaToolName );

  auto transformations = stripMVAFeatureTransformations( inputFeatureNames );

  auto transformator = std::make_shared<FeaturePreparation>();
  transformator->setFunctions( transformations );

  m_classifier = m_toolProvider->tool<ITaggingClassifierFactory>( mvaToolName, m_toolProvider )->taggingClassifier();

  std::vector<size_t> indices;
  for ( auto& n : inputFeatureNames ) {
    auto man_featureName = m_manualFeaturePrefix + n;
    if ( m_nameIndexMap.count( n ) == 1 ) {
      indices.push_back( m_nameIndexMap.at( n ) );
    } else if ( m_nameIndexMap.count( man_featureName ) ) {
      indices.push_back( m_nameIndexMap.at( man_featureName ) );
    } else {
      throw std::invalid_argument( "Specify feature `" + n +
                                   "` before using it"
                                   " as an MVA input variable." );
    }
  }

  // define and return mva function
  return [this, indices, transformator]() -> double {
    std::vector<double> inputValues;
    inputValues.reserve( indices.size() );
    for ( auto& i : indices ) { inputValues.push_back( m_features.at( i ) ); }
    return m_classifier->getClassifierValue( transformator->prepareFeatures( inputValues ) );
  };
}

std::function<double()> FeatureGenerator::initializeANNPIDFunctor( const std::string& featureName ) {
  if ( !m_toolProvider )
    throw std::invalid_argument( "You tried to access a ANNPID feature "
                                 "variable, but the `m_toolProvider` tool is not initilized "
                                 "correctly. Check if you have called FeatureGenerator's"
                                 "`setToolProvider()` with a properly configured "
                                 "GaudiTool before initializing any MVA features." );

  if ( !m_ANNPIDTool ) {
    m_ANNPIDTool = m_toolProvider->tool<ANNGlobalPID::IChargedProtoANNPIDTool>( "ANNGlobalPID::ChargedProtoANNPIDTool",
                                                                                "ChargedProtoANNPID" );
  }

  // parse the feature name
  auto matches            = TaggingHelpers::matchRegex( featureName, m_ANNPIDFeaturePattern );
  auto particleTypeString = matches.at( 1 );
  auto tuningString       = matches.at( 3 );

  if ( tuningString.empty() ) {
    // if no explicit tuning is given, return the ProbNN stored in
    // additionalInfo (same as LoKi functor ProbNN variables)
    auto infoKey = m_pidNamingMap.at( particleTypeString ).second;
    return [this, infoKey]() -> double {
      auto proto = m_taggingParticle->proto();
      if ( !proto ) {
        // @TODO warn user
        return -1000;
      } else {
        // using the same default value as in LoKi functor here
        return m_taggingParticle->proto()->info( infoKey, -1 );
      }
    };
  } else {
    // otherwise use the ANNPIDTool to compute the correct pid
    auto pid = m_pidNamingMap.at( particleTypeString ).first;

    return [this, pid, tuningString]() -> double {
      return m_ANNPIDTool->annPID( m_taggingParticle->proto(), pid, tuningString ).value;
    };
  }
}

void FeatureGenerator::parseMVAFeatureName( const std::string& fullName, std::vector<std::string>& inputFeatureNames,
                                            std::string& mvaToolName ) {
  auto matches     = TaggingHelpers::matchRegex( fullName, m_MVAParsingRegexPattern );
  mvaToolName      = matches.at( 1 );
  auto indexString = matches.at( 2 );
  boost::split( inputFeatureNames, indexString, boost::is_any_of( "," ) );

  BOOST_LOG_TRIVIAL( trace ) << "[featureGenerator::parseMVAFeatureName] "
                             << "parsed input features for mva";
  for ( auto& s : inputFeatureNames ) {
    boost::trim( s );
    BOOST_LOG_TRIVIAL( trace ) << "[featureGenerator::parseMVAFeatureName] " << s;
  }
}

void FeatureGenerator::setToolProvider( const GaudiTool* provider ) { m_toolProvider = provider; }

template <>
void FeatureGenerator::setTool( ILoKiWrapper* tool ) {
  m_LoKiWrapper = tool;
  m_LoKiWrapper->setOutlets( &m_taggingParticle, &m_signalCandidate, &m_reconstructionVertex );
}

template <>
void FeatureGenerator::setTool( ITaggingUtils* tool ) {
  m_utils = tool;
}

template <>
void FeatureGenerator::setTool( IParticleDescendants* tool ) {
  m_descendantsTool = tool;
}

template <>
void FeatureGenerator::setTool( IMonteCarloFeatures* tool ) {
  m_mcFeatures = tool;
}

template <>
void FeatureGenerator::setTool( ICaloElectron* tool ) {
  m_caloElectronTool = tool;
}

template <>
void FeatureGenerator::setTool( IDistanceCalculator* tool ) {
  m_distanceCalculatorTool = tool;
}

template <>
void FeatureGenerator::setTool( IDVAlgorithm* tool ) {
  m_DVAlgorithm = tool;
}

IMonteCarloFeatures* FeatureGenerator::getMCFeatures() {
  if ( m_mcFeatures ) {
    return m_mcFeatures;
  } else {
    throw std::invalid_argument( "You try to access MC information but did not "
                                 "specify a `IMonteCarloFeatures`. Please use "
                                 "`setMonteCarloFeatures()` to set a tool." );
  }
}

std::vector<const LHCb::RecVertex*> FeatureGenerator::getPileUpVertices() {
  if ( m_pileUpVertices.size() )
    return m_pileUpVertices;
  else
    return FeatureGeneratorCache::getInstance().currentPileUpVertices;
}

const LHCb::RecVertex::Range* FeatureGenerator::getOtherVertices() {
  if ( m_otherVertices && m_otherVertices->size() )
    return m_otherVertices;
  else
    return FeatureGeneratorCache::getInstance().currentOtherVertices;
}

void FeatureGenerator::setReconstructionVertex( const LHCb::RecVertex*& reconstrucionVertex ) {
  m_reconstructionVertex = reconstrucionVertex;
}

void FeatureGenerator::invalidateFeatures() { m_featuresValid = false; }

void FeatureGenerator::invalidateCache() {
  m_featureCache.clear();
  m_featureCache = std::vector<std::vector<double>>( m_features.size() );
}

const std::vector<std::string>& FeatureGenerator::getFeatureNames() const { return m_activeFeatureAliases; }

void FeatureGenerator::addToCache( const std::vector<double>& values ) {
  if ( !m_featureCache.size() ) m_featureCache = std::vector<std::vector<double>>( values.size() );
  for ( size_t i = 0; i < values.size(); ++i ) { m_featureCache.at( i ).push_back( values.at( i ) ); }
}

const std::vector<std::vector<double>>& FeatureGenerator::getFullFeatureMatrix() { return m_featureCache; }

std::vector<std::string> FeatureGenerator::stripMVAFeatureTransformations( std::vector<std::string>& input,
                                                                           bool                      strip ) {
  std::vector<std::string> transformations;
  transformations.reserve( input.size() );
  for ( auto& fullName : input ) {
    auto matches = TaggingHelpers::matchRegex( fullName, m_MVATransformationRegexPattern );
    if ( matches.empty() ) {
      transformations.push_back( "" );
    } else {
      auto transformName = matches.at( 1 ), featureName = matches.at( 2 );
      if ( strip ) fullName = featureName;
      transformations.push_back( transformName );
    }
  }
  return transformations;
}

void FeatureGenerator::updateFeatureAtPosition( const size_t& pos, double& value ) {
  auto index = m_featureIndices.at( pos );
  if ( !m_manualFeatureIndicesMap.at( index ) ) { value = m_activeFunctors.at( index )(); }
  m_features.at( index ) = value;
}
