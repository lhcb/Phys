/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Event/Particle.h"

static const InterfaceID IID_ILoKiWrapper( "ILoKiWrapper", 1, 0 );

class ILoKiWrapper : virtual public IAlgTool {
public:
  static const InterfaceID&       interfaceID() { return IID_ILoKiWrapper; }
  virtual std::function<double()> getFunctor( const std::string& functorName ) = 0;
  virtual void setOutlets( const LHCb::Particle* const* pTaggingParticle, const LHCb::Particle* const* pSignalCandidate,
                           const LHCb::RecVertex* const* pVertexBase )         = 0;
};
