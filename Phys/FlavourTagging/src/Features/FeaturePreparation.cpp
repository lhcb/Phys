/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "FeaturePreparation.h"

const std::unordered_map<std::string, std::function<double( double )>> FeaturePreparation::m_functions = {
    {"", []( double v ) { return v; }},
    {"log",
     []( double v ) {
       if ( v <= 0 )
         return -999.0;
       else
         return std::log( v );
     }},
    {"ilog",
     []( double v ) {
       if ( v >= 1 )
         return -999.0;
       else
         return std::log( 1 - v );
     }},
    {"logit",
     []( double v ) {
       if ( v <= 0 or v >= 1 )
         return 0.0;
       else
         return std::log( v ) - std::log( 1 - v );
     }},
};

void FeaturePreparation::setFunctions( const std::vector<std::string>&           featureNames,
                                       const std::map<std::string, std::string>& functionMap ) {
  m_activeFunctions.clear();
  m_activeFunctions.reserve( featureNames.size() );
  for ( auto& featureName : featureNames ) {
    auto selectedFunction           = m_functions.at( "" );
    auto correspondingFunctionTuple = functionMap.find( featureName );
    if ( correspondingFunctionTuple != functionMap.end() ) {
      auto localFunctionTuple = m_functions.find( correspondingFunctionTuple->second );
      if ( localFunctionTuple != m_functions.end() ) { selectedFunction = localFunctionTuple->second; }
    }
    m_activeFunctions.push_back( selectedFunction );
  }
}

void FeaturePreparation::setFunctions( const std::vector<std::string>& transformations ) {
  m_activeFunctions.clear();
  m_activeFunctions.reserve( transformations.size() );
  for ( auto& transformation : transformations ) { m_activeFunctions.push_back( m_functions.at( transformation ) ); }
}

std::vector<double> FeaturePreparation::prepareFeatures( std::vector<double> features ) {
  size_t i = 0;
  for ( auto& v : features ) { v = m_activeFunctions.at( i++ )( v ); }
  return features;
}
