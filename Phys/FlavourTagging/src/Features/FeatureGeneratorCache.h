/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include <memory>
#include <mutex>
#include <vector>

#include "Event/RecVertex.h"

struct FeatureGeneratorCache {
public:
  static FeatureGeneratorCache& getInstance() {
    // see http://stackoverflow.com/a/11711991/2834918
    static FeatureGeneratorCache instance;
    return instance;
  }

  std::vector<const LHCb::RecVertex*> currentPileUpVertices;
  const LHCb::RecVertex::Range*       currentOtherVertices;

  FeatureGeneratorCache( const FeatureGeneratorCache& ) = delete;
  FeatureGeneratorCache( FeatureGeneratorCache&& )      = delete;
  FeatureGeneratorCache& operator=( const FeatureGeneratorCache& ) = delete;
  FeatureGeneratorCache& operator=( FeatureGeneratorCache&& ) = delete;

protected:
  FeatureGeneratorCache() {}
  ~FeatureGeneratorCache() {}
};
