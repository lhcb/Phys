/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Event/Particle.h"
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/StatusCode.h"
#include "LoKi/IHybridFactory.h"
#include "LoKi/PhysTypes.h"

#include <functional>
#include <map>
#include <string>
#include <vector>

#include "boost/algorithm/string/predicate.hpp"

#include "ILoKiWrapper.h"

class LoKiWrapper : public GaudiTool, virtual public ILoKiWrapper {
public:
  LoKiWrapper( const std::string& type, const std::string& name, const IInterface* interface );

  virtual StatusCode initialize() override;

  virtual std::function<double()> getFunctor( const std::string& functorName ) override;

  virtual void setOutlets( const LHCb::Particle* const* pTaggingParticle, const LHCb::Particle* const* pSignalCandidate,
                           const LHCb::RecVertex* const* pRecVertex ) override;

private:
  LoKi::IHybridFactory*         m_factory           = nullptr;
  const LHCb::Particle* const*  m_pTaggingParticle  = nullptr;
  const LHCb::Particle* const*  m_pSignalCandidate  = nullptr;
  const LHCb::RecVertex* const* m_pAssociatedVertex = nullptr;
};
