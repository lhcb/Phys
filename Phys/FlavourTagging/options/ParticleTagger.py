###############################################################################
# (c) Copyright 2020-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
 Tagging sequences: provides variables specific for FT  training.
 Sequences can be imported in any otpion file and appended to DecayTreeTuple sequance
"""
__author__ = "Basem Khanji <basem.khanji@cern.ch>"

##############################################################################################
##### SETTINGS
from Configurables import DaVinci, LoKi__Hybrid__TupleTool, LoKi__Hybrid__ArrayTupleTool, FilterDesktop
from PhysSelPython.Wrappers import (AutomaticData, Selection,
                                    SelectionSequence, DataOnDemand, Selection,
                                    MergedSelection)
import GaudiKernel.SystemOfUnits as Units
##############################################################################################

##############################################################################################
UdrlyTr_Suffix = 'Tr_T_'
loki_var_udrlyTr = {
    UdrlyTr_Suffix + "PT":
    "PT",
    UdrlyTr_Suffix + "PX":
    "PX",
    UdrlyTr_Suffix + "PY":
    "PY",
    UdrlyTr_Suffix + "PZ":
    "PZ",
    UdrlyTr_Suffix + "P":
    "P",
    UdrlyTr_Suffix + "E":
    "E",
    UdrlyTr_Suffix + "Eta":
    "ETA",
    UdrlyTr_Suffix + "THETA":
    "atan ((PT/PZ))",
    UdrlyTr_Suffix + "Phi":
    "PHI",
    UdrlyTr_Suffix + "MinIP":
    "MIPDV(PRIMARY)",
    UdrlyTr_Suffix + "MinIPChi2":
    "MIPCHI2DV(PRIMARY)",
    UdrlyTr_Suffix + "BPVIP":
    "BPVIP()",
    UdrlyTr_Suffix + "BPVIPCHI2":
    "BPVIPCHI2()",
    UdrlyTr_Suffix + "PIDe":
    "PIDe",
    UdrlyTr_Suffix + "PIDmu":
    "PIDmu",
    UdrlyTr_Suffix + "PIDp":
    "PIDp",
    UdrlyTr_Suffix + "PIDK":
    "PIDK",
    UdrlyTr_Suffix + "PROBNNe":
    "PROBNNe",  #"PPINFO(PROBNNe)"
    UdrlyTr_Suffix + "PROBNNmu":
    "PROBNNmu",  #"PPINFO(PROBNNmu)"
    UdrlyTr_Suffix + "PROBNNk":
    "PROBNNk",  #"PPINFO(PROBNNk)"
    UdrlyTr_Suffix + "PROBNNpi":
    "PROBNNpi",  #"PPINFO(PROBNNpi)"
    UdrlyTr_Suffix + "PROBNNp":
    "PROBNNp",  #"PPINFO(PROBNNp)"
    UdrlyTr_Suffix + "PROBNNghost":
    "PROBNNghost",  #"PPINFO(PROBNNghost)"
    UdrlyTr_Suffix + "TRCHI2DOF":
    "TRCHI2DOF",
    UdrlyTr_Suffix + "TRPCHI2":
    "TRPCHI2",
    UdrlyTr_Suffix + "TRTYPE":
    "TRTYPE",
    #UdrlyTr_Suffix + "MC_KEY"           : "KEY",
    #UdrlyTr_Suffix + "MC_MOTHER"        : "MCMOTHER(MCID,-1e6)",
    #UdrlyTr_Suffix + "MC_GD_MOTHER"     : "MCMOTHER[MCMOTHER]",
    UdrlyTr_Suffix + "Charge":
    "Q",
    UdrlyTr_Suffix + "TRCLONEDIST":
    "CLONEDIST",
    UdrlyTr_Suffix + "TRGHOSTPROB":
    "TRGHOSTPROB",
    UdrlyTr_Suffix + "TrFIRSTHITZ":
    "TRFUN( TrFIRSTHITZ )",
    UdrlyTr_Suffix + "TrFITTCHI2NDOF":
    "TRFUN( TrFITTCHI2/TrFITTNDOF )",
    UdrlyTr_Suffix + "TRFITVELOCHI2NDOF":
    "TRFUN( TrFITVELOCHI2/TrFITVELONDOF )",
    UdrlyTr_Suffix + "TRFITTCHI2":
    "TRFUN( TrFITTCHI2 )",
    UdrlyTr_Suffix + "TRFITMATCHCHI2":
    "TRFUN( TrFITMATCHCHI2 )",
    UdrlyTr_Suffix + "HASMUON":
    "switch( HASMUON , 1., 0.)",
    UdrlyTr_Suffix + "ISMUON":
    "switch( ISMUON  , 1., 0.)",
    UdrlyTr_Suffix + "HASRICH":
    "switch( HASRICH , 1., 0.)",
    UdrlyTr_Suffix + "HcalE":
    "PPINFO( LHCb.ProtoParticle.CaloHcalE , -10)",
    UdrlyTr_Suffix + "EcalE":
    "PPINFO( LHCb.ProtoParticle.CaloEcalE , -10)",
    UdrlyTr_Suffix + "PrsE":
    "PPINFO( LHCb.ProtoParticle.CaloPrsE , -10)",
    UdrlyTr_Suffix + "NSHARED":
    "PPINFO( LHCb.ProtoParticle.MuonNShared , -1)",
    UdrlyTr_Suffix + "VeloCharge":
    "PPINFO( LHCb.ProtoParticle.VeloCharge , -10)"
    #,UdrlyTr_Suffix + "AALLSAMEBPV"     : "switch(ALLSAMEBPV , 0,1)"
}
loki_Avar_udrlyTr = {
    UdrlyTr_Suffix + "ADOCA": "ADOCA(1,2)",
    UdrlyTr_Suffix + "ACHI2DOCA": "ACHI2DOCA(1,2)",
    UdrlyTr_Suffix + "AALLSAMEBPV": "switch(AALLSAMEBPV( -1, -1 , 0.8  ),0,1)"
}

##############################################################################################


##############################################################################################
def createVeloTracks():
    from Configurables import ChargedProtoParticleMaker, DaVinci
    veloprotos = ChargedProtoParticleMaker("ProtoPMaker")
    veloprotos.Inputs = ["Rec/Track/Best"]
    veloprotos.Output = "Rec/ProtoP/myProtoPMaker/ProtoParticles"
    DaVinci().appendToMainSequence([veloprotos])
    from Configurables import ProtoParticleCALOFilter, CombinedParticleMaker, NoPIDsParticleMaker
    from CommonParticles.Utils import trackSelector, updateDoD
    algorithm = NoPIDsParticleMaker(
        'StdNoPIDsVeloPions',
        Particle='pion',
    )
    algorithm.Input = "Rec/ProtoP/myProtoPMaker/ProtoParticles"
    selector = trackSelector(algorithm, trackTypes=['Velo'])
    locations = updateDoD(algorithm)
    DaVinci().appendToMainSequence([algorithm])


def addParticleTaggerTuple(tuple, decay):
    createVeloTracks()
    LongPions = DataOnDemand(Location='Phys/StdAllNoPIDsPions/Particles')
    UpStreamPions = DataOnDemand(Location='Phys/StdNoPIDsUpPions/Particles')
    DnStreamPions = DataOnDemand(Location='Phys/StdNoPIDsDownPions/Particles')
    # VeloPions     = DataOnDemand( Location ='Phys/StdNoPIDsVeloPions/Particles')
    AllPions = MergedSelection(
        "AllPions" + decay.tuple_name,
        RequiredSelections=[LongPions, UpStreamPions,
                            DnStreamPions])  # , VeloPions ] )
    #AllPion_fd    = FilterDesktop("AllPion_fd"+ decay.tuple_name , Code = "(HASPROTO) & (HASTRACK) & (ISBASIC) & (switch(INTES('"+ decay.inputs +"', False) , 0 , 1)>0)") #
    #AllPions_sel  = Selection( "AllPions_sel"+ decay.tuple_name , Algorithm = AllPion_fd  , RequiredSelections = [ AllPions ] )
    AllPions_seq_Filtered = SelectionSequence(
        'AllPions_seq_Filtered' + decay.tuple_name, TopSelection=AllPions)
    DaVinci().appendToMainSequence([AllPions_seq_Filtered])

    #loki_mc = tuple.addTupleTool("LoKi::Hybrid::MCTupleTool/LoKi_MC")
    #loki_mc.Preambulo = [ "from LoKiMC.functions import *" ]
    #loki_mc.Variables = {"MC_MOTHER_P"   : "MCMOTHER(MCP, -1e6)"}

    LoKiVariables_othertracks = tuple.addTupleTool(
        "LoKi::Hybrid::ArrayTupleTool/LoKiVariables_othertracks")
    LoKiVariables_othertracks.Preambulo = [
        "from LoKiTracks.decorators import *"
    ]
    LoKiVariables_othertracks.Variables = loki_var_udrlyTr
    LoKiVariables_othertracks.AVariables = loki_Avar_udrlyTr
    LoKiVariables_othertracks.Source = "SOURCE('" + AllPions_seq_Filtered.outputLocation(
    ) + "', )"  #
    # LoKiVariables_othertracks.Source         =  "SOURCE('" + "Phys/TaggingIFTTracks/Particles" + "', )" #
    LoKiVariables_othertracks.OutputLevel = 1
    TupleToolIsoGeneric = tuple.addTupleTool("TupleToolIsoGeneric/Iso_" +
                                             decay.tuple_name)
    TupleToolIsoGeneric.ParticlePath = AllPions_seq_Filtered.outputLocation()
    # TupleToolIsoGeneric.ParticlePath         =  "Phys/TaggingIFTTracks/Particles"


def addParticleTaggerMCTuple(tuple, decay):
    # createVeloTracks()
    LongPions = DataOnDemand(Location='Phys/StdAllNoPIDsPions/Particles')
    UpStreamPions = DataOnDemand(Location='Phys/StdNoPIDsUpPions/Particles')
    DnStreamPions = DataOnDemand(Location='Phys/StdNoPIDsDownPions/Particles')
    # VeloPions     = DataOnDemand( Location ='Phys/StdNoPIDsVeloPions/Particles')
    AllPions = MergedSelection(
        "AllPions" + decay.tuple_name,
        RequiredSelections=[LongPions, UpStreamPions,
                            DnStreamPions])  # , VeloPions ] )
    #AllPion_fd    = FilterDesktop("AllPion_fd"+ decay.tuple_name , Code = "(HASPROTO) & (HASTRACK) & (ISBASIC) & (switch(INTES('"+ decay.inputs +"', False) , 0 , 1)>0)") #
    #AllPions_sel  = Selection( "AllPions_sel"+ decay.tuple_name , Algorithm = AllPion_fd  , RequiredSelections = [ AllPions ] )
    AllPions_seq_Filtered = SelectionSequence(
        'AllPions_seq_Filtered' + decay.tuple_name, TopSelection=AllPions)
    DaVinci().appendToMainSequence([AllPions_seq_Filtered])

    #loki_mc = tuple.addTupleTool("LoKi::Hybrid::MCTupleTool/LoKi_MC")
    #loki_mc.Preambulo = [ "from LoKiMC.functions import *" ]
    #loki_mc.Variables = {"MC_MOTHER_P"   : "MCMOTHER(MCP, -1e6)"}

    LoKiVariables_othertracks = tuple.addTupleTool(
        "LoKi::Hybrid::ArrayTupleTool/LoKiVariables_othertracks")
    LoKiVariables_othertracks.Preambulo = [
        "from LoKiTracks.decorators import *"
    ]
    LoKiVariables_othertracks.Variables = loki_var_udrlyTr
    LoKiVariables_othertracks.AVariables = loki_Avar_udrlyTr
    LoKiVariables_othertracks.Source = "SOURCE('" + AllPions_seq_Filtered.outputLocation(
    ) + "', )"  #
    LoKiVariables_othertracks.OutputLevel = 1
    # TupleToolIsoGeneric                      =  tuple.addTupleTool("TupleToolIsoGeneric/Iso_" + decay.tuple_name )
    # TupleToolIsoGeneric.ParticlePath         =  AllPions_seq_Filtered.outputLocation()
    MCTupleToolHierarchyNTracks = tuple.addTupleTool(
        "MCTupleToolHierarchyNTracks/MC_NTracks_" + decay.tuple_name)
    MCTupleToolHierarchyNTracks.ParticlePath = AllPions_seq_Filtered.outputLocation(
    )
    # MCTupleToolHierarchyNTracks.FragmentationStudy = True
    MCTupleToolHierarchyNTracks.Mother_id = 531


##############################################################################################
