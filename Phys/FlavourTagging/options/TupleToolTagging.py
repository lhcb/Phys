###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""short python script to test new development area of FT"""

from Configurables import (
    DecayTreeTuple,
    DaVinci,
    TrackScaleState,
    CondDB,
)

from DecayTreeTuple.Configuration import *

tuple = DecayTreeTuple("Bd2Dpi")

tuple.Inputs = [
    '/Event/BhadronCompleteEvent/Phys/B02DPiD2HHHBeauty2CharmLine/Particles'
]  # detached stripping Line
tuple.Decay = "[[B0]CC -> (D- -> pi- pi- K+) pi+]CC"
tuple.ReFitPVs = True
# tuple.OutputLevel = 2

branches = {}

descriptor_B = "^([[B0]CC -> (D- -> pi- pi- K+) pi+]CC)"
name_bbranch = "B0"
branches[name_bbranch] = descriptor_B

tuple.addBranches(branches)

tuple.ToolList = []

# Configure TupleToolTagging
tt_tagging = tuple.addTupleTool("TupleToolTagging")
# tt_tagging.OutputLevel = 2
tt_tagging.Verbose = True

# Configure BTaggingTool
from Configurables import (BTaggingTool, TaggerMuonTool, TaggerElectronTool,
                           TaggerNEWKaonOppositeTool, TaggerVertexChargeTool,
                           TaggerCharmTool, TaggerPionBDTSameTool,
                           TaggerProtonSameTool)

btagtool = tt_tagging.addTool(BTaggingTool, name="MyBTaggingTool")
os_muon_tagger = btagtool.addTool(TaggerMuonTool, name="OSMuonTagger")
os_electron_tagger = btagtool.addTool(
    TaggerElectronTool, name="OSElectronTagger")
os_kaon_tagger = btagtool.addTool(
    TaggerNEWKaonOppositeTool, name="OSKaonTagger")
os_vtxch_tagger = btagtool.addTool(
    TaggerVertexChargeTool, name="OSVtxChTagger")
os_charm_tagger = btagtool.addTool(TaggerCharmTool, name="OSCharmTagger")
ss_pion_tagger = btagtool.addTool(TaggerPionBDTSameTool, name="SSPionTagger")
ss_proton_tagger = btagtool.addTool(
    TaggerProtonSameTool, name="SSprotonTagger")

btagtool.EnabledTaggersMap = {
    "OSMuon": os_muon_tagger.getFullName(),
    "OSElectron": os_electron_tagger.getFullName(),
    "OSKaon": os_kaon_tagger.getFullName(),
    "OSVtxCh": os_vtxch_tagger.getFullName(),
    "OSCharm": os_charm_tagger.getFullName(),
    "SSPion": ss_pion_tagger.getFullName(),
    "SSProton": ss_proton_tagger.getFullName()
}

btagtool.CallOrder = [
    "OSMuon", "OSElectron", "OSKaon", "OSVtxCh", "OSCharm", "SSPion",
    "SSProton"
]
tt_tagging.TaggingToolName = btagtool.getFullName()

# DaVinci configuration
DaVinci().TupleFile = "DTT.root"
DaVinci().Simulation = False
DaVinci().UserAlgorithms = [tuple]

DaVinci().EvtMax = 5000
DaVinci().DataType = "2012"
CondDB(LatestGlobalTagByDataType="2012")
# EventSelector
input = [
    '/fhgfs/groups/e5/lhcb/bookkeeping/LHCb/Collision12/BHADRONCOMPLETEEVENT.DST/00041836/0000/00041836_00000198_1.bhadroncompleteevent.dst',
    '/fhgfs/groups/e5/lhcb/bookkeeping/LHCb/Collision12/BHADRONCOMPLETEEVENT.DST/00041836/0000/00041836_00000221_1.bhadroncompleteevent.dst',
    '/fhgfs/groups/e5/lhcb/bookkeeping/LHCb/Collision12/BHADRONCOMPLETEEVENT.DST/00041836/0000/00041836_00000292_1.bhadroncompleteevent.dst'
]
EventSelector().Input = input
