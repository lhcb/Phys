###############################################################################
# (c) Copyright 2020-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
 Script to create a DecayTreeTuple with the information necessary for the
 training of the tagging alorithms. The channels used here is Bd2JpsiKstar.

 ***********************************************************
 Modified to add other tracks for inclusive tagging studies.
 ***********************************************************

"""
__author__ = "Vanessa Mueller, Julian Wishahi", "Alessandro Camboni"
__credits__ = ["Florian Kruse", "Ramon Niet", "Mirco Dorigo"]

##########################################################################
# IMPORTS (PEP8 specifies that these should go to the top of a file)
##########################################################################

import os
import sys
import re
import copy
os.getcwd()
sys.path.append(os.getcwd())

from ParticleTagger import addParticleTaggerTuple

from Configurables import DaVinci

from Configurables import (
    GaudiSequencer,
    CondDB,
    CheckPV,
)

from GaudiConf import IOHelper

from Configurables import (FilterDesktop, ChargedProtoANNPIDConf)

from PhysSelPython.Wrappers import Selection, SelectionSequence, DataOnDemand

from Configurables import (BTaggingTool, OSMuonTagger, OSElectronTagger,
                           OSKaonTagger, OSVtxChTagger)

from FlavourTagging.Tunings import applyTuning as applyFTTuning
from Configurables import (TrackSmearState, TrackScaleState)

from Configurables import (DecayTreeTuple, EventTuple)

from DecayTreeTuple.Configuration import *

##########################################################################
# SETTINGS
##########################################################################
DaVinci().DataType = '2017'
DaVinci().InputType = 'DST'

DaVinci().EvtMax = 1000
DaVinci().SkipEvents = 0
DaVinci().PrintFreq = 2000
# DaVinci().DataType = "2016"
DaVinci().Simulation = False
#DaVinci().VerboseMessages = True
DaVinci().TupleFile = "DTT.root"

if not DaVinci().Simulation and DaVinci().DataType == "2012":
    CondDB(LatestGlobalTagByDataType="2012")

if not DaVinci().Simulation and DaVinci().DataType == "2011":
    CondDB(LatestGlobalTagByDataType="2011")

if not DaVinci().Simulation and DaVinci().DataType == "2015":
    CondDB(LatestGlobalTagByDataType="2015")

if not DaVinci().Simulation and DaVinci().DataType == "2016":
    CondDB(LatestGlobalTagByDataType="2016")

if not DaVinci().Simulation and DaVinci().DataType == "2017":
    CondDB(LatestGlobalTagByDataType="2017")

# apply preselection
preselection = True

##########################################################################
# GLOBALS
##########################################################################
prefix = ''
if DaVinci().Simulation:
    prefix = "AllStreams"
else:
    prefix = "Dimuon"

print("prefix is " + prefix)


class Decay(object):
    def __init__(self, other=None):
        if other is not None:
            self.__dict__ = other.__dict__.copy()


##########################################################################
# SELECTIONS
##########################################################################
seqB2JpsiX = GaudiSequencer('seqB2JpsiX')
TupleSeq = GaudiSequencer('TupleSeq')

if preselection:

    inputBd2JpsiKst = DataOnDemand(
        Location=prefix + "/Phys/BetaSBd2JpsiKstarDetachedLine/Particles")

    cutsBd2JpsiKst_mu = "INGENERATION( (ABSID=='mu+') & (MIPCHI2DV(PRIMARY)>16) & (PT>500*MeV) & (PIDmu>0),2 )"
    cutsBd2JpsiKst_jpsi = "INTREE( (ID=='J/psi(1S)') & (M>3036*MeV) & (M<3156*MeV) )"
    cutsBd2JpsiKst_k = "INGENERATION( (ABSID=='K+') & (MIPCHI2DV(PRIMARY)>2) & (PIDK > 0) & (PT>500*MeV),2 )"
    cutsBd2JpsiKst_pi = "INGENERATION( (ABSID=='pi+') & (MIPCHI2DV(PRIMARY)>2) & (PT>500*MeV),2 )"
    cutsBd2JpsiKst_kst = "INTREE( (ABSID=='K*(892)0') & (M>826*MeV) & (M<966*MeV) )"
    cutsBd2JpsiKst_b = "(M>5000*MeV) & (M<6000*MeV) & (BPVIPCHI2()<25) & (BPVLTIME('PropertimeFitter/properTime:PUBLIC')>0.2*ps)"

    cutsBd2JpsiKst = "(" + cutsBd2JpsiKst_mu + " & " + cutsBd2JpsiKst_jpsi + " & " + cutsBd2JpsiKst_k + \
        " & " + cutsBd2JpsiKst_pi + " & " + \
        cutsBd2JpsiKst_kst + " & " + cutsBd2JpsiKst_b + ")"
    # cutsBd2JpsiKst = "ALL"

    filterBd2JpsiKst = FilterDesktop("filterBd2JpsiKst", Code=cutsBd2JpsiKst)

    selBd2JpsiKst = Selection(
        "selBd2JpsiKst",
        Algorithm=filterBd2JpsiKst,
        RequiredSelections=[inputBd2JpsiKst])

    seqBd2JpsiKst = SelectionSequence(
        "seqBd2JpsiKst", TopSelection=selBd2JpsiKst)

    DaVinci().appendToMainSequence([seqBd2JpsiKst.sequence()])

##########################################################################
# DECAYS
##########################################################################
bd2jpsikstar_detached_dict = {
    "tuple_name":
    "Bd2JpsiKstarDetached",
    "decay_descriptor":
    "[B0 -> ^(J/psi(1S) -> ^mu+ ^mu-) ^(K*(892)0 -> ^K+ ^pi-)]CC",
    "inputs":
    seqBd2JpsiKst.outputLocation(),
    "p2vv":
    "Bd2KstarMuMuAngleCalculator",
    "daughters_to_constrain": [["J/psi(1S)"]],
}
bd2jpsikstar_detached = Decay()
bd2jpsikstar_detached.__dict__ = bd2jpsikstar_detached_dict

#===============================================================================
# dictionaries for global variables
#-------------------------------------------------------------------------------
# dictionary for matching particle names in DecayDescriptor with branch names
dict_decay_to_name = {
    'B_s0': 'B',
    'B0': 'B',
    'B+': 'B',
    'J/psi(1S)': 'Jpsi',
    'K*(892)0': 'Kstz',
    'mu+': 'muplus',
    'mu-': 'muminus',
    'K+': 'Kplus',
    'K-': 'Kminus',
    'pi+': 'piplus',
    'pi-': 'piminus'
}

#-------------------------------------------------------------------------------
# dictionary for LoKi::Hybrid::TupleTool
# for all branches
loki_variables = {
    "LOKI_ENERGY": "E",
    "LOKI_ETA": "ETA",
    "LOKI_PHI": "PHI",
    "LOKI_FDS": "BPVDLS"
}
loki_variables_B = {
    "LOKI_ETA": "ETA",
    "LOKI_ENERGY": "E",
    "LOKI_PHI": "PHI",
    "LOKI_FDCHI2": "BPVVDCHI2",
    "LOKI_FDS": "BPVDLS",
    "LOKI_DIRA": "BPVDIRA",
    ###### DecayTreeFitVariables
    "LOKI_DTF_CTAU": "DTF_CTAU( 0, True )",
    "LOKI_DTF_CTAU_NOPV": "DTF_CTAU( 0, False )",
    #"LOKI_DTF_CTAUS"        : "DTF_CTAUSIGNIFICANCE( 0, True )",
    "LOKI_DTF_CHI2NDOF": "DTF_CHI2NDOF( True )",
    "LOKI_DTF_CTAUERR": "DTF_CTAUERR( 0, True )",
    "LOKI_DTF_CTAUERR_NOPV": "DTF_CTAUERR( 0, False )",
    "LOKI_MASS_JpsiConstr": "DTF_FUN ( M , True , 'J/psi(1S)' )",
    "LOKI_MASS_JpsiConstr_NoPVConstr": "DTF_FUN ( M , False , 'J/psi(1S)' )",
    "LOKI_MASSERR_JpsiConstr": "sqrt(DTF_FUN ( M2ERR2 , True , 'J/psi(1S)' ))",
    "LOKI_DTF_VCHI2NDOF": "DTF_FUN ( VFASPF(VCHI2/VDOF) , True )",
    "LOKI_DTF_CTAU_D1": "DTF_CTAU(1, True)",
    "LOKI_DTF_CTAU_D2": "DTF_CTAU(2, True)",
    "LOKI_DTF_CTAUERR_D1": "DTF_CTAUERR(1, True)",
    "LOKI_DTF_CTAUERR_D2": "DTF_CTAUERR(2, True)"
}

dict_trigger_lines = {
    '2011': {
        'L0': [
            'L0PhysicsDecision', 'L0MuonDecision', 'L0DiMuonDecision',
            'L0MuonHighDecision', 'L0HadronDecision', 'L0ElectronDecision',
            'L0PhotonDecision'
        ],
        'HLT1': [
            'Hlt1GlobalDecision', 'Hlt1DiMuonHighMassDecision',
            'Hlt1TrackMuonDecision', 'Hlt1TrackAllL0Decision'
        ],
        'HLT2': [
            'Hlt2GlobalDecision', 'Hlt2DiMuonJPsiDecision',
            'Hlt2DiMuonDetachedJPsiDecision'
        ],
    },
    '2012': {
        'L0': [
            'L0PhysicsDecision', 'L0MuonDecision', 'L0DiMuonDecision',
            'L0MuonHighDecision', 'L0HadronDecision', 'L0ElectronDecision',
            'L0PhotonDecision'
        ],
        'HLT1': [
            'Hlt1GlobalDecision', 'Hlt1DiMuonHighMassDecision',
            'Hlt1TrackMuonDecision', 'Hlt1TrackAllL0Decision'
        ],
        'HLT2': [
            'Hlt2GlobalDecision', 'Hlt2DiMuonJPsiDecision',
            'Hlt2DiMuonDetachedJPsiDecision'
        ],
    },
    '2015': {
        'L0': [
            'L0PhysicsDecision', 'L0MuonDecision', 'L0DiMuonDecision',
            'L0MuonHighDecision', 'L0HadronDecision', 'L0ElectronDecision',
            'L0PhotonDecision'
        ],
        'HLT1': [
            'Hlt1GlobalDecision', 'Hlt1DiMuonHighMassDecision',
            'Hlt1TrackMVADecision', 'Hlt1TwoTrackMVADecision',
            'Hlt1TrackMuonMVADecision'
        ],
        'HLT2': [
            'Hlt2GlobalDecision', 'Hlt2DiMuonJPsiDecision',
            'Hlt2DiMuonDetachedJPsiDecision'
        ],
    },
    '2016': {
        'L0': [
            'L0PhysicsDecision', 'L0MuonDecision', 'L0DiMuonDecision',
            'L0MuonHighDecision', 'L0HadronDecision', 'L0ElectronDecision',
            'L0PhotonDecision'
        ],
        'HLT1': [
            'Hlt1GlobalDecision', 'Hlt1DiMuonHighMassDecision',
            'Hlt1TrackMVADecision', 'Hlt1TwoTrackMVADecision',
            'Hlt1TrackMuonDecision', 'Hlt1TrackMuonMVADecision'
        ],
        'HLT2': [
            'Hlt2GlobalDecision', 'Hlt2DiMuonJPsiDecision',
            'Hlt2DiMuonDetachedJPsiDecision'
        ],
    },
    '2017': {
        'L0': [
            'L0PhysicsDecision', 'L0MuonDecision', 'L0DiMuonDecision',
            'L0MuonHighDecision', 'L0HadronDecision', 'L0ElectronDecision',
            'L0PhotonDecision'
        ],
        'HLT1': [
            'Hlt1GlobalDecision', 'Hlt1DiMuonHighMassDecision',
            'Hlt1TrackMVADecision', 'Hlt1TwoTrackMVADecision',
            'Hlt1TrackMuonDecision', 'Hlt1TrackMuonMVADecision'
        ],
        'HLT2': [
            'Hlt2GlobalDecision', 'Hlt2DiMuonJPsiDecision',
            'Hlt2DiMuonDetachedJPsiDecision'
        ],
    },
}

triggerlists = dict_trigger_lines[DaVinci().DataType]
trigger_lines = triggerlists['L0'] + triggerlists['HLT1'] + triggerlists['HLT2']

stripping_lines = ['StrippingBetaSBd2JpsiKstarDetachedLineDecision']


################################################################################
# Create TupleTools
################################################################################
def CreateTupleTool(tuple_config):
    print "-----------------------------------------------------------------------"
    print "Adding decay " + tuple_config.decay_descriptor
    print "Location:", tuple_config.inputs
    print "Tuple name:", tuple_config.tuple_name

    dttuple = DecayTreeTuple(tuple_config.tuple_name)
    dttuple.OutputLevel = 6
    dttuple.Inputs = [tuple_config.inputs]
    dttuple.Decay = tuple_config.decay_descriptor
    dttuple.ReFitPVs = True
    dttuple.ToolList = []

    addParticleTaggerTuple(dttuple, tuple_config)

    tt_kin = dttuple.addTupleTool('TupleToolKinematic')
    tt_kin.Verbose = False

    tt_prim = dttuple.addTupleTool('TupleToolPrimaries')
    tt_prim.Verbose = False

    tt_proptime = dttuple.addTupleTool('TupleToolPropertime')
    tt_proptime.Verbose = False

    tt_geometry = dttuple.addTupleTool('TupleToolGeometry')
    tt_geometry.Verbose = True
    tt_geometry.RefitPVs = True
    tt_geometry.FillMultiPV = True

    tt_eventinfo = dttuple.addTupleTool('TupleToolEventInfo')
    tt_eventinfo.Verbose = True

    tt_recostats = dttuple.addTupleTool("TupleToolRecoStats")

    tt_trackinfo = dttuple.addTupleTool('TupleToolTrackInfo')
    tt_trackinfo.Verbose = True

    tt_trackpos = dttuple.addTupleTool('TupleToolTrackPosition')
    tt_trackpos.Z = 7500.

    tt_pid = dttuple.addTupleTool('TupleToolPid')
    tt_pid.Verbose = True

    tt_pid = dttuple.addTupleTool("TupleToolANNPID/TTPidAnn")
    tt_pid.Verbose = True

    tt_stripping = dttuple.addTupleTool("TupleToolStripping")
    tt_stripping.Verbose = True
    tt_stripping.StrippingList = stripping_lines

    # tt_tagging = dttuple.addTupleTool("TupleToolTagging")
    # tt_tagging.Verbose = True
    # tt_tagging.AddMVAFeatureInfo = True
    # tt_tagging.AddTagPartsInfo = False
    # tt_tagging.OutputLevel = INFO
    # btagtool = tt_tagging.addTool(BTaggingTool, name = "MyBTaggingTool")
    # applyFTTuning(btagtool, tuning_version="Summer2017Optimisation")
    # tt_tagging.TaggingToolName = btagtool.getFullName()

    tt_tagging = dttuple.addTupleTool("TupleToolTagging")
    tt_tagging.Verbose = True
    tt_tagging.AddMVAFeatureInfo = True
    tt_tagging.AddTagPartsInfo = True
    tt_tagging.OutputLevel = INFO
    btagtool = tt_tagging.addTool(BTaggingTool, name="MyBTaggingTool")
    applyFTTuning(btagtool, tuning_version="TupleDevelopment")
    tt_tagging.TaggingToolName = btagtool.getFullName()

    name_jpsi_decaydec = "J/psi(1S)"
    branches = {}
    descriptor_B = tuple_config.decay_descriptor.replace("^", "")
    descriptor_jpsi = descriptor_B.replace("(" + name_jpsi_decaydec,
                                           "^(" + name_jpsi_decaydec)
    name_bbranch = ""
    if "B_s0" in descriptor_B:
        print "This is a Bs0 decay"
        name_bbranch = dict_decay_to_name["B_s0"]
    elif "B0" in descriptor_B:
        print "This is a B0 decay"
        name_bbranch = dict_decay_to_name["B0"]
    elif "B+" in descriptor_B:
        print "This is a B+ decay"
        name_bbranch = dict_decay_to_name["B+"]

    branches[name_bbranch] = "^(" + descriptor_B + ")"
    branches[dict_decay_to_name[name_jpsi_decaydec]] = descriptor_jpsi

    print "Decay branches:", branches
    dttuple.addBranches(branches)
    # tt_loki_B = dttuple.__getattr__(name_bbranch).addTupleTool("LoKi::Hybrid::TupleTool/tt_loki_B")
    # if "loki_variables_B" in tuple_config.__dict__:
    #     tt_loki_B.Variables = dict(loki_variables_B.items() + tuple_config.loki_variables_B.items())
    # else:
    #     tt_loki_B.Variables = loki_variables_B
    dttuple.addTupleTool('TupleToolDecay/' + name_bbranch)
    tt_tistos_b = dttuple.__getattr__(name_bbranch).addTupleTool(
        "TupleToolTISTOS")
    tt_tistos_b.VerboseL0 = True
    tt_tistos_b.VerboseHlt1 = True
    tt_tistos_b.VerboseHlt2 = True
    tt_tistos_b.FillL0 = True
    tt_tistos_b.FillHlt1 = True
    tt_tistos_b.FillHlt2 = True
    tt_tistos_b.OutputLevel = INFO
    tt_tistos_b.TriggerList = trigger_lines

    if "p2vv" in tuple_config.__dict__:
        print descriptor_B, "is a P2VV decay"
        tt_p2vv = dttuple.__getattr__(name_bbranch).addTupleTool(
            "TupleToolP2VV")
        tt_p2vv.Calculator = tuple_config.p2vv

    if DaVinci().Simulation:
        tt_mct = dttuple.addTupleTool(
            "TupleToolMCTruth", name=("TupleToolMCTruth"))
        tt_mct.ToolList = ["MCTupleToolReconstructed"]

        tt_mct.ToolList += [
            "MCTupleToolKinematic", "MCTupleToolHierarchy", "MCTupleToolPID"
        ]
        dttuple.addTupleTool(
            "TupleToolMCBackgroundInfo", name=("TupleToolMCBackgroundInfo"))
        dttuple.TupleToolMCBackgroundInfo.addTool(
            BackgroundCategory("BackgroundCategory"))

    return dttuple


TupleSeq.Members += [CreateTupleTool(bd2jpsikstar_detached)]
TupleSeq.ModeOR = True
TupleSeq.ShortCircuit = False
checkPV = CheckPV("CheckForOnePV")
checkPV.MinPVs = 1
print checkPV

##############################################################################

seqB2JpsiX.Members += [checkPV, TupleSeq]
seqB2JpsiX.ModeOR = True
seqB2JpsiX.ShortCircuit = False

evtTuple = EventTuple()
evtTuple.ToolList += ["TupleToolEventInfo"]

subseq_annpid = GaudiSequencer('SeqANNPID')
conf_ann_pid = ChargedProtoANNPIDConf('ANNPIDConf')
conf_ann_pid.RecoSequencer = subseq_annpid

if DaVinci().Simulation:
    DaVinci().UserAlgorithms += [mctuple]
    smear = SMEAR('Smear')
    DaVinci().UserAlgorithms += [smear]

if not DaVinci().Simulation:
    scaler = TrackScaleState('Scaler')
    DaVinci().UserAlgorithms += [scaler]

DaVinci().UserAlgorithms += [seqB2JpsiX, evtTuple]

DaVinci().DataType = '2017'
DaVinci().InputType = 'DST'
CondDB().Tags['DDDB'] = 'dddb-20170721-3'

from Gaudi.Configuration import *
from GaudiConf import IOHelper
IOHelper('ROOT').inputFiles([
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/LHCb/Collision17/DIMUON.DST/00071700/0000/00071700_00000046_1.dimuon.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/LHCb/Collision17/DIMUON.DST/00071700/0000/00071700_00000080_1.dimuon.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/LHCb/Collision17/DIMUON.DST/00071700/0000/00071700_00000130_1.dimuon.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/LHCb/Collision17/DIMUON.DST/00071700/0000/00071700_00000462_1.dimuon.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/LHCb/Collision17/DIMUON.DST/00071700/0000/00071700_00000546_1.dimuon.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/LHCb/Collision17/DIMUON.DST/00071700/0000/00071700_00000707_1.dimuon.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/LHCb/Collision17/DIMUON.DST/00071700/0000/00071700_00000893_1.dimuon.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/LHCb/Collision17/DIMUON.DST/00071700/0000/00071700_00000969_1.dimuon.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/LHCb/Collision17/DIMUON.DST/00071700/0000/00071700_00001115_1.dimuon.dst',
    'PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/LHCb/Collision17/DIMUON.DST/00071700/0000/00071700_00001238_1.dimuon.dst',
],
                            clear=True)
