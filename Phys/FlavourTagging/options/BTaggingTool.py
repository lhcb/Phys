###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

##############################################################################
#
# Tagging options
#
##############################################################################

print(" ")
print(
    "==========================================================================="
)
print(
    "  WARNING WARNING WARNING WARNING WARNING WARNING WARNING WARNING WARNING"
)
print(" ")
print("    The syntax :-")
print(" ")
print("""      importOptions("$FLAVOURTAGGINGROOT/options/BTaggingTool.py")""")
print(" ")
print("    is OBSOLETE. Please instead use :-")
print(" ")
print("      from Configurables import FlavourTaggingConf")
print("      ftConf = FlavourTaggingConf()")
print(" ")
print(
    "  WARNING WARNING WARNING WARNING WARNING WARNING WARNING WARNING WARNING"
)
print(
    "==========================================================================="
)
print(" ")

from Configurables import FlavourTaggingConf
# Just create an instance. Nothing to configure as of yet
ftConf = FlavourTaggingConf()

##############################################################################
