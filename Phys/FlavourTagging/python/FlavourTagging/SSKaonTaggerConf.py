###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Configurables import (
    TaggerKaonSameTool,
    TaggerNEWKaonSameTool,
    TaggerSSKaon_dev_Tool,
)

from FlavourTagging.ClassicTunings import setTuningProperties

# classic tunings
Stripping21 = TaggerKaonSameTool(name='SSKaonTagger')
setTuningProperties(Stripping21, 'Stripping21')

Stripping23NNet = TaggerNEWKaonSameTool(name='SSKaonTagger_Stripping23NNet')
setTuningProperties(Stripping23NNet, 'Stripping23')

Stripping23NNetDev = TaggerSSKaon_dev_Tool(
    name='SSKaonTaggerDev_Stripping23NNet')
setTuningProperties(Stripping23NNetDev, 'Stripping23')
