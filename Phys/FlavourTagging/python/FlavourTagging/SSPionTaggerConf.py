###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Configurables import (
    TaggerPionSameTool,
    TaggerPionBDTSameTool,
    SSPionTagger,
    SSPionClassifierFactory,
)

from FlavourTagging.ClassicTunings import setTuningProperties

# classic tunings
Stripping21 = TaggerPionSameTool(name='SSPionTagger_Stripping21')
setTuningProperties(Stripping21, 'Stripping21')

ClassicBDT = TaggerPionBDTSameTool(name='SSPionTagger_ClassicBDT')
# There seems to be no tuning for this tagger

Development = SSPionTagger(name='SSPionTagger_Development')
Development.addTool(SSPionClassifierFactory, name='mva')
Development.mva.ClassifierName = 'SSPion_Data_Run1_All_Bd2Dpi_TMVA_BDT_v1r0'
Development.mva.ClassifierType = 'TMVA'

Development.Aliases = {
    "P":
    "P/GeV",
    "Pt":
    "PT/GeV",
    "ipsig":
    "IPSig",
    "gprob":
    "TRGHP",
    "dphi":
    "phiDistance",
    "dR":
    "DeltaR",
    "deta":
    "etaDistance",
    "dQ_pi":
    "DeltaQ",
    "BPt":
    "Signal_PT",
    "Pt_tot":
    "Signal_TagPart_PT",
    "PIDK":
    "PIDK",
    "lcs":
    "TRCHI2DOF",
    "PVndof":
    "BPV(VDOF)",
    "IPPUs":
    "IPPUSig",
    "mva":
    "MVA[{}]("
    "log(P), log(Pt), log(ipsig), gprob, log(dphi), dR, log(deta),"
    "dQ_pi, log(BPt), log(Pt_tot), PIDK, lcs, PVndof"
    ")".format(Development.mva.getFullName()),
}

Development.SelectionPipeline = [
    [
        'PIDp<5',
        'PIDK<5',
        'Pt>0.4',
        'ipsig<4',
        'IPPUs>3',
        'gprob<0.5',
        'dQ_pi<1.2*GeV',
        'Pt_tot>3*GeV',
        # 'BPt>3.0*GeV',
        'Signal_TagPart_CHI2NDOF<100',
        'deta<1.2',  # same cut as preselection
        'dphi<1.1',  # same cut as preselection
        'IsSignalDaughter==0',
        'ABSID==211',
        # 'lcs<5',
    ],
    [
        'P',
        'Pt',
        'ipsig',
        'gprob',
        'dphi',
        'dR',
        'deta',
        'dQ_pi',
        'BPt',
        'Pt_tot',
        'PIDK',
        'lcs',
        'PVndof',
        'mva',
    ]
]
