###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Configurables import (
    TaggerElectronTool,
    OSElectronTagger,
    OSElectronClassifierFactory,
)

from FlavourTagging.ClassicTunings import setTuningProperties

# classic tunings
Stripping23 = TaggerElectronTool(name="OSElectronTagger_Stripping23")
setTuningProperties(Stripping23, 'Stripping23')

Stripping21 = TaggerElectronTool(name='OSElectronTagger_Stripping21')
setTuningProperties(Stripping21, 'Stripping21')

Development = OSElectronTagger(name='OSElectronTagger_Development')
Development.addTool(OSElectronClassifierFactory, name='ElectronMVA')
Development.ElectronMVA.ClassifierType = 'TMVA'
Development.ElectronMVA.ClassifierName = 'OSElectron_Data_Run1_All_Bu2JpsiK_TMVA_MLP_v1r0'

Development.Aliases = {
    "ptB":
    "Signal_PT/GeV",
    "partP":
    "P/GeV",
    "partPt":
    "PT/GeV",
    "IPs":
    "IPSig",
    "partlcs":
    "TRCHI2DOF",
    "ghostProb":
    "TRGHP",
    "IPPU":
    "IPPUSig",
    "mult":
    "countTracks",
    "mva":
    "MVA[{}]("
    "mult, log(partP), log(partPt), log(ptB), log(IPs),"
    "log(partlcs), eOverP, log(ghostProb), log(IPPU)"
    ")".format(Development.ElectronMVA.getFullName()),
}

Development.SelectionPipeline = [
    [
        "PP_InAccHcal==1",
        "TRTYPE==3",
        "TRCHI2DOF<3",
        "TRLH>-999",
        "TRGHP<0.4",
        "MuonPIDIsMuon==0",
        # cut that is not yet tagged by kaon or muon tagger missing
        "PROBNNpi<0.8",
        "PROBNNp<0.8",
        "PROBNNk<0.8",
        "PROBNNe>0.1",
        "PROBNNe-PROBNNpi>-0.8",
        "PIDe>-1",
        "partPt>1.1",
        "partP>0",
        "IPErr!=0",
        "IPs>3.5",
        "IPPU>4.0",
        "IsSignalDaughter==0",
        "minPhiDistance>0.005",
        "eOverP<2",
        "eOverP>0.85",
        "PP_VeloCharge>0",
        "PP_VeloCharge<1.4",
        "ptB",
        # the current implementation of the OSElectronTagger needs the mva
        # value to be the last feature
    ],
    [
        "mult",
        "partP",
        "partPt",
        "ptB",
        "IPs",
        "partlcs",
        "eOverP",
        "ghostProb",
        "IPPU",
        "mva",
    ],
]

################################
# 1st iteration (September 2017)
################################

Summer2017Opt = OSElectronTagger(name='OSElectronTagger_Summer2017Opt')
Summer2017Opt.addTool(OSElectronClassifierFactory, name='ElectronMVA')
Summer2017Opt.ElectronMVA.ClassifierType = 'TMVA'
Summer2017Opt.ElectronMVA.ClassifierName = 'OSElectron_Data_Run2_All_Bu2JpsiK_XGB_BDT_v1r0'

Summer2017Opt.Aliases = {
    "PROBNNe_PROBNNpi":
    "PROBNNe-PROBNNpi",
    "mva1":
    "MVA[{}]("
    "countTracks, P, PT, Signal_PT, IPSig, TRCHI2DOF,"
    "eOverP, TRGHP, IPPUSig"
    ")".format(Summer2017Opt.ElectronMVA.getFullName()),
}

Summer2017Opt.SelectionPipeline = [
    [
        "PP_InAccHcal==1",
        "TRTYPE==3",
        "TRCHI2DOF<3",
        "TRGHP<0.280",
        "MuonPIDIsMuon==0",
        "PROBNNpi<0.990",
        "PROBNNp<0.833",
        "PROBNNk<0.633",
        "PROBNNe>0.128",
        "PROBNNe-PROBNNpi>-1.894",
        "PROBNNmu<0.142",
        "PIDe>4.710",
        "PT>1240",
        "P>5349",
        "IPErr!=0",
        "IPSig>1.450",
        "IPPUSig>9.218",
        "IsSignalDaughter==0",
        "minPhiDistance>0.00771",
        "eOverP<2",
        "eOverP>0.85",
        "PP_VeloCharge>0",
        "PP_VeloCharge<1.4",
    ],
    [
        "countTracks",
        "P",
        "PT",
        "Signal_PT",
        "IPSig",
        "TRCHI2DOF",
        "eOverP",
        "TRGHP",
        "IPPUSig",
        "mva1",
    ],
]

#############################################
# Final 2017 iteration (nominal Run1 taggers)
#############################################

Summer2017Opt_Run1 = OSElectronTagger(
    name='OSElectronTagger_Summer2017Opt_Run1')
Summer2017Opt_Run1.addTool(OSElectronClassifierFactory, name='ElectronMVA')
Summer2017Opt_Run1.ElectronMVA.ClassifierType = 'TMVA'
Summer2017Opt_Run1.ElectronMVA.ClassifierName = 'OSElectron_Data_Run1_All_Bu2JpsiK_XGB_BDT_v1r0'

Summer2017Opt_Run1.Aliases = {
    "BPVIPCHI2":
    "BPVIPCHI2()",
    "mva1":
    "MVA[{}]("
    "PT,"
    "IPPUSig,"
    "TRGHP,"
    "countTracks,"
    "Signal_PT,"
    "eOverP,"
    "AbsIP,"
    "IPErr,"
    "BPVIPCHI2,"
    "DeltaR"
    ")".format(Summer2017Opt_Run1.ElectronMVA.getFullName()),
}

Summer2017Opt_Run1.SelectionPipeline = [
    [
        "IsSignalDaughter==0",
        "MuonPIDIsMuon==0",
        "TRCHI2DOF<3",
        "PP_InAccHcal==1",
        "PP_VeloCharge>0",
        "PP_VeloCharge<1.4",
        "IPErr!=0",
        "eOverP<2",
        "eOverP>0.85",
        "TRTYPE==3",
        "P>3113.641968357261",
        "PT>1132.1202858954884",
        "TRGHP<0.8606785372127446",
        "minPhiDistance>=0.00803358872286969",
        "IPSig>0.02001354198539968",
        "PROBNNmu<0.9376480079276076",
        "PROBNNpi<0.9375142821029713",
        "PROBNNk<0.7650669539596838",
        "PROBNNp<0.7186940204085509",
        "PROBNNe>0.060968160133519106",
        "IPPUSig>12.100831932197247",
        "PIDe>4.55464748643044",
    ],
    [
        "PT",
        "IPPUSig",
        "TRGHP",
        "countTracks",
        "Signal_PT",
        "eOverP",
        "AbsIP",
        "IPErr",
        "BPVIPCHI2",
        "DeltaR",
        "mva1",
    ],
]

#############################################
# Final 2017 iteration (nominal Run1 taggers) => NO VELO CHARGE
#############################################

Summer2017Opt_Run1_noVeloCharge = OSElectronTagger(
    name='OSElectronTagger_Summer2017Opt_Run1_noVeloCharge')
Summer2017Opt_Run1_noVeloCharge.addTool(
    OSElectronClassifierFactory, name='ElectronMVA')
Summer2017Opt_Run1_noVeloCharge.ElectronMVA.ClassifierType = 'TMVA'
Summer2017Opt_Run1_noVeloCharge.ElectronMVA.ClassifierName = 'OSElectron_Data_Run1_All_Bu2JpsiK_XGB_BDT_v1r0'

Summer2017Opt_Run1_noVeloCharge.Aliases = {
    "BPVIPCHI2":
    "BPVIPCHI2()",
    "mva1":
    "MVA[{}]("
    "PT,"
    "IPPUSig,"
    "TRGHP,"
    "countTracks,"
    "Signal_PT,"
    "eOverP,"
    "AbsIP,"
    "IPErr,"
    "BPVIPCHI2,"
    "DeltaR"
    ")".format(Summer2017Opt_Run1_noVeloCharge.ElectronMVA.getFullName()),
}

Summer2017Opt_Run1_noVeloCharge.SelectionPipeline = [
    [
        "IsSignalDaughter==0",
        "MuonPIDIsMuon==0",
        "TRCHI2DOF<3",
        "PP_InAccHcal==1",
        "IPErr!=0",
        "eOverP<2",
        "eOverP>0.85",
        "TRTYPE==3",
        "P>3113.641968357261",
        "PT>1132.1202858954884",
        "TRGHP<0.8606785372127446",
        "minPhiDistance>=0.00803358872286969",
        "IPSig>0.02001354198539968",
        "PROBNNmu<0.9376480079276076",
        "PROBNNpi<0.9375142821029713",
        "PROBNNk<0.7650669539596838",
        "PROBNNp<0.7186940204085509",
        "PROBNNe>0.060968160133519106",
        "IPPUSig>12.100831932197247",
        "PIDe>4.55464748643044",
    ],
    [
        "PT",
        "IPPUSig",
        "TRGHP",
        "countTracks",
        "Signal_PT",
        "eOverP",
        "AbsIP",
        "IPErr",
        "BPVIPCHI2",
        "DeltaR",
        "mva1",
    ],
]

#############################################
# Final 2017 iteration (nominal Run2 taggers)
#############################################

Summer2017Opt_v2_Run2 = OSElectronTagger(
    name='OSElectronTagger_Summer2017Opt_v2_Run2')
Summer2017Opt_v2_Run2.addTool(OSElectronClassifierFactory, name='ElectronMVA')
Summer2017Opt_v2_Run2.ElectronMVA.ClassifierType = 'TMVA'
Summer2017Opt_v2_Run2.ElectronMVA.ClassifierName = 'OSElectron_Data_Run2_All_Bu2JpsiK_XGB_BDT_v2r0'

Summer2017Opt_v2_Run2.Aliases = {
    "BPVIPCHI2":
    "BPVIPCHI2()",
    "mva1":
    "MVA[{}]("
    "PT,"
    "IPPUSig,"
    "countTracks,"
    "Signal_PT,"
    "eOverP,"
    "AbsIP,"
    "IPErr,"
    "BPVIPCHI2,"
    "PROBNNghost_MC15TuneV1,"
    "DeltaQ,"
    "etaDistance,"
    "DeltaR"
    ")".format(Summer2017Opt_v2_Run2.ElectronMVA.getFullName()),
}

Summer2017Opt_v2_Run2.SelectionPipeline = [
    [
        "IsSignalDaughter==0",
        "MuonPIDIsMuon==0",
        "TRCHI2DOF<3",
        "PP_InAccHcal==1",
        "PP_VeloCharge>0",
        "PP_VeloCharge<1.4",
        "IPErr!=0",
        "eOverP<2",
        "eOverP>0.85",
        "TRTYPE==3",
        "P>5035.169513449979",
        "PT>1403.3298418685027",
        "TRGHP<0.842566731041535",
        "minPhiDistance>=0.016728744332624838",
        "IPSig>0.0419896167433678",
        "PROBNNmu_MC15TuneV1<0.15808731889714595",
        "PROBNNpi_MC15TuneV1<0.9833355298219992",
        "PROBNNk_MC15TuneV1<0.6949348710000995",
        "PROBNNp_MC15TuneV1<0.27070346277844964",
        "PROBNNe_MC15TuneV1>0.24262849376048545",
        "IPPUSig>9.334968382765219",
        "PIDe>4.33257736223062",
    ],
    [
        "PT",
        "IPPUSig",
        "countTracks",
        "Signal_PT",
        "eOverP",
        "AbsIP",
        "IPErr",
        "BPVIPCHI2",
        "PROBNNghost_MC15TuneV1",
        "DeltaQ",
        "etaDistance",
        "DeltaR",
        "mva1",
    ],
]

#############################################
# Final 2017 iteration (nominal Run2 taggers) => NO VELO CHARGE
#############################################

Summer2017Opt_v2_Run2_noVeloCharge = OSElectronTagger(
    name='OSElectronTagger_Summer2017Opt_v2_Run2')
Summer2017Opt_v2_Run2_noVeloCharge.addTool(
    OSElectronClassifierFactory, name='ElectronMVA')
Summer2017Opt_v2_Run2_noVeloCharge.ElectronMVA.ClassifierType = 'TMVA'
Summer2017Opt_v2_Run2_noVeloCharge.ElectronMVA.ClassifierName = 'OSElectron_Data_Run2_All_Bu2JpsiK_XGB_BDT_v2r0'

Summer2017Opt_v2_Run2_noVeloCharge.Aliases = {
    "BPVIPCHI2":
    "BPVIPCHI2()",
    "mva1":
    "MVA[{}]("
    "PT,"
    "IPPUSig,"
    "countTracks,"
    "Signal_PT,"
    "eOverP,"
    "AbsIP,"
    "IPErr,"
    "BPVIPCHI2,"
    "PROBNNghost_MC15TuneV1,"
    "DeltaQ,"
    "etaDistance,"
    "DeltaR"
    ")".format(Summer2017Opt_v2_Run2_noVeloCharge.ElectronMVA.getFullName()),
}

Summer2017Opt_v2_Run2_noVeloCharge.SelectionPipeline = [
    [
        "IsSignalDaughter==0",
        "MuonPIDIsMuon==0",
        "TRCHI2DOF<3",
        "PP_InAccHcal==1",
        "IPErr!=0",
        "eOverP<2",
        "eOverP>0.85",
        "TRTYPE==3",
        "P>5035.169513449979",
        "PT>1403.3298418685027",
        "TRGHP<0.842566731041535",
        "minPhiDistance>=0.016728744332624838",
        "IPSig>0.0419896167433678",
        "PROBNNmu_MC15TuneV1<0.15808731889714595",
        "PROBNNpi_MC15TuneV1<0.9833355298219992",
        "PROBNNk_MC15TuneV1<0.6949348710000995",
        "PROBNNp_MC15TuneV1<0.27070346277844964",
        "PROBNNe_MC15TuneV1>0.24262849376048545",
        "IPPUSig>9.334968382765219",
        "PIDe>4.33257736223062",
    ],
    [
        "PT",
        "IPPUSig",
        "countTracks",
        "Signal_PT",
        "eOverP",
        "AbsIP",
        "IPErr",
        "BPVIPCHI2",
        "PROBNNghost_MC15TuneV1",
        "DeltaQ",
        "etaDistance",
        "DeltaR",
        "mva1",
    ],
]

#######################################
# Run2 tagger optimised on Bu2D0pi
#######################################

Summer2017Opt_v1_Run2_Bu2D0pi = OSElectronTagger(
    name='OSElectronTagger_Summer2017Opt_v1_Run2_Bu2D0pi')
Summer2017Opt_v1_Run2_Bu2D0pi.addTool(
    OSElectronClassifierFactory, name='ElectronMVA')
Summer2017Opt_v1_Run2_Bu2D0pi.ElectronMVA.ClassifierType = 'TMVA'
Summer2017Opt_v1_Run2_Bu2D0pi.ElectronMVA.ClassifierName = 'OSElectron_Data_Run2_All_Bu2D0pi_XGBoost_BDT_v1r0'

Summer2017Opt_v1_Run2_Bu2D0pi.Aliases = {
    "BPVIPCHI2":
    "BPVIPCHI2()",
    "mva1":
    "MVA[{}]("
    "PT,"
    "IPPUSig,"
    "TRGHP,"
    "countTracks,"
    "Signal_PT,"
    "eOverP,"
    "AbsIP,"
    "IPErr,"
    "BPVIPCHI2,"
    "DeltaQ,"
    "etaDistance,"
    "DeltaR"
    ")".format(Summer2017Opt_v1_Run2_Bu2D0pi.ElectronMVA.getFullName()),
}

Summer2017Opt_v1_Run2_Bu2D0pi.SelectionPipeline = [
    [
        "IsSignalDaughter==0",
        "MuonPIDIsMuon==0",
        "TRCHI2DOF<3",
        "PP_InAccHcal==1",
        "PP_VeloCharge>0",
        "PP_VeloCharge<1.4",
        "IPErr!=0",
        "eOverP<2",
        "eOverP>0.85",
        "TRTYPE==3",
        "P>2246.262105806614",
        "PT>1263.3757637582146",
        "TRGHP<0.3476701870139611",
        "minPhiDistance>=0.02991088312365981",
        "IPSig>1.409708246571706",
        "PROBNNmu_MC15TuneV1<0.26285729014094217",
        "PROBNNpi_MC15TuneV1<0.9798783542274111",
        "PROBNNk_MC15TuneV1<0.9537421690096696",
        "PROBNNp_MC15TuneV1<0.7316835510621134",
        "PROBNNe_MC15TuneV1>0.03989689224497624",
        "IPPUSig>2.7578744882638135",
        "PIDe>-0.6905539376470706",
    ],
    [
        "PT",
        "IPPUSig",
        "TRGHP",
        "countTracks",
        "Signal_PT",
        "eOverP",
        "AbsIP",
        "IPErr",
        "BPVIPCHI2",
        "DeltaQ",
        "etaDistance",
        "DeltaR",
        "mva1",
    ],
]
