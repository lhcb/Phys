###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Configurables import (
    TaggerProtonSameTool,
    SSProtonTagger,
    SSProtonClassifierFactory,
)

from FlavourTagging.ClassicTunings import setTuningProperties

# classic tunings
ClassicBDT = TaggerProtonSameTool(name='SSProtonTagger_ClassicBDT')
# There seems to be no tuning for this tagger

Development = SSProtonTagger(name='SSProtonTagger_Development')
Development.addTool(SSProtonClassifierFactory, name='mva')
Development.mva.ClassifierName = 'SSProton_Data_Run1_All_Bd2Dpi_TMVA_BDT_v1r1'
Development.mva.ClassifierType = 'TMVA'

Development.Aliases = {
    "P":
    "P/GeV",
    "Pt":
    "PT/GeV",
    "ipsig":
    "IPSig",  #"BPVIPCHI2()",
    "gprob":
    "TRGHP",
    "dphi":
    "phiDistance",
    "dR":
    "DeltaR",
    "deta":
    "etaDistance",
    "dQ_p":
    "DeltaQ",
    "BPt":
    "Signal_PT",
    "Pt_tot":
    "Signal_TagPart_PT",
    "PIDK":
    "PIDK",
    "lcs":
    "TRCHI2DOF",
    "PVndof":
    "BPV(VDOF)",
    "IPPUs":
    "IPPUSig",
    'mva':
    'MVA[{}]('
    'log(P), log(Pt), log(ipsig), dR , log(deta), dQ_p, log(Pt_tot),'
    'log(PVndof), log(PIDp)'
    ')'.format(Development.mva.getFullName()),
}

Development.SelectionPipeline = [
    [
        "IsSignalDaughter==0",
        "IPPUs>3",
        "PIDp>5",
        "Pt>0.4",
        "ipsig<4",
        "gprob<0.5",
        "dQ_p<1.3*GeV",
        # "BPt>3*GeV",
        "Signal_TagPart_CHI2NDOF<100",
        "deta<1.2",  # same cut as preselection
        "dphi<1.2",  # same cut as preselection
        "Pt_tot>3*GeV",
        # "ABSID==2212",
        'P',
        'Pt',
        'ipsig',
        'dR',
        'PVndof',
        'mva',
    ],
]
