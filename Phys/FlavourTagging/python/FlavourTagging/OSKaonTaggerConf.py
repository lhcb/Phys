###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Configurables import (
    TaggerKaonOppositeTool,
    TaggerNEWKaonOppositeTool,
    OSKaonTagger,
    OSKaonClassifierFactory,
)

from FlavourTagging.ClassicTunings import setTuningProperties

# classic tunings
Stripping21 = TaggerKaonOppositeTool(name='OSKaonTagger_Stripping21')
setTuningProperties(Stripping21, 'Stripping21')

Stripping23 = TaggerKaonOppositeTool(name='OSKaonTagger_Stripping23')
setTuningProperties(Stripping23, 'Stripping23')

Stripping23NNet = TaggerNEWKaonOppositeTool(
    name='OSKaonTagger_Stripping23NNet')
setTuningProperties(Stripping23NNet, 'Stripping23')

Development = OSKaonTagger(name='OSKaonTagger_Development')
Development.addTool(OSKaonClassifierFactory, name='KaonMVA')
Development.KaonMVA.ClassifierType = 'TMVA'
Development.KaonMVA.ClassifierName = 'OSKaon_Data_Run1_All_Bu2JpsiK_TMVA_MLP_v1r0'

Development.Aliases = {
    "partP":
    "P/GeV",
    "partPt":
    "PT/GeV",
    "ptB":
    "Signal_PT/GeV",
    "partlcs":
    "TRCHI2DOF",
    "ghostProb":
    "TRGHP",
    "mult":
    "countTracks",
    "PIDNNk":
    "PROBNNk",
    "PIDNNpi":
    "PROBNNpi",
    "PIDNNp":
    "PROBNNp",
    "nnkrec":
    "nPV",
    "IPPU":
    "IPPUSig",
    "delProbNNk_p":
    "PROBNNk-PROBNNp",
    "delProbNNk_pi":
    "PROBNNk-PROBNNpi",
    "mva":
    "MVA[{}]("
    "mult, log(partP), log(partPt), nnkrec, log(ptB), log(IPSig),"
    "log(partlcs), PIDNNk, PIDNNpi, PIDNNp, log(ghostProb), log(IPPU)"
    ")".format(Development.KaonMVA.getFullName()),
}

Development.SelectionPipeline = [[
    "partPt>0.7",
    "partP>2.0",
    "TRTYPE==3",
    "partlcs<3",
    "minPhiDistance>0.005",
    "MuonPIDIsMuon==0",
    "TRLH>-999",
    "ghostProb<0.35",
    "PROBNNmu<0.8",
    "PROBNNe<0.8",
    "PIDNNpi<0.8",
    "PIDNNk>0.25",
    "PIDNNp<0.8",
    "PROBNNk-PROBNNp>0.0",
    "PROBNNk-PROBNNpi>-0.6",
    "PIDK>-100",
    "PIDK!=0",
    "PIDK-PIDp>-300",
    "IPPU>6.0",
    "IPSig>4.0",
    "AbsIP<1.6",
    "IsSignalDaughter==0",
],
                                 [
                                     "mult",
                                     "partP",
                                     "partPt",
                                     "nnkrec",
                                     "ptB",
                                     "IPSig",
                                     "partlcs",
                                     "PIDNNk",
                                     "PIDNNpi",
                                     "PIDNNp",
                                     "ghostProb",
                                     "IPPU",
                                     "mva",
                                 ]]

Summer2017Opt = OSKaonTagger(name='OSKaonTagger_Summer2017Opt')
Summer2017Opt.addTool(OSKaonClassifierFactory, name='KaonMVA')
Summer2017Opt.KaonMVA.ClassifierType = 'TMVA'
Summer2017Opt.KaonMVA.ClassifierName = 'OSKaon_Data_Run2_All_Bu2JpsiK_XGBoost_BDT_v2r0'
Summer2017Opt.Aliases = {
    'PROBNNk_PROBNNp':
    'PROBNNk-PROBNNp',
    'PROBNNk_PROBNNpi':
    'PROBNNk-PROBNNpi',
    'mva1':
    'MVA[{}]('
    'countTracks, P, PT, nPV, Signal_PT, IPSig, TRCHI2DOF, PROBNNk,'
    'PROBNNpi, PROBNNp, TRGHP, IPPUSig'
    ')'.format(Summer2017Opt.KaonMVA.getFullName()),
}

Summer2017Opt.SelectionPipeline = [
    [
        'P>5000',
        'TRTYPE==3',
        'TRCHI2DOF<=3',
        'minPhiDistance>=0.0025',
        'MuonPIDIsMuon==0',
        'TRGHP<0.52',
        'PROBNNmu<0.81',
        'PROBNNpi<0.96',
        'PROBNNp<0.9',
        'PROBNNe<0.89',
        'PROBNNk>0.35',
        'PROBNNk_PROBNNp>0',
        'PROBNNk_PROBNNpi>0',
        'IPSig>3.77',
        'IPPUSig>4',
        'AbsIP<0.85',
    ],
    [
        'countTracks',
        'P',
        'PT',
        'nPV',
        'Signal_PT',
        'IPSig',
        'TRCHI2DOF',
        'PROBNNk',
        'PROBNNpi',
        'PROBNNp',
        'TRGHP',
        'IPPUSig',
        'mva1',
    ],
]

Summer2017Opt_v2_Run2 = OSKaonTagger(name='OSKaonTagger_Summer2017Opt_v2_Run2')
Summer2017Opt_v2_Run2.addTool(OSKaonClassifierFactory, name='KaonMVA')
Summer2017Opt_v2_Run2.KaonMVA.ClassifierType = 'TMVA'
Summer2017Opt_v2_Run2.KaonMVA.ClassifierName = 'OSKaon_Data_Run2_All_Bu2JpsiK_XGBoost_BDT_v1r0'
Summer2017Opt_v2_Run2.Aliases = {
    # 'PROBNNk_PROBNNp': 'PROBNNk_MC15TuneV1-PROBNNp_MC15TuneV1',
    # 'PROBNNk_PROBNNpi': 'PROBNNk_MC15TuneV1-PROBNNpi_MC15TuneV1',
    'mva1':
    'MVA[{}]('
    'countTracks, P, PT, nPV, Signal_PT, IPSig, TRCHI2DOF, PROBNNk_MC15TuneV1,'
    'PROBNNpi_MC15TuneV1, PROBNNp_MC15TuneV1, TRGHP, IPPUSig'
    ')'.format(Summer2017Opt_v2_Run2.KaonMVA.getFullName()),
}

Summer2017Opt_v2_Run2.SelectionPipeline = [
    [
        'P>5000',
        'TRTYPE==3',
        'TRCHI2DOF<=3',
        'minPhiDistance>=0.0025',
        'MuonPIDIsMuon==0',
        'TRGHP<0.52',
        'PROBNNmu_MC15TuneV1<0.81',
        'PROBNNpi_MC15TuneV1<0.96',
        'PROBNNp_MC15TuneV1<0.9',
        'PROBNNe_MC15TuneV1<0.89',
        'PROBNNk_MC15TuneV1>0.35',
        # 'PROBNNk_PROBNNp>0',
        # 'PROBNNk_PROBNNpi>0',
        'IPSig>3.77',
        'IPPUSig>4',
        'AbsIP<0.85',
    ],
    [
        'countTracks',
        'P',
        'PT',
        'nPV',
        'Signal_PT',
        'IPSig',
        'TRCHI2DOF',
        'PROBNNk_MC15TuneV1',
        'PROBNNpi_MC15TuneV1',
        'PROBNNp_MC15TuneV1',
        'TRGHP',
        'IPPUSig',
        'mva1',
    ],
]

Summer2017Opt_v1_Run2_Bu2D0pi = OSKaonTagger(
    name='OSKaonTagger_Summer2017Opt_v1_Run2_Bu2D0pi')
Summer2017Opt_v1_Run2_Bu2D0pi.addTool(OSKaonClassifierFactory, name='KaonMVA')
Summer2017Opt_v1_Run2_Bu2D0pi.KaonMVA.ClassifierType = 'TMVA'
Summer2017Opt_v1_Run2_Bu2D0pi.KaonMVA.ClassifierName = 'OSKaon_Data_Run2_All_Bu2D0pi_XGBoost_BDT_v1r0'
Summer2017Opt_v1_Run2_Bu2D0pi.Aliases = {
    # 'PROBNNk_PROBNNp': 'PROBNNk_MC15TuneV1-PROBNNp_MC15TuneV1',
    # 'PROBNNk_PROBNNpi': 'PROBNNk_MC15TuneV1-PROBNNpi_MC15TuneV1',
    'mva1':
    'MVA[{}]('
    'countTracks, P, PT, nPV, Signal_PT, IPSig, TRCHI2DOF, PROBNNk_MC15TuneV1,'
    'PROBNNpi_MC15TuneV1, PROBNNp_MC15TuneV1, TRGHP, IPPUSig'
    ')'.format(Summer2017Opt_v1_Run2_Bu2D0pi.KaonMVA.getFullName()),
}

Summer2017Opt_v1_Run2_Bu2D0pi.SelectionPipeline = [
    [
        'IsSignalDaughter==0',
        'TRTYPE==3',
        'TRCHI2DOF<3',
        'MuonPIDIsMuon==0',
        "P>7451.575026928098",
        "PT>403.60610851943984",
        "TRGHP<0.8753798706142382",
        "minPhiDistance>=0.002798630718528145",
        "IPSig>3.718919286869216",
        "PROBNNmu_MC15TuneV1<0.9362979311285927",
        "PROBNNpi_MC15TuneV1<0.7288510924948121",
        "PROBNNe_MC15TuneV1<0.7119200102595166",
        "PROBNNk_MC15TuneV1>0.6368818843832009",
        "PROBNNp_MC15TuneV1<0.9038163278290787",
        # "PROBNNk_PROBNNp>0.2062873089278459",
        # "PROBNNk_PROBNNpi>0.1993228027012357",
        "AbsIP<1.624098560592783",
        "IPPUSig>6.260549462913483",
    ],
    [
        'countTracks',
        'P',
        'PT',
        'nPV',
        'Signal_PT',
        'IPSig',
        'TRCHI2DOF',
        'PROBNNk_MC15TuneV1',
        'PROBNNpi_MC15TuneV1',
        'PROBNNp_MC15TuneV1',
        'TRGHP',
        'IPPUSig',
        'mva1',
    ],
]

# Tagger is using the correctly trained versions of the mistag evaluation BDT
Summer2017Opt_v3_Run2 = OSKaonTagger(name='OSKaonTagger_Summer2017Opt_v3_Run2')
Summer2017Opt_v3_Run2.addTool(OSKaonClassifierFactory, name='KaonMVA')
Summer2017Opt_v3_Run2.KaonMVA.ClassifierType = 'TMVA'
Summer2017Opt_v3_Run2.KaonMVA.ClassifierName = 'OSKaon_Data_Run2_All_Bu2JpsiK_XGBoost_BDT_v2r0'
Summer2017Opt_v3_Run2.Aliases = {
    # 'PROBNNk_PROBNNp': 'PROBNNk_MC15TuneV1-PROBNNp_MC15TuneV1',
    # 'PROBNNk_PROBNNpi': 'PROBNNk_MC15TuneV1-PROBNNpi_MC15TuneV1',
    'mva1':
    'MVA[{}]('
    'countTracks, P, PT, nPV, Signal_PT, IPSig, TRCHI2DOF, PROBNNk_MC15TuneV1,'
    'PROBNNpi_MC15TuneV1, PROBNNp_MC15TuneV1, TRGHP, IPPUSig'
    ')'.format(Summer2017Opt_v3_Run2.KaonMVA.getFullName()),
}

# Tagger is using the same preselection as v2
Summer2017Opt_v3_Run2.SelectionPipeline = Summer2017Opt_v2_Run2.SelectionPipeline
