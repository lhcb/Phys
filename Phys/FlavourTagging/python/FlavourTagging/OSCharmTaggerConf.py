###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Configurables import (
    TaggerCharmTool, )

from FlavourTagging.ClassicTunings import setTuningProperties

# classic tunings
Stripping21 = TaggerCharmTool(name="OSCharmTagger_Stripping21")
setTuningProperties(Stripping21, 'Stripping21')

Stripping23 = TaggerCharmTool(name="OSCharmTagger_Stripping23")
setTuningProperties(Stripping23, 'Stripping23')
