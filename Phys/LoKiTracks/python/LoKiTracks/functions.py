#!/usr/bin/env python
###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# =============================================================================
## @file
#  The set of basic objects from LoKiTracks library
#
#        This file is a part of LoKi project -
#    "C++ ToolKit  for Smart and Friendly Physics Analysis"
#
#  The package has been designed with the kind help from
#  Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
#  contributions and advices from G.Raven, J.van Tilburg,
#  A.Golutvin, P.Koppenburg have been used in the design.
#
#  @author Vanya BELYAEV ibelyaev@physics.syr.edu
#  @date 2007-06-09
# =============================================================================
"""
The set of basic objects from LoKiTracks library

      This file is a part of LoKi project -
``C++ ToolKit  for Smart and Friendly Physics Analysis''

The package has been designed with the kind help from
Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
contributions and advices from G.Raven, J.van Tilburg,
A.Golutvin, P.Koppenburg have been used in the design.
"""
# =============================================================================
__author__ = "Vanya BELYAEV  Ivan.Belyaev@nikhef.nl "
__date__ = "2010-07-17"
__version__ = "CVS tag $Name:$, version $Revision$ "
# =============================================================================

import LoKiCore.decorators as _LoKiCore

# Namespaces:
from LoKiCore.basic import cpp, std, LoKi
LHCb = cpp.LHCb

## @see LoKi::Tracks::FastDOCAToBeamLine
Tr_FASTDOCATOBEAMLINE = LoKi.Tracks.FastDOCAToBeamLine

## @see LoKi::Tracks::TrFILTER
TrFILTER = LoKi.Tracks.Filter
## @see LoKi::Tracks::Tr_FILTER
Tr_FILTER = LoKi.Tracks.Filter

# =============================================================================
# The END
# =============================================================================
