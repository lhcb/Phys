/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
// Include files
// ============================================================================
// STD & STL
// ============================================================================
#include <functional>
#include <sstream>
// ============================================================================
// Event
// ============================================================================
#include "Event/Track.h"
// ============================================================================
// Track Interfaces
// ============================================================================
#include "TrackInterfaces/ITrackSelector.h"
// ============================================================================
// LoKi
// ============================================================================
#include "LoKi/Constants.h"
#include "LoKi/GetTools.h"
#include "LoKi/Tracks.h"
// ============================================================================
/** @file
 *  Implementation file for classes from the namespace LoKi::Tracks
 *
 *  This file is a part of LoKi project -
 *    "C++ ToolKit  for Smart and Friendly Physics Analysis"
 *
 *  The package has been designed with the kind help from
 *  Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
 *  contributions and advices from G.Raven, J.van Tilburg,
 *  A.Golutvin, P.Koppenburg have been used in the design.
 *
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date 2007-08-08
 *
 */
// ============================================================================

// ============================================================================
// constructor from the tool name
// ============================================================================
LoKi::Tracks::Filter::Filter( const std::string& nick )
    : LoKi::AuxFunBase( std::tie( nick ) ), LoKi::Tracks::Selector(), m_nick( nick ) {
  if ( gaudi() ) { getSelector(); }
}
// ============================================================================
void LoKi::Tracks::Filter::getSelector() const {
  const ITrackSelector* s = LoKi::GetTools::trackSelector( *this, m_nick );
  setSelector( s );
  Assert( 0 != s, "ITrackSelector* points to NULL" );
}
// ============================================================================
// MANDATORY: the only one essential method
// ============================================================================
LoKi::Tracks::Filter::result_type LoKi::Tracks::Filter::operator()( LoKi::Tracks::Filter::argument t ) const {
  //
  if ( 0 == t ) {
    Error( "LHCb::Track* points to NULL, return 'false'" ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    return false;
  }
  //
  if ( !selector() ) { getSelector(); }
  //
  return eval( t );
}
// ============================================================================
// OPTIONAL: the nice printout
// ============================================================================
std::ostream& LoKi::Tracks::Filter::fillStream( std::ostream& s ) const { return s << "TrFILTER('" << m_nick << "')"; }
// ============================================================================

// ============================================================================
// Constructor from bound
// ============================================================================
LoKi::Tracks::FastDOCAToBeamLine::FastDOCAToBeamLine( const double bound )
    : LoKi::AuxFunBase( std::tie( bound ) )
    , LoKi::BeamSpot( bound )
    , LoKi::BasicFunctors<const LHCb::Track*>::Function()
    , m_beamLine() {}
// ============================================================================
// Constructor from bound & condname
// ============================================================================
LoKi::Tracks::FastDOCAToBeamLine::FastDOCAToBeamLine( const double bound, const std::string& condname )
    : LoKi::AuxFunBase( std::tie( bound, condname ) )
    , LoKi::BeamSpot( bound, condname )
    , LoKi::BasicFunctors<const LHCb::Track*>::Function()
    , m_beamLine() {}
// ============================================================================
// Update beamspot position
// ============================================================================
StatusCode LoKi::Tracks::FastDOCAToBeamLine::updateCondition() {
  StatusCode sc = LoKi::BeamSpot::updateCondition();
  if ( sc.isFailure() ) { return sc; } // RETURN
  //
  m_beamLine = LoKi::FastVertex::Line( Gaudi::XYZPoint( x(), y(), 0. ), Gaudi::XYZVector( 0., 0., 1. ) );
  //
  return sc;
}
// ============================================================================
// MANDATORY: the only one essential method
// ============================================================================
LoKi::Tracks::FastDOCAToBeamLine::result_type LoKi::Tracks::FastDOCAToBeamLine::
                                              operator()( LoKi::Tracks::FastDOCAToBeamLine::argument t ) const {
  double doca;
  LoKi::FastVertex::distance( t, m_beamLine, doca ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  return doca;
}

// ============================================================================
// The END
// ============================================================================
