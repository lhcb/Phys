/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: $
#ifndef HLT1TRIGGERTISTOS_H
#define HLT1TRIGGERTISTOS_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "Kernel/ITriggerTisTos.h" // Interface

#include "TriggerTisTos.h"

/** @class Hlt1TriggerTisTos Hlt1TriggerTisTos.h
 *
 *  @author Tomasz Skwarnicki
 *  @date   2013-11-30
 *
 *  Same tool as TriggerTisTos; just different input locations
 */
class Hlt1TriggerTisTos : public TriggerTisTos, virtual public ITriggerTisTos {
public:
  /// Standard constructor
  Hlt1TriggerTisTos( const std::string& type, const std::string& name, const IInterface* parent );

  virtual ~Hlt1TriggerTisTos(); ///< Destructor

  StatusCode initialize() override;

private:
};
#endif // HLT1TRIGGERTISTOS_H
