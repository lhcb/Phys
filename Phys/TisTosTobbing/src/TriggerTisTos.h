/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: TriggerTisTos.h,v 1.11 2010-07-21 21:22:17 tskwarni Exp $
#ifndef TRIGGERTISTOS_H
#define TRIGGERTISTOS_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "Kernel/ITriggerTisTos.h" // Interface

#include "TriggerSelectionTisTos.h"

/** @class TriggerTisTos TriggerTisTos.h
 *
 *  @author Tomasz Skwarnicki
 *  @date   2007-08-20
 *
 *  Hit based implementation of Tis,Tos'ing Trigger specified via trigger name pattern.
 *  @sa  TriggerSelectionTisTos for inherited member functions (e.g. to define Offline Input).
 *  @sa  ITriggerTisTos docs for more explanation.
 *  This interface also defines inlined shortcuts to set Offline Input and or Trigger Input and get an output in one
 * call.
 */
class TriggerTisTos : public TriggerSelectionTisTos, virtual public ITriggerTisTos {
public:
  /// Standard constructor
  TriggerTisTos( const std::string& type, const std::string& name, const IInterface* parent );

  ~TriggerTisTos(); ///< Destructor

  StatusCode initialize() override;

  /// erase previous Trigger Input
  void setTriggerInput() override;

  /// add Trigger Selection Name pattern to Trigger Input; pattern may contain wild character *, all matches will be
  /// added
  void addToTriggerInput( const std::string& selectionNameWithWildChar ) override;

  /// Complete classification of the Trigger Input (see ITisTis::TisTosTob for the meaning)
  unsigned int tisTosTrigger() override;

  /// check for TOS  - may be faster than using tisTosTrigger()
  bool tosTrigger() override;

  /// check for TIS  - may be faster than using tisTosTrigger()
  bool tisTrigger() override;

  /// check for TUS (Trigger Used Signal: TPS or TOS) - may be faster than using tisTosTrigger()
  bool tusTrigger() override;

  /// analysis report
  std::string analysisReportTrigger() override;

  /// returns Trigger Selection names matching optional pattern of decision,tis,tos for previously set Trigger and
  /// Offline Inputs
  std::vector<std::string> triggerSelectionNames( unsigned int decisionRequirement = kAnything,
                                                  unsigned int tisRequirement      = kAnything,
                                                  unsigned int tosRequirement      = kAnything,
                                                  unsigned int tpsRequirement      = kAnything ) override;

  /// list of HltObjectSummaries from Selections satisfying TOS,TIS requirements (define Trigger and Offline Input
  /// before calling)
  std::vector<const LHCb::HltObjectSummary*> hltObjectSummaries( unsigned int tisRequirement = kAnything,
                                                                 unsigned int tosRequirement = kAnything,
                                                                 unsigned int tpsRequirement = kAnything ) override;

  /// Switch to allow intermediate selection
  bool m_allowIntermediate;

private:
  /// if true then warning about empty trigger inputs are printed
  bool m_trigInputWarn;

  /// obtain all known trigger names
  void getTriggerNames();

  static const std::vector<std::string> m_empty_selections;

  /// content of Trigger Input (list of trigger selection names)
  std::vector<std::string> m_triggerInput_Selections;

  /// all known trigger names
  std::vector<std::string> m_triggerNames;
};
#endif // TRIGGERTISTOS_H
