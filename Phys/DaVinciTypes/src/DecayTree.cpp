/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
// Include file
// ============================================================================
// PhysEvent
// ============================================================================
#include "Event/Particle.h"
// ============================================================================
// local
// ============================================================================
#include "Kernel/DecayTree.h"
#include "Kernel/TreeCloners.h"
// ============================================================================
/** @file
 *  Implementation of class LHCb::DecayTree
 *  @see LHcb::DecayTree
 */
// ============================================================================
/*  constructor from the decay head
 *  @attention the decay tree is cloned!
 *  @param head the decay tree to be cloned
 */
// ============================================================================
LHCb::DecayTree::DecayTree( const LHCb::Particle& head ) : m_head(), m_clonemap() {
  m_head.reset( DaVinci::cloneTree( &head, m_clonemap ) );
}
// ============================================================================
/*  destructor
 *  @attention the cloned decay tree is delete!
 */
// ============================================================================
LHCb::DecayTree::~DecayTree() { DaVinci::deleteTree( m_head.release() ); }
// ============================================================================
// find clone of particle in original decay tree
// ============================================================================
const LHCb::Particle* LHCb::DecayTree::findClone( const LHCb::Particle& orig ) const {
  auto it = m_clonemap.find( &orig );
  return it != m_clonemap.end() ? it->second : nullptr;
}
// ============================================================================
// find a particle in a decay tree based on PID
// ============================================================================
void LHCb::DecayTree::findInTree( const LHCb::Particle* particle, const LHCb::ParticleID& pid,
                                  LHCb::Particle::ConstVector& result ) {
  if ( !particle ) { return; }
  //
  if ( particle->particleID() == pid ) { result.push_back( particle ); }
  //
  for ( const LHCb::Particle* dau : particle->daughters() ) {
    if ( !dau ) { continue; }
    findInTree( dau, pid, result );
  }
}
// ============================================================================
// find a particle in a decay tree based on PID
// ============================================================================
const LHCb::Particle* LHCb::DecayTree::findFirstInTree( const LHCb::Particle* particle, const LHCb::ParticleID& pid ) {
  if ( !particle ) { return 0; }
  //
  if ( particle->particleID() == pid ) { return particle; }
  //
  for ( const LHCb::Particle* dau : particle->daughters() ) {
    if ( !dau ) { continue; }
    //
    const LHCb::Particle* found = findFirstInTree( dau, pid );
    //
    if ( found ) { return found; }
  }
  //
  return 0;
}
// ============================================================================
// find particle based on PID
// ============================================================================
const LHCb::Particle* LHCb::DecayTree::find( const LHCb::ParticleID& pid ) const {
  if ( valid() ) { return findFirstInTree( m_head.get(), pid ); }
  return 0;
}
// ============================================================================
// take ownership of all particles in decay treeL
// ============================================================================
LHCb::Particle* LHCb::DecayTree::release() {
  m_clonemap.clear();      //     clear the links
  return m_head.release(); // invalidate the head
}
// ============================================================================
// The END
// ============================================================================
