/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
// Include file
// ============================================================================
// RecEvent
// ============================================================================
#include "Event/RecVertex.h"
// ============================================================================
// local
// ============================================================================
#include "Kernel/RecVertexHolder.h"
// ============================================================================
/** @file
 *  Implementation of class LHCb::RecVertexHolder
 *  @see LHcb::RecVertexHolder
 *  @author Vanya BELYAEV Ivan.BElyaev@cern.ch
 *  @date 2010-12-02
 */
// ============================================================================
// constructor from the vertex
// ============================================================================
LHCb::RecVertexHolder::RecVertexHolder( const LHCb::RecVertex* vertex ) : m_vertex( vertex ) {}
// =============================================================================
/*  destructor
 *  @attention object, not registered in TES will be  deleted!
 */
// =============================================================================
LHCb::RecVertexHolder::~RecVertexHolder() {
  // do not delete the objects registred in TES
  if ( m_vertex && m_vertex->parent() ) { m_vertex.release(); }
}
// ============================================================================
// The END
// ============================================================================
