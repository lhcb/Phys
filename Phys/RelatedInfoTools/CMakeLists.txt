###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Phys/RelatedInfoTools
---------------------
#]=======================================================================]

gaudi_add_module(RelatedInfoTools
    SOURCES
        src/AddRelatedInfo.cpp
        src/RelInfoConeIsolation.cpp
        src/RelInfoConeVariables.cpp
        src/RelInfoConeVariablesForEW.cpp
        src/RelInfoCylVariables.cpp
        src/RelInfoPFVariables.cpp
        src/RelInfoVertexIsolation.cpp
        src/RelInfoVertexIsolationDetached.cpp
    LINK
        Boost::headers
        Gaudi::GaudiAlgLib
        Gaudi::GaudiKernel
        LHCb::CaloUtils
        LHCb::LHCbKernel
        LHCb::PhysEvent
        LHCb::RelationsLib
        Phys::DaVinciInterfacesLib
        Phys::DaVinciKernelLib
        Phys::DaVinciTypesLib
        Phys::LoKiPhysLib
)
