###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################


def checkForMissingData():

    from Configurables import GaudiSequencer, TESCheck

    from Configurables import DataOnDemandSvc
    dod = DataOnDemandSvc()
    dod.Dump = True

    trigEvTES = "Trigger/RawEvent"
    from Configurables import RawEventSelectiveCopy
    trigRawBankCopy = RawEventSelectiveCopy('CopyTriggerRawEvent')
    trigRawBankCopy.RawBanksToCopy = [
        'ODIN', 'HltSelReports', 'HltDecReports', 'HltRoutingBits',
        'HltVertexReports', 'L0Calo', 'L0CaloFull', 'L0DU', 'L0Muon',
        'L0MuonProcCand', 'L0PU'
    ]
    trigRawBankCopy.OutputRawEventLocation = trigEvTES

    dod.AlgMap[trigEvTES] = trigRawBankCopy

    recSumTES = "Rec/Summary"
    from Configurables import RecSummaryAlg
    summary = RecSummaryAlg("CreateRecSummary")

    dod.AlgMap[recSumTES] = summary
