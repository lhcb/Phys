/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id$
// ============================================================================
#ifndef LOKI_PARTICLES22_H
#define LOKI_PARTICLES22_H 1
// ============================================================================
// Include files
// ============================================================================
// LoKi
// ============================================================================
#include "LoKi/Particles1.h"
#include "LoKi/Vertices1.h"
// ============================================================================
namespace LoKi {
  // ==========================================================================
  namespace Particles {
    // ========================================================================
    /** @class DaughterVertexDistance
     *  Simple functor wich evalautes "the distance" between
     *  the 'daughetrs' vertices for the given cascade decays
     *
     *  @see LoKi::Cuts::D2DVVD
     *
     *  @attention indices start form 1, index 0 correspond sto mother particle
     *
     *  The distance is evaluated using LoKi::Vertices::VertexDistance functor
     *
     *  @see LoKi::Vertices::VertexDistance
     *  @see LoKi::Cuts::VVDIST
     *  @see LoKi::Cuts::VVD
     *  @author Vanya BELYAEV  Ivan.BElyaev@nikhef.nl
     *  @date 2008-09-29
     */
    class GAUDI_API DaughterVertexDistance : public LoKi::BasicFunctors<const LHCb::Particle*>::Function {
    public:
      // ======================================================================
      /// constructor form the daughter indices
      DaughterVertexDistance( const unsigned int d1, const unsigned int d2 = 0 );
      /// MANDATORY: clone method ("virtual constructor")
      DaughterVertexDistance* clone() const override { return new DaughterVertexDistance( *this ); }
      /// MANDATORY: the only one essential method
      result_type operator()( argument p ) const override;
      /// OPTIONAL: nice printout
      std::ostream& fillStream( std::ostream& s ) const override;
      // ======================================================================
    private:
      // ======================================================================
      /// the default constructor is disabled
      DaughterVertexDistance(); // the default constructor is disabled
      // ======================================================================
    private:
      // ======================================================================
      /// the index of the first daughter
      unsigned int m_d1; // the index of the first daughter
      /// the index of the second daughter
      unsigned int m_d2; // the index of the second daughter
      /// the actual 'distance'-functor
      LoKi::Vertices::VertexDistance m_vfun; // the actual 'distance'-functor
      // ======================================================================
    };
    // ========================================================================
    /** @class DaughterVertexsignedDistance
     *  Simple functor wich evalautes "the signed distance" between
     *  the 'daughetrs' vertices for the given cascade decays
     *
     *  @see LoKi::Cuts::D2DVVDSIGN
     *
     *  @attention indices start form 1, index 0 correspond sto mother particle
     *
     *  The distance is evaluated using LoKi::Vertices::VertexSignedDistance functor
     *
     *  @see LoKi::Vertices::VertexSignedDistance
     *  @see LoKi::Cuts::VVDSIGN
     *  @author Vanya BELYAEV  Ivan.BElyaev@nikhef.nl
     *  @date 2008-09-29
     */
    class GAUDI_API DaughterVertexSignedDistance : public LoKi::BasicFunctors<const LHCb::Particle*>::Function {
    public:
      // ======================================================================
      /// constructor form the daughter indices
      DaughterVertexSignedDistance( const unsigned int d1, const unsigned int d2 = 0 );
      /// MANDATORY: clone method ("virtual constructor")
      DaughterVertexSignedDistance* clone() const override { return new DaughterVertexSignedDistance( *this ); }
      /// MANDATORY: the only one essential method
      result_type operator()( argument p ) const override;
      /// OPTIONAL: nice printout
      std::ostream& fillStream( std::ostream& s ) const override;
      // ======================================================================
    private:
      // ======================================================================
      /// the default constructor is disabled
      DaughterVertexSignedDistance(); // the default constructor is disabled
      // ======================================================================
    private:
      // ======================================================================
      /// the index of the first daughter
      unsigned int m_d1; // the index of the first daughter
      /// the index of the second daughter
      unsigned int m_d2; // the index of the second daughter
      /// the actual 'distance'-functor
      LoKi::Vertices::VertexSignedDistance m_vfun; // the actual functor
      // ======================================================================
    };
    // ========================================================================
    /** @class DaughterVertexDistanceChi2
     *  Simple functor wich evalautes "the \f$\chi^2\f$-distance" between
     *  the 'daughetrs' vertices for the given cascade decays
     *
     *  @see LoKi::Cuts::D2DVVDCHI2
     *
     *  @attention indices start form 1, index 0 correspond sto mother particle
     *
     *  The distance is evaluated using LoKi::Vertices::VertexDistanceChi2 functor
     *
     *  @see LoKi::Vertices::VertexDistanceChi2
     *  @see LoKi::Cuts::VVDCHI2
     *  @author Vanya BELYAEV  Ivan.BElyaev@nikhef.nl
     *  @date 2008-09-29
     */
    class GAUDI_API DaughterVertexDistanceChi2 : public LoKi::BasicFunctors<const LHCb::Particle*>::Function {
    public:
      // ======================================================================
      /// constructor form the daughter indices
      DaughterVertexDistanceChi2( const unsigned int d1, const unsigned int d2 = 0 );
      /// MANDATORY: clone method ("virtual constructor")
      DaughterVertexDistanceChi2* clone() const override { return new DaughterVertexDistanceChi2( *this ); }
      /// MANDATORY: the only one essential method
      result_type operator()( argument p ) const override;
      /// OPTIONAL: nice printout
      std::ostream& fillStream( std::ostream& s ) const override;
      // ======================================================================
    private:
      // ======================================================================
      /// the default constructor is disabled
      DaughterVertexDistanceChi2(); // the default constructor is disabled
      // ======================================================================
    private:
      // ======================================================================
      /// the index of the first daughter
      unsigned int m_d1; // the index of the first daughter
      /// the index of the second daughter
      unsigned int m_d2; // the index of the second daughter
      /// the actual 'distance'-functor
      LoKi::Vertices::VertexChi2Distance m_vfun; // the actual functor
      // ======================================================================
    };
    // ========================================================================
    /** @class DaughterVertexDistanceDot
     *  Simple functor wich evalautes "the DOT-distance" between
     *  the 'daughetrs' vertices for the given cascade decays,
     *  It is a projected distance (on the momentum
     *  direction of the 'first' index)
     *
     *  @see LoKi::Cuts::D2DVVDDOT
     *
     *  @attention indices start form 1, index 0 correspond sto mother particle
     *
     *  The distance is evaluated using LoKi::Particles::VertexDotDistance functor
     *
     *  @see LoKi::Particles::VertexDotDistance
     *  @see LoKi::Cuts::VDDOT
     *  @author Vanya BELYAEV  Ivan.BElyaev@nikhef.nl
     *  @date 2008-09-29
     */
    class GAUDI_API DaughterVertexDistanceDot : public LoKi::BasicFunctors<const LHCb::Particle*>::Function {
    public:
      // ======================================================================
      /// constructor form the daughter indices
      DaughterVertexDistanceDot( const unsigned int d1, const unsigned int d2 = 0 );
      /// MANDATORY: clone method ("virtual constructor")
      DaughterVertexDistanceDot* clone() const override { return new DaughterVertexDistanceDot( *this ); }
      /// MANDATORY: the only one essential method
      result_type operator()( argument p ) const override;
      /// OPTIONAL: nice printout
      std::ostream& fillStream( std::ostream& s ) const override;
      // ======================================================================
    private:
      // ======================================================================
      /// the default constructor is disabled
      DaughterVertexDistanceDot(); // the default constructor is disabled
      // ======================================================================
    private:
      // ======================================================================
      /// the index of the first daughter
      unsigned int m_d1; // the index of the first daughter
      /// the index of the second daughter
      unsigned int m_d2; // the index of the second daughter
      /// the actual 'distance'-functor
      LoKi::Particles::VertexDotDistance m_vfun; // the actual functor
      // ======================================================================
    };
    // ========================================================================
    /** @class DaughterVertexDistanceSignedChi2
     *  Simple functor wich evalautes "the signed \f$\chi^2\f$-distance" between
     *  the 'daughetrs' vertices for the given cascade decays.
     *  The sign is evaluated from the sign of the expression:
     *   \f$ \left( \mathbf{v}_1-\mathbf{v}_2\right)\cdot \mathbf{p}_1 \f$
     *
     *  @see LoKi::Cuts::D2DVVDCHI2SIGN
     *
     *  @attention indices start form 1, index 0 correspond sto mother particle
     *
     *  The distance is evaluated using LoKi::Vertices::VertexDistanceChi2 functor
     *
     *  @see LoKi::Vertices::VertexDistanceChi2
     *  @see LoKi::Cuts::VVDCHI2
     *  @author Vanya BELYAEV  Ivan.BElyaev@nikhef.nl
     *  @date 2008-09-29
     */
    class GAUDI_API DaughterVertexDistanceSignedChi2 : public LoKi::BasicFunctors<const LHCb::Particle*>::Function {
    public:
      // ======================================================================
      /// constructor form the daughter indices
      DaughterVertexDistanceSignedChi2( const unsigned int d1, const unsigned int d2 = 0 );
      /// MANDATORY: clone method ("virtual constructor")
      DaughterVertexDistanceSignedChi2* clone() const override { return new DaughterVertexDistanceSignedChi2( *this ); }
      /// MANDATORY: the only one essential method
      result_type operator()( argument p ) const override;
      /// OPTIONAL: nice printout
      std::ostream& fillStream( std::ostream& s ) const override;
      // ======================================================================
    private:
      // ======================================================================
      /// the default constructor is disabled
      DaughterVertexDistanceSignedChi2(); // the default constructor is disabled
      // ======================================================================
    private:
      // ======================================================================
      /// the index of the first daughter
      unsigned int m_d1; // the index of the first daughter
      /// the index of the second daughter
      unsigned int m_d2; // the index of the second daughter
      /// the actual 'distance'-functor
      LoKi::Vertices::VertexChi2Distance m_vfun; // the actual functor
      // ======================================================================
    };
    // ========================================================================
  } // namespace Particles
  // ==========================================================================
} //                                                      end of namespace LoKi
// ============================================================================
//                                                                      The END
// ============================================================================
#endif // LOKI_PARTICLES22_H
