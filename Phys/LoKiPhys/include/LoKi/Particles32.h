/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
#ifndef LOKI_PARTICLES32_H
#define LOKI_PARTICLES32_H 1
// ============================================================================
// Include files
// ============================================================================
// LoKi
// ============================================================================
#include "LoKi/AuxDesktopBase.h"
#include "LoKi/PhysTypes.h"
// ============================================================================
namespace LoKi {
  // ==========================================================================
  namespace Particles {
    // ========================================================================
    /** @class BestPrimaryVertexAdaptor
     *  Simple adaptor,that delegates the evaluation of "vertex"
     *  functor to "best-primary-vertex"
     *  @see LoKi::Cuts::BPV
     *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
     *  @date 2010-02-19
     */
    class GAUDI_API BestPrimaryVertexAdaptor : public LoKi::BasicFunctors<const LHCb::Particle*>::Function,
                                               public virtual LoKi::AuxDesktopBase {
    public:
      // ======================================================================
      /// constructor from vertex-function
      BestPrimaryVertexAdaptor( const LoKi::PhysTypes::VFunc& vfun );
      /// MANDATORY: clone method ("virtual constructor")
      BestPrimaryVertexAdaptor* clone() const override;
      /// MANDATORY: the only one essential method
      result_type operator()( argument p ) const override;
      /// OPTIONAL: the nice string representation
      std::ostream& fillStream( std::ostream& s ) const override;
      // ======================================================================
    private:
      // ======================================================================
      /// the actual vertex-functor
      LoKi::PhysTypes::VFun m_vfun; // the actual vertex-functor
      // ======================================================================
    };
    // ========================================================================
  } // namespace Particles
  // ==========================================================================
} //                                                      end of namespace LoKi
// ============================================================================
//                                                                      The END
// ============================================================================
#endif // LOKI_PARTICLES32_H
