/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
#ifndef LOKI_PARTICLES39_H
#define LOKI_PARTICLES39_H 1
// ============================================================================
// Include files
// ============================================================================
// LoKiCore
// ============================================================================
// #include "LoKi/Listener.h"
#include "LoKi/UniqueKeeper.h"
// ============================================================================
// LoKiPhys
// ============================================================================
#include "LoKi/PhysTypes.h"
// ============================================================================
/** @file LoKi/Particles39.h
 *
 *  This file is a part of LoKi project -
 *    "C++ ToolKit  for Smart and Friendly Physics Analysis"
 *
 *  The package has been designed with the kind help from
 *  Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
 *  contributions and advices from G.Raven, J.van Tilburg,
 *  A.Golutvin, P.Koppenburg have been used in the design.
 *
 *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
 *  @date 2012-01-17
 *
 */
namespace LoKi {
  // ==========================================================================
  namespace Particles {
    // ========================================================================
    /** @class MinMaxDistance
     *  simple class to calculate min/max "distance" between
     *  the particle and set of particles
     *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
     *  @date 2012-01-17
     */
    class MinMaxDistance : public LoKi::BasicFunctors<const LHCb::Particle*>::Function,
                           public LoKi::UniqueKeeper<LHCb::Particle> {
    protected:
      // ======================================================================
      /// the actual type of "distance"-function
      typedef double ( *dist_func )( const LHCb::Particle*, const LHCb::Particle* );
      // ======================================================================
    public:
      // ======================================================================
      MinMaxDistance( const bool minval, const LHCb::Particle::Range& parts = LHCb::Particle::Range(),
                      dist_func dist = 0 );
      /// MANDATORY: clone method ("virtual constructor")
      MinMaxDistance* clone() const override;
      /// MANDATORY: the only essential method
      result_type operator()( argument p ) const override;
      // ======================================================================
    protected:
      // ======================================================================
      double distance( const LHCb::Particle* p1, const LHCb::Particle* p2 ) const;
      // ======================================================================
      /// the actual computation
      double distance( const LHCb::Particle* p ) const;
      bool   minimum() const { return m_minimum; }
      // ======================================================================
    private:
      // ======================================================================
      /// minmax flag
      bool m_minimum; // minmax flag
      /// the distance function
      dist_func m_distance; // the distance function
      // ======================================================================
    };
    // ========================================================================
    /** @class MinMaxDistanceWithSource
     *  simple class to calculate min/max "distance" between
     *  the particle and set of particles
     *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
     *  @date 2012-01-17
     */
    class MinMaxDistanceWithSource : public LoKi::Particles::MinMaxDistance {
    public:
      // ======================================================================
      MinMaxDistanceWithSource( const bool minval, const LoKi::BasicFunctors<const LHCb::Particle*>::Source& source,
                                dist_func dist = 0 );
      /// MANDATORY: clone method ("virtual constructor")
      MinMaxDistanceWithSource* clone() const override;
      /// MANDATORY: the only essential method
      result_type operator()( argument p ) const override;
      // ======================================================================
    protected:
      // ======================================================================
      typedef LoKi::BasicFunctors<const LHCb::Particle*>::Source iSource;
      typedef LoKi::Assignable<iSource>::Type                    Source;
      // ======================================================================
      const iSource& source() const { return m_source; }
      // ======================================================================
    private:
      // ======================================================================
      /// the source
      Source m_source; // the source
      // ======================================================================
    };
    // ========================================================================
    /** @class MinDR2
     *  Evalue the minimal \f$\left(\Delta R\right)^2\f$ with respect
     *  to set of particles
     *  @see LoKi::Cuts::MINDR2
     *  @author Vanya BELYAEV Ivan.BElyaev@itep.ru
     *  @date 2012-01-18
     */
    struct MinDR2 : MinMaxDistanceWithSource {
      // ======================================================================
      /// constructor from the source
      MinDR2( const LoKi::BasicFunctors<const LHCb::Particle*>::Source& source );
      /// constructor from the location & filter
      MinDR2( const std::string& location, const LoKi::BasicFunctors<const LHCb::Particle*>::Predicate& cut =
                                               LoKi::BasicFunctors<const LHCb::Particle*>::BooleanConstant( true ) );
      /// constructor from the locations & filter
      MinDR2( const std::vector<std::string>&                              locations,
              const LoKi::BasicFunctors<const LHCb::Particle*>::Predicate& cut =
                  LoKi::BasicFunctors<const LHCb::Particle*>::BooleanConstant( true ) );
      /// MANDATORY: clone method ("virtual constructor")
      MinDR2* clone() const override;
      /// OPTIONAL: nice printout
      std::ostream& fillStream( std::ostream& s ) const override;
      // ======================================================================
    };
    // ========================================================================
    /** @class MaxDR2
     *  Evalue the maximal \f$\left(\Delta R\right)^2\f$ with respect
     *  to set of particles
     *  @see LoKi::Cuts::MAXDR2
     *  @author Vanya BELYAEV Ivan.BElyaev@itep.ru
     *  @date 2012-01-18
     */
    struct MaxDR2 : MinMaxDistanceWithSource {
      // ======================================================================
      /// constructor from the source
      MaxDR2( const LoKi::BasicFunctors<const LHCb::Particle*>::Source& source );
      /// constructor from the location & filter
      MaxDR2( const std::string& location, const LoKi::BasicFunctors<const LHCb::Particle*>::Predicate& cut =
                                               LoKi::BasicFunctors<const LHCb::Particle*>::BooleanConstant( true ) );
      /// constructor from the locations & filter
      MaxDR2( const std::vector<std::string>&                              locations,
              const LoKi::BasicFunctors<const LHCb::Particle*>::Predicate& cut =
                  LoKi::BasicFunctors<const LHCb::Particle*>::BooleanConstant( true ) );
      /// MANDATORY: clone method ("virtual constructor")
      MaxDR2* clone() const override;
      /// OPTIONAL: nice printout
      std::ostream& fillStream( std::ostream& s ) const override;
      // ======================================================================
    };
    // ======================================================================
    /** @class MinKL
     *  Evaluate the minimal  KK-divergency with respect
     *  to set of particles
     *  @see LoKi::Cuts::MINKL
     *  @author Vanya BELYAEV Ivan.BElyaev@itep.ru
     *  @date 2012-01-18
     */
    struct MinKL : MinMaxDistanceWithSource {
      // ======================================================================
      /// constructor from the source
      MinKL( const LoKi::BasicFunctors<const LHCb::Particle*>::Source& source );
      /// constructor from the location & filter
      MinKL( const std::string& location, const LoKi::BasicFunctors<const LHCb::Particle*>::Predicate& cut =
                                              LoKi::BasicFunctors<const LHCb::Particle*>::BooleanConstant( true ) );
      /// constructor from the locations & filter
      MinKL( const std::vector<std::string>&                              locations,
             const LoKi::BasicFunctors<const LHCb::Particle*>::Predicate& cut =
                 LoKi::BasicFunctors<const LHCb::Particle*>::BooleanConstant( true ) );
      /// MANDATORY: clone method ("virtual constructor")
      MinKL* clone() const override;
      /// OPTIONAL: nice printout
      std::ostream& fillStream( std::ostream& s ) const override;
      // ======================================================================
    };
    // ========================================================================
    /** @class MinAngle
     *  Evaluate the minimal angle with respect
     *  to set of particles
     *  @see LoKi::Cuts::MINANGLE
     *  @see LoKi::PhysKinematics::deltaAlpha
     *  @author Vanya BELYAEV Ivan.BElyaev@itep.ru
     *  @date 2012-01-18
     */
    struct MinAngle : MinMaxDistanceWithSource {
      // ======================================================================
      /// constructor from the source
      MinAngle( const LoKi::BasicFunctors<const LHCb::Particle*>::Source& source );
      /// constructor from the location & filter
      MinAngle( const std::string& location, const LoKi::BasicFunctors<const LHCb::Particle*>::Predicate& cut =
                                                 LoKi::BasicFunctors<const LHCb::Particle*>::BooleanConstant( true ) );
      /// constructor from the locations & filter
      MinAngle( const std::vector<std::string>&                              locations,
                const LoKi::BasicFunctors<const LHCb::Particle*>::Predicate& cut =
                    LoKi::BasicFunctors<const LHCb::Particle*>::BooleanConstant( true ) );
      /// MANDATORY: clone method ("virtual constructor")
      MinAngle* clone() const override;
      /// OPTIONAL: nice printout
      std::ostream& fillStream( std::ostream& s ) const override;
      // ======================================================================
    };
    // ========================================================================
    /** @class MinM2
     *  Evaluate the minnmal mass-difference with respect to set of particles
     *  @see LoKi::Cuts::MINM2
     *  @see LoKi::PhysKinematics::minM2
     *  @author Vanya BELYAEV Ivan.BElyaev@itep.ru
     *  @date 2012-01-18
     */
    struct MinM2 : MinMaxDistanceWithSource {
      // ======================================================================
      /// constructor from the source
      MinM2( const LoKi::BasicFunctors<const LHCb::Particle*>::Source& source );
      /// constructor from the location & filter
      MinM2( const std::string& location, const LoKi::BasicFunctors<const LHCb::Particle*>::Predicate& cut =
                                              LoKi::BasicFunctors<const LHCb::Particle*>::BooleanConstant( true ) );
      /// constructor from the locations & filter
      MinM2( const std::vector<std::string>&                              locations,
             const LoKi::BasicFunctors<const LHCb::Particle*>::Predicate& cut =
                 LoKi::BasicFunctors<const LHCb::Particle*>::BooleanConstant( true ) );
      /// MANDATORY: clone method ("virtual constructor")
      MinM2* clone() const override;
      /// OPTIONAL: nice printout
      std::ostream& fillStream( std::ostream& s ) const override;
      // ======================================================================
    };
    // ========================================================================
    /** @class MaxIDs
     *  Evaluate the maximal overlap (in term of LHCIDs fraction)
     *  to set of particles
     *  @see LoKi::Cuts::MAXIDS
     *  @author Vanya BELYAEV Ivan.BElyaev@itep.ru
     *  @date 2012-01-18
     */
    struct MaxIDs : MinMaxDistanceWithSource {
      // ======================================================================
      /// constructor from the source
      MaxIDs( const LoKi::BasicFunctors<const LHCb::Particle*>::Source& source );
      /// constructor from the location & filter
      MaxIDs( const std::string& location, const LoKi::BasicFunctors<const LHCb::Particle*>::Predicate& cut =
                                               LoKi::BasicFunctors<const LHCb::Particle*>::BooleanConstant( true ) );
      /// constructor from the locations & filter
      MaxIDs( const std::vector<std::string>&                              locations,
              const LoKi::BasicFunctors<const LHCb::Particle*>::Predicate& cut =
                  LoKi::BasicFunctors<const LHCb::Particle*>::BooleanConstant( true ) );
      /// MANDATORY: clone method ("virtual constructor")
      MaxIDs* clone() const override;
      /// OPTIONAL: nice printout
      std::ostream& fillStream( std::ostream& s ) const override;
      // ======================================================================
    };
  } // namespace Particles
  // ==========================================================================
  namespace Cuts {
    // ========================================================================
    /** @typedef MINDR2
     *  simple functor to evaluate \f$ \min \left( \Delta R \right)^2 \f$
     *  @see LoKi::Particles::MinDR2
     *  @see LoKi::Particles::MinMaxDistanceWithSource
     *  @see LoKi::Particles::MinMaxDistance
     *  @see LoKi::PhysKinematics::deltaR2
     *  @see LoKi::Cuts::MAXDR2
     *  @author Vanya BEYAEV  Ivan.BElyaev@itep.ru
     *  @date 2012-01-18
     */
    typedef LoKi::Particles::MinDR2 MINDR2;
    // ========================================================================
    /** @typedef MAXDR2
     *  simple functor to evaluate \f$ \max \left( \Delta R \right)^2 \f$
     *  @see LoKi::Particles::MaxDR2
     *  @see LoKi::Particles::MinMaxDistanceWithSource
     *  @see LoKi::Particles::MinMaxDistance
     *  @see LoKi::PhysKinematics::deltaR2
     *  @see LoKi::Cuts::MINDR2
     *  @author Vanya BEYAEV  Ivan.BElyaev@itep.ru
     *  @date 2012-01-18
     */
    typedef LoKi::Particles::MaxDR2 MAXDR2;
    // ========================================================================
    /** @typedef MINKL
     *  simple functor to evaluate minimal KL-divergency
     *  @see LoKi::Particles::MinKL
     *  @see LoKi::PhysKinematics::kullback
     *  @author Vanya BEYAEV  Ivan.BElyaev@itep.ru
     *  @date 2012-01-18
     */
    typedef LoKi::Particles::MinKL MINKL;
    // ========================================================================
    /** @typedef MAXIDS
     *  simple functor to evaluate the maximal overlap (interms of LHCbIDs)
     *  @see LoKi::Particles::MaxIDs
     *  @see LHCb::HashIDs
     *  @see LHCb::HashIDs::overlap
     *  @author Vanya BEYAEV  Ivan.BElyaev@itep.ru
     *  @date 2012-01-18
     */
    typedef LoKi::Particles::MaxIDs MAXIDS;
    // ========================================================================
    /** @typedef MINANGLE
     *  simple functor to evaluate the minimal angle between particles
     *  @see LoKi::Particles::MinAngle
     *  @see LoKi::PhysKinematics::deltaAlpha
     *  @author Vanya BEYAEV  Ivan.BElyaev@itep.ru
     *  @date 2012-01-18
     */
    typedef LoKi::Particles::MinAngle MINANGLE;
    // ========================================================================
    /** @typedef MINM2
     *  simple functor to evaluate the minimal angle between particles
     *  @see LoKi::Particles::MinM2
     *  @see LoKi::PhysKinematics::minM2
     *  @author Vanya BEYAEV  Ivan.BElyaev@itep.ru
     *  @date 2012-01-18
     */
    typedef LoKi::Particles::MinM2 MINM2;
    // ========================================================================
  } // namespace Cuts
  // ==========================================================================
} //                                                      end of namespace LoKi
// ============================================================================
// The END
// ============================================================================
#endif // LOKI_PARTICLES39_H
