/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
#ifndef LOKI_PARTICLES28_H
#define LOKI_PARTICLES28_H 1
// ============================================================================
// Include files
// ============================================================================
// LoKi
// ============================================================================
#include "LoKi/Particles27.h"
// ============================================================================
/** @file LoKi/Particles28.h
 *  The file with functors for various "refit"
 *  @author Vanya BELYAEV Ivan.BElyaev@nikhef.nl
 *  @date   2009-04-30
 */
namespace LoKi {
  // ==========================================================================
  namespace Particles {
    // ========================================================================
    /** @class ReFitter
     *  Helper functor to perform re-fit of the particle using
     *  IParticleReFit tool
     *  @see LoKi::Cuts::REFIT_
     *  @see LoKi::Cuts::REFIT
     *  @see LoKi::Particles::ReFit
     *  @see IParticleReFitter
     *  @author Vanya BELYAEV Ivan.BElyaev@nikhef.nl
     *  @date   2009-04-30
     */
    class GAUDI_API ReFitter : public LoKi::Particles::ReFit {
    public:
      // ======================================================================
      /// constructor from the tool name
      ReFitter( const std::string& name );
      /// MANDATORY: clone method ("virtual constructor")
      ReFitter* clone() const override { return new ReFitter( *this ); }
      /// MANDATORY: the only one essential method
      result_type operator()( argument p ) const override;
      /// OPTIONAL: nice printout
      std::ostream& fillStream( std::ostream& s ) const override;
      // ======================================================================
    private:
      // ======================================================================
      /// the refitter name
      std::string m_name; // the refitter name
      // ======================================================================
    };
    // ========================================================================
    /** @class MassFitter
     *  Helper functor to perform mass-constrained fit of the particle using
     *  IMassFit tool
     *  @see LoKi::Cuts::MFIT_
     *  @see LoKi::Cuts::MFIT
     *  @see LoKi::Particles::MassFit
     *  @see IMassFit
     *  @author Vanya BELYAEV Ivan.BElyaev@nikhef.nl
     *  @date   2009-04-30
     */
    class GAUDI_API MassFitter : public LoKi::Particles::MassFit {
    public:
      // ======================================================================
      /// constructor from the tool name
      MassFitter( const std::string& name = "" );
      /// constructor from the tool name
      MassFitter( const double mass, const std::string& name = "" );
      /// constructor from the tool name
      MassFitter( const std::string& name, const double mass );
      /// MANDATORY: clone method ("virtual constructor")
      MassFitter* clone() const override { return new MassFitter( *this ); }
      /// MANDATORY: the only one essential method
      result_type operator()( argument p ) const override;
      /// OPTIONAL: nice printout
      std::ostream& fillStream( std::ostream& s ) const override;
      // ======================================================================
    private:
      // ======================================================================
      /// the toolname
      std::string m_name; // the toolname
      // ======================================================================
    };
    // ========================================================================
  } //                                               end of namespace Particles
  // ==========================================================================
  namespace Cuts {
    // ========================================================================
    /** @typedef REFIT
     *  Simple predicate to perform the re=-fit of particle candidate
     *
     *  @code
     *
     *   const REFIT fit = REFIT ( fitter ) ;
     *
     *   const LHCb::Particle* B = ... ;
     *
     *   const bool ok = fit ( B ) ;
     *   if ( !ok ) {  ... error here ... }
     *
     *  @endcode
     *  @see IParticleReFitter
     *  @see LoKi::Particle::ReFitter
     *  @see LoKi::Particle::ReFit
     *  @see LoKi::Cuts::REFIT_
     *  @see LoKi::Cuts::REFITTER
     *  @author Vanya BELYAEV Ivan.BElyaev@nikhef.nl
     *  @date   2009-04-30
     */
    using REFIT = LoKi::Particles::ReFitter;
    // ========================================================================
    /** @typedef REFITTER
     *  Simple predicate to perform the re=-fit of particle candidate
     *
     *  @code
     *
     *   const REFITER fit = REFITTER ( fitter ) ;
     *
     *   const LHCb::Particle* B = ... ;
     *
     *   const bool ok = fit ( B ) ;
     *   if ( !ok ) {  ... error here ... }
     *
     *  @endcode
     *  @see IParticleReFitter
     *  @see LoKi::Particle::ReFitter
     *  @see LoKi::Particle::ReFit
     *  @see LoKi::Cuts::REFIT_
     *  @see LoKi::Cuts::REFIT
     *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
     *  @date   2009-04-30
     */
    using REFITTER = LoKi::Particles::ReFitter;
    // ========================================================================
    /** @var MFIT
     *  Simple predicate to perform mass-constained fit fo particle candidate
     *
     *  @code
     *
     *   const LHCb::Particle* B1 = ... ;
     *   // fir to the nominal mass
     *   const bool ok1 = MFIT ( B1 ) ;
     *   if ( !ok1 ) {  ... error here ... }
     *
     *  @endcode
     *  @see IMassFit
     *  @see LoKi::Particle::MassFit
     *  @see LoKi::Particle::MFIT_
     *  @see LoKi::Particle::MassFit
     *  @author Vanya BELYAEV Ivan.BElyaev@nikhef.nl
     *  @date   2009-04-30
     */
    const auto MFIT = LoKi::Particles::MassFitter{};
    // ========================================================================
    /** @typedef MFITTER
     *  Simple predicate to perform mass-constained fit fo particle candidate
     *
     *  @code
     *
     *   const LHCb::Particle* B1 = ... ;
     *
     *   const MFITTER mfit = MFITTER ( 5.40 * GeV ) ;
     *
     *   const bool ok = mfit ( B )
     *   if ( !ok ) {  ... error here ... }
     *
     *  @endcode
     *  @see IMassFit
     *  @see LoKi::Particle::MassFit
     *  @see LoKi::Particle::MFIT_
     *  @see LoKi::Particle::MFIT
     *  @see LoKi::Particle::MASSFIT
     *  @see LoKi::Particle::MassFit
     *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
     *  @date   2009-04-30
     */
    using MFITTER = LoKi::Particles::MassFitter;
    // ========================================================================
    /** @typedef MASSFIT
     *  Simple predicate to perform mass-constained fit fo particle candidate
     *
     *  @code
     *
     *   const LHCb::Particle* B1 = ... ;
     *
     *   const MASSFIT mfit = MASSFIT( 5.40 * GeV ) ;
     *
     *   const bool ok = mfit ( B )
     *   if ( !ok ) {  ... error here ... }
     *
     *  @endcode
     *  @see IMassFit
     *  @see LoKi::Particle::MassFit
     *  @see LoKi::Particle::MFIT
     *  @see LoKi::Particle::MFIT_
     *  @see LoKi::Particle::MASSFIT
     *  @see LoKi::Particle::MassFit
     *  @author Vanya BELYAEV Ivan.BElyaev@nikhef.nl
     *  @date   2009-04-30
     */
    using MASSFIT = LoKi::Particles::MassFitter;
    // ========================================================================
  } // namespace Cuts
  // ==========================================================================
} //                                                      end of namespace LoKi
// ============================================================================
//                                                                      The END
// ============================================================================
#endif // LOKI_PARTICLES28_H
