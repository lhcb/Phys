/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
#ifndef LOKI_PARTICLES18_H
#define LOKI_PARTICLES18_H 1
// ============================================================================
// Include files
// ============================================================================
// LoKiCore
// ============================================================================
#include "LoKi/ExtraInfo.h"
// ============================================================================
// LoKiPhys
// ============================================================================
#include "LoKi/PhysTypes.h"
// ============================================================================
namespace LoKi {
  // ==========================================================================
  namespace Particles {
    // ========================================================================
    /** @class HasInfo
     *  Trivial predicate which evaluates LHCb::Particle::hasInfo
     *  function
     *
     *  It relies on the method LHCb::Particle::hasInfo
     *
     *  @see LHCb::Particle
     *  @see LoKi::Cuts::HASINFO
     *
     *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
     *  @date 2006-02-15
     */
    class GAUDI_API HasInfo : public LoKi::ExtraInfo::CheckInfo<const LHCb::Particle*> {
    public:
      // ======================================================================
      // constructor from the key
      HasInfo( int key )
          : LoKi::AuxFunBase( std::tie( key ) ), LoKi::ExtraInfo::CheckInfo<const LHCb::Particle*>( key ) {}
      /// clone method (mandatory!)
      HasInfo* clone() const override { return new HasInfo( *this ); }
      /// the specific printout
      std::ostream& fillStream( std::ostream& s ) const override;
      // ======================================================================
    };
    // ========================================================================
    /** @class Info
     *  Trivial function which evaluates LHCb::Particle::info
     *
     *  It relies on the method LHCb::Particle::info
     *
     *  @see LHCb::Particle
     *  @see LoKi::Cuts::INFO
     *
     *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
     *  @date 2006-02-15
     */
    class GAUDI_API Info : public LoKi::ExtraInfo::GetInfo<const LHCb::Particle*, double> {
    public:
      // ======================================================================
      // constructor from the key
      Info( int key, double def )
          : LoKi::AuxFunBase( std::tie( key, def ) ), LoKi::ExtraInfo::GetInfo<const LHCb::Particle*>( key, def ) {}
      /// clone method (mandatory!)
      Info* clone() const override { return new Info( *this ); }
      /// the specific printout
      std::ostream& fillStream( std::ostream& s ) const override;
      // ======================================================================
    };
    // ========================================================================
  } // namespace Particles
  // ==========================================================================
} //                                                      end of namespace LoKi
// ============================================================================
//                                                                      The END
// ============================================================================
#endif // LOKI_PARTICLES18_H
