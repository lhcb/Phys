/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
#ifndef LOKI_PARTICLES13_H
#define LOKI_PARTICLES13_H 1
// ============================================================================
// Include files
// ============================================================================
// LoKiPhys
// ============================================================================
#include "LoKi/PhysTypes.h"
// ============================================================================
namespace LHCb {
  class State;
}
// ============================================================================
/** @file
 *
 *  This file is a part of LoKi project -
 *    "C++ ToolKit  for Smart and Friendly Physics Analysis"
 *
 *  The package has been designed with the kind help from
 *  Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
 *  contributions and advices from G.Raven, J.van Tilburg,
 *  A.Golutvin, P.Koppenburg have been used in the design.
 *
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date 2006-02-23
 */
// ============================================================================
namespace LoKi {
  // ==========================================================================
  namespace Particles {
    // ========================================================================
    /** @class TrackChi2
     *  The trivial funtion which evaluates LHCb::Track::chi2
     *
     *  @see LHCb::Particle
     *  @see LHCb::Track
     *  @see LHCb::Track::chi2
     *  @see LoKi::Cuts::TRCHI2
     *
     *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
     *  @date 2006-02-23
     */
    class GAUDI_API TrackChi2 final : public LoKi::BasicFunctors<const LHCb::Particle*>::Function {
    public:
      // ======================================================================
      /// Default Constructor
      TrackChi2() : AuxFunBase{std::tie()} {}
      /// MANDATORY: clone method ("vitual constructor")
      TrackChi2* clone() const override { return new TrackChi2( *this ); }
      /// MANDATORY: the only one essential method
      result_type operator()( argument p ) const override;
      /// OPTIONAL:  the specific printout
      std::ostream& fillStream( std::ostream& s ) const override;
      // ======================================================================
    };
    // ========================================================================
    /** @class TrackChi2PerDoF
     *  The trivial funtion which evaluates LHCb::Track::chi2PerDoF
     *
     *  @see LHCb::Particle
     *  @see LHCb::Track
     *  @see LHCb::Track::chi2PerDoF
     *  @see LoKi::Cuts::TRCHI2DOF
     *
     *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
     *  @date 2006-02-23
     */
    class GAUDI_API TrackChi2PerDoF final : public LoKi::BasicFunctors<const LHCb::Particle*>::Function {
    public:
      // ======================================================================
      /// Default Constructor
      TrackChi2PerDoF() : AuxFunBase{std::tie()} {}
      /// MANDATORY: clone method ("vitual constructor")
      TrackChi2PerDoF* clone() const override { return new TrackChi2PerDoF( *this ); }
      /// MANDATORY: the only one essential method
      result_type operator()( argument p ) const override;
      /// OPTIONAL:  the specific printout
      std::ostream& fillStream( std::ostream& s ) const override;
      // ======================================================================
    };
    // ========================================================================
    /** @class TrackChi2Prob
     *  The trivial function whcih evaluates the track's
     *  \f$\chi^2\f$-probability.
     *
     *  The GSL routine <c>gsl_cdf_chisq_Q</c> is used for evaluation
     *  @see LHCb::Track
     *  @see LoKi::TRPCHI2
     *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
     *  @date 2008-04-10
     */
    class GAUDI_API TrackChi2Prob final : public LoKi::BasicFunctors<const LHCb::Particle*>::Function {
    public:
      // ======================================================================
      /// Default Constructor
      TrackChi2Prob() : AuxFunBase{std::tie()} {}
      /// MANDATORY: clone method ("virtual constructor")
      TrackChi2Prob* clone() const override { return new TrackChi2Prob( *this ); }
      /// MANDATORY: the only one essential method
      result_type operator()( argument p ) const override;
      /// OPTIONAL: the specific printout
      std::ostream& fillStream( std::ostream& s ) const override;
      // ======================================================================
    };
    // ========================================================================
    /** @class TrackHasState
     *  The trivial funtion which evaluates LHCb::Track::hasStateAt
     *
     *  @see LHCb::Particle
     *  @see LHCb::Track
     *  @see LHCb::Track::hasStateAt
     *  @see LoKi::Cuts::HASSTATE
     *
     *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
     *  @date 2006-02-23
     */
    class GAUDI_API TrackHasState final : public LoKi::BasicFunctors<const LHCb::Particle*>::Predicate {
    public:
      // ======================================================================
      /// constructor from the state location
      TrackHasState( const LHCb::State::Location location );
      /// MANDATORY: clone method ("vitual constructor")
      TrackHasState* clone() const override { return new TrackHasState( *this ); }
      /// MANDATORY: the only one essential method
      result_type operator()( argument p ) const override;
      /// OPTIONAL:  the specific printout
      std::ostream& fillStream( std::ostream& s ) const override;
      // ======================================================================
    private:
      // ======================================================================
      // location of the state
      LHCb::State::Location m_location;
      // ======================================================================
    };
    // ========================================================================
    /** @class TrackType
     *  The trivial function which evaluates LHCb::Track::type
     *
     *  @see LHCb::Particle
     *  @see LHCb::Track
     *  @see LHCb::Track::Type
     *  @see LHCb::Track::type
     *  @see LoKi::Cuts::TRTYPE
     *
     *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
     *  @date 2006-02-23
     */
    class GAUDI_API TrackType final : public LoKi::BasicFunctors<const LHCb::Particle*>::Function {
    public:
      // ======================================================================
      /// Default Constructor
      TrackType() : AuxFunBase{std::tie()} {}
      /// MANDATORY: clone method ("vitual constructor")
      TrackType* clone() const override { return new TrackType( *this ); }
      /// MANDATORY: the only one essential method
      result_type operator()( argument p ) const override;
      /// OPTIONAL:  the specific printout
      std::ostream& fillStream( std::ostream& s ) const override;
      // ======================================================================
    };
    // ========================================================================
    /** @class TrackHistory
     *  The trivial function which evaluates LHCb::Track::history
     *
     *  @see LHCb::Particle
     *  @see LHCb::Track
     *  @see LHCb::Track::History
     *  @see LHCb::Track::history
     *  @see LoKi::Cuts::TRHISTORY
     *
     *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
     *  @date 2006-02-23
     */
    class GAUDI_API TrackHistory final : public LoKi::BasicFunctors<const LHCb::Particle*>::Function {
    public:
      // ======================================================================
      /// Default Constructor
      TrackHistory() : AuxFunBase{std::tie()} {}
      /// MANDATORY: clone method ("vitual constructor")
      TrackHistory* clone() const override { return new TrackHistory( *this ); }
      /// MANDATORY: the only one essential method
      result_type operator()( argument p ) const override;
      /// OPTIONAL:  the specific printout
      std::ostream& fillStream( std::ostream& s ) const override;
      // ======================================================================
    };
    // ========================================================================
    /** @class TrackHistoryFit
     *  The trivial function which evaluates LHCb::Track::historyFit
     *
     *  @see LHCb::Particle
     *  @see LHCb::Track
     *  @see LHCb::Track::HistoryFit
     *  @see LHCb::Track::historyFit
     *  @see LoKi::Cuts::TRHISTFIT
     *
     *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
     *  @date 2006-02-23
     */
    class GAUDI_API TrackHistoryFit final : public LoKi::BasicFunctors<const LHCb::Particle*>::Function {
    public:
      // ======================================================================
      /// Default Constructor
      TrackHistoryFit() : AuxFunBase{std::tie()} {}
      /// MANDATORY: clone method ("vitual constructor")
      TrackHistoryFit* clone() const override { return new TrackHistoryFit( *this ); }
      /// MANDATORY: the only one essential method
      result_type operator()( argument p ) const override;
      /// OPTIONAL:  the specific printout
      std::ostream& fillStream( std::ostream& s ) const override;
      // ======================================================================
    };
    // ========================================================================
    /** @class TrackStatus
     *  The trivial function which evaluates LHCb::Track::status
     *
     *  @see LHCb::Particle
     *  @see LHCb::Track
     *  @see LHCb::Track::Status
     *  @see LHCb::Track::status
     *  @see LoKi::Cuts::TRSTATUS
     *
     *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
     *  @date 2006-02-23
     */
    class GAUDI_API TrackStatus final : public LoKi::BasicFunctors<const LHCb::Particle*>::Function {
    public:
      // ======================================================================
      /// Default Constructor
      TrackStatus() : AuxFunBase{std::tie()} {}
      /// MANDATORY: clone method ("vitual constructor")
      TrackStatus* clone() const override { return new TrackStatus( *this ); }
      /// MANDATORY: the only one essential method
      result_type operator()( argument p ) const override;
      /// OPTIONAL:  the specific printout
      std::ostream& fillStream( std::ostream& s ) const override;
      // ======================================================================
    };
    // ========================================================================
    /** @class GhostProbability
     *  The trivial function which evaluates "ghost probability"
     *  @see LoKi::Cuts::TRGHOSTPROB
     *  @see LoKi::Cuts::TRGP
     *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
     *  @date 2009-10-02
     */
    class GAUDI_API GhostProbability final : public LoKi::BasicFunctors<const LHCb::Particle*>::Function {
    public:
      // ======================================================================
      /// Default Constructor
      GhostProbability() : AuxFunBase{std::tie()} {}
      /// MANDATORY: clone method ("vitual constructor")
      GhostProbability* clone() const override { return new GhostProbability( *this ); }
      /// MANDATORY: the only one essential method
      result_type operator()( argument p ) const override;
      /// OPTIONAL:  the specific printout
      std::ostream& fillStream( std::ostream& s ) const override;
      // ======================================================================
    };
    // ========================================================================
    /** @class Track Likelihood
     *  The trivial function which evaluates "track likelihood"
     *  @see LoKi::Cuts::TRLIKELIHOOD
     *  @see LoKi::Cuts::TRLH
     *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
     *  @date 2009-10-02
     */
    class GAUDI_API TrackLikelihood final : public LoKi::BasicFunctors<const LHCb::Particle*>::Function {
    public:
      // ======================================================================
      /// Default Constructor
      TrackLikelihood() : AuxFunBase{std::tie()} {}
      /// MANDATORY: clone method ("vitual constructor")
      TrackLikelihood* clone() const override { return new TrackLikelihood( *this ); }
      /// MANDATORY: the only one essential method
      result_type operator()( argument p ) const override;
      /// OPTIONAL:  the specific printout
      std::ostream& fillStream( std::ostream& s ) const override;
      // ======================================================================
    };
    // ========================================================================
  } // namespace Particles
  // ==========================================================================
} //                                                      end of namespace LoKi
// ============================================================================
// The END
// ============================================================================
#endif // LOKI_PARTICLES12_H
