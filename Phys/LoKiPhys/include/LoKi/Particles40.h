/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
#ifndef LOKI_PARTICLES40_H
#define LOKI_PARTICLES40_H 1
// ============================================================================
// Include files
// ============================================================================
#include <Event/Particle.h>
// ============================================================================
// LoKiCore
// ============================================================================
#include "LoKi/ExtraInfo.h"
// ============================================================================
// LoKiPhys
// ============================================================================
/** @file LoKi/Particles40.h
 *
 *  This file is a part of LoKi project -
 *    "C++ ToolKit  for Smart and Friendly Physics Analysis"
 *
 *  The package has been designed with the kind help from
 *  Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
 *  contributions and advices from G.Raven, J.van Tilburg,
 *  A.Golutvin, P.Koppenburg have been used in the design.
 *
 *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
 *  @date 2012-02-27
 *
 */
namespace LoKi {
  // ==========================================================================
  namespace Particles {
    // ========================================================================
    /** @class SmartInfo
     *
     *  Trivial function which:
     *    - checks the presence of information in LHCb::Particle::extraInfo
     *    - if the information present, it returns it
     *    - for missing infomation, use function to evaluate it
     *    - (optionally) fill th emissing field
     *
     *  @see LHCb::Particle
     *  @see LoKi::Cuts::SINFO
     *  @see LoKi::ExtraInfo::GetSmartInfo
     *  @see LoKi::ExtraInfo::info
     *  @see LoKi::ExtraInfo::hasInfo
     *  @see LoKi::ExtraInfo::setInfo
     *
     *  @author Vanya BELYAEV Ivan.Belyaev@cern.ch
     *  @date 2012-02-27
     */
    struct GAUDI_API SmartInfo : LoKi::ExtraInfo::GetSmartInfo<const LHCb::Particle*> {
      // ======================================================================
      /** constructor from fuction, key and update-flag
       *  @param index the key in LHCb::Particle::extraInfo table
       *  @param fun functionto be evaluated for missing keys
       *  @param update the flag to allow the insert of mnissing information
       */
      SmartInfo( const int index, const LoKi::BasicFunctors<const LHCb::Particle*>::Function& fun,
                 const bool update = false );
      /** constructor from fuction, key and update-flag
       *  @param fun functionto be evaluated for missing keys
       *  @param index the key in LHCb::Particle::extraInfo table
       *  @param update the flag to allow the insert of mnissing information
       */
      SmartInfo( const LoKi::BasicFunctors<const LHCb::Particle*>::Function& fun, const int index,
                 const bool update = false );
      /// clone method (mandatory!)
      SmartInfo* clone() const override;
      /// the specific printout
      std::ostream& fillStream( std::ostream& s ) const override;
      // ======================================================================
    };
    // ========================================================================
  } // namespace Particles
  // ==========================================================================
  namespace Vertices {
    // ========================================================================
    /** @class SmartInfo
     *
     *  Trivial function which:
     *    - checks the presence of information in LHCb::VertexBase::extraInfo
     *    - if the information present, it returns it
     *    - for missing infomation, use function to evaluate it
     *    - (optionally) fill th emissing field
     *
     *  @see LHCb::VertexBase
     *  @see LoKi::Cuts::VSINFO
     *  @see LoKi::ExtraInfo::GetSmartInfo
     *  @see LoKi::ExtraInfo::info
     *  @see LoKi::ExtraInfo::hasInfo
     *  @see LoKi::ExtraInfo::setInfo
     *
     *  @author Vanya BELYAEV Ivan.Belyaev@cern.ch
     *  @date 2012-02-27
     */
    struct GAUDI_API SmartInfo : LoKi::ExtraInfo::GetSmartInfo<const LHCb::VertexBase*> {
      // ======================================================================
      /** constructor from fuction, key and update-flag
       *  @param index the key in LHCb::Particle::extraInfo table
       *  @param fun functionto be evaluated for missing keys
       *  @param update the flag to allow the insert of mnissing information
       */
      SmartInfo( const int index, const LoKi::BasicFunctors<const LHCb::VertexBase*>::Function& fun,
                 const bool update = false );
      /** constructor from fuction, key and update-flag
       *  @param fun functionto be evaluated for missing keys
       *  @param index the key in LHCb::Particle::extraInfo table
       *  @param update the flag to allow the insert of mnissing information
       */
      SmartInfo( const LoKi::BasicFunctors<const LHCb::VertexBase*>::Function& fun, const int index,
                 const bool update = false );
      /// clone method (mandatory!)
      SmartInfo* clone() const override;
      /// the specific printout
      std::ostream& fillStream( std::ostream& s ) const override;
      // ======================================================================
    };
    // ========================================================================
  } // namespace Vertices
  // ==========================================================================
  namespace Cuts {
    // ========================================================================
    /** @typedef SINFO
     *  "SmartInfo"
     *  Trivial function which:
     *    - checks the presence of information in LHCb::Particle::extraInfo
     *    - if the information present, it returns it
     *    - for missing infomation, use function to evaluate it
     *    - (optionally) fill th emissing field
     *  @code
     *
     *   const Fun fun           = SINFO ( 105566 , PT , true ) ;
     *   const LHCb::Particle* p = ... ;
     *   const double result     = fun ( p ) ;
     *
     *  @endcode
     *  @see LoKi::Particles::SmartInfo
     */
    typedef LoKi::Particles::SmartInfo SINFO;
    // ========================================================================
    /** @typedef VSINFO
     *  "SmartInfo"
     *  Trivial function which:
     *    - checks the presence of information in LHCb::VertexBase::extraInfo
     *    - if the information present, it returns it
     *    - for missing infomation, use function to evaluate it
     *    - (optionally) fill th emissing field
     *  @code
     *
     *   const VFun fun            = VSINFO ( 105566 , VZ , true ) ;
     *   const LHCb::VertexBase* v = ... ;
     *   const double result       = fun ( v ) ;
     *
     *  @endcode
     *  @see LoKi::Vertices::SmartInfo
     */
    typedef LoKi::Vertices::SmartInfo VSINFO;
    // ========================================================================
  } // namespace Cuts
  // ==========================================================================
} //                                                      End of namespace LoKi
// ============================================================================
//                                                                      The END
// ============================================================================
#endif // LOKI_PARTICLES40_H
