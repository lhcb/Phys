/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
#ifndef LOKI_LOKIPHYS_H
#define LOKI_LOKIPHYS_H 1
// ============================================================================
// Include files
// ============================================================================
// LoKiPhys
// ============================================================================
#include "LoKi/ATypes.h"
#include "LoKi/PhysRangeTypes.h"
#include "LoKi/PhysTypes.h"
//
// Particles
//
#include "LoKi/Particles.h"
//
#include "LoKi/Particles0.h"
#include "LoKi/Particles1.h"
#include "LoKi/Particles2.h"
#include "LoKi/Particles3.h"
#include "LoKi/Particles4.h"
#include "LoKi/Particles5.h"
#include "LoKi/Particles6.h"
#include "LoKi/Particles7.h"
#include "LoKi/Particles8.h"
#include "LoKi/Particles9.h"
//
#include "LoKi/Particles10.h"
#include "LoKi/Particles11.h"
#include "LoKi/Particles12.h"
#include "LoKi/Particles13.h"
#include "LoKi/Particles14.h"
#include "LoKi/Particles15.h"
#include "LoKi/Particles16.h"
#include "LoKi/Particles17.h"
#include "LoKi/Particles18.h"
#include "LoKi/Particles19.h"
//
#include "LoKi/Particles20.h"
#include "LoKi/Particles21.h"
#include "LoKi/Particles22.h"
#include "LoKi/Particles23.h"
#include "LoKi/Particles24.h"
#include "LoKi/Particles25.h"
#include "LoKi/Particles26.h"
#include "LoKi/Particles27.h"
#include "LoKi/Particles28.h"
#include "LoKi/Particles29.h"
//
#include "LoKi/Particles30.h"
#include "LoKi/Particles31.h"
#include "LoKi/Particles32.h"
#include "LoKi/Particles33.h"
#include "LoKi/Particles34.h"
#include "LoKi/Particles35.h"
#include "LoKi/Particles36.h"
#include "LoKi/Particles37.h"
#include "LoKi/Particles38.h"
#include "LoKi/Particles39.h"
//
#include "LoKi/Particles40.h"
#include "LoKi/Particles41.h"
#include "LoKi/Particles42.h"
#include "LoKi/Particles43.h"
#include "LoKi/Particles44.h"
#include "LoKi/Particles45.h"
#include "LoKi/Particles46.h"
#include "LoKi/Particles47.h"
#include "LoKi/Particles48.h"
#include "LoKi/Particles49.h"
//
#include "LoKi/Particles50.h"
//
#include "LoKi/BeamLineFunctions.h"
//
//
#include "LoKi/Vertices.h"
#include "LoKi/Vertices0.h"
#include "LoKi/Vertices1.h"
#include "LoKi/Vertices2.h"
#include "LoKi/Vertices3.h"
#include "LoKi/Vertices4.h"
#include "LoKi/Vertices5.h"
//
// Other
//
#include "LoKi/ChildSelector.h"
//
#include "LoKi/PhysDump.h"
#include "LoKi/PhysSinks.h"
#include "LoKi/PhysSources.h"
// ============================================================================
/** @file
 *
 *  This file is a part of LoKi project -
 *    "C++ ToolKit  for Smart and Friendly Physics Analysis"
 *
 *  The package has been designed with the kind help from
 *  Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
 *  contributions and advices from G.Raven, J.van Tilburg,
 *  A.Golutvin, P.Koppenburg have been used in the design.
 *
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date 2006-01-28
 */
// ============================================================================
namespace LoKi {
  // ==========================================================================
  /** @namespace LoKi::Particles
   *  The major namespace with implementatioin of all
   *  "Particle"-functions and cuts
   *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
   *  @date 2006-01-28
   */
  namespace Particles {}
  // ==========================================================================
  /** @namesapce LoKi::Vertices
   *  The major namespace with implementation of all
   *  "Particle"-functions and cuts
   *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
   *  @date 2006-01-28
   */
  namespace Vertices {}
  // ==========================================================================
  /** @namespace LoKi::PhysTypes
   *  The major namespace with basic classes for physics analysis
   *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
   *  @date 2006-01-28
   */
  namespace PhysTypes {}
  // ==========================================================================
} //                                                      end of namespace LoKi
// ============================================================================
//                                                                      The END
// ============================================================================
#endif // LOKI_LOKIPHYS_H
