/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef LOKI_TwoBody_POINTINGMASS_H
#define LOKI_TwoBody_POINTINGMASS_H 1

#include "Event/Particle.h"
#include "LoKi/AuxDesktopBase.h"
#include "LoKi/BasicFunctors.h"
#include "LoKi/ChildSelector.h"
#include "LoKi/IDecay.h"
#include "LoKi/Particles0.h"
#include "LoKi/VertexHolder.h"
#include "LoKi/iTree.h"

#include <boost/optional.hpp>

namespace LoKi {
  namespace Particles {
    /***
     * Functor responsible for calculating a primitive version of the "pointing mass":
     * a mass computed by constraining the perpendicular momentum to the flight direction
     * to be in balance, such that the decay indeed points back to the PV.
     *
     * Unlike the traditional corrected mass, which does this with the assumption of a missing
     * massless particle, this functor assumes the momentum of one of the tracks,
     * the "probe track", can be altered to make up for this momentum. The direction
     * of this probe track momentum is not changed; only the magnitude is scaled.
     *
     * This functor has three roles reserved for special particles: a probe (which
     * was defined above), a tag and a mother particle. Optionally, an "extra"
     * particle can be defined.
     *
     * The tag particle is used as-is in the calculation and the main input for the
     * calculation of the momentum perpendicular to the flight direction of the mother.
     * The mother particle is only used as an input for the vertex position.
     *
     * With the combination of the tag, the probe and the "mother", the invariant mass
     * can be computed of the 2-track combination. It is also possible to compute
     * the invariant mass of an n-prong decay, using the "extra" particle.
     * The extra particle is not used in the calculation of the perpendicular momentum,
     * but its total momentum is added to the computed tag+probe momentum to calculate
     * another invariant mass. An example is given at the end of this doc.
     *
     * The most prominent use-case, and also the reason this functor is written, is the
     * reconstruction of decays in which one of the daughter's only leaves a Velo track
     * in the LHCb detector: the angles are well-determined, but the magnitude of
     * this velo track is unknown.
     *
     * The functor supports an additional particle to be added in the mass calculation,
     * with the clear case of the D*(2010) -> D0 (K pi) pi in mind, in which one of the
     * D0 daughters is the probe track. In this case you can compute two pointing
     * constraint masses: the D* and the D0.
     *
     * As an example: using the decay descriptor
     * '[D*(2010)+ ->  ^(D0 ->  ^K- ^[pi-]CC)  ^pi+]CC'
     * The probe is the the pi-, which is only reconstructed as velo track.
     * (note the extra CC, as velo tracks do not have a well-defined charge)
     * The tag is the Kaon, which is reconstructed as a long track.
     * The mother particle
     *
     * Using this decay chain we can define two pointing masses: that of the
     * D0 and that of the D*(2010). Using the following definition in your
     * LoKi__Hybrid__TupleTool for the top-level particle will use the
     * D0 as a mother particle.
     *
     *     "POINTINGMASS_DZERO" : "POINTINGMASS ( LoKi.Child.Selector(1), LoKi.Child.Selector(1,1),
     * LoKi.Child.Selector(1,2) )" "POINTINGMASS_DSTAR" : "POINTINGMASS ( LoKi.Child.Selector(1),
     * LoKi.Child.Selector(1,1), LoKi.Child.Selector(1,2), LoKi.Child.Selector(2) )"
     *
     * More complicated decays, such as the 4-prong D0 decay coming from a
     * D*(2010)+ resonance, can also be dealt with in this fashion, by
     * creating a virtual intermediate particle for all tag tracks.
     *
     * The functor is made to replace a lengthy calculation done in a HLT2 turbo line,
     * see https://its.cern.ch/jira/browse/LBHLT-49.
     *
     * In the case that the functor fails, for example because the decay structure
     * does not fit the description given, LoKi::Constants::InvalidMass is
     * returned.
     *
     * @author Mika Vesterinen <mika.vesterinen@cern.ch> [original],
     *         Laurent Dufour <laurent.dufour@cern.ch> [C++ adaptation],
     *         Sascha Stahl <sascha.stahl@cern.ch> [validation]
     */
    class GAUDI_API PointingMass : public LoKi::BasicFunctors<const LHCb::Particle*>::Function,
                                   public virtual LoKi::AuxDesktopBase,
                                   public LoKi::Vertices::VertexHolder {
    public:
      // ======================================================================
      /// Computing the pointing-constraint mass for a 3-body decay
      /// (the example of the D*(2010) mass in the class doc)
      PointingMass( const LoKi::Child::Selector& mother, const LoKi::Child::Selector& tagA,
                    const LoKi::Child::Selector& probe, const LoKi::Child::Selector& extraParticle );

      /// Computing the pointing-constraint mass for a 2-body decay
      /// (the example of the D0 mass in the class doc)
      PointingMass( const LoKi::Child::Selector& mother, const LoKi::Child::Selector& tagA,
                    const LoKi::Child::Selector& probe );

      /// MANDATORY: virtual destructor
      virtual ~PointingMass();

      /// MANDATORY: clone method ("virtual constructor")
      PointingMass* clone() const override;

      /// MANDATORY: the only one essential method
      result_type operator()( argument p ) const override;

      /// some printout
      std::ostream& fillStream( std::ostream& s ) const override;
      // ======================================================================
    private:
      // ======================================================================
      /// the default constructor is disabled
      PointingMass(); // the default constructor is disabled
      // ======================================================================

      /**
       * Container of the particle pointers involved,
       * just as a helper function.
       */
      struct DecayStructure {
        const LHCb::Particle* mother;
        const LHCb::Particle* tag;
        const LHCb::Particle* probe;
      };

      // ======================================================================
      /// get the proper decay components
      /// When optional fails, some part of the decay chain was invalid.
      // ======================================================================
      boost::optional<const LoKi::Particles::PointingMass::DecayStructure>
      getComponents( const LHCb::Particle* p ) const;

      // ======================================================================
      LoKi::Child::Selector                  m_mother; //  rule to find the first particle
      LoKi::Child::Selector                  m_tag;    // rule to find the second particle
      LoKi::Child::Selector                  m_probe;  // rule to find the second particle
      boost::optional<LoKi::Child::Selector> m_extra;  // rule to find the optional additional particle for the mass
                                                       // calculation
      // ======================================================================
    };
  } // namespace Particles

  namespace Cuts {
    // ========================================================================
    /** @typedef POINTINGMASS
     *  "PointingMass"
     *  Corrected-mass-like functor which scales one of the momentum vectors
     *  to balance the transverse momentum with respect to a flight direction.
     *
     *  @see LoKi::Particles::PointingMass
     */
    typedef LoKi::Particles::PointingMass POINTINGMASS;
    // ========================================================================
  } // namespace Cuts
} // namespace LoKi

#endif
