/*****************************************************************************\
* (c) Copyright 2000-2024 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
#ifndef LOKI_GETPHYSDESKTOP_H
#  define LOKI_GETPHYSDESKTOP_H 1

#  include <GaudiKernel/Kernel.h>
// ============================================================================
// Include files
// ============================================================================
// forward declarations
// ============================================================================
struct IDVAlgorithm;
class IAlgContextSvc;
namespace LoKi {
  struct ILoKiSvc;
}
// ============================================================================
namespace LoKi {
  // ==========================================================================
  /** get the desktop from Algorithm Context Service
   *  @param  svc pointer to Algorithm Context Service
   *  @return the pointer to desktop
   *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
   *  date 2008-01-16
   */
  GAUDI_API
  IDVAlgorithm* getPhysDesktop( const IAlgContextSvc* svc );
  // ==========================================================================
  /** get the desktop from LoKi Service
   *  @param  pointer to LoKi Service
   *  @return the pointer to desktop
   *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
   *  date 2008-01-16
   */
  GAUDI_API
  IDVAlgorithm* getPhysDesktop( const LoKi::ILoKiSvc* svc );
  // ==========================================================================
  /** get the desktop using the chain LoKi -> Algorithm Context -> DVAlgorithm
   *  @return the pointer to desktop
   *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
   *  date 2008-01-16
   */
  GAUDI_API
  IDVAlgorithm* getPhysDesktop();
  // ==========================================================================
} //                                                      end of namespace LoKi
// ============================================================================
// The END
// ============================================================================
#endif // LOKI_GETPHYSDESKTOP_H
// ============================================================================
