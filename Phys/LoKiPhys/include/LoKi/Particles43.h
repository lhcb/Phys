/*****************************************************************************\
* (c) Copyright 2000-2024 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
#ifndef LOKI_PARTICLES43_H
#  define LOKI_PARTICLES43_H 1
// ============================================================================
// Include files
#  include <Event/Particle.h>
#  include <LoKi/BasicFunctors.h>
#  include <LoKi/CoreTypes.h>
#  include <LoKi/PhysTypes.h>
#  include <LoKi/UniqueKeeper.h>
#  include <iosfwd>
// ============================================================================
/** @file LoKi/Particles43.h
 *
 *  This file is a part of LoKi project -
 *    "C++ ToolKit  for Smart and Friendly Physics Analysis"
 *
 *  The package has been designed with the kind help from
 *  Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
 *  contributions and advices from G.Raven, J.van Tilburg,
 *  A.Golutvin, P.Koppenburg have been used in the design.
 *
 *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
 *  @date   2012-07-25
 */
namespace LoKi {
  // ==========================================================================
  namespace Particles {
    // ========================================================================
    /** @class SumInR2Cone
     *  sum some function over R2-cone
     *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
     *  @date   2012-07-25
     *
     */
    class SumInR2Cone : public LoKi::BasicFunctors<const LHCb::Particle*>::Function,
                        public LoKi::UniqueKeeper<LHCb::Particle> {
      // ======================================================================
    public:
      // ======================================================================
      /** constructor form value of \f$\left(\Delta R\right)^2\f$
       *  @param dr2 the DR2-value
       *  @param fun the functor for summation
       *  @param parts the particles
       *  @param init  the initial value
       */
      SumInR2Cone( const double dr2, const LoKi::BasicFunctors<const LHCb::Particle*>::Function& fun,
                   const LHCb::Particle::Range& parts = LHCb::Particle::Range(), const double init = 0 );
      // ======================================================================
      /// MANDATORY: clone method ("virtual constructor")
      SumInR2Cone* clone() const override;
      /// MANDATORY: the only essential method
      result_type operator()( argument p ) const override;
      // ======================================================================
    private:
      // ======================================================================
      /// the default constructor is disabled
      SumInR2Cone(); // the default constructor is disabled
      // ======================================================================
    protected:
      // ======================================================================
      /// calcualte the sum
      double sum( const LHCb::Particle* p ) const; // calcualte the sum
      double dr2() const { return m_dr2; }
      double init() const { return m_init; }
      // ======================================================================
    private:
      // ======================================================================
      /// the function to be summed
      LoKi::Types::Fun m_fun; // the function to be summed
      /// the DR2 value
      double m_dr2; //     the DR2 value
      /// the initial value
      double m_init; // the initial value
      // ======================================================================
    };
    // ========================================================================
    /** @class SumInR2ConeWithSource
     *  sum some function over R2-cone
     *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
     *  @date   2012-07-25
     *
     */
    class SumInR2ConeWithSource : public SumInR2Cone {
      // ======================================================================
    public:
      // ======================================================================
      /// constructor from the source
      SumInR2ConeWithSource( const double dr2, const LoKi::BasicFunctors<const LHCb::Particle*>::Function& fun,
                             const LoKi::BasicFunctors<const LHCb::Particle*>::Source& source, const double init = 0 );
      /// constructor from the TES location
      SumInR2ConeWithSource( const double dr2, const LoKi::BasicFunctors<const LHCb::Particle*>::Function& fun,
                             const std::string&                                           location,
                             const LoKi::BasicFunctors<const LHCb::Particle*>::Predicate& cut =
                                 LoKi::BasicFunctors<const LHCb::Particle*>::BooleanConstant( true ),
                             const double init = 0 );
      /// constructor from the TES locations
      SumInR2ConeWithSource( const double dr2, const LoKi::BasicFunctors<const LHCb::Particle*>::Function& fun,
                             const std::vector<std::string>&                              locations,
                             const LoKi::BasicFunctors<const LHCb::Particle*>::Predicate& cut =
                                 LoKi::BasicFunctors<const LHCb::Particle*>::BooleanConstant( true ),
                             const double init = 0 );
      /// MANDATORY : clone method ("virtual constructor")
      SumInR2ConeWithSource* clone() const override;
      /// MANDATORY: the only essential method
      result_type operator()( argument p ) const override;
      /// OPTIONAL: nice printout
      std::ostream& fillStream( std::ostream& s ) const override;
      // ======================================================================
    protected:
      // ======================================================================
      typedef LoKi::BasicFunctors<const LHCb::Particle*>::Source iSource;
      typedef LoKi::Assignable<iSource>::Type                    Source;
      // ======================================================================
      const iSource& source() const { return m_source; }
      // ======================================================================
    private:
      // ======================================================================
      /// the source
      Source m_source; // the source
      // ======================================================================
      //
    };
    // ========================================================================
  } // namespace Particles
  // ==========================================================================
  namespace Cuts {
    // ========================================================================
    /** @typedef SUMINR2CONE
     *  @see LoKi::Particles::SumInR2Cone
     *
     *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
     *  @date   2012-07-25
     */
    typedef LoKi::Particles::SumInR2Cone SUMINR2CONE;
    // ========================================================================
    /** @typedef SUMINCONE
     *  @see LoKi::Particles::SumInR2ConeWithSource
     *
     *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
     *  @date   2012-07-25
     */
    typedef LoKi::Particles::SumInR2ConeWithSource SUMCONE;
    // ========================================================================
  } // namespace Cuts
  // ==========================================================================
} //                                                      end of namespace LoKi
// ============================================================================
//                                                                      The END
// ============================================================================
#endif // LOKI_PARTICLES43_H
// ============================================================================
