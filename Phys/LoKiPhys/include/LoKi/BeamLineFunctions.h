/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef LOKI_BEAMLINEFUNCTIONS_H
#define LOKI_BEAMLINEFUNCTIONS_H 1
// ============================================================================
// Include files
// ============================================================================
// GaudiKernel
// ============================================================================
// Event
// ============================================================================
#include "Event/Particle.h"
#include "Event/Track.h"
#include "Event/VertexBase.h"
#include "GaudiKernel/IUpdateManagerSvc.h"
#include "GaudiKernel/SmartRef.h"
// ============================================================================
// LoKi
// ============================================================================
#include "LoKi/BasicFunctors.h"
#include "LoKi/BeamSpot.h"
#include "LoKi/Interface.h"
#include "LoKi/ParticleCuts.h"
// ============================================================================
/** @file
 *  Collection of "beam-line"-related functors
 *
 *  This file is part of LoKi project:
 *   ``C++ ToolKit for Smart and Friendly Physics Analysis''
 *
 *  The package has been designed with the kind help from
 *  Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
 *  contributions and advices from G.Raven, J.van Tilburg,
 *  A.Golutvin, P.Koppenburg have been used in the design.
 *
 *  @author Vanya BELYAEV Ivan.Belyaev@cern.ch
 *  @date   2011-03-10
 */
// ============================================================================
namespace LoKi {
  // ==========================================================================
  namespace Vertices {
    // ========================================================================
    /** @class BeamSpotRho
     *
     *  functor to evaluate the radial distance ("rho") with respect to
     *  the middle of Velo as measured by the X and Y resolvers
     *
     *  @attention if the velo is opened return -1.
     *
     *  @see LoKi::Cuts::VX_BEAMSPOTRHO
     *  @author Vanya Belyaev Ivan.Belyaev@cern.ch
     *  @author Victor COCO   Victor.Coco@cern.ch
     *  @date   2011-03-10
     */
    class GAUDI_API BeamSpotRho : public LoKi::BeamSpot, public LoKi::BasicFunctors<const LHCb::VertexBase*>::Function {
    public:
      // ======================================================================
      /// Constructor from resolver bouns
      BeamSpotRho( const double bound );
      /// Constructor from resolved bound and condition name
      BeamSpotRho( const double bound, const std::string& condname );
      /// MANDATORY: virtual destructor
      virtual ~BeamSpotRho();
      // ======================================================================
    public:
      // ======================================================================
      /** MANDATORY: clone method ("virtual constructor")
       *  @attention if the velo is opened return -1
       */
      BeamSpotRho* clone() const override;
      /// MANDATORY: the only one essential method
      result_type operator()( argument v ) const override;
      /// OPTIONAL: nice printout
      std::ostream& fillStream( std::ostream& s ) const override;
      // ======================================================================
    private:
      // ======================================================================
      /// default constructor is disabled
      BeamSpotRho(); // default constructor is disabled
      // ======================================================================
    };
    // ========================================================================
  } // namespace Vertices
  // ==========================================================================

  // ==========================================================================
  namespace Particles {
    // ========================================================================
    /** @class DistanceToBeamLine
     *
     *  functor to evaluate the signed distance with respect to the axis
     *  through the middle of Velo as measured by the X and Y resolvers.
     *
     *  @attention returns the signed distance.
     *
     *  @author Vanya Belyaev Ivan.Belyaev@cern.ch
     *  @author Victor COCO   Victor.Coco@cern.ch
     *  @author Wouter Hulsbergen
     *  @date   2018-01-12
     */
    class GAUDI_API DistanceToBeamLine : public LoKi::BeamSpot,
                                         public LoKi::BasicFunctors<const LHCb::Particle*>::Function {
    public:
      // ======================================================================
      // ======================================================================
      /// default constructor
      DistanceToBeamLine();
      /// MANDATORY: virtual destructor
      virtual ~DistanceToBeamLine();
      // ======================================================================
    public:
      // ======================================================================
      /** MANDATORY: clone method ("virtual constructor")
       *  @attention if the velo is opened return -1
       */
      DistanceToBeamLine* clone() const override;
      /// MANDATORY: the only one essential method
      result_type operator()( argument p ) const override;
      /// OPTIONAL: nice printout
      std::ostream& fillStream( std::ostream& s ) const override;
      // ======================================================================
    private:
      /// Cut to select only particles with track daughters in tree
      Types::Cut m_cut;
      // ======================================================================
    };
    // ========================================================================
  } // namespace Particles
  // ==========================================================================
  namespace Cuts {
    // ========================================================================
    /** @typedef  VX_BEAMSPOTRHO
     *  functor to evaluate the radial distance ("rho") with respect to
     *  the middle of Velo as measured by the X and Y resolvers
     *  @author Vanya BELYAEV Ivan.Belyaev@cern.ch
     *  @author Victor COCO   Victor.Coco@cern.ch
     *  @date 2011-03-11
     */
    typedef LoKi::Vertices::BeamSpotRho VX_BEAMSPOTRHO;
    // ========================================================================
    /** @typedef  BEAMLINEDOCA
     *  functor to evaluate the signed distance with respect to the axis
     *  through the middle of Velo as measured by the X and Y resolvers.
     *  @author Vanya BELYAEV Ivan.Belyaev@cern.ch
     *  @author Victor COCO   Victor.Coco@cern.ch
     *  @author Wouter Hulsbergen
     *  @date 2018-01-12
     */
    typedef LoKi::Particles::DistanceToBeamLine BEAMLINEDOCA;
    // ========================================================================
  } // namespace Cuts
  // ==========================================================================
} //                                                      end of namespace LoKi
// ============================================================================
//                                                                      The END
// ============================================================================
#endif // LOKI_BEAMLINEFUNCTIONS_H
