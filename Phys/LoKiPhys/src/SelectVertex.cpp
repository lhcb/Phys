/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
// Include files
// ============================================================================
// GaudiAlg
// ============================================================================
#include "GaudiAlg/GaudiTool.h"
// ============================================================================
// LoKi
// ============================================================================
#include "LoKi/Algs.h"
#include "LoKi/Functions.h"
#include "LoKi/Math.h"
#include "LoKi/PhysRangeTypes.h"
#include "LoKi/PhysTypes.h"
#include "LoKi/SelectVertex.h"
// ============================================================================
/** @file
 *
 *  Implementation file for class LoKi::SelectVertex
 *  @see LoKi::SelectVertex
 *
 *  This file is part of LoKi project:
 *   ``C++ ToolKit for Smart and Friendly Physics Analysis''
 *
 *  The package has been designed with the kind help from
 *  Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
 *  contributions and advices from G.Raven, J.van Tilburg,
 *  A.Golutvin, P.Koppenburg have been used in the design.
 *
 *  @date 2006-11-27
 *  @author Vanya  BELYAEV ibelyaev@physics.syr.edu
 */
// ============================================================================
// Min
// ============================================================================
const LHCb::VertexBase* LoKi::SelectVertex::selectMin( const LoKi::Types::VRange& vs, const LoKi::Types::VFunc& fun,
                                                       const LoKi::Types::VCuts& cut ) {
  LoKi::Types::VRange::const_iterator ipv = LoKi::select_min( vs.begin(), vs.end(), fun, cut );
  return vs.end() == ipv ? 0 : *ipv;
}
// ============================================================================
const LHCb::Vertex* LoKi::SelectVertex::selectMin( const LHCb::Vertex::Container* vs, const LoKi::Types::VFunc& fun,
                                                   const LoKi::Types::VCuts& cut ) {
  if ( 0 == vs ) { return 0; }
  LHCb::Vertex::Container::const_iterator ipv = LoKi::select_min( vs->begin(), vs->end(), fun, cut );
  return vs->end() == ipv ? 0 : *ipv;
}
// ============================================================================
const LHCb::RecVertex* LoKi::SelectVertex::selectMin( const LHCb::RecVertex::Container* vs,
                                                      const LoKi::Types::VFunc& fun, const LoKi::Types::VCuts& cut ) {
  if ( 0 == vs ) { return 0; }
  LHCb::RecVertex::Container::const_iterator ipv = LoKi::select_min( vs->begin(), vs->end(), fun, cut );
  return vs->end() == ipv ? 0 : *ipv;
}
// ============================================================================
// Max
// ============================================================================
const LHCb::VertexBase* LoKi::SelectVertex::selectMax( const LoKi::Types::VRange& vs, const LoKi::Types::VFunc& fun,
                                                       const LoKi::Types::VCuts& cut ) {
  LoKi::Types::VRange::const_iterator ipv = LoKi::select_max( vs.begin(), vs.end(), fun, cut );
  return vs.end() == ipv ? 0 : *ipv;
}
// ============================================================================
const LHCb::Vertex* LoKi::SelectVertex::selectMax( const LHCb::Vertex::Container* vs, const LoKi::Types::VFunc& fun,
                                                   const LoKi::Types::VCuts& cut ) {
  if ( 0 == vs ) { return 0; }
  LHCb::Vertex::Container::const_iterator ipv = LoKi::select_max( vs->begin(), vs->end(), fun, cut );
  return vs->end() == ipv ? 0 : *ipv;
}
// ============================================================================
const LHCb::RecVertex* LoKi::SelectVertex::selectMax( const LHCb::RecVertex::Container* vs,
                                                      const LoKi::Types::VFunc& fun, const LoKi::Types::VCuts& cut ) {
  if ( 0 == vs ) { return 0; }
  LHCb::RecVertex::Container::const_iterator ipv = LoKi::select_max( vs->begin(), vs->end(), fun, cut );
  return vs->end() == ipv ? 0 : *ipv;
}
// ============================================================================
// The END
// ============================================================================
