/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
// Include files
// ============================================================================
// LoKi
// ============================================================================
#include "LoKi/Particles18.h"
// ============================================================================
/** @file
 *  Implementation file for class from file LoKi/Particles18.h
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date 2007-07-17
 */
// ============================================================================
/*  constructor from "info"
 *  @param key info index/mark/key
 */
// ============================================================================
// the specific printout
// ============================================================================
std::ostream& LoKi::Particles::HasInfo::fillStream( std::ostream& s ) const {
  return s << "HASINFO(" << index() << ")";
}
// ============================================================================
// the specific printout
// ============================================================================
std::ostream& LoKi::Particles::Info::fillStream( std::ostream& s ) const {
  return s << "INFO(" << index() << "," << value() << ")";
}
// ============================================================================

// ============================================================================
// The END
// ============================================================================
