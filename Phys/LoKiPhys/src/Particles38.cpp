/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
// Include files
// ============================================================================
// LoKi
// ============================================================================
#include "LoKi/Particles38.h"
#include "LoKi/Child.h"
// ============================================================================
/** @file
 *
 * Implementation file for functions from "M_corr" family by Mike Williams
 *
 *  This file is a part of
 *  <a href="http://cern.ch/lhcb-comp/Analysis/LoKi/index.html">LoKi project:</a>
 *  ``C++ ToolKit for Smart and Friendly Physics Analysis''
 *
 *  The package has been designed with the kind help from
 *  Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
 *  contributions and advices from G.Raven, J.van Tilburg,
 *  A.Golutvin, P.Koppenburg have been used in the design.
 *
 *  @author Vanya Belyaev Ivan.Belyaev@nikhef.nl
 *  @date   2010-10-23
 */
// ============================================================================
/*
 * 2017-01-17
 * Guido Andreassi, Violaine Bellée, Pavol Štefko
 * Added BremMCorrected functor to compute the HOP mass. See description of the HOP mass here:
 * <a href="https://cds.cern.ch/record/2102345?ln=en">LHCb-INT-2015-037</a>
 */

/// anonymos namespace to hide local varibales
namespace {
  // ==========================================================================
  /// the invalid 3-momentum
  const LoKi::ThreeVector s_VECTOR = LoKi::ThreeVector( 0, 0, -1 * Gaudi::Units::TeV );
  /// the invalid 3Dpoint
  const LoKi::Point3D s_POINT = LoKi::Point3D( 0, 0, -1 * Gaudi::Units::km );

  double dPTdx( const double dAdx, const double dBdx, const double PT, const double A, const double B ) {
    return 1 / PT * -0.5 * ( 2 * A * B * dAdx - A * A * dBdx ) / ( B * B );
  }
  //=========================================================================
  // -- Calculate the corrected mass error
  //=========================================================================
  double mCorrErrors( const LoKi::ThreeVector sv, const LoKi::ThreeVector pv, const Gaudi::LorentzVector p,
                      const Gaudi::SymMatrix7x7 covP, const Gaudi::SymMatrix3x3 covPV ) {
    // --
    // -- m_corr = sqrt( m_vis² + pT² ) + pT
    // --
    // -- To transform the errors on the vertices and the momentum to the corrected mass, one essentially needs to do
    // -- mcorr_err² = Sum_i,j( d(m_corr)/dx_i d(m_corr)/dx_j M_ij )_PV + Sum_n,m( d(m_corr)/dx_n d(m_corr)/dx_m M_nm
    // )_SV
    // -- where M_ij is the covariance matrix of the PV, and M_nm the covariance matrix of the SV, including
    // uncertainties of the momenta
    // -- of the particles that formed the SV.
    // --
    // -- For the vertex coordinates:
    // -- d(m_corr) / dx = d(m_corr)/dpT * dpT/dx
    // -- d(m_corr)/dpT =  1/2 * 1/std::sqrt( m_vis² + pT² ) * 2 * pT + 1;
    // -- pT = sqrt( (p_vec - (x_SV_vec - x_PV_vec) * A/B)² )
    // -- with A = px(x_SV - x_PV) +  py(y_SV - y_PV) +  pz(z_SV - z_PV)
    // -- and  B = (x_SV - x_PV)² + (y_SV - y_PV)² + (z_SV - z_PV)², or the magnitude squared of x_SV_vec - x_PV_vec
    // --
    // -- For the momentum coordinates:
    // -- m_vis² = E² - (px² + py² + pz²)
    // -- d(m_corr) / dpx = 1/2 * 1 / sqrt( m_vis² + pT²) * ( -2A/B(x_SV-x_PV) ) + 1/pT * ( px - 2A/B(x_SV - x_PV) )
    // -- d(m_corr) / dE  = 1/2 * / sqrt( m_vis² + pT²) * 2E

    const double x_SV = sv.x(); // SV
    const double y_SV = sv.y(); // SV
    const double z_SV = sv.z(); // SV
    const double x_PV = pv.x(); // PV
    const double y_PV = pv.y(); // PV
    const double z_PV = pv.z(); // PV
    const double px   = p.Px();
    const double py   = p.Py();
    const double pz   = p.Pz();
    const double E    = p.E();

    const LoKi::ThreeVector pVec( p.Px(), p.Py(), p.Pz() );
    const double            PT = std::sqrt( ( LoKi::Kinematics::perpendicular( pVec, ( sv - pv ) ) ).mag2() );
    const double            A  = px * ( x_SV - x_PV ) + py * ( y_SV - y_PV ) + pz * ( z_SV - z_PV );
    const double            B  = ( sv - pv ).Mag2();

    const double invSqrtMassPT = 1 / std::sqrt( p.M2() + PT * PT );
    const double dMcorrdPT     = 0.5 * invSqrtMassPT * 2 * PT + 1;

    // -- First let's calculate the derivates of 'A' and 'B'
    // -- A
    const double dAdx_SV = px;
    const double dAdy_SV = py;
    const double dAdz_SV = pz;

    const double dAdx_PV = -px;
    const double dAdy_PV = -py;
    const double dAdz_PV = -pz;

    const double dAdpx = x_SV - x_PV;
    const double dAdpy = y_SV - y_PV;
    const double dAdpz = z_SV - z_PV;

    // -- B
    const double dBdx_SV = 2 * ( x_SV - x_PV );
    const double dBdy_SV = 2 * ( y_SV - y_PV );
    const double dBdz_SV = 2 * ( z_SV - z_PV );

    const double dBdx_PV = -2 * ( x_SV - x_PV );
    const double dBdy_PV = -2 * ( y_SV - y_PV );
    const double dBdz_PV = -2 * ( z_SV - z_PV );

    // -- the vertices
    const double dMcdx_SV = dMcorrdPT * dPTdx( dAdx_SV, dBdx_SV, PT, A, B );
    const double dMcdy_SV = dMcorrdPT * dPTdx( dAdy_SV, dBdy_SV, PT, A, B );
    const double dMcdz_SV = dMcorrdPT * dPTdx( dAdz_SV, dBdz_SV, PT, A, B );

    const double dMcdx_PV = dMcorrdPT * dPTdx( dAdx_PV, dBdx_PV, PT, A, B );
    const double dMcdy_PV = dMcorrdPT * dPTdx( dAdy_PV, dBdy_PV, PT, A, B );
    const double dMcdz_PV = dMcorrdPT * dPTdx( dAdz_PV, dBdz_PV, PT, A, B );

    // -- the momentum
    const double dMcdpx = -1 * invSqrtMassPT * A / B * dAdpx + 1 / PT * ( px - A / B * dAdpx );
    const double dMcdpy = -1 * invSqrtMassPT * A / B * dAdpy + 1 / PT * ( py - A / B * dAdpy );
    const double dMcdpz = -1 * invSqrtMassPT * A / B * dAdpz + 1 / PT * ( pz - A / B * dAdpz );
    const double dMcdE  = invSqrtMassPT * E;

    // -- the errors on the vertices
    const double errsqVertex =
        // -- the diagonal for SV
        covP( 0, 0 ) * dMcdx_SV * dMcdx_SV + covP( 1, 1 ) * dMcdy_SV * dMcdy_SV + covP( 2, 2 ) * dMcdz_SV * dMcdz_SV +
        // -- the diagonal for PV
        covPV( 0, 0 ) * dMcdx_PV * dMcdx_PV + covPV( 1, 1 ) * dMcdy_PV * dMcdy_PV +
        covPV( 2, 2 ) * dMcdz_PV * dMcdz_PV +
        // -- the cross terms for SV
        covP( 0, 1 ) * 2. * dMcdx_SV * dMcdy_SV + covP( 0, 2 ) * 2. * dMcdx_SV * dMcdz_SV +
        covP( 1, 2 ) * 2. * dMcdy_SV * dMcdz_SV +
        // -- the cross terms for PV
        covPV( 0, 1 ) * 2. * dMcdx_PV * dMcdy_PV + covPV( 0, 2 ) * 2. * dMcdx_PV * dMcdz_PV +
        covPV( 1, 2 ) * 2. * dMcdy_PV * dMcdz_PV;

    // -- the errors on the momentum x vertex
    const double errsqMom =
        // -- the diagonal for the momentum
        covP( 3, 3 ) * dMcdpx * dMcdpx + covP( 4, 4 ) * dMcdpy * dMcdpy + covP( 5, 5 ) * dMcdpz * dMcdpz +
        covP( 6, 6 ) * dMcdE * dMcdE +
        // -- momentum x momomentum cross terms
        covP( 3, 4 ) * 2. * dMcdpx * dMcdpy + covP( 3, 5 ) * 2. * dMcdpx * dMcdpz + covP( 3, 6 ) * 2. * dMcdpx * dMcdE +
        covP( 4, 5 ) * 2. * dMcdpy * dMcdpz + covP( 4, 6 ) * 2. * dMcdpy * dMcdE + covP( 5, 6 ) * 2. * dMcdpz * dMcdE +
        // -- momentum x position terms
        covP( 0, 3 ) * 2. * dMcdx_SV * dMcdpx + covP( 1, 3 ) * 2. * dMcdy_SV * dMcdpx +
        covP( 2, 3 ) * 2. * dMcdz_SV * dMcdpx + covP( 0, 4 ) * 2. * dMcdx_SV * dMcdpy +
        covP( 1, 4 ) * 2. * dMcdy_SV * dMcdpy + covP( 2, 4 ) * 2. * dMcdz_SV * dMcdpy +
        covP( 0, 5 ) * 2. * dMcdx_SV * dMcdpz + covP( 1, 5 ) * 2. * dMcdy_SV * dMcdpz +
        covP( 2, 5 ) * 2. * dMcdz_SV * dMcdpz + covP( 0, 6 ) * 2. * dMcdx_SV * dMcdE +
        covP( 1, 6 ) * 2. * dMcdy_SV * dMcdE + covP( 2, 6 ) * 2. * dMcdz_SV * dMcdE;

    return std::sqrt( errsqVertex + errsqMom );
  }

  // -- Rest frame approximation, see e.g. LHCb-PAPER-2015-025
  LoKi::LorentzVector pRestFrameApprox( LoKi::LorentzVector P, LoKi::ThreeVector flightDir, double mass ) {
    const double               visMass = P.M();
    const double               pZ      = P.Vect().Z();
    const double               alpha   = flightDir.Theta();
    const double               Bmom    = mass / visMass * pZ * std::sqrt( 1 + std::tan( alpha ) * std::tan( alpha ) );
    const Gaudi::XYZVector     pB      = Bmom * flightDir;
    const double               EB      = std::sqrt( mass * mass + pB.Mag2() );
    const Gaudi::LorentzVector p4B( pB.X(), pB.Y(), pB.Z(), EB );
    return p4B;
  }

  // ==========================================================================
} // namespace
// ============================================================================
/*  constructor from the primary vertex
 *  @param x the x-position of primary vertex
 *  @param y the x-position of primary vertex
 *  @param z the x-position of primary vertex
 */
// ============================================================================
LoKi::Particles::PtFlight::PtFlight( const double x, const double y, const double z )
    : AuxFunBase{std::tie( x, y, z )}
    , LoKi::Particles::TransverseMomentumRel( s_VECTOR )
    , LoKi::Vertices::VertexHolder( LoKi::Point3D( x, y, z ) ) {}
// ============================================================================
/*  constructor from the primary vertex
 *  @param pv  the primary vertex
 */
// ============================================================================
LoKi::Particles::PtFlight::PtFlight( const LHCb::VertexBase* pv )
    : LoKi::Particles::TransverseMomentumRel( s_VECTOR ), LoKi::Vertices::VertexHolder( pv ) {}
// ============================================================================
/*  constructor from the primary vertex
 *  @param point  the position of primary vertex
 */
// ============================================================================
LoKi::Particles::PtFlight::PtFlight( const LoKi::Point3D& point )
    : AuxFunBase{std::tie( point )}
    , LoKi::Particles::TransverseMomentumRel( s_VECTOR )
    , LoKi::Vertices::VertexHolder( point ) {}
// ============================================================================
// MANDATORY: clone method ("virtual constructor")
// ============================================================================
LoKi::Particles::PtFlight* LoKi::Particles::PtFlight::clone() const { return new LoKi::Particles::PtFlight( *this ); }
// ============================================================================
// MANDATORY: the only one essential method
// ============================================================================
LoKi::Particles::PtFlight::result_type LoKi::Particles::PtFlight::
                                       operator()( LoKi::Particles::PtFlight::argument p ) const {
  if ( 0 == p ) {
    Error( "Invalid argument, return 'Invalid Momentum'" ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    return LoKi::Constants::InvalidMomentum;
  }
  // get the decay vertex:
  const LHCb::VertexBase* vx = p->endVertex();
  if ( 0 == vx ) {
    Error( "EndVertex is invalid, return 'Invalid Momentum'" ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    return LoKi::Constants::InvalidMomentum;
  }
  //
  Assert( LoKi::Vertices::VertexHolder::valid(), "Vertex-Information is not valid" );
  //
  return ptFlight( p->momentum(), vx->position(), position() );
}
// ============================================================================
//  OPTIONAL: the specific printout
// ============================================================================
std::ostream& LoKi::Particles::PtFlight::fillStream( std::ostream& s ) const {
  return s << "PTFLIGHT(" << position().X() << "," << position().Y() << "," << position().Z() << ")";
}
// ============================================================================

// ============================================================================
/*  constructor from the primary vertex
 *  @param x the x-position of primary vertex
 *  @param y the x-position of primary vertex
 *  @param z the x-position of primary vertex
 */
// ============================================================================
LoKi::Particles::MCorrected::MCorrected( const double x, const double y, const double z )
    : AuxFunBase{std::tie( x, y, z )}, LoKi::Particles::PtFlight( x, y, z ) {}
// ============================================================================
/*  constructor from the primary vertex
 *  @param pv  the primary vertex
 */
// ============================================================================
LoKi::Particles::MCorrected::MCorrected( const LHCb::VertexBase* pv ) : LoKi::Particles::PtFlight( pv ) {}
// ============================================================================
/*  constructor from the primary vertex
 *  @param point  the position of primary vertex
 */
// ============================================================================
LoKi::Particles::MCorrected::MCorrected( const LoKi::Point3D& point )
    : AuxFunBase{std::tie( point )}, LoKi::Particles::PtFlight( point ) {}
// ============================================================================
// MANDATORY: clone method ("virtual constructor")
// ============================================================================
LoKi::Particles::MCorrected* LoKi::Particles::MCorrected::clone() const {
  return new LoKi::Particles::MCorrected( *this );
}
// ============================================================================
// MANDATORY: the only one essential method
// ============================================================================
LoKi::Particles::MCorrected::result_type LoKi::Particles::MCorrected::
                                         operator()( LoKi::Particles::MCorrected::argument p ) const {
  if ( 0 == p ) {
    Error( "Invalid argument, return 'Invalid Mass'" ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    return LoKi::Constants::InvalidMass;
  }
  // get the decay vertex:
  const LHCb::VertexBase* vx = p->endVertex();
  if ( 0 == vx ) {
    Error( "EndVertex is invalid, return 'Invalid Mass'" ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    return LoKi::Constants::InvalidMass;
  }
  //
  Assert( LoKi::Vertices::VertexHolder::valid(), "Vertex-Information is not valid" );
  //
  return mCorrFlight( p->momentum(), vx->position(), position() );
}
// ============================================================================
//  OPTIONAL: the specific printout
// ============================================================================
std::ostream& LoKi::Particles::MCorrected::fillStream( std::ostream& s ) const {
  return s << "CORRM(" << position().X() << "," << position().Y() << "," << position().Z() << ")";
}
// ============================================================================

// ============================================================================
// constructor
// ============================================================================
LoKi::Particles::PtFlightWithBestVertex::PtFlightWithBestVertex()
    : AuxFunBase{std::tie()}, LoKi::Particles::PtFlight( s_POINT ) {}
// ============================================================================
// MANDATORY: clone method ("virtual constructor")
// ============================================================================
LoKi::Particles::PtFlightWithBestVertex* LoKi::Particles::PtFlightWithBestVertex::clone() const {
  return new LoKi::Particles::PtFlightWithBestVertex( *this );
}
// ============================================================================
// MANDATORY: the only one essential method
// ============================================================================
LoKi::Particles::PtFlightWithBestVertex::result_type LoKi::Particles::PtFlightWithBestVertex::
                                                     operator()( LoKi::Particles::PtFlightWithBestVertex::argument p ) const {
  if ( 0 == p ) {
    Error( "Invalid argument, return 'Invalid Momentum'" ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    return LoKi::Constants::InvalidMomentum;
  }
  // get the decay vertex:
  const LHCb::VertexBase* vx = p->endVertex();
  if ( 0 == vx ) {
    Error( "EndVertex is invalid, return 'Invalid Momentum'" ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    return LoKi::Constants::InvalidMomentum;
  }
  //
  const LHCb::VertexBase* pv = bestVertex( p );
  if ( 0 == pv ) {
    Error( "BestVertex is invalid, return 'Invalid Momentum'" ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    return LoKi::Constants::InvalidMomentum;
  }
  //
  setVertex( pv );
  //
  Assert( LoKi::Vertices::VertexHolder::valid(), "Vertex-Information is not valid" );
  //
  return ptFlight( p->momentum(), vx->position(), position() );
}
// ============================================================================
//  OPTIONAL: the specific printout
// ============================================================================
std::ostream& LoKi::Particles::PtFlightWithBestVertex::fillStream( std::ostream& s ) const {
  return s << "BPVPTFLIGHT";
}
// ============================================================================

// ============================================================================
// constructor
// ============================================================================
LoKi::Particles::PtFlightDaughterWithBestVertex::PtFlightDaughterWithBestVertex( const std::string&          name,
                                                                                 LHCb::IParticlePropertySvc* ppsvc )
    : AuxFunBase{std::tie()}, LoKi::Particles::PtFlight( s_POINT ), m_pid( 0 ) {

  const LHCb::ParticleProperty* pp = 0;
  if ( ppsvc ) {
    pp = ppsvc->find( name );
  } else {
    pp = LoKi::Particles::ppFromName( name );
  }
  if ( 0 == pp ) { Exception( "PtFlightDaughterWithBestVertex(): Unknow particle name '" + name + "'" ); }

  m_pid = pp->pdgID().abspid();
}
// ============================================================================
// MANDATORY: clone method ("virtual constructor")
// ============================================================================
LoKi::Particles::PtFlightDaughterWithBestVertex* LoKi::Particles::PtFlightDaughterWithBestVertex::clone() const {
  return new LoKi::Particles::PtFlightDaughterWithBestVertex( *this );
}
// ============================================================================
// MANDATORY: the only one essential method
// ============================================================================
LoKi::Particles::PtFlightDaughterWithBestVertex::result_type LoKi::Particles::PtFlightDaughterWithBestVertex::
                                                             operator()( LoKi::Particles::PtFlightDaughterWithBestVertex::argument p ) const {
  if ( 0 == p ) {
    Error( "Invalid argument, return 'Invalid Momentum'" ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    return LoKi::Constants::InvalidMomentum;
  }

  // -- We want the flight direction of the mother, but the momentum of the daughter
  // -- Used e.g. for B -> rho mu nu, where one wants the endvertex of the B (=rho+mu), but the momentum of the rho
  const LHCb::Particle::ConstVector& daughters = p->daughtersVector();
  auto                               it = std::find_if( daughters.begin(), daughters.end(),
                          [&]( const LHCb::Particle* dau ) { return dau->particleID().abspid() == m_pid; } );
  if ( it == daughters.end() || !( *it ) ) {
    Error( "Daughter not found, return 'Invalid Momentum'" ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    return LoKi::Constants::InvalidMomentum;
  }
  const LHCb::Particle* partForPT = *it;

  // get the decay vertex of the 'mother':
  const LHCb::VertexBase* vx = p->endVertex();
  if ( 0 == vx ) {
    Error( "EndVertex is invalid, return 'Invalid Momentum'" ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    return LoKi::Constants::InvalidMomentum;
  }
  //
  const LHCb::VertexBase* pv = bestVertex( p );
  if ( 0 == pv ) {
    Error( "BestVertex is invalid, return 'Invalid Momentum'" ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    return LoKi::Constants::InvalidMomentum;
  }
  //
  setVertex( pv );
  //
  Assert( LoKi::Vertices::VertexHolder::valid(), "Vertex-Information is not valid" );
  //
  return ptFlight( partForPT->momentum(), vx->position(), position() );
}
// ============================================================================
//  OPTIONAL: the specific printout
// ============================================================================
std::ostream& LoKi::Particles::PtFlightDaughterWithBestVertex::fillStream( std::ostream& s ) const {
  return s << "BPVPTFLIGHTDAUGHTER";
}
// ============================================================================

// ============================================================================
// MANDATORY: clone method ("virtual constructor")
// ============================================================================
LoKi::Particles::MCorrectedWithBestVertex* LoKi::Particles::MCorrectedWithBestVertex::clone() const {
  return new LoKi::Particles::MCorrectedWithBestVertex( *this );
}
// ============================================================================
// MANDATORY: the only one essential method
// ============================================================================
LoKi::Particles::MCorrectedWithBestVertex::result_type LoKi::Particles::MCorrectedWithBestVertex::
                                                       operator()( LoKi::Particles::MCorrectedWithBestVertex::argument p ) const {
  if ( 0 == p ) {
    Error( "Invalid argument, return 'Invalid Mass'" ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    return LoKi::Constants::InvalidMass;
  }
  // get the decay vertex:
  const LHCb::VertexBase* vx = p->endVertex();
  if ( 0 == vx ) {
    Error( "EndVertex is invalid, return 'Invalid Mass'" ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    return LoKi::Constants::InvalidMass;
  }
  //
  const LHCb::VertexBase* pv = bestVertex( p );
  if ( 0 == pv ) {
    Error( "BestVertex is invalid, return 'Invalid Mass'" ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    return LoKi::Constants::InvalidMass;
  }
  //
  setVertex( pv );
  //
  Assert( LoKi::Vertices::VertexHolder::valid(), "Vertex-Information is not valid" );
  //
  return mCorrFlight( p->momentum(), vx->position(), position() );
}
// ============================================================================
//  OPTIONAL: the specific printout
// ============================================================================
std::ostream& LoKi::Particles::MCorrectedWithBestVertex::fillStream( std::ostream& s ) const { return s << "BPVCORRM"; }
// ============================================================================
// ============================================================================
// MANDATORY: clone method ("virtual constructor")
// ============================================================================
LoKi::Particles::MCorrectedErrorWithBestVertex* LoKi::Particles::MCorrectedErrorWithBestVertex::clone() const {
  return new LoKi::Particles::MCorrectedErrorWithBestVertex( *this );
}
// ============================================================================
// MANDATORY: the only one essential method
// ============================================================================
LoKi::Particles::MCorrectedErrorWithBestVertex::result_type LoKi::Particles::MCorrectedErrorWithBestVertex::
                                                            operator()( LoKi::Particles::MCorrectedErrorWithBestVertex::argument p ) const {
  if ( 0 == p ) {
    Error( "Invalid argument, return 'Invalid Mass'" ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    return LoKi::Constants::InvalidMass;
  }
  // get the decay vertex:
  const LHCb::VertexBase* vx = p->endVertex();
  if ( 0 == vx ) {
    Error( "EndVertex is invalid, return 'Invalid Mass'" ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    return LoKi::Constants::InvalidMass;
  }
  //
  const LHCb::VertexBase* pv = bestVertex( p );
  if ( 0 == pv ) {
    Error( "BestVertex is invalid, return 'Invalid Mass'" ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    return LoKi::Constants::InvalidMass;
  }
  //
  setVertex( pv );
  //
  Assert( LoKi::Vertices::VertexHolder::valid(), "Vertex-Information is not valid" );
  //

  LoKi::ThreeVector sv( vx->position().X(), vx->position().Y(), vx->position().Z() );
  LoKi::ThreeVector primV( pv->position().X(), pv->position().Y(), pv->position().Z() );

  return mCorrErrors( sv, primV, p->momentum(), p->covMatrix(), pv->covMatrix() );
}
// ============================================================================
//  OPTIONAL: the specific printout
// ============================================================================
std::ostream& LoKi::Particles::MCorrectedErrorWithBestVertex::fillStream( std::ostream& s ) const {
  return s << "BPVCORRMERROR";
}
// ============================================================================

// ============================================================================
// constructor with a range of indices
// ============================================================================
LoKi::Particles::InvMassMotherConstraintWithBestVertex::InvMassMotherConstraintWithBestVertex(
    const std::string& nameMissing, const LoKi::Particles::InvMassMotherConstraintWithBestVertex::Indices indices,
    const bool posSolution, LHCb::IParticlePropertySvc* ppsvc )
    : LoKi::AuxFunBase( std::tie( indices ) )
    , LoKi::Particles::PtFlight( s_POINT )
    , m_indices( std::move( indices ) )
    , m_posSolution( posSolution )
    , m_ppsvc( ppsvc ) {

  std::sort( m_indices.begin(), m_indices.end() );

  const LHCb::ParticleProperty* pp = nullptr;
  if ( ppsvc ) {
    pp = ppsvc->find( nameMissing );
  } else {
    pp = LoKi::Particles::ppFromName( nameMissing );
  }
  if ( !pp ) { Exception( "InvMassMotherConstraintWithBestVertex(): Unknow particle name '" + nameMissing + "'" ); }

  m_massMissing = pp->mass();
}

// ============================================================================
// constructor with one index
// ============================================================================
LoKi::Particles::InvMassMotherConstraintWithBestVertex::InvMassMotherConstraintWithBestVertex(
    const std::string& nameMissing, const unsigned int index1, const bool posSolution,
    LHCb::IParticlePropertySvc* ppsvc )
    : LoKi::Particles::PtFlight( s_POINT ), m_indices( {index1} ), m_posSolution( posSolution ), m_ppsvc( ppsvc ) {

  std::sort( m_indices.begin(), m_indices.end() );

  const LHCb::ParticleProperty* pp = nullptr;
  if ( ppsvc ) {
    pp = ppsvc->find( nameMissing );
  } else {
    pp = LoKi::Particles::ppFromName( nameMissing );
  }
  if ( !pp ) { Exception( "InvMassMotherConstraintWithBestVertex(): Unknow particle name '" + nameMissing + "'" ); }

  m_massMissing = pp->mass();
}
// ============================================================================
// constructor with two indices
// ============================================================================
LoKi::Particles::InvMassMotherConstraintWithBestVertex::InvMassMotherConstraintWithBestVertex(
    const std::string& nameMissing, const unsigned int index1, const unsigned int index2, const bool posSolution,
    LHCb::IParticlePropertySvc* ppsvc )
    : LoKi::Particles::PtFlight( s_POINT )
    , m_indices( {index1, index2} )
    , m_posSolution( posSolution )
    , m_ppsvc( ppsvc ) {

  std::sort( m_indices.begin(), m_indices.end() );

  const LHCb::ParticleProperty* pp = nullptr;
  if ( ppsvc ) {
    pp = ppsvc->find( nameMissing );
  } else {
    pp = LoKi::Particles::ppFromName( nameMissing );
  }
  if ( !pp ) { Exception( "InvMassMotherConstraintWithBestVertex(): Unknow particle name '" + nameMissing + "'" ); }

  m_massMissing = pp->mass();
}
// ============================================================================
// constructor with three indices
// ============================================================================
LoKi::Particles::InvMassMotherConstraintWithBestVertex::InvMassMotherConstraintWithBestVertex(
    const std::string& nameMissing, const unsigned int index1, const unsigned int index2, const unsigned int index3,
    const bool posSolution, LHCb::IParticlePropertySvc* ppsvc )
    : LoKi::Particles::PtFlight( s_POINT )
    , m_indices( {index1, index2, index3} )
    , m_posSolution( posSolution )
    , m_ppsvc( ppsvc ) {

  std::sort( m_indices.begin(), m_indices.end() );

  const LHCb::ParticleProperty* pp = nullptr;
  if ( ppsvc ) {
    pp = ppsvc->find( nameMissing );
  } else {
    pp = LoKi::Particles::ppFromName( nameMissing );
  }
  if ( !pp ) { Exception( "InvMassMotherConstraintWithBestVertex(): Unknow particle name '" + nameMissing + "'" ); }

  m_massMissing = pp->mass();
}
// ============================================================================
// MANDATORY: clone method ("virtual constructor")
// ============================================================================
LoKi::Particles::InvMassMotherConstraintWithBestVertex*
LoKi::Particles::InvMassMotherConstraintWithBestVertex::clone() const {
  return new LoKi::Particles::InvMassMotherConstraintWithBestVertex( *this );
}
// ============================================================================
// MANDATORY: the only one essential method
// ============================================================================
LoKi::Particles::InvMassMotherConstraintWithBestVertex::result_type
LoKi::Particles::InvMassMotherConstraintWithBestVertex::
operator()( LoKi::Particles::InvMassMotherConstraintWithBestVertex::argument p ) const {

  if ( !p ) {
    Error( "Invalid argument, return 'Invalid Mass'" ).ignore();
    return LoKi::Constants::InvalidMass;
  }
  // get the decay vertex:
  const LHCb::VertexBase* sv = p->endVertex();
  if ( !sv ) {
    Error( "EndVertex is invalid, return 'Invalid Mass'" ).ignore();
    return LoKi::Constants::InvalidMass;
  }
  //
  const LHCb::VertexBase* pv = bestVertex( p );
  if ( !pv ) {
    Error( "BestVertex is invalid, return 'Invalid Mass'" ).ignore();
    return LoKi::Constants::InvalidMass;
  }
  //
  setVertex( pv );
  //
  Assert( LoKi::Vertices::VertexHolder::valid(), "Vertex-Information is not valid" );
  //

  // -- Get the mass of the mother from its name
  double                        motherMass = -1.0;
  const LHCb::ParticleProperty* pp         = nullptr;
  if ( m_ppsvc ) {
    pp = m_ppsvc->find( p->particleID() );
  } else {
    pp = LoKi::Particles::ppFromPID( p->particleID() );
  }
  if ( !pp ) {
    Exception( "InvMassMotherConstraintWithBestVertex(): Unknow particle id '" + p->particleID().toString() + "'" );
  }
  motherMass = pp->mass();

  // -- solve the quadratic equation to get the 4-momentum of the mother
  // -- see e.g. https://arxiv.org/pdf/1901.01593.pdf
  LoKi::ThreeVector pVec( p->momentum().Px(), p->momentum().Py(), p->momentum().Pz() );
  LoKi::ThreeVector flightDir = ( sv->position() - pv->position() ).Unit();
  const double      para      = pVec.Dot( flightDir );

  const double mH2 = -motherMass * motherMass - p->momentum().M2() + m_massMissing * m_massMissing;

  const double a = 4 * ( p->momentum().E() * p->momentum().E() - para * para );
  const double b = 4 * mH2 * para;
  const double c = 4 * motherMass * motherMass * p->momentum().E() * p->momentum().E() - mH2 * mH2;

  const double d = std::max( 0.0, b * b - 4 * a * c );

  const double pBAbsConstrPlus  = ( -b + std::sqrt( d ) ) / ( 2.0 * a );
  const double pBAbsConstrMinus = ( -b - std::sqrt( d ) ) / ( 2.0 * a );

  const LoKi::ThreeVector p3BPlus  = pBAbsConstrPlus * flightDir;
  const LoKi::ThreeVector p3BMinus = pBAbsConstrMinus * flightDir;

  const double EBPlus  = std::sqrt( motherMass * motherMass + p3BPlus.Mag2() );
  const double EBMinus = std::sqrt( motherMass * motherMass + p3BMinus.Mag2() );

  const Gaudi::LorentzVector p4BPlus( p3BPlus.X(), p3BPlus.Y(), p3BPlus.Z(), EBPlus );
  const Gaudi::LorentzVector p4BMinus( p3BMinus.X(), p3BMinus.Y(), p3BMinus.Z(), EBMinus );

  // -- These are the two solutions for the 4-momentum of the missinng particle
  Gaudi::LorentzVector p4MissPlus  = p4BPlus - p->momentum();
  Gaudi::LorentzVector p4MissMinus = p4BMinus - p->momentum();

  // -- now add the 4-momenta of the other particles we want
  for ( Indices::const_iterator index = m_indices.begin(); m_indices.end() != index; ++index ) {
    if ( 0 == *index ) {
      Warning( "Index 0 stands for the mother particle. Will be skipped" ).ignore();
      continue;
    }
    const LHCb::Particle* daughter = LoKi::Child::child( p, *index );
    if ( !daughter ) {
      Error( "'Daughter' is invalid, return 'InvalidMass'" ).ignore();
      return LoKi::Constants::InvalidMass; // RETURN
    }
    p4MissPlus += daughter->momentum();
    p4MissMinus += daughter->momentum();
  }

  return m_posSolution ? p4MissPlus.M() : p4MissMinus.M();
}
// ============================================================================
//  OPTIONAL: the specific printout
// ============================================================================
std::ostream& LoKi::Particles::InvMassMotherConstraintWithBestVertex::fillStream( std::ostream& s ) const {
  return s << "BPVMASSMOMCONSTR";
}
// ============================================================================

// ========== //00oo..oo00// ===============
// Implementation of the Bremshtralung corrected B mass
// ============================================================================
/*  constructor from the primary vertex
 *  @param x the x-position of primary vertex
 *  @param y the x-position of primary vertex
 *  @param z the x-position of primary vertex
 */
// ============================================================================
LoKi::Particles::BremMCorrected::BremMCorrected( const double x, const double y, const double z )
    : LoKi::Particles::PtFlight( x, y, z ) {}
// ============================================================================
/*  constructor from the primary vertex
 *  @param pv  the primary vertex
 */
// ============================================================================
LoKi::Particles::BremMCorrected::BremMCorrected( const LHCb::VertexBase* pv ) : LoKi::Particles::PtFlight( pv ) {}
// ============================================================================
/*  constructor from the primary vertex
 *  @param point  the position of primary vertex
 */
// ============================================================================
LoKi::Particles::BremMCorrected::BremMCorrected( const LoKi::Point3D& point ) : LoKi::Particles::PtFlight( point ) {}
// ============================================================================
// MANDATORY: clone method ("virtual constructor")
// ============================================================================
LoKi::Particles::BremMCorrected* LoKi::Particles::BremMCorrected::clone() const {
  return new LoKi::Particles::BremMCorrected( *this );
}
// ============================================================================
// MANDATORY: the only one essential method
// ============================================================================
double                                       LoKi::Particles::BremMCorrected::m_mass = -1 * Gaudi::Units::GeV;
LHCb::ParticleID                             LoKi::Particles::BremMCorrected::m_pid  = LHCb::ParticleID();
LoKi::Particles::BremMCorrected::result_type LoKi::Particles::BremMCorrected::
                                             operator()( LoKi::Particles::BremMCorrected::argument p ) const {
  if ( 0 > m_mass || 0 == m_pid.pid() ) {
    m_mass = LoKi::Particles::massFromName( "e-" );
    m_pid  = LoKi::Particles::pidFromName( "e-" );
  }

  if ( 0 == p ) {
    Error( "Invalid argument, return 'Invalid Mass'" ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    return LoKi::Constants::InvalidMass;
  }
  // get the decay vertex:
  const LHCb::VertexBase* vx = p->endVertex();
  if ( 0 == vx ) {
    Error( "EndVertex is invalid, return 'Invalid Mass'" ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    return LoKi::Constants::InvalidMass;
  }
  Assert( LoKi::Vertices::VertexHolder::valid(), "Vertex-Information is not valid" );

  LHCb::Particle::ConstVector m_all_electrons;
  LHCb::Particle::ConstVector m_rest_electrons;
  LHCb::Particle::ConstVector m_electron_mothers;
  LHCb::Particle::ConstVector m_others;

  classify( p, m_all_electrons, m_rest_electrons, m_electron_mothers, m_others );

  LorentzVector P_h_tot, P_e_tot;
  LorentzVector P_e_corr_tot, P_e_corr_temp;

  for ( const auto child : ( m_others ) ) { P_h_tot += ( child->momentum() ); }

  for ( const auto child : ( m_electron_mothers ) ) { P_e_tot += ( child->momentum() ); }

  for ( const auto child : ( m_rest_electrons ) ) { P_e_tot += ( child->momentum() ); }

  double pt_h = ptFlight( P_h_tot, p->endVertex()->position(), position() );
  double pt_e = ptFlight( P_e_tot, p->endVertex()->position(), position() );

  double alpha = pt_h / pt_e;

  for ( const auto child : m_all_electrons ) {
    double E_e = sqrt( pow( ( alpha * child->momentum().X() ), 2 ) + pow( ( alpha * child->momentum().Y() ), 2 ) +
                       pow( ( alpha * child->momentum().Z() ), 2 ) + ( m_mass * m_mass ) );
    P_e_corr_temp.SetXYZT( alpha * child->momentum().X(), alpha * child->momentum().Y(), alpha * child->momentum().Z(),
                           E_e );
    P_e_corr_tot += P_e_corr_temp;
  }

  LorentzVector P = P_h_tot + P_e_corr_tot;

  double corr_mass = P.M();

  return corr_mass;
}

// method which classifies all particles in the decay chain into following containers:
// m_all_electrons    - all electrons
// m_electron_mothers - all particles that decay exclusively into electrons and/or positrons (i.e. no hadronic/muonic
// daughters) m_rest_electrons   - electrons whose mother particle is not in 'm_electron_mothers' (i.e. comes from a
// decaty that has also hadronic component) m_others           - all non-electron particles (i.e. either particles that
// don't have an electron/positron among its daughters or 'stable' particles coming from semi-electronic decays)
bool LoKi::Particles::BremMCorrected::classify( const LHCb::Particle*        parent,
                                                LHCb::Particle::ConstVector& m_all_electrons,
                                                LHCb::Particle::ConstVector& m_rest_electrons,
                                                LHCb::Particle::ConstVector& m_electron_mothers,
                                                LHCb::Particle::ConstVector& m_others ) const {
  if ( !parent->isBasicParticle() ) {
    // if the particle has only electronic daughters push the particle into 'm_electron_mothers'; also put all the
    // daughters of the particle into 'm_all_electrons'
    if ( has_only_electrons( parent ) ) {
      m_electron_mothers.emplace_back( parent );
      for ( const auto& child : parent->daughtersVector() ) { m_all_electrons.emplace_back( child ); }
    }
    // if the particle has an electron among the daughters apply the 'classify' method recursively on its children; if
    // it has no electronic component push it to 'm_others'
    else {
      if ( has_electron( parent ) ) {
        for ( const auto& child : parent->daughtersVector() ) {
          classify( child, m_all_electrons, m_rest_electrons, m_electron_mothers, m_others );
        }
      } else
        m_others.emplace_back( parent );
    }
  }
  // if the particle is a basic particle put it into both 'm_all_electrons' and 'm_rest_electrons' containers if it is
  // an electron/positron and into 'm_others' if it is not an electron/positron
  else {
    if ( parent->particleID().abspid() == m_pid.abspid() ) {
      m_all_electrons.emplace_back( parent );
      m_rest_electrons.emplace_back( parent );
    } else
      m_others.emplace_back( parent );
  }
  return true;
}

// method that checks whether a particle has only electronic daughters
bool LoKi::Particles::BremMCorrected::has_only_electrons( const LHCb::Particle* parent ) const {
  for ( const auto& child : parent->daughtersVector() ) {
    if ( child->particleID().abspid() != m_pid.abspid() ) return false;
  }
  return true;
}

// method that checks whether a particle has at least one electron in its decay chain
bool LoKi::Particles::BremMCorrected::has_electron( const LHCb::Particle* parent ) const {
  for ( const auto& child : parent->daughtersVector() ) {
    if ( !child->isBasicParticle() ) {
      if ( has_electron( child ) ) return true;
    } else if ( child->particleID().abspid() == m_pid.abspid() )
      return true;
  }
  return false;
}
// ============================================================================
//  OPTIONAL: the specific printout
// ============================================================================
std::ostream& LoKi::Particles::BremMCorrected::fillStream( std::ostream& s ) const {
  return s << "HOPM(" << position().X() << "," << position().Y() << "," << position().Z() << ")";
}
// ============================================================================

// ============================================================================
// MANDATORY: clone method ("virtual constructor")
// ============================================================================
LoKi::Particles::BremMCorrectedWithBestVertex* LoKi::Particles::BremMCorrectedWithBestVertex::clone() const {
  return new LoKi::Particles::BremMCorrectedWithBestVertex( *this );
}
// ============================================================================
// MANDATORY: the only one essential method
// ============================================================================
LoKi::Particles::BremMCorrectedWithBestVertex::result_type LoKi::Particles::BremMCorrectedWithBestVertex::
                                                           operator()( LoKi::Particles::BremMCorrectedWithBestVertex::argument p ) const {

  if ( 0 == p ) {
    Error( "Invalid argument, return 'Invalid Mass'" ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    return LoKi::Constants::InvalidMass;
  }
  // get the decay vertex:
  const LHCb::VertexBase* vx = p->endVertex();
  if ( 0 == vx ) {
    Error( "EndVertex is invalid, return 'Invalid Mass'" ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    return LoKi::Constants::InvalidMass;
  }

  const LHCb::VertexBase* pv = bestVertex( p );
  if ( 0 == pv ) {
    Error( "BestVertex is invalid, return 'Invalid Mass'" ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    return LoKi::Constants::InvalidMass;
  }

  setVertex( pv );

  Assert( LoKi::Vertices::VertexHolder::valid(), "Vertex-Information is not valid" );

  BremMCorrected bremcorr( pv );

  return bremcorr( p );
}

// ============================================================================
//  OPTIONAL: the specific printout
// ============================================================================
std::ostream& LoKi::Particles::BremMCorrectedWithBestVertex::fillStream( std::ostream& s ) const {
  return s << "BPVHOPM()";
}
// ============================================================================

// ============================================================================
// constructor
// ============================================================================
LoKi::Particles::PDaughterRestFrameApproxWithBestVertex::PDaughterRestFrameApproxWithBestVertex(
    const std::string& name, LHCb::IParticlePropertySvc* ppsvc )
    : AuxFunBase{std::tie()}, LoKi::Particles::PtFlight( s_POINT ), m_pid( 0 ), m_ppsvc( ppsvc ) {

  const LHCb::ParticleProperty* pp = 0;
  if ( ppsvc ) {
    pp = ppsvc->find( name );
  } else {
    pp = LoKi::Particles::ppFromName( name );
  }
  if ( 0 == pp ) { Exception( "PDaughterRestFrameApproxWithBestVertex(): Unknow particle name '" + name + "'" ); }

  m_pid = pp->pdgID().abspid();
}
// ============================================================================
// constructor with mass value
// ============================================================================
LoKi::Particles::PDaughterRestFrameApproxWithBestVertex::PDaughterRestFrameApproxWithBestVertex(
    const std::string& name, const double mass, LHCb::IParticlePropertySvc* ppsvc )
    : AuxFunBase{std::tie()}, LoKi::Particles::PtFlight( s_POINT ), m_pid( 0 ), m_motherMass( mass ), m_ppsvc( ppsvc ) {

  const LHCb::ParticleProperty* pp = 0;
  if ( ppsvc ) {
    pp = ppsvc->find( name );
  } else {
    pp = LoKi::Particles::ppFromName( name );
  }
  if ( 0 == pp ) { Exception( "PDaughterRestFrameApproxWithBestVertex(): Unknow particle name '" + name + "'" ); }

  m_pid = pp->pdgID().abspid();
}
// ============================================================================
// MANDATORY: clone method ("virtual constructor")
// ============================================================================
LoKi::Particles::PDaughterRestFrameApproxWithBestVertex*
LoKi::Particles::PDaughterRestFrameApproxWithBestVertex::clone() const {
  return new LoKi::Particles::PDaughterRestFrameApproxWithBestVertex( *this );
}
// ============================================================================
// MANDATORY: the only one essential method
// ============================================================================
LoKi::Particles::PDaughterRestFrameApproxWithBestVertex::result_type
LoKi::Particles::PDaughterRestFrameApproxWithBestVertex::
operator()( LoKi::Particles::PDaughterRestFrameApproxWithBestVertex::argument p ) const {
  if ( 0 == p ) {
    Error( "Invalid argument, return 'Invalid Momentum'" ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    return LoKi::Constants::InvalidMomentum;
  }

  const LHCb::Particle::ConstVector& daughters = p->daughtersVector();
  auto                               it        = std::find_if( daughters.begin(), daughters.end(),
                          [&]( const LHCb::Particle* dau ) { return dau->particleID().abspid() == m_pid; } );
  if ( it == daughters.end() || !( *it ) ) {
    Error( "Daughter not found, return 'Invalid Momentum'" ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    return LoKi::Constants::InvalidMomentum;
  }
  const LHCb::Particle* partForP = *it;

  // get the decay vertex of the 'mother':
  const LHCb::VertexBase* vx = p->endVertex();
  if ( 0 == vx ) {
    Error( "EndVertex is invalid, return 'Invalid Momentum'" ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    return LoKi::Constants::InvalidMomentum;
  }
  //
  const LHCb::VertexBase* pv = bestVertex( p );
  if ( 0 == pv ) {
    Error( "BestVertex is invalid, return 'Invalid Momentum'" ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    return LoKi::Constants::InvalidMomentum;
  }
  //
  setVertex( pv );
  //
  Assert( LoKi::Vertices::VertexHolder::valid(), "Vertex-Information is not valid" );
  //

  double mass = -1.0;
  if ( m_motherMass < 0.0 ) {
    const LHCb::ParticleProperty* pp = nullptr;
    if ( m_ppsvc ) {
      pp = m_ppsvc->find( p->particleID() );
    } else {
      pp = LoKi::Particles::ppFromPID( p->particleID() );
    }
    if ( 0 == pp ) {
      Exception( "PDaughterRestFrameApproxWithBestVertex(): Unknow particle id '" + p->particleID().toString() + "'" );
    }
    mass = pp->mass();
  } else {
    mass = m_motherMass;
  }

  if ( mass < 0.0 )
    Exception( "PDaughterRestFrameApproxWithBestVertex(): Invalid mass for mother particle: " +
               std::to_string( mass ) );

  LoKi::LorentzVector p4B = pRestFrameApprox( p->momentum(), ( vx->position() - position() ).Unit(), mass );
  return LoKi::Kinematics::restMomentum( partForP->momentum(), p4B );
}
// ============================================================================
//  OPTIONAL: the specific printout
// ============================================================================
std::ostream& LoKi::Particles::PDaughterRestFrameApproxWithBestVertex::fillStream( std::ostream& s ) const {
  return s << "BPVPDAUGHTERRESTFRAMEAPPROX";
}
// ============================================================================

// ============================================================================
// constructor
// ============================================================================
LoKi::Particles::EDaughterRestFrameApproxWithBestVertex::EDaughterRestFrameApproxWithBestVertex(
    const std::string& name, LHCb::IParticlePropertySvc* ppsvc )
    : AuxFunBase{std::tie()}, LoKi::Particles::PtFlight( s_POINT ), m_pid( 0 ), m_ppsvc( ppsvc ) {

  const LHCb::ParticleProperty* pp = 0;
  if ( ppsvc ) {
    pp = ppsvc->find( name );
  } else {
    pp = LoKi::Particles::ppFromName( name );
  }
  if ( 0 == pp ) { Exception( "EDaughterRestFrameApproxWithBestVertex(): Unknow particle name '" + name + "'" ); }

  m_pid = pp->pdgID().abspid();
}
// ============================================================================
// constructor with mass value
// ============================================================================
LoKi::Particles::EDaughterRestFrameApproxWithBestVertex::EDaughterRestFrameApproxWithBestVertex(
    const std::string& name, const double mass, LHCb::IParticlePropertySvc* ppsvc )
    : AuxFunBase{std::tie()}, LoKi::Particles::PtFlight( s_POINT ), m_pid( 0 ), m_motherMass( mass ), m_ppsvc( ppsvc ) {

  const LHCb::ParticleProperty* pp = 0;
  if ( ppsvc ) {
    pp = ppsvc->find( name );
  } else {
    pp = LoKi::Particles::ppFromName( name );
  }
  if ( 0 == pp ) { Exception( "EDaughterRestFrameApproxWithBestVertex(): Unknow particle name '" + name + "'" ); }

  m_pid = pp->pdgID().abspid();
}
// ============================================================================
// MANDATORY: clone method ("virtual constructor")
// ============================================================================
LoKi::Particles::EDaughterRestFrameApproxWithBestVertex*
LoKi::Particles::EDaughterRestFrameApproxWithBestVertex::clone() const {
  return new LoKi::Particles::EDaughterRestFrameApproxWithBestVertex( *this );
}
// ============================================================================
// MANDATORY: the only one essential method
// ============================================================================
LoKi::Particles::EDaughterRestFrameApproxWithBestVertex::result_type
LoKi::Particles::EDaughterRestFrameApproxWithBestVertex::
operator()( LoKi::Particles::EDaughterRestFrameApproxWithBestVertex::argument p ) const {
  if ( 0 == p ) {
    Error( "Invalid argument, return 'Invalid Momentum'" ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    return LoKi::Constants::InvalidMomentum;
  }

  const LHCb::Particle::ConstVector& daughters = p->daughtersVector();
  auto                               it        = std::find_if( daughters.begin(), daughters.end(),
                          [&]( const LHCb::Particle* dau ) { return dau->particleID().abspid() == m_pid; } );
  if ( it == daughters.end() || !( *it ) ) {
    Error( "Daughter not found, return 'Invalid Momentum'" ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    return LoKi::Constants::InvalidMomentum;
  }
  const LHCb::Particle* partForP = *it;

  // get the decay vertex of the 'mother':
  const LHCb::VertexBase* vx = p->endVertex();
  if ( 0 == vx ) {
    Error( "EndVertex is invalid, return 'Invalid Momentum'" ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    return LoKi::Constants::InvalidMomentum;
  }
  //
  const LHCb::VertexBase* pv = bestVertex( p );
  if ( 0 == pv ) {
    Error( "BestVertex is invalid, return 'Invalid Momentum'" ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    return LoKi::Constants::InvalidMomentum;
  }
  //
  setVertex( pv );
  //
  Assert( LoKi::Vertices::VertexHolder::valid(), "Vertex-Information is not valid" );
  //

  double mass = -1.0;
  if ( m_motherMass < 0.0 ) {
    const LHCb::ParticleProperty* pp = nullptr;
    if ( m_ppsvc ) {
      pp = m_ppsvc->find( p->particleID() );
    } else {
      pp = LoKi::Particles::ppFromPID( p->particleID() );
    }
    if ( 0 == pp ) {
      Exception( "PDaughterRestFrameApproxWithBestVertex(): Unknow particle id '" + p->particleID().toString() + "'" );
    }
    mass = pp->mass();
  } else {
    mass = m_motherMass;
  }

  if ( mass < 0.0 )
    Exception( "PDaughterRestFrameApproxWithBestVertex(): Invalid mass for mother particle: " +
               std::to_string( mass ) );

  LoKi::LorentzVector p4B = pRestFrameApprox( p->momentum(), ( vx->position() - position() ).Unit(), mass );
  return LoKi::Kinematics::restEnergy( partForP->momentum(), p4B );
}
// ============================================================================
//  OPTIONAL: the specific printout
// ============================================================================
std::ostream& LoKi::Particles::EDaughterRestFrameApproxWithBestVertex::fillStream( std::ostream& s ) const {
  return s << "BPVEDAUGHTERRESTFRAMEAPPROX";
}
// ============================================================================

// ============================================================================
// The END
// ============================================================================
