/*****************************************************************************\
* (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
// Include files
// ============================================================================
// GaudiKernel
// ============================================================================
#include "GaudiKernel/IAlgContextSvc.h"
#include "GaudiKernel/IToolSvc.h"
#include "GaudiKernel/SmartIF.h"
// ============================================================================
// GaudiAlg
// ============================================================================
#include "GaudiAlg/GaudiAlgorithm.h"
#include "GaudiAlg/GetAlgs.h"
// ============================================================================
// local
// ============================================================================
#include "LoKi/ParticleProperties.h"
#include "LoKi/Particles50.h"
// ============================================================================
namespace {
  // ==========================================================================
  /// the default name for the tool
  const std::string s_toolname = "DedxGlobalPID::ChargedProtoDedxPIDTool/ChargedProtoDedxPID:PUBLIC";
  const DedxGlobalPID::IChargedProtoDedxPIDTool* const s_tool = nullptr;
  // ==========================================================================
} // namespace
// ============================================================================
/** constructor from particle hypothesis, detector type, version
 *  @param hypothesis The PID hypothesis to evaluate for this particle
 *  @param detector   Subdetector dedx info to use
 *  @param version    PDD version to use
 */
// ============================================================================
LoKi::Particles::DedxPID::DedxPID( const std::string& hypothesis, const std::string& detector,
                                   const std::string& version, const std::string& type )
    : LoKi::Particles::DedxPID::DedxPID( hypothesis, detector, version, type, s_toolname ) {}
// ============================================================================
// mandatory: virtual destructor
// ============================================================================
LoKi::Particles::DedxPID::~DedxPID() {
  if ( m_tool && !gaudi() ) { m_tool.reset(); }
}
// ============================================================================
// MANDATORY: clone method ("virtual contructor")
// ============================================================================
LoKi::Particles::DedxPID* LoKi::Particles::DedxPID::clone() const { return new LoKi::Particles::DedxPID( *this ); }
// ============================================================================
// ======================================================================
/** constructor from particle hypothesis, detector type, version & tool
 *  @param hypothesis The PID hypothesis to evaluate for this particle
 *  @param detector   Subdetector dedx info to use
 *  @param version    PDD version to use
 *  @param tool       typename for the tool
 */
// ============================================================================
LoKi::Particles::DedxPID::DedxPID( const std::string& hypothesis, const std::string& detector,
                                   const std::string& version, const std::string& type, const std::string& tool )
    : LoKi::AuxFunBase( std::tie( hypothesis, detector, version, type, tool ) )
    , LoKi::BasicFunctors<const LHCb::Particle*>::Function()
    , s_hypothesis( hypothesis )
    , s_detector( detector )
    , s_version( version )
    , s_type( type )
    , m_toolname( tool )
    , m_tool( s_tool ) {
  // now need to get the tool
  const LoKi::ILoKiSvc* svc0 = lokiSvc();
  Assert( nullptr != svc0, "Can't get LoKi service!" );
  LoKi::ILoKiSvc* svc = const_cast<LoKi::ILoKiSvc*>( svc0 );
  // first try to use context  stuff
  // get the context service:
  SmartIF<IAlgContextSvc> cntx( svc );
  if ( cntx ) {
    // 2. get 'simple' algorithm from the context:
    GaudiAlgorithm* alg = Gaudi::Utils::getGaudiAlg( cntx );
    if ( 0 != alg ) {
      // get the tool from the algorithm
      const DedxGlobalPID::IChargedProtoDedxPIDTool* pid =
          alg->tool<DedxGlobalPID::IChargedProtoDedxPIDTool>( m_toolname, alg );
      if ( nullptr != pid ) { m_tool = pid; }
    }
    //
  }
  //
  if ( !m_tool ) {
    // try to use  Tool Service
    SmartIF<IToolSvc> tsvc( svc );
    if ( tsvc ) {
      const DedxGlobalPID::IChargedProtoDedxPIDTool* pid = nullptr;
      StatusCode                                     sc  = tsvc->retrieveTool( m_toolname, pid );
      if ( sc.isSuccess() && 0 != pid ) { m_tool = pid; }
    }
  }
  //
  Assert( m_tool, "Can't retrieve tool '" + m_toolname + "'" );

  // PID Hypothesis
  m_hypothesis =
      ( s_HE.end() != std::find( s_HE.begin(), s_HE.end(), s_hypothesis )
            ? Hypothesis::HE
            : s_GC.end() != std::find( s_GC.begin(), s_GC.end(), s_hypothesis )
                  ? Hypothesis::GC
                  : s_Z1.end() != std::find( s_Z1.begin(), s_Z1.end(), s_hypothesis ) ? Hypothesis::Z1
                                                                                      : Hypothesis::NoHypothesis );

  // Version
  m_version =
      ( s_V1.end() != std::find( s_V1.begin(), s_V1.end(), s_version )
            ? Version::V1
            : s_V2.end() != std::find( s_V2.begin(), s_V2.end(), s_version )
                  ? Version::V2
                  : s_V3.end() != std::find( s_V3.begin(), s_V3.end(), s_version ) ? Version::V3 : Version::NoVersion );

  // Detector
  m_detector = ( s_VELO.end() != std::find( s_VELO.begin(), s_VELO.end(), s_detector )
                     ? Detector::VELO
                     : s_R.end() != std::find( s_R.begin(), s_R.end(), s_detector )
                           ? Detector::R
                           : s_PHI.end() != std::find( s_PHI.begin(), s_PHI.end(), s_detector )
                                 ? Detector::PHI
                                 : s_TT.end() != std::find( s_TT.begin(), s_TT.end(), s_detector )
                                       ? Detector::TT
                                       : s_IT.end() != std::find( s_IT.begin(), s_IT.end(), s_detector )
                                             ? Detector::IT
                                             : s_IT1.end() != std::find( s_IT1.begin(), s_IT1.end(), s_detector )
                                                   ? Detector::IT1
                                                   : s_IT2.end() != std::find( s_IT2.begin(), s_IT2.end(), s_detector )
                                                         ? Detector::IT2
                                                         : Detector::NoDetector );

  // Type
  m_type = ( s_LL2D.end() != std::find( s_LL2D.begin(), s_LL2D.end(), s_type ) ? Type::LL2D : Type::NoType );
}
// ============================================================================
// MANDATORY: the only one important method
// ============================================================================
LoKi::Particles::DedxPID::result_type LoKi::Particles::DedxPID::
                                      operator()( LoKi::Particles::DedxPID::argument p ) const {
  //
  if ( !p ) {
    Error( "Invalid LHCb::Particle*, return -10000" ).ignore();
    return -10000;
  }
  //
  const LHCb::ProtoParticle* pp = p->proto();
  if ( !p ) {
    Error( "Invalid LHCb::ProtoParticle*, return -9999" ).ignore();
    return -9999;
  }
  //
  // use the tool!
  std::optional<double> res = m_tool->dedxPID( pp, m_hypothesis, m_detector, m_version, m_type );
  //
  if ( !res ) {
    Error( "Invalid result from IChargedProtoDedxPIDTool" ).ignore();
    return err_val;
  }
  return *res;
}
// ============================================================================
// OPTIONAL: nice printout
// ============================================================================
std::ostream& LoKi::Particles::DedxPID::fillStream( std::ostream& s ) const {
  s << "DedxPID('" << s_hypothesis << "','" << s_detector << "','" << s_version << "', '" << s_type << "'";
  if ( m_toolname != s_toolname ) { s << ",'" << s_toolname << "'"; }
  return s << ")";
}
// ============================================================================
// The END
// ============================================================================
