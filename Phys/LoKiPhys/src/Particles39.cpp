/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
// Include files
// ============================================================================
// STD & STL
// ============================================================================
#include <functional>
// ============================================================================
// LoKiCode
// ============================================================================
#include "LoKi/Algs.h"
#include "LoKi/Constants.h"
// ============================================================================
// LoKiPhys
// ============================================================================
#include "LoKi/Particles39.h"
#include "LoKi/PhysKinematics.h"
#include "LoKi/PhysSources.h"
// ============================================================================
/** @file
 *  Implementation file for functions form file LoKi/Particles39.h
 *
 *  This file is a part of LoKi project -
 *    "C++ ToolKit  for Smart and Friendly Physics Analysis"
 *
 *  The package has been designed with the kind help from
 *  Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
 *  contributions and advices from G.Raven, J.van Tilburg,
 *  A.Golutvin, P.Koppenburg have been used in the design.
 *
 *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
 *  @date 2012-01-17
 *
 */
// ============================================================================
// constructor
// ============================================================================
LoKi::Particles::MinMaxDistance::MinMaxDistance( const bool minmax, const LHCb::Particle::Range& parts,
                                                 LoKi::Particles::MinMaxDistance::dist_func dist )
    : LoKi::BasicFunctors<const LHCb::Particle*>::Function()
    , LoKi::UniqueKeeper<LHCb::Particle>( parts.begin(), parts.end() )
    , m_minimum( minmax )
    , m_distance( dist ) {
  if ( !m_distance ) { m_distance = &LoKi::PhysKinematics::deltaR2; }
}
// ============================================================================
double LoKi::Particles::MinMaxDistance::distance( const LHCb::Particle* p1, const LHCb::Particle* p2 ) const {
  return ( *m_distance )( p1, p2 );
}
// ============================================================================
// MANDATORY: clone method ("virtual constructor")
// ============================================================================
LoKi::Particles::MinMaxDistance* LoKi::Particles::MinMaxDistance::clone() const {
  return new LoKi::Particles::MinMaxDistance( *this );
}
// ============================================================================
// MANDATORY: the only essential method
// ============================================================================
LoKi::Particles::MinMaxDistance::result_type LoKi::Particles::MinMaxDistance::
                                             operator()( LoKi::Particles::MinMaxDistance::argument p ) const {
  //
  if ( 0 == p ) {
    Error( "LHCb::Particle* points to NULL, return Infinity" ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    //
    return minimum() ? LoKi::Constants::PositiveInfinity : LoKi::Constants::NegativeInfinity;
    //
  }
  //
  return distance( p );
}
// ============================================================================
// the actual computation
// ============================================================================
double LoKi::Particles::MinMaxDistance::distance( const LHCb::Particle* p ) const {
  //
  auto pa = minimum() ? LoKi::Algs::extremum(
                            begin(), end(), [&]( const LHCb::Particle* q ) { return this->distance( p, q ); },
                            std::less<double>() )
                      : LoKi::Algs::extremum(
                            begin(), end(), [&]( const LHCb::Particle* q ) { return this->distance( p, q ); },
                            std::greater<double>() );
  //
  return ( pa.first != end() ) ? pa.second
                               : ( minimum() ? LoKi::Constants::PositiveInfinity : LoKi::Constants::NegativeInfinity );
}
// ============================================================================

// ============================================================================
// Constructor
// ============================================================================
LoKi::Particles::MinMaxDistanceWithSource::MinMaxDistanceWithSource(
    const bool minmax, const LoKi::BasicFunctors<const LHCb::Particle*>::Source& source,
    LoKi::Particles::MinMaxDistance::dist_func dist )
    : LoKi::Particles::MinMaxDistance::MinMaxDistance( minmax, LHCb::Particle::Range(), dist ), m_source( source ) {}
// ============================================================================
// MANDATORY: clone method ("virtual constructor")
// ============================================================================
LoKi::Particles::MinMaxDistanceWithSource* LoKi::Particles::MinMaxDistanceWithSource::clone() const {
  return new LoKi::Particles::MinMaxDistanceWithSource( *this );
}
// ============================================================================
// MANDATORY: the only essential method
// ============================================================================
LoKi::Particles::MinMaxDistanceWithSource::result_type LoKi::Particles::MinMaxDistanceWithSource::
                                                       operator()( LoKi::Particles::MinMaxDistanceWithSource::argument p ) const {
  //
  if ( 0 == p ) {
    Error( "LHCb::Particle* points to NULL, return Infinity" ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    //
    return minimum() ? LoKi::Constants::PositiveInfinity : LoKi::Constants::NegativeInfinity;
    //
  }
  // check the event
  if ( !sameEvent() ) {
    typedef LoKi::UniqueKeeper<LHCb::Particle> KEEPER;
    const KEEPER&                              keep1 = *this;
    KEEPER&                                    keep2 = const_cast<KEEPER&>( keep1 );
    //
    // clear the list of particles
    keep2.clear();
    // get the particles form the source
    LHCb::Particle::ConstVector particles = source()(); // NB
    // fill the functor with particles
    keep2.addObjects( particles.begin(), particles.end() );
    if ( empty() ) {
      Warning( "Empty list of particles is loaded!" ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    }
    // update the event:
    setEvent();
  }
  // use the functor
  return distance( p );
}
// ============================================================================

// ============================================================================
// constructor from the source
// ============================================================================
LoKi::Particles::MinDR2::MinDR2( const LoKi::BasicFunctors<const LHCb::Particle*>::Source& source )
    : LoKi::AuxFunBase( std::tie( source ) )
    , LoKi::Particles::MinMaxDistanceWithSource( true, source, &LoKi::PhysKinematics::deltaR2 ) {}
// ============================================================================
// constructor from the location & filter
// ============================================================================
LoKi::Particles::MinDR2::MinDR2( const std::string&                                           location,
                                 const LoKi::BasicFunctors<const LHCb::Particle*>::Predicate& cut )
    : LoKi::AuxFunBase( std::tie( location, cut ) )
    , LoKi::Particles::MinMaxDistanceWithSource( true, LoKi::Particles::SourceTES( location, cut ),
                                                 &LoKi::PhysKinematics::deltaR2 ) {}
// ============================================================================
// constructor from the location & filter
// ============================================================================
LoKi::Particles::MinDR2::MinDR2( const std::vector<std::string>&                              locations,
                                 const LoKi::BasicFunctors<const LHCb::Particle*>::Predicate& cut )
    : LoKi::AuxFunBase( std::tie( locations, cut ) )
    , LoKi::Particles::MinMaxDistanceWithSource( true, LoKi::Particles::SourceTES( locations, cut ),
                                                 &LoKi::PhysKinematics::deltaR2 ) {}
// ============================================================================
// MANDATORY: clone method ("virtual constructor")
// ============================================================================
LoKi::Particles::MinDR2* LoKi::Particles::MinDR2::clone() const { return new LoKi::Particles::MinDR2( *this ); }
// ============================================================================
// OPTIONAL: nice printout
// ============================================================================
std::ostream& LoKi::Particles::MinDR2::fillStream( std::ostream& s ) const {
  return s << " MINDR2( " << source() << ") ";
}
// ============================================================================

// ============================================================================
// constructor from the source
// ============================================================================
LoKi::Particles::MaxDR2::MaxDR2( const LoKi::BasicFunctors<const LHCb::Particle*>::Source& source )
    : LoKi::AuxFunBase( std::tie( source ) )
    , LoKi::Particles::MinMaxDistanceWithSource( false, source, &LoKi::PhysKinematics::deltaR2 ) {}
// ============================================================================
// constructor from the location & filter
// ============================================================================
LoKi::Particles::MaxDR2::MaxDR2( const std::string&                                           location,
                                 const LoKi::BasicFunctors<const LHCb::Particle*>::Predicate& cut )
    : LoKi::AuxFunBase( std::tie( location, cut ) )
    , LoKi::Particles::MinMaxDistanceWithSource( false, LoKi::Particles::SourceTES( location, cut ),
                                                 &LoKi::PhysKinematics::deltaR2 ) {}
// ============================================================================
// constructor from the location & filter
// ============================================================================
LoKi::Particles::MaxDR2::MaxDR2( const std::vector<std::string>&                              locations,
                                 const LoKi::BasicFunctors<const LHCb::Particle*>::Predicate& cut )
    : LoKi::AuxFunBase( std::tie( locations, cut ) )
    , LoKi::Particles::MinMaxDistanceWithSource( false, LoKi::Particles::SourceTES( locations, cut ),
                                                 &LoKi::PhysKinematics::deltaR2 ) {}
// ============================================================================
// MANDATORY: clone method ("virtual constructor")
// ============================================================================
LoKi::Particles::MaxDR2* LoKi::Particles::MaxDR2::clone() const { return new LoKi::Particles::MaxDR2( *this ); }
// ============================================================================
// OPTIONAL: nice printout
// ============================================================================
std::ostream& LoKi::Particles::MaxDR2::fillStream( std::ostream& s ) const {
  return s << " MAXDR2( " << source() << ") ";
}
// ============================================================================

// ============================================================================
// constructor from the source
// ============================================================================
LoKi::Particles::MinKL::MinKL( const LoKi::BasicFunctors<const LHCb::Particle*>::Source& source )
    : LoKi::AuxFunBase( std::tie( source ) )
    , LoKi::Particles::MinMaxDistanceWithSource( true, source, &LoKi::PhysKinematics::kullback ) {}
// ============================================================================
// constructor from the location & filter
// ============================================================================
LoKi::Particles::MinKL::MinKL( const std::string&                                           location,
                               const LoKi::BasicFunctors<const LHCb::Particle*>::Predicate& cut )
    : LoKi::AuxFunBase( std::tie( location, cut ) )
    , LoKi::Particles::MinMaxDistanceWithSource( true, LoKi::Particles::SourceTES( location, cut ),
                                                 &LoKi::PhysKinematics::kullback ) {}
// ============================================================================
// constructor from the location & filter
// ============================================================================
LoKi::Particles::MinKL::MinKL( const std::vector<std::string>&                              locations,
                               const LoKi::BasicFunctors<const LHCb::Particle*>::Predicate& cut )
    : LoKi::AuxFunBase( std::tie( locations, cut ) )
    , LoKi::Particles::MinMaxDistanceWithSource( true, LoKi::Particles::SourceTES( locations, cut ),
                                                 &LoKi::PhysKinematics::kullback ) {}
// ============================================================================
// MANDATORY: clone method ("virtual constructor")
// ============================================================================
LoKi::Particles::MinKL* LoKi::Particles::MinKL::clone() const { return new LoKi::Particles::MinKL( *this ); }
// ============================================================================
// OPTIONAL: nice printout
// ============================================================================
std::ostream& LoKi::Particles::MinKL::fillStream( std::ostream& s ) const {
  return s << " MINKL( " << source() << ") ";
}
// ============================================================================

// ============================================================================
// constructor from the source
// ============================================================================
LoKi::Particles::MaxIDs::MaxIDs( const LoKi::BasicFunctors<const LHCb::Particle*>::Source& source )
    : LoKi::AuxFunBase( std::tie( source ) )
    , LoKi::Particles::MinMaxDistanceWithSource( false, source, &LoKi::PhysKinematics::overlap ) {}
// ============================================================================
// constructor from the location & filter
// ============================================================================
LoKi::Particles::MaxIDs::MaxIDs( const std::string&                                           location,
                                 const LoKi::BasicFunctors<const LHCb::Particle*>::Predicate& cut )
    : LoKi::AuxFunBase( std::tie( location, cut ) )
    , LoKi::Particles::MinMaxDistanceWithSource( false, LoKi::Particles::SourceTES( location, cut ),
                                                 &LoKi::PhysKinematics::overlap ) {}
// ============================================================================
// constructor from the location & filter
// ============================================================================
LoKi::Particles::MaxIDs::MaxIDs( const std::vector<std::string>&                              locations,
                                 const LoKi::BasicFunctors<const LHCb::Particle*>::Predicate& cut )
    : LoKi::AuxFunBase( std::tie( locations, cut ) )
    , LoKi::Particles::MinMaxDistanceWithSource( false, LoKi::Particles::SourceTES( locations, cut ),
                                                 &LoKi::PhysKinematics::overlap ) {}
// ============================================================================
// MANDATORY: clone method ("virtual constructor")
// ============================================================================
LoKi::Particles::MaxIDs* LoKi::Particles::MaxIDs::clone() const { return new LoKi::Particles::MaxIDs( *this ); }
// ============================================================================
// OPTIONAL: nice printout
// ============================================================================
std::ostream& LoKi::Particles::MaxIDs::fillStream( std::ostream& s ) const {
  return s << " MAXIDS( " << source() << ") ";
}
// ============================================================================

// ============================================================================
// constructor from the source
// ============================================================================
LoKi::Particles::MinAngle::MinAngle( const LoKi::BasicFunctors<const LHCb::Particle*>::Source& source )
    : LoKi::AuxFunBase( std::tie( source ) )
    , LoKi::Particles::MinMaxDistanceWithSource( true, source, &LoKi::PhysKinematics::deltaAlpha ) {}
// ============================================================================
// constructor from the location & filter
// ============================================================================
LoKi::Particles::MinAngle::MinAngle( const std::string&                                           location,
                                     const LoKi::BasicFunctors<const LHCb::Particle*>::Predicate& cut )
    : LoKi::AuxFunBase( std::tie( location, cut ) )
    , LoKi::Particles::MinMaxDistanceWithSource( true, LoKi::Particles::SourceTES( location, cut ),
                                                 &LoKi::PhysKinematics::deltaAlpha ) {}
// ============================================================================
// constructor from the location & filter
// ============================================================================
LoKi::Particles::MinAngle::MinAngle( const std::vector<std::string>&                              locations,
                                     const LoKi::BasicFunctors<const LHCb::Particle*>::Predicate& cut )
    : LoKi::AuxFunBase( std::tie( locations, cut ) )
    , LoKi::Particles::MinMaxDistanceWithSource( true, LoKi::Particles::SourceTES( locations, cut ),
                                                 &LoKi::PhysKinematics::deltaAlpha ) {}
// ============================================================================
// MANDATORY: clone method ("virtual constructor")
// ============================================================================
LoKi::Particles::MinAngle* LoKi::Particles::MinAngle::clone() const { return new LoKi::Particles::MinAngle( *this ); }
// ============================================================================
// OPTIONAL: nice printout
// ============================================================================
std::ostream& LoKi::Particles::MinAngle::fillStream( std::ostream& s ) const {
  return s << " MINANGLE( " << source() << ") ";
}
// ============================================================================

// ============================================================================
// constructor from the source
// ============================================================================
LoKi::Particles::MinM2::MinM2( const LoKi::BasicFunctors<const LHCb::Particle*>::Source& source )
    : LoKi::AuxFunBase( std::tie( source ) )
    , LoKi::Particles::MinMaxDistanceWithSource( true, source, &LoKi::PhysKinematics::deltaM2 ) {}
// ============================================================================
// constructor from the location & filter
// ============================================================================
LoKi::Particles::MinM2::MinM2( const std::string&                                           location,
                               const LoKi::BasicFunctors<const LHCb::Particle*>::Predicate& cut )
    : LoKi::AuxFunBase( std::tie( location, cut ) )
    , LoKi::Particles::MinMaxDistanceWithSource( true, LoKi::Particles::SourceTES( location, cut ),
                                                 &LoKi::PhysKinematics::deltaM2 ) {}
// ============================================================================
// constructor from the location & filter
// ============================================================================
LoKi::Particles::MinM2::MinM2( const std::vector<std::string>&                              locations,
                               const LoKi::BasicFunctors<const LHCb::Particle*>::Predicate& cut )
    : LoKi::AuxFunBase( std::tie( locations, cut ) )
    , LoKi::Particles::MinMaxDistanceWithSource( true, LoKi::Particles::SourceTES( locations, cut ),
                                                 &LoKi::PhysKinematics::deltaM2 ) {}
// ============================================================================
// MANDATORY: clone method ("virtual constructor")
// ============================================================================
LoKi::Particles::MinM2* LoKi::Particles::MinM2::clone() const { return new LoKi::Particles::MinM2( *this ); }
// ============================================================================
// OPTIONAL: nice printout
// ============================================================================
std::ostream& LoKi::Particles::MinM2::fillStream( std::ostream& s ) const {
  return s << " MINM2( " << source() << ") ";
}
// ============================================================================

// ============================================================================
// The END
// ============================================================================
