/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
// Include files
// ============================================================================
// LoKi
// ============================================================================
#include "LoKi/Particles28.h"
#include "LoKi/GetTools.h"
// ============================================================================
/** @file
 *  The file with functors for various "refit"
 *  @author Vanya BELYAEV Ivan.BElyaev@nikhef.nl
 *  @date   2009-04-30
 */
// ============================================================================
namespace {
  // ==========================================================================
  /// the invalid tool
  const IParticleReFitter* const s_REFIT = nullptr;
  /// the invalid tool
  const IMassFit* const s_MFIT = nullptr;
  // ==========================================================================
} // end of anonymous namespace
// ============================================================================
// constructor from the tool name
// ============================================================================
LoKi::Particles::ReFitter::ReFitter( const std::string& name )
    : LoKi::AuxFunBase( std::tie( name ) ), LoKi::Particles::ReFit( s_REFIT ), m_name( name ) {}
// ============================================================================
// MANDATORY: the only one essential method
// ============================================================================
LoKi::Particles::ReFitter::result_type LoKi::Particles::ReFitter::
                                       operator()( LoKi::Particles::ReFitter::argument p ) const {
  if ( !p ) {
    Error( "LHCb::Particle* points to NULL, return 'False'" ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    return false;
  }
  //
  if ( !fitter() ) { setFitter( LoKi::GetTools::particleReFitter( *this, m_name ) ); }
  //
  const LHCb::Particle* cp  = p;
  LHCb::Particle*       ncp = const_cast<LHCb::Particle*>( cp );
  //
  StatusCode sc = reFit( ncp );
  //
  return sc.isSuccess();
}
// ============================================================================
// OPTIONAL: nice printout
// ============================================================================
std::ostream& LoKi::Particles::ReFitter::fillStream( std::ostream& s ) const {
  return s << "REFIT('" << m_name << "')";
}
// ============================================================================

// ============================================================================
// constructor from the tool name
// ============================================================================
LoKi::Particles::MassFitter::MassFitter( const std::string& name )
    : LoKi::AuxFunBase( std::tie( name ) ), LoKi::Particles::MassFit( s_MFIT ), m_name( name ) {}
// ============================================================================
// constructor from the tool name
// ============================================================================
LoKi::Particles::MassFitter::MassFitter( const double mass, const std::string& name )
    : LoKi::AuxFunBase( std::tie( mass, name ) ), LoKi::Particles::MassFit( s_MFIT, mass ), m_name( name ) {}
// ============================================================================
// constructor from the tool name
// ============================================================================
LoKi::Particles::MassFitter::MassFitter( const std::string& name, const double mass )
    : LoKi::AuxFunBase( std::tie( name, mass ) ), LoKi::Particles::MassFit( s_MFIT, mass ), m_name( name ) {}
// ============================================================================
// MANDATORY: the only one essential method
// ============================================================================
LoKi::Particles::MassFitter::result_type LoKi::Particles::MassFitter::
                                         operator()( LoKi::Particles::MassFitter::argument p ) const {
  if ( !p ) {
    Error( "Invalid Particle, return 'false'" ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    return false;
  }
  //
  if ( !fitter() ) { setFitter( LoKi::GetTools::massFitter( *this, m_name ) ); }
  //
  const LHCb::Particle* cp  = p;
  LHCb::Particle*       ncp = const_cast<LHCb::Particle*>( cp );
  //
  StatusCode sc = flag() ? fit( ncp, mass() ) : fit( ncp );
  //
  return sc.isSuccess();
}
// ============================================================================
// OPTIONAL: nice printout
// ============================================================================
std::ostream& LoKi::Particles::MassFitter::fillStream( std::ostream& s ) const {
  if ( !flag() ) { return s << "MFIT"; }
  return s << "MFITTER('" << m_name << "'," << mass() << ")";
}
// ============================================================================

// ============================================================================
// The END
// ============================================================================
