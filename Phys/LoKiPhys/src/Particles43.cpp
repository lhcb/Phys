/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
// Include files
// ============================================================================
// STD & STL
// ============================================================================
#include <functional>
// ============================================================================
// LoKiCode
// ============================================================================
#include "LoKi/Algs.h"
#include "LoKi/Constants.h"
#include "LoKi/Functors.h"
// ============================================================================
// LoKiPhys
// ============================================================================
#include "LoKi/Particles17.h"
#include "LoKi/Particles43.h"
#include "LoKi/Particles8.h"
#include "LoKi/Particles9.h"
#include "LoKi/PhysKinematics.h"
#include "LoKi/PhysSources.h"
// ============================================================================
/** @file
 *  Implementation file for functions form file LoKi/Particles39.h
 *
 *  This file is a part of LoKi project -
 *    "C++ ToolKit  for Smart and Friendly Physics Analysis"
 *
 *  The package has been designed with the kind help from
 *  Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
 *  contributions and advices from G.Raven, J.van Tilburg,
 *  A.Golutvin, P.Koppenburg have been used in the design.
 *
 *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
 *  @date 2012-01-17
 */
// ============================================================================
/*  constructor form value of \f$\left(\Delta R\right)^2\f$
 *  @param dr2 the DR2-value
 *  @param fun the functor for summation
 *  @param parts the particles
 *  @param init  the initial value
 */
// ============================================================================
LoKi::Particles::SumInR2Cone::SumInR2Cone( const double                                                dr2,
                                           const LoKi::BasicFunctors<const LHCb::Particle*>::Function& fun,
                                           const LHCb::Particle::Range& parts, const double init )
    : LoKi::BasicFunctors<const LHCb::Particle*>::Function()
    , LoKi::UniqueKeeper<LHCb::Particle>( parts.begin(), parts.end() )
    , m_fun( fun )
    , m_dr2( dr2 )
    , m_init( init ) {}
// ============================================================================
// MANDATORY: clone method ("virtual constructor")
// ============================================================================
LoKi::Particles::SumInR2Cone* LoKi::Particles::SumInR2Cone::clone() const {
  return new LoKi::Particles::SumInR2Cone( *this );
}
// ============================================================================
// MANDATORY: the only essential method
// ============================================================================
LoKi::Particles::SumInR2Cone::result_type LoKi::Particles::SumInR2Cone::
                                          operator()( LoKi::Particles::SumInR2Cone::argument p ) const {
  //
  if ( !p ) {
    Error( "LHCb::Particle* points to NULL, return 'NegativeInfinity'" )
        .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    return LoKi::Constants::NegativeInfinity; // RETURN
  }
  //
  return sum( p );
}
// ============================================================================
// calcualet tehsum
// ============================================================================
double LoKi::Particles::SumInR2Cone::sum( const LHCb::Particle* p ) const {
  using namespace LoKi::Particles;
  return empty() ? m_init
                 : LoKi::Algs::accumulate( begin(), end(), m_fun, ( DeltaR2( p ) < m_dr2 ) && !HasProtos( p ), m_init,
                                           std::plus<double>() );
}
// ===========================================================================

// ===========================================================================
// constructor from the source
// ===========================================================================
LoKi::Particles::SumInR2ConeWithSource::SumInR2ConeWithSource(
    const double dr2, const LoKi::BasicFunctors<const LHCb::Particle*>::Function& fun,
    const LoKi::BasicFunctors<const LHCb::Particle*>::Source& source, const double init )
    : LoKi::AuxFunBase( std::tie( dr2, fun, source, init ) )
    , LoKi::Particles::SumInR2Cone( dr2, fun, LHCb::Particle::Range(), init )
    , m_source( source ) {}
// ===========================================================================
// constructor from the source
// ===========================================================================
LoKi::Particles::SumInR2ConeWithSource::SumInR2ConeWithSource(
    const double dr2, const LoKi::BasicFunctors<const LHCb::Particle*>::Function& fun, const std::string& location,
    const LoKi::BasicFunctors<const LHCb::Particle*>::Predicate& cut, const double init )
    : LoKi::AuxFunBase( std::tie( dr2, fun, location, cut, init ) )
    , LoKi::Particles::SumInR2Cone( dr2, fun, LHCb::Particle::Range(), init )
    , m_source( LoKi::Particles::SourceTES( location, cut ) ) {}
// ===========================================================================
// constructor from the source
// ===========================================================================
LoKi::Particles::SumInR2ConeWithSource::SumInR2ConeWithSource(
    const double dr2, const LoKi::BasicFunctors<const LHCb::Particle*>::Function& fun,
    const std::vector<std::string>& location, const LoKi::BasicFunctors<const LHCb::Particle*>::Predicate& cut,
    const double init )
    : LoKi::AuxFunBase( std::tie( dr2, fun, location, cut, init ) )
    , LoKi::Particles::SumInR2Cone( dr2, fun, LHCb::Particle::Range(), init )
    , m_source( LoKi::Particles::SourceTES( location, cut ) ) {}
// ===========================================================================
// MANDATORY : clone method ("virtual constructor")
// ===========================================================================
LoKi::Particles::SumInR2ConeWithSource* LoKi::Particles::SumInR2ConeWithSource::clone() const {
  return new LoKi::Particles::SumInR2ConeWithSource( *this );
}
// ===========================================================================
// MANDATORY: the only essential method
// ===========================================================================
LoKi::Particles::SumInR2ConeWithSource::result_type LoKi::Particles::SumInR2ConeWithSource::
                                                    operator()( LoKi::Particles::SumInR2ConeWithSource::argument p ) const {
  //
  if ( !p ) {
    Error( "LHCb::Particle* points to NULL, return NegativeInfinity" )
        .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    return LoKi::Constants::NegativeInfinity;
  }
  // check the event
  if ( !sameEvent() ) {
    typedef LoKi::UniqueKeeper<LHCb::Particle> KEEPER;
    const KEEPER&                              keep1 = *this;
    KEEPER&                                    keep2 = const_cast<KEEPER&>( keep1 );
    //
    // clear the list of particles
    keep2.clear();
    // get the particles form the source
    LHCb::Particle::ConstVector particles = source()(); // NB
    // fill the functor with particles
    keep2.addObjects( particles.begin(), particles.end() );
    if ( empty() ) {
      Warning( "Empty list of particles is loaded!" ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    }
    // update the event:
    setEvent();
  }
  // use the functor
  return sum( p );
}
// ============================================================================
// OPTIONAL: nice printout
// ============================================================================
std::ostream& LoKi::Particles::SumInR2ConeWithSource::fillStream( std::ostream& s ) const {
  s << " SUMCONE( " << dr2() << "," << source();
  if ( 0 != init() ) { s << "," << init(); }
  return s << ") ";
}
// ===========================================================================

// ============================================================================
// The END
// ============================================================================
