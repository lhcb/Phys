/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id:$
// ============================================================================
#ifndef COMBINERS_COUNTERS_H
#define COMBINERS_COUNTERS_H 1
// ============================================================================
// Include files
// ============================================================================
// STD&STL
// ============================================================================
#include <string>
// ============================================================================
namespace {
  // ==========================================================================
  const std::string s_combination        = "#pass combcut";
  const std::string s_combination12      = "#pass comb12 cut";
  const std::string s_combination123     = "#pass comb123 cut";
  const std::string s_combination1234    = "#pass comb1234 cut";
  const std::string s_combination12345   = "#pass comb12345 cut";
  const std::string s_combination123456  = "#pass comb123456 cut";
  const std::string s_combination1234567 = "#pass comb1234567 cut";
  const std::string s_mother             = "#pass mother cut";
  // ==========================================================================
} // namespace
// ============================================================================
//                                                                      The END
// ============================================================================
#endif // COMBINERS_COUNTERS_H
