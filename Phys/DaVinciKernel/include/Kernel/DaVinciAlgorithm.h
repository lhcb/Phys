/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef KERNEL_DaVinciAlgorithm_H
#define KERNEL_DaVinciAlgorithm_H 1

// Local base class
#include "Kernel/DVCommonBase.h"

// Gaudi base class
#include "GaudiAlg/GaudiAlgorithm.h"

/** @class DaVinciAlgorithm Kernel/DaVinciAlgorithm.h
 *
 *  DaVinci Algorithm
 *
 *  @author Chris Jones
 *  @date   2012-06-22
 */
class DaVinciAlgorithm : public DVCommonBase<GaudiAlgorithm> {

public:
  /// Standard constructor
  DaVinciAlgorithm( const std::string& name, ISvcLocator* pSvcLocator )
      : DVCommonBase<GaudiAlgorithm>( name, pSvcLocator ) {}
};

#endif // KERNEL_DaVinciAlgorithm_H
