/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: $
#ifndef KERNEL_PARTICLEPREDICATES_H
#define KERNEL_PARTICLEPREDICATES_H 1

// Include files
#include "Event/Particle.h"

/** @namespace DaVinci Kernel/ParticlePredicates.h
 *
 *
 *  @author Juan Palacios
 *  @date   2011-01-10
 */
namespace DaVinci {

  /** @namespace Utils Kernel/ParticlePredicates.h
   *
   *
   *  @author Juan Palacios
   *  @date   2011-01-10
   */
  namespace Utils {

    class IParticlePredicate {
    public:
      virtual ~IParticlePredicate() {}

    public:
      virtual bool operator()( const LHCb::Particle* obj ) const = 0;
    };

    ///  Functor to check if a Particle is in the TES.
    class ParticleInTES : virtual public IParticlePredicate {
    public:
      ~ParticleInTES() {}

    public:
      typedef const LHCb::Particle* argument_type;
      typedef bool                  result_type;
      inline bool operator()( const LHCb::Particle* obj ) const override { return ( obj && obj->parent() ); }
    };

    class ParticleTrue : virtual public IParticlePredicate {
    public:
      ~ParticleTrue() {}

    public:
      typedef const LHCb::Particle* argument_type;
      typedef bool                  result_type;

      inline bool operator()( const LHCb::Particle* ) const override { return true; }
    };

    class ParticleFalse : virtual public IParticlePredicate {
    public:
      ~ParticleFalse() {}

    public:
      typedef const LHCb::Particle* argument_type;
      typedef bool                  result_type;
      inline bool                   operator()( const LHCb::Particle* ) const override { return false; }
    };

  } // namespace Utils

} // namespace DaVinci

#endif // KERNEL_PARTICLEPREDICATES_H
