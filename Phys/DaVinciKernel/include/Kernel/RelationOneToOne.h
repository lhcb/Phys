/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: RelationOneToOne.h,v 1.1 2009-09-01 16:10:14 jpalac Exp $
#ifndef KERNEL_RELATIONONETOONE_H
#define KERNEL_RELATIONONETOONE_H 1

// Include files
#include "GaudiKernel/StatusCode.h"

namespace DaVinci {
  /** @class RelationOneToOne Kernel/RelationOneToOne.h
   *
   *
   *  @author Juan PALACIOS
   *  @date   2009-09-01
   */
  template <class T>
  class RelationOneToOne {

  public:
    typedef T::From_ From_;
    typedef T::To_   To_;
    typedef T::Range Range;

  public:
    /// Standard constructor
    RelationOneToOne( const T& table ) : m_table( table ) {}

  public:
    inline StatusCode i_relate( From_ from, To_ to ) const {
      return m_table.relations( from ).empty() ? m_table.i_relate( from, to ) : StatusCode::FAILURE;
    }

    inline StatusCode i_removeFrom( From_ from ) { return m_table.i_reloveFrom( from ); }

    inline StatusCode i_relateWithOverwrite( From_ from, To_ to ) const {
      return ( m_table.relations( from ).empty()
                   ? m_table.i_relate( from, to )
                   : m_table.i_removeFrom( from ).isSuccess() ? m_table.i_relate( from, to ) : StatusCode::FAILURE );
      // if ( m_table.relations(from).empty() ) return m_table.i_relate(from, to);
      // if( m_table.i_removeFrom(from).isFailure() ) return StatusCode::FAILURE;
      // return m_table.i_relate(from, to);
    }

    inline StatusCode i_clear() { return m_table.i_clear(); }

    inline StatusCode i_relations( From_ from ) { return m_table.i_relations( from ); }

    inline StatusCode i_relations() { return m_table.i_relations(); }

    virtual ~RelationOneToOne(); ///< Destructor

  public:
    inline const T& table() const { return m_table; }

  private:
    /// Standard constructor
    RelationOneToOne();

  private:
    T& m_table;
  };

} // namespace DaVinci
#endif // KERNEL_RELATIONONETOONE_H
