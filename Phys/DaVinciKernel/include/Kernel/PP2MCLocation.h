/*****************************************************************************\
* (c) Copyright 2000-2024 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: PP2MCLocation.h,v 1.1 2006-08-18 12:00:26 jpalac Exp $
#ifndef KERNEL_PP2MCLOCATION_H
#define KERNEL_PP2MCLOCATION_H 1

#include <Event/ProtoParticle.h>
#include <string>

namespace LHCb {
  /** @namespace ProtoParticle2MCLocation PP2MCLocation.h Kernel/PP2MCLocation.h
   *
   *
   *  @author Juan PALACIOS
   *  @date   2006-08-18
   */
  namespace ProtoParticle2MCLocation {
    const std::string Charged  = "Relations/" + LHCb::ProtoParticleLocation::Charged;
    const std::string Neutrals = "Relations/" + LHCb::ProtoParticleLocation::Neutrals;
  } // namespace ProtoParticle2MCLocation
} // namespace LHCb
#endif // KERNEL_PP2MCLOCATION_H
