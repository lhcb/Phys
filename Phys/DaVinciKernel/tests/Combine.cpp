/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id$
// ============================================================================
// Include files
// ============================================================================
// GaudiKernel
// ============================================================================
#include "GaudiKernel/Kernel.h"
#include "GaudiKernel/StatusCode.h"
// ============================================================================
// Phys/Event
// ============================================================================
#include "Event/Particle.h"
#include "Event/Vertex.h"
// ============================================================================
// Local
// ============================================================================
#include "Combine.h"
// ============================================================================
// placeholder for combiner
// ============================================================================
StatusCode Combine::combine( LHCb::Particle& p, LHCb::Vertex& v ) {
  // some fake action
  v.setPosition( p.referencePoint() );
  p.setReferencePoint( v.position() );
  //
  p.setEndVertex( &v );
  p.setParticleID( LHCb::ParticleID( v.outgoingParticles().size() ) );
  //
  return StatusCode::SUCCESS;
}
// ============================================================================
StatusCode Combine::store( LHCb::Particle* p ) {
  delete p;
  return StatusCode::SUCCESS;
}
// ==========================================================================
StatusCode Combine::store( LHCb::Vertex* v ) {
  delete v;
  return StatusCode::SUCCESS;
}
// ==========================================================================

// ============================================================================
// The END
// ============================================================================
