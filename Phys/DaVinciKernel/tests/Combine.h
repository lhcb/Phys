/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id$
// ============================================================================
#ifndef TESTS_COMBINE_H
#define TESTS_COMBINE_H 1
// ============================================================================
// Include files
// ============================================================================
// GaudiKernel
// ============================================================================
#include "GaudiKernel/Kernel.h"
#include "GaudiKernel/StatusCode.h"
// ============================================================================
// Phys/Event
// ============================================================================
#include "Event/Particle.h"
#include "Event/Vertex.h"
// ============================================================================
namespace LHCb {
  class Particle;
  class Vertex;
} // namespace LHCb
// ============================================================================
namespace Combine {
  // ==========================================================================
  // placeholder for combiner
  GAUDI_API
  StatusCode combine( LHCb::Particle& p, LHCb::Vertex& v );
  // ==========================================================================
  GAUDI_API
  StatusCode store( LHCb::Particle* p );
  // ==========================================================================
  GAUDI_API
  StatusCode store( LHCb::Vertex* v );
  // ==========================================================================
} // namespace Combine
// ============================================================================
// The END
// ============================================================================
#endif // TESTS_COMBINE_H
