/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: DaVinciInterfacesDict.h,v 1.13 2010-07-21 21:19:27 tskwarni Exp $
#ifndef DICT_JETTAGGINGDICT_H
#define DICT_JETTAGGINGDICT_H 1

// Include files

/** @file JetTaggingDict.h
 *
 *
 *  @author Victor Coco
 *  @date   2013-02-06
 */
// ============================================================================
// DaVinciKernel
// ============================================================================
#include "Kernel/IJetTagTool.h"

#endif // DICT_JETTAGGING_H
