/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// ============================================================================
// Includes
// ----------------------------------------------------------------------------
// Local
// ----------------------------------------------------------------------------
#include "BDTTag.h"
// ============================================================================

// ----------------------------------------------------------------------------
/** Constructor */
BDTTag::BDTTag( const std::string& name, ISvcLocator* svc ) : DaVinciAlgorithm( name, svc ), m_tagger( 0 ) {

  declareProperty( "GhostFac", m_gfac = 1., "Scaling factor for ghost particle momenta" );
  declareProperty( "Bdt0Weights", m_bdt0Weights = "data/bdt_configs/LHCb_ANA_2014_076_BDT0.weights.xml",
                   "BDT0 TMVA weights filename and path from $JETTAGGINGROOT." );
  declareProperty( "Bdt1Weights", m_bdt1Weights = "data/bdt_configs/LHCb_ANA_2014_076_BDT1.weights.xml",
                   "BDT1 TMVA weights filename and path from $JETTAGGINGROOT." );
  declareProperty( "TmvaOptions", m_tmvaOptions = "Silent", "Options to pass to the TMVA reader." );
  declareProperty( "FitName", m_fitName = "LoKi::VertexFitter", "Name of the vertex fitter tool used to create SVRs." );
  declareProperty( "DstName", m_dstName = "LoKi::DistanceCalculator:PUBLIC",
                   "Name of the distance calculator tool used to create SVRs." );
  declareProperty( "DR", m_dr = 0.5,
                   "The maximum dR(SVR flight direction, jet momentum) "
                   "for linking two-body SVRs." );
  declareProperty( "Backwards", m_backwards = false,
                   "If true, build backwards SVRs by reversing the SVR flight "
                   "direction." );
  declareProperty( "PrtSelect", m_prtSelect = true, "If true, apply the default selection to the particles." );
  declareProperty( "NbvSelect", m_nbvSelect = true, "If true, apply the default selection to the n-body SVRs." );
  declareProperty( "NbvSort", m_nbvSort = "pt", "Sort the n-body SVRs by \"pt\", \"bdt0\", or \"bdt1\"." );
  declareProperty( "TbvLocation", m_tbvLocation = "",
                   "Optional TES location of two-body SVRs. If not set, the "
                   "two-body SVRs will be automatically built." );
  declareProperty( "PrtLocation", m_prtLocation = "Phys/StdAllNoPIDsPions/Particles",
                   "TES location of particles used to build the SVRs." );
}

// ----------------------------------------------------------------------------
/** Initialize the algorithm */
StatusCode BDTTag::initialize() {

  DaVinciAlgorithm::initialize().ignore();

  // Initialize the BDT tagger
  if ( !m_tagger ) m_tagger = tool<IJetTagTool>( "LoKi::BDTTag", this );
  if ( !m_tagger ) return Error( "Could not retrieve LoKi::BDTTag" );
  GaudiTool* tagger = dynamic_cast<GaudiTool*>( m_tagger );
  tagger->setProperty( "Bdt0Weights", m_bdt0Weights ).ignore();
  tagger->setProperty( "Bdt1Weights", m_bdt1Weights ).ignore();
  tagger->setProperty( "TmvaOptions", m_tmvaOptions ).ignore();
  tagger->setProperty( "FitName", m_fitName ).ignore();
  tagger->setProperty( "DstName", m_dstName ).ignore();
  tagger->setProperty( "DR", m_dr ).ignore();
  tagger->setProperty( "Backwards", m_backwards ).ignore();
  tagger->setProperty( "PrtSelect", m_prtSelect ).ignore();
  tagger->setProperty( "NbvSelect", m_nbvSelect ).ignore();
  tagger->setProperty( "NbvSort", m_nbvSort ).ignore();
  tagger->setProperty( "FitName", m_fitName ).ignore();
  tagger->setProperty( "TbvLocation", m_tbvLocation ).ignore();
  tagger->setProperty( "PrtLocation", m_prtLocation ).ignore();

  return StatusCode::SUCCESS;
}

// ----------------------------------------------------------------------------
/** Excecute the algorithm */
StatusCode BDTTag::execute() {

  // Grab the input jets
  m_inputs = particles();

  // Container for the secondary vertices
  LHCb::Particles* svs = new LHCb::Particles();
  put( svs, outputLocation() + "/SVs" );

  // Loop over the jets
  for ( const Prt* inJet : m_inputs ) {

    // Tag the jet
    m_tags.clear();
    if ( !( m_tagger->calculateJetProperty( inJet, m_tags ) ) && ( m_tags.count( "Tag" ) == 0 ) ) continue;

    // Create the output jet
    Prt* outJet = inJet->clone();

    // Loop over the tags
    for ( int iTag = 0; iTag < m_tags["Tag"]; iTag++ ) {

      // Create the output SV
      Prt* sv = new Prt();
      if ( !setInfo( sv, iTag ) ) continue;
      sv->setMomentum( Gaudi::LorentzVector( sv->info( 2, 0 ) * m_gfac, sv->info( 3, 0 ) * m_gfac,
                                             sv->info( 4, 0 ) * m_gfac, sv->info( 5, 0 ) * m_gfac ) );
      svs->insert( sv );

      // Add SV to the jet's daughters
      outJet->addToDaughters( sv );
    }

    // Add output jet to the TES
    markParticle( outJet );
  }

  setFilterPassed( true );
  return StatusCode::SUCCESS;
}

// ----------------------------------------------------------------------------
/** Finalize */
StatusCode BDTTag::finalize() { return StatusCode::SUCCESS; }

// ----------------------------------------------------------------------------
/** Store info from the BDT tagger in a particle */
bool BDTTag::setInfo( Prt* prt, const int& index ) {

  // Check for valid index
  int nTags = m_tags["Tag"];
  if ( ( index < 0 ) || ( index >= nTags ) ) return false;

  // Set the BDT info
  std::stringstream pre;
  pre << "Tag" << index << "_";
  prt->addInfo( 0, m_tags[pre.str() + "bdt0"] );
  prt->addInfo( 1, m_tags[pre.str() + "bdt1"] );
  prt->addInfo( 2, m_tags[pre.str() + "px"] );
  prt->addInfo( 3, m_tags[pre.str() + "py"] );
  prt->addInfo( 4, m_tags[pre.str() + "pz"] );
  prt->addInfo( 5, m_tags[pre.str() + "e"] );
  prt->addInfo( 6, m_tags[pre.str() + "m"] );
  prt->addInfo( 7, m_tags[pre.str() + "pt"] );
  prt->addInfo( 8, m_tags[pre.str() + "fdrMin"] );
  prt->addInfo( 9, m_tags[pre.str() + "ptSvrJet"] );
  prt->addInfo( 10, m_tags[pre.str() + "nTrk"] );
  prt->addInfo( 11, m_tags[pre.str() + "nTrkJet"] );
  prt->addInfo( 12, m_tags[pre.str() + "drSvrJet"] );
  prt->addInfo( 13, m_tags[pre.str() + "absQSum"] );
  prt->addInfo( 14, m_tags[pre.str() + "mCor"] );
  prt->addInfo( 15, m_tags[pre.str() + "fdChi2"] );
  prt->addInfo( 16, m_tags[pre.str() + "ipChi2Sum"] );
  prt->addInfo( 17, m_tags[pre.str() + "pass"] );
  prt->addInfo( 18, m_tags[pre.str() + "tau"] );
  prt->addInfo( 19, m_tags[pre.str() + "z"] );
  prt->addInfo( 20, m_tags[pre.str() + "backwards"] );

  return true;
}

// ============================================================================
// Declare the algorithm factory
DECLARE_COMPONENT( BDTTag )
// ============================================================================