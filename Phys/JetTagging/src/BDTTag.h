/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#ifndef BDTTAG_H
#define BDTTAG_H 1

// ============================================================================
// Includes
// ----------------------------------------------------------------------------
// Gaudi
// ----------------------------------------------------------------------------
#include "GaudiAlg/GaudiTool.h"
// ----------------------------------------------------------------------------
// DaVinci
// ----------------------------------------------------------------------------
#include "Kernel/DaVinciAlgorithm.h"
#include "Kernel/IJetTagTool.h"
// ----------------------------------------------------------------------------
// Event
// ----------------------------------------------------------------------------
#include "Event/Particle.h"
// ============================================================================

/**
 *  Jet tagging via the secondary vertex (SVR) method of LHCb-ANA-2014-074.
 *
 *  This class acts as a DaVinci algorithm wrapper for LoKi::BDTTag. The input
 *  jets are each run through Loki::BDTTag, and the output secondary vertices
 *  are included amongst the daughter particles of the output jet, with their
 *  momentum scalable by some ghost particle factor. When looping over daughter
 *  particles, one can check if the particle is a BDT tagged SV by using the
 *  hasInfo method. The BDT output information can then be accessed by the info
 *  method, with the appropriate keys found in the bdtInfo enumerator.
 *
 * @class   BDTTag
 * @file    BDTTag.h
 * @author  Jake Pfaller, Philip Ilten, Mike Williams
 * @date    11-11-2024
 */
class BDTTag : public DaVinciAlgorithm {

public:
  // --------------------------------------------------------------------------
  /** Constructor
   */
  BDTTag( const std::string& name, ISvcLocator* svc );

  // --------------------------------------------------------------------------
  /** Destructor
   */
  ~BDTTag(){};

  // --------------------------------------------------------------------------
  /** Initialize the algorithm
   *
   *  @return StatusCode
   */
  StatusCode initialize() override;

  // --------------------------------------------------------------------------
  /** Excecute the algorithm
   *
   *  Runs the BDT tagging algorithm of LHCb-ANA-2014-074 on a container of
   *  jets. Any jets with a tagged SV are output into a new container, with
   *  the SVs clustered into the jet as ghost particles.
   *
   *  @return StatusCode
   */
  StatusCode execute() override;

  // --------------------------------------------------------------------------
  /** Finalize
   *
   *  @return StatusCode
   */
  StatusCode finalize() override;

  // Enumerator for BDT info
  enum bdtInfo {
    bdt0,
    bdt1,
    px,
    py,
    pz,
    e,
    m,
    pt,
    fdrMin,
    ptSvrJet,
    nTrk,
    nTrkJet,
    drSvrJet,
    absQSum,
    mCor,
    fdChi2,
    ipChi2Sum,
    pass,
    tau,
    z,
    backwards
  };

protected:
  // Useful types -------------------------------------------------------------

  typedef LHCb::Particle Prt;

  // --------------------------------------------------------------------------
  /** Store info from the BDT tagger in a particle
   *
   *  The information from LoKi::BDTTag is stored in the map m_tags, and this
   *  method then takes that info and stores it in the given particle. The
   *  given index corresponds to index of a tagged SV, sorted by pT. Returns
   *  false if requested index does not exist.
   */
  bool setInfo( Prt* prt, const int& index );

  // Members ------------------------------------------------------------------

  // Input jets
  Prt::Range m_inputs;
  // Tagged SV information
  std::map<std::string, double> m_tags;
  // BDT Tagging tool
  IJetTagTool* m_tagger;
  // Ghost particle momentum factor
  double m_gfac;
  /**
   * BDT0 TMVA weights filename and path from $JETTAGGINGROOT.
   *
   * Configured via "Bdt0Weights" with a default of
   * "data/bdt_configs/LHCb_ANA_2014_076_BDT0.weights.xml". Changing
   * this property takes effect only after initialization. Warning,
   * changing this invalidates the results of LHCb-ANA-2014-074.
   */
  std::string m_bdt0Weights;
  /**
   * BDT1 TMVA weights filename and path from $JETTAGGINGROOT.
   *
   * Configured via "Bdt1Weights" with a default of
   * "data/bdt_configs/LHCb_ANA_2014_076_BDT1.weights.xml". Changing
   * this property takes effect only after initialization. Warning,
   * changing this invalidates the results of LHCb-ANA-2014-074.
   */
  std::string m_bdt1Weights;
  /**
   * Options to pass to the TMVA reader.
   *
   * Configured via "TmvaOptions" with a default of
   * "Silent". Changing this property takes effect only after
   * initialization. To switch to verbose TMVA output change this
   * option to "V".
   */
  std::string m_tmvaOptions;
  /**
   * Name of the vertex fitter tool used to create SVRs.
   *
   * Configured via "FitName" with a default of
   * "OfflineVertexFitter". Changing this property takes effect only
   * after initialization. Warning, changing this invalidates the
   * results of LHCb-ANA-2014-074.
   */
  std::string m_fitName;
  /**
   * Name of the distance calculator tool used to create SVRs.
   *
   * Configured via "DstName" with a default of
   * "LoKi::DistanceCalculator:PUBLIC". Changing this property takes
   * effect only after initialization. Warning, changing this
   * invalidates the results of LHCb-ANA-2014-074.
   */
  std::string m_dstName;
  /**
   * The maximum dR(SVR flight direction, jet momentum) for linking
   * two-body SVRs.
   *
   * Configured via "DR" with a default of 0.5. Only two-body SVRs
   * with dR(SVR flight direction, jet momentum) < m_dr are linked
   * to build SVR candidates for a jet. Changing this property takes
   * effect only after initialization. Warning, changing this
   * invalidates the results of LHCb-ANA-2014-074.
   */
  double m_dr;
  /**
   * If true, build backwards n-body SVRs by reversing the SVR
   * flight directions.
   *
   * Configured via "Backwards" with a default of false. Backwards
   * SVRs are used in LHCb-ANA-2014-074 to cross-check light jet
   * backgrounds. If this flag is set to true the n-body SVRs are
   * built with two-body SVRs satisfying dR(-SVR flight direction,
   * jet momentum) < m_dr. Additionally, the flight direction of the
   * n-body vertex is reversed when calculating dR with the
   * jet. This property can be set at any time and will take
   * immediate effect when calculateJetProperty is called.
   */
  bool m_backwards;
  /**
   * If true, apply the default selection to the particles.
   *
   * Configured via "PrtSelect" with a default of true. The particle
   * selection applied is given by Table 3 in LHCb-ANA-2014-074. One
   * can apply a different selection by filtering particles,
   * providing the filtered location via m_prtLocation, and setting
   * m_prtSelect to false. Changing this property takes effect only
   * after the next event is read if calculateJetProperty has
   * already been called. Warning, changing the particle selection
   * invalidates the results of LHCb-ANA-2014-074.
   */
  bool m_prtSelect;
  /**
   * If true, apply the default selection to the n-body SVRs.
   *
   * Configured via "NbvSelect" with a default of true. The n-body
   * vertex selection is given by Table 5 in LHCb-ANA-2014-074. This
   * property can be set at any time and will take immediate effect
   * when calculateJetProperty is called. Warning, changing the
   * n-body selection invalidates the results of LHCb-ANA-2014-074.
   */
  bool m_nbvSelect;
  /**
   * Sort the n-body SVRs by "pt", "bdt0", or "bdt1".
   *
   * Configured via "NbvSort" with a default of "pt". If the value
   * of m_nbvSort is not set to one of the three recognized options,
   * the n-body SVRs will not be sorted. This sorting only affects
   * the order in which the SVR information is written out via
   * calculateJetProperty. This property can be set at any time and
   * will take immediate effect when calculateJetProperty is called.
   */
  std::string m_nbvSort;
  /**
   * TES location of particles used to build the SVRs.
   *
   * Configured via "PrtLocation" with a default of
   * "Phys/StdAllNoPIDsPions/Particles". Changing this property
   * takes effect only after the next event is read if
   * calculateJetProperty has already been called. Warning, changing
   * the particle location invalidates the results of
   * LHCb-ANA-2014-074.
   */
  std::string m_prtLocation;
  /**
   * Optional TES location of two-body SVRs. If not set, the
   * two-body SVRs will be automatically built.
   *
   * Configured via "TbvLocation" with a default of "". The two-body
   * SVRs used to build the final n-body SVRs are created from the
   * particles provided by m_prtLocation. However, if a valid TES
   * location is provided by m_tbvLocation, all two-body
   * combinations from this location are used instead. Changing this
   * property takes effect only after the next event is read if
   * calculateJetProperty has already been called. Warning, changing
   * the SVR location invalidates the results of LHCb-ANA-2014-074.
   */
  std::string m_tbvLocation;

private:
  // Default constructor is disabled
  BDTTag() = delete;

  // Copy constructor is disabled
  BDTTag( BDTTag& ) = delete;

  // Assignment operator is disabled
  BDTTag& operator=( const BDTTag& ) = delete;
};

#endif // DAVINCIBDTTAG_H