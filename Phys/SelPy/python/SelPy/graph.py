#!/usr/bin/env python
# -*- coding: utf-8 -*-
###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# =============================================================================
"""Module for selection vizualisation tools

>>> selection = ....

Make a graph fro the given selection:
>>> graph ( selection, format = 'png' , filename = 'graph.png' )

Visualize given selection:
>>> view  ( selection )

"""

from __future__ import print_function
from builtins import object

__all__ = (
    ## 'graphEdges'     , ## obsolete
    ## 'GraphEdges'     , ## obsolete
    ## 'SelectionGraph' , ## obsolete
    ## 'dot'            , ## obsolete
    ## '_dot'           , ## obsolete
    'graph',
    'view')

__author__ = 'Juan Palacios palacios@physik.uzh.ch'
# =============================================================================

import pydot


class GraphEdges(object):
    """ Return a list of all the edges of the selection tree for a given
    selection object.
    """

    def __init__(self):
        self._edges = []

    def __call__(self, selection):
        self._edges = []
        self._getEdges(selection)
        return self._edges

    def _getEdges(self, selection):

        reqsels = selection.requiredSelections()
        if not reqsels and hasattr(selection, "_sel"):
            sel = selection._sel
            if hasattr(sel, 'requiredSelections'):
                reqsels = sel.requiredSelections()

        for sel in reqsels:
            if (sel.name(), selection.name()) not in self._edges:
                self._edges.append((sel.name(), selection.name()))
            self._getEdges(sel)


graphEdges = GraphEdges()


def _dot(selection):
    """
    Return a pydot.Dot object made from the edges of selection's graph.
    """
    return pydot.graph_from_edges(graphEdges(selection), directed=True)


# =============================================================================
# New stuff
# =============================================================================


def mainSel(name, label):
    return pydot.Node(
        name,
        label=label,
        shape='ellipse',
        fillcolor='red',
        color='red',
        style='filled')


def reguSel(name, label):
    return pydot.Node(
        name,
        label=label,
        shape='ellipse',
        fillcolor='yellow',
        color='yellow',
        style='filled')


def filterSel(name, label):
    return pydot.Node(
        name,
        label=label,
        shape='rectangle',
        fillcolor='yellow',
        color='yellow',
        style='filled')


def combineSel(name, label):
    return pydot.Node(
        name,
        label=label,
        shape='octagon',
        fillcolor='yellow',
        color='red',
        style='filled')


def tesSel(name, label):
    return pydot.Node(
        name,
        label=label,
        shape='folder',
        fillcolor='green',
        color='green',
        style='filled')


def mergedSel(name, label):
    return pydot.Node(
        name,
        label=label,
        shape='note',
        fillcolor='yellow',
        color='yellow',
        style='filled')


def tupleSel(name, label):
    return pydot.Node(
        name,
        label=label,
        shape='component',
        fillcolor='yellow',
        color='yellow',
        style='filled')


def payloadSel(name, label):
    return pydot.Node(
        name,
        label=label,
        shape='component',
        fillcolor='magenta',
        color='magenta',
        style='filled')


def eventSel(name, label):
    return pydot.Node(
        name,
        label=label,
        shape='note',
        fillcolor='green',
        color='green',
        style='filled')


def passSel(name, label):
    return pydot.Node(
        name,
        label=label,
        shape='component',
        fillcolor='cyan',
        color='cyan',
        style='filled')


def name_node(name):
    return name.replace(":", "_")


def nodeSel(sel):

    name = name_node(sel.name())
    label = sel.name()
    if 0 <= label.find(' '): label = '"%s"' % label
    if 0 <= label.find(':'): label = '"%s"' % label

    try:

        from PhysSelPython.Wrappers import AutomaticData, MergedSelection, PayloadSelection, TupleSelection
        if isinstance(sel, AutomaticData): return tesSel(name, label)
        elif isinstance(sel, MergedSelection): return mergedSel(name, label)
        elif isinstance(sel, TupleSelection): return tupleSel(name, label)
        elif isinstance(sel, PayloadSelection): return payloadSel(name, label)

    except:
        pass

    try:

        from SelPy.selection import EventSelection as EVTSEL
        from SelPy.selection import PassThroughSelection as PASSEL
        if isinstance(sel, EVTSEL): return eventSel(name, label)
        elif isinstance(sel, PASSEL): return passSel(name, label)
    except:
        raise
        pass

    try:

        alg = sel.algorithm()
        from Configurables import (
            FilterDesktop, CombineParticles, DaVinci__N3BodyDecays,
            DaVinci__N4BodyDecays, DaVinci__N5BodyDecays,
            DaVinci__N6BodyDecays, DaVinci__N7BodyDecays,
            DaVinci__N8BodyDecays)

        combiners = (CombineParticles, DaVinci__N3BodyDecays,
                     DaVinci__N4BodyDecays, DaVinci__N5BodyDecays,
                     DaVinci__N6BodyDecays, DaVinci__N7BodyDecays,
                     DaVinci__N8BodyDecays)

        if isinstance(alg, FilterDesktop): return filterSel(name, label)
        elif isinstance(alg, combiners): return combineSel(name, label)

    except:

        pass

    return reguSel(name, label)


class SelectionGraph(object):
    def __init__(self, selections):

        self._nodes = set()
        self._edges = set()
        self._graph = pydot.Dot('graphname', graph_type='digraph')

        if not hasattr(selections, '__iter__'): selections = [selections]

        _main = True
        for sel in selections:

            name = name_node(sel.name())
            if name in self._nodes: continue

            self._nodes.add(name)
            if _main: node = mainSel(name, sel.name())
            else: node = nodeSel(sel)
            _main = False

            self._graph.add_node(node)

            self._add(sel)

    ## add selection to the graph
    def add_sel(self, selection):
        """
        Add the selection to the grath
        """
        n = name_node(selection.name())
        if not n in self._nodes:
            self._graph.add_node(nodeSel(selection))
            self._nodes.add(n)
        self._add(selection)

    def _add_edge(self, node1, node2, **attrs):
        edge = node1, node2
        if not edge in self._edges:
            self._graph.add_edge(pydot.Edge(*edge, **attrs))
            self._edges.add(edge)

    def _add(self, sel):

        reqsels = sel.requiredSelections()
        n1 = name_node(sel.name())
        for rs in reqsels:
            n2 = name_node(rs.name())
            if not n2 in self._nodes:
                self._graph.add_node(nodeSel(rs))
                self._nodes.add(n2)
            self._add_edge(n2, n1)
            self._add(rs)  ## recursion

        if reqsels: return

        ## merged selection?
        if hasattr(sel, "_sel"):
            sel_ = sel._sel
            if hasattr(sel_, 'requiredSelections'):
                reqsels = sel_.requiredSelections()

        for rs in reqsels:
            n2 = name_node(rs.name())
            if not n2 in self._nodes:
                self._graph.add_node(nodeSel(rs))
                self._nodes.add(n2)
            self._add_edge(n2, n1, style='dotted')
            self._add(rs)  ## recursion


def dot(selections):
    """
    Return a pydot.Dot object made from the edges of selection's graph.
    """
    g = SelectionGraph(selections)
    return g._graph


# =============================================================================
## @class generate the name for temporary file
#  delete it at the end
class _TmpFile_(object):
    def __init__(self, suffix='png'):
        self.suffix = '.' + suffix

    def __enter__(self):
        import tempfile
        self.name = tempfile.mktemp(suffix=self.suffix)
        return self.name

    def __exit__(self, *_):
        import os
        try:
            if os.file.exists(self.name):
                os.remove(self.name)
        except:
            pass


## temporary replacement of write function from pydot
def _write_(d, filename='graph', format=None, prog='dot'):
    """ Temporary replacement of write function from pydot
    """
    if format.lower() in (None, '', 'raw', 'dot'):
        r = d.write(filename, prog=prog)
        return filename if r else None  ## RETURN

    with _TmpFile_(suffix='dot') as ftmp:

        r = d.write(ftmp, prog=prog)
        if not r: return None

        import subprocess, os
        args = [prog, '-T%s' % format, '-o%s' % filename, ftmp]
        p = subprocess.Popen(args, shell=False)
        o, e = p.communicate()
        if not e and 0 == p.returncode and os.path.exists(filename):
            return filename
        else:
            print('Pydot error: %s' % e)

    return None


# =============================================================================
## make graph for the given selection
#  Possible programs:
#  - dot   : ``hierarchical'' or layered drawings of directed graphs.
#            This is the default tool to use if edges have directionality.
#  - neato : ``spring model'' layouts.  This is the default tool to use if the graph
#            is not too large (about 100 nodes) and you don't know anything else about it.
#            Neato attempts to minimize a global energy function,
#            which is equivalent to statistical multi-dimensional scaling.
#  - fdp   : ``spring model'' layouts similar to those of neato, but does this by
#            reducing forces rather than working with energy.
#  - sfdp  : multiscale version of fdp for the layout of large graphs.
#  - twopi : radial layouts, after Graham Wills 97. Nodes are placed on concentric
#            circles depending their distance from a given root node.
#  - circo : circular layout, after Six and Tollis 99, Kauffman and Wiese 02.
#            This is suitable for certain diagrams of multiple cyclic structures,
#            such as certain telecommunications networks.
#
# In practice only ``dot'' produces reasonable plots (for my taste)
def graph(selections, format='dot', filename=None, prog='dot'):
    """Make graph for the given selection

    >>> selection = ...
    >>> graph ( selections , format = 'jpeg' )

    Possible programs:
    - dot   : ``hierarchical'' or layered drawings of directed graphs.
              This is the default tool to use if edges have directionality.
    - neato : ``spring model'' layouts.  This is the default tool to use if the graph
              is not too large (about 100 nodes) and you don't know anything else about it.
              Neato attempts to minimize a global energy function,
              which is equivalent to statistical multi-dimensional scaling.
    - fdp   : ``spring model'' layouts similar to those of neato, but does this by
              reducing forces rather than working with energy.
    - sfdp  : multiscale version of fdp for the layout of large graphs.
    - twopi : radial layouts, after Graham Wills 97. Nodes are placed on concentric
              circles depending their distance from a given root node.
    - circo : circular layout, after Six and Tollis 99, Kauffman and Wiese 02.
              This is suitable for certain diagrams of multiple cyclic structures,
              such as certain telecommunications networks.

    In practice only ``dot'' produces reasonable plots (for my taste)
    """
    dt = dot(selections)
    if not filename:
        if hasattr(selections, 'name'): selname = selections.name()
        else: selname = '_'.join([i.name() for i in selections])
        filename = selname
    #
    ## return filename if dt.write ( filename , format = format , prog = prog ) else None
    return _write_(dt, filename, format, prog)


# =============================================================================
# prepare the graph and view it
#  Possible programs:
#  - dot   : ``hierarchical'' or layered drawings of directed graphs.
#            This is the default tool to use if edges have directionality.
#  - neato : ``spring model'' layouts.  This is the default tool to use if the graph
#            is not too large (about 100 nodes) and you don't know anything else about it.
#            Neato attempts to minimize a global energy function,
#            which is equivalent to statistical multi-dimensional scaling.
#  - fdp   : ``spring model'' layouts similar to those of neato, but does this by
#            reducing forces rather than working with energy.
#  - sfdp  : multiscale version of fdp for the layout of large graphs.
#  - twopi : radial layouts, after Graham Wills 97. Nodes are placed on concentric
#            circles depending their distance from a given root node.
#  - circo : circular layout, after Six and Tollis 99, Kauffman and Wiese 02.
#            This is suitable for certain diagrams of multiple cyclic structures,
#            such as certain telecommunications networks.
#
# In practice only ``dot'' produces reasonable plots (for my taste)
def view(selection, command=None, format='png', prog='dot'):
    """ View the graph for a Selection type object.
    Example:
    sel = Selection ( ... )
    view(sel)
    view(sel, command = 'open -a Preview')

    Possible programs:
    - dot   : ``hierarchical'' or layered drawings of directed graphs.
              This is the default tool to use if edges have directionality.
    - neato : ``spring model'' layouts.  This is the default tool to use if the graph
              is not too large (about 100 nodes) and you don't know anything else about it.
              Neato attempts to minimize a global energy function,
              which is equivalent to statistical multi-dimensional scaling.
    - fdp   : ``spring model'' layouts similar to those of neato, but does this by
              reducing forces rather than working with energy.
    - sfdp  : multiscale version of fdp for the layout of large graphs.
    - twopi : radial layouts, after Graham Wills 97. Nodes are placed on concentric
              circles depending their distance from a given root node.
    - circo : circular layout, after Six and Tollis 99, Kauffman and Wiese 02.
              This is suitable for certain diagrams of multiple cyclic structures,
              such as certain telecommunications networks.

    In practice only ``dot'' produces reasonable plots (for my taste)

    """

    with _TmpFile_(format) as ofile:

        if not graph(selection, format, filename=ofile, prog=prog):
            print('Error producing dot-graph for %s ' % selection.name())
            return

        if not command:
            import distutils.spawn as ds
            for i in ('eog', 'display', 'gnome-photos', 'gimp', 'gthumb',
                      'google-chrome'):
                command = ds.find_executable(i)
                if command: break

        if not command:
            print('No valid command is found!')
            return

        import subprocess
        try:
            subprocess.check_call("%s %s " % (command, ofile), shell=True)
        except subprocess.CalledProcessError:
            pass


# =============================================================================
# The END
# =============================================================================
