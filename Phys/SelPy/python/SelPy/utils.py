###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
General helpers for maniplating selections and lists.
"""
from builtins import filter
from builtins import range
from past.builtins import basestring
from builtins import object
from functools import reduce
__author__ = "Juan PALACIOS palacios@physik.uzh.ch"

__all__ = ('flatSelectionList', 'treeSelectionList', 'flattenList',
           'isIterable', 'removeDuplicates', 'update_dict_overlap',
           'ClonableObject', 'NamedObject', 'UniquelyNamedObject', 'NameError',
           'NonEmptyInputLocations', 'IncompatibleInputLocations',
           'IncompatibleOutputLocation')


class ClonableObject(object):
    """
    Base class for objects that are to be cloned with a selection of their
    constructor argument values. Clonable classes must inherit from this.
    Example:

    class Dummy(ClonableObject) :
       def __init__(self, a, b, c) :
           ClonableObject.__init__(self, locals())
           self.a = a
           self.b = b
           self.c = c

    d0 = Dummy(1,2,3)
    d1 = d0.clone(b=999)
    print d0.a, d0.b, d0.c
    print d1.a, d1.b, d1.c
    """

    def __init__(self, kwargs):
        self._ctor_dict = dict(kwargs)
        if 'self' in self._ctor_dict:
            del self._ctor_dict['self']

    def clone(self, **kwargs):
        new_dict = update_dict_overlap(self._ctor_dict, kwargs)
        return self.__class__(**new_dict)


class NamedObject(object):
    def __init__(self, name):
        self._name = name

    def name(self):
        return self._name


class UniquelyNamedObject(NamedObject):
    __used_names = []

    def __init__(self, name):
        if name in UniquelyNamedObject.__used_names:
            raise NameError('Name ' + name +
                            ' has already been used. Pick a new one.')
        NamedObject.__init__(self, name)
        UniquelyNamedObject.__used_names.append(name)


def decaytool():
    '''Get the decay tool used for parsing decay descriptors.'''
    import GaudiPython.Bindings

    toolSvc = GaudiPython.Bindings.AppMgr().toolSvc()
    # This is taken from Phys/Bender/python/Bender/Fixes_Gaudi.py
    dectool = GaudiPython.Bindings.Helper.tool(toolSvc._its, 'LoKi::Decay',
                                               'decaytool', None, True)
    dectool = GaudiPython.Bindings.InterfaceCast('Decays::IDecay')(dectool)
    return dectool


def decayTree(desc):
    '''Use the decay tool to parse a decay descriptor for the Tree object.'''
    dectool = decaytool()
    tree = dectool.tree(desc.replace(']cc', ']CC'))
    return tree


def flattenDescriptor(desc):
    '''Use the decay tool to parse a decay descriptor.'''
    tree = decayTree(desc)
    treestr = tree.toString()
    return treestr


def splitDescriptor(desc):
    '''Split a decay descriptor into its head and decay products.'''
    splitdesc = desc.split('->')
    lhs = splitdesc[0]
    rhs = '->'.join(splitdesc[1:])
    lhs = lhs.strip()
    rhs = rhs.strip()
    if lhs.startswith('('):
        lhs = lhs[1:].strip()
        rhs = rhs[:-1].strip()
    return lhs, rhs


class SelectionBase(object):
    def __init__(self, algorithm, outputLocation, requiredSelections):
        self._algorithm = algorithm
        self._outputLocation = outputLocation
        self._requiredSelections = list(requiredSelections)

    def algorithm(self):
        return self._algorithm

    def outputLocation(self):
        return self._outputLocation

    def requiredSelections(self):
        return list(self._requiredSelections)

    def _algDecayDescriptors(self):
        '''Get the decay descriptors of the algorithm, without any parsing.'''
        inputdescs = []
        if hasattr(self._algorithm, 'DecayDescriptor'):
            inputdescs += [self._algorithm.DecayDescriptor]
        if hasattr(self._algorithm, 'DecayDescriptors'):
            inputdescs += self._algorithm.DecayDescriptors
        return inputdescs

    def algDecayDescriptors(self):
        '''Get the decay descriptors of the algorithm.'''
        inputdescs = self._algDecayDescriptors()
        if not inputdescs:
            return []
        descs = []
        for desc in [_f for _f in inputdescs if _f]:
            desc = flattenDescriptor(desc).strip()
            if '||' in desc:
                desc = desc.strip('()')
            descs += [_f for _f in (d.strip() for d in desc.split('||')) if _f]
        return descs

    def inputDecayDescriptors(self):
        '''Get the decay descriptors of the inputs, sorted by the head of their decays.'''
        descs = {}
        for sel in self.requiredSelections():
            seldescs = sel.decayDescriptors()
            for seldesc in seldescs:
                head, rhs = splitDescriptor(seldesc)
                if head not in descs:
                    descs[head] = []
                descs[head].append(seldesc)
        return descs

    def decayDescriptors(self):
        '''Get the full decay descriptors, piecing together the top level and input descriptors.
        Works for the vast majority of cases, but by no means guaranteed to work. One case for
        which it will definitely fail is if you have two identical particles in the tree with
        different final states (it'll give them the same final state).'''
        inputdescs = self.inputDecayDescriptors()
        algdescs = self.algDecayDescriptors()
        if not algdescs:
            return reduce(lambda x, y: x + y, list(inputdescs.values()), [])
        if not inputdescs:
            return algdescs

        # Get every combination of the decay modes for the inputs.
        inputdescs = list(inputdescs.values())
        indices = [0] * len(inputdescs)
        inputcombinations = []
        while indices[-1] != len(inputdescs[-1]):
            comb = []
            for i, descs in enumerate(inputdescs):
                comb.append(descs[indices[i]])
            inputcombinations.append(comb)
            indices[0] += 1
            for i in range(len(inputdescs) - 1):
                if indices[i] == len(inputdescs[i]):
                    indices[i] = 0
                    indices[i + 1] += 1

        # Loop over the top level descriptors and all combinations of the
        # decay modes of the inputs and substitute them in.
        # This method won't work if you have two identical particles in the
        # tree with different final states ...
        returndescs = set()
        for algdesc in algdescs:
            for incomb in inputcombinations:
                alglhs, algrhs = splitDescriptor(algdesc)
                for indesc in incomb:
                    inlhs, inrhs = splitDescriptor(indesc)
                    algrhs = algrhs.replace(inlhs, indesc)
                returndescs.add('( ' + alglhs + ' -> ' + algrhs + ' )')
        return list(returndescs)

    def topDecayDescriptors(self):
        '''Get the top level decay descriptors, without recursion through the selection sequence.'''
        descs = self.algDecayDescriptors()
        if not descs:
            for sel in self.requiredSelections():
                descs += sel.topDecayDescriptors()
        return descs

    def decayHeads(self):
        '''Get the heads of the decay trees in the descriptors.'''
        descs = self.topDecayDescriptors()
        heads = set()
        for desc in descs:
            heads.add(splitDescriptor(desc)[0])
        return list(heads)


class CloneCallable(object):
    """
    Simple wrapper class to make an object's call method equivalent to
    its clone method
    """

    def __init__(self, clonable):
        self._clonable = clonable

    def __call__(self, *args, **kwargs):
        return self._clonable.clone(*args, **kwargs)


def treeSelectionList(selection):
    """
    Return a nested list with all the selections needed by a selection, including itself.
    """
    _selList = [
        treeSelectionList(sel) for sel in selection.requiredSelections()
    ] + [selection]
    return _selList


def isIterable(obj):
    '''
    Test if an object is iterable but not a string type.
    '''
    from collections.abc import Iterable
    return isinstance(obj, Iterable) and not isinstance(obj, str)


def flattenList(tree):
    """flattenList(tree) -> flat list
    Return a flat list containing all the non-iterable elements retrieved tree and tree's sub-trees.
    """
    flatList = []
    for member in tree:
        if isIterable(member):
            flatList.extend(flattenList(member))
        else:
            flatList.append(member)
    return flatList


def removeDuplicates(obj_list):
    """
    Remove all but the first instance of an object from the a list.
    Remove all NoneType instances.
    """
    clean_list = []
    for obj in obj_list:
        if obj not in clean_list and obj != None:
            clean_list.append(obj)
    return clean_list


def flatSelectionList(selection):
    """
    Return a flat selection list containing all the selections required
    by selection, in the correct order of execution. Duplicates are removed,
    keeping the first one.
    """
    return removeDuplicates(flattenList(treeSelectionList(selection)))


def update_dict_overlap(dict0, dict1):
    """
    Replace entries from dict0 with those from dict1 that have
    keys present in dict0.
    """
    result = dict(dict0)
    for key in dict0:
        if key in dict1:
            result[key] = dict1[key]
    return result


def compatibleSequences(seq0, seq1):
    if len(seq0) != len(seq1):
        return False
    for x in seq0:
        if x not in seq1:
            return False
    return True


def connectToRequiredSelections(selection, inputSetter):
    """
    Make selection get input data from its requiredSelections via an inputSetter method (str).
    """
    _outputLocations = [
        sel.outputLocation() for sel in selection.requiredSelections()
    ]
    _outputLocations = [s for s in _outputLocations if s != '']
    configurable = selection.algorithm()

    if inputSetter and hasattr(configurable, inputSetter):
        _inputLocations = getattr(configurable, inputSetter)
        if len(_inputLocations) != 0:
            if not compatibleSequences(_outputLocations, _inputLocations):
                raise IncompatibleInputLocations('InputLocations of input algorithm incompatible with RequiredSelections!'\
                                                 '\nInputLocations: '+str(_inputLocations)+\
                                                 '\nRequiredSelections: '+str(_outputLocations))

    if inputSetter:
        configurable.__setattr__(inputSetter, list(_outputLocations))


def makeOutputLocation(name, branch, leaf):
    """
    Create an output location path in the form branch/name/leaf.
    Check against double '/' or ending with '/'.
    """
    _outputLocation = name
    if branch != '':
        _outputLocation = branch + '/' + _outputLocation
    if leaf != '':
        _outputLocation = _outputLocation + '/' + leaf
        _outputLocation.replace('//', '/')
    if _outputLocation.endswith('/'):
        _outputLocation = _outputLocation[:_outputLocation.rfind('/')]
    return _outputLocation


def setOutputLocation(selection, setter):
    configurable = selection.algorithm()
    selOutput = selection.outputLocation()
    if setter and hasattr(configurable, setter):
        confOutput = getattr(configurable, setter)
        if confOutput != selOutput:
            raise IncompatibleOutputLocation('Output of input algorithm incompatible with Selection!'\
                                             '\nOutput: '+confOutput+\
                                             '\nSelection.outputLocation(): '+selOutput)
    if setter:
        configurable.__setattr__(setter, selOutput)


class NameError(Exception):
    pass


class NonEmptyInputLocations(Exception):
    pass


class IncompatibleInputLocations(Exception):
    pass


class IncompatibleOutputLocation(Exception):
    pass
