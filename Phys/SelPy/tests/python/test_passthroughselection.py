#!/usr/bin/env python
###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
Test suite for selection module.
'''

__author__ = "Juan PALACIOS juan.palacios@nikhef.nl"

from pytest import raises
from SelPy.configurabloids import MockConfGenerator
from SelPy.selection import PassThroughSelection, AutomaticData, NameError


def test_PassThroughSelection_duplicate_name_raises():
    with raises(NameError):
        alg = MockConfGenerator()
        reqSel = AutomaticData(Location='PassThroughDOD')
        es = PassThroughSelection(
            'PTUniqueSelNameTest', ConfGenerator=alg, RequiredSelection=reqSel)
        PassThroughSelection(
            'PTUniqueSelNameTest', ConfGenerator=alg, RequiredSelection=reqSel)


def test_PassThroughSelection_name():
    alg = MockConfGenerator()
    reqSel = AutomaticData(Location='PassThroughDOD')
    sel = PassThroughSelection(
        'PTSelNameTest', ConfGenerator=alg, RequiredSelection=reqSel)
    assert sel.name() == 'PTSelNameTest'


def test_PassThroughSelection_outputLocation():
    alg = MockConfGenerator()
    reqSel = AutomaticData(Location='Pass/Through/DOD')
    sel = PassThroughSelection(
        'PTSelOutputTest', ConfGenerator=alg, RequiredSelection=reqSel)
    assert sel.outputLocation() == 'Pass/Through/DOD'


def test_PassThroughSelection_outputLocation_with_user_defined_datasetter():
    alg = MockConfGenerator()
    reqSel = AutomaticData(Location='Pass/Through/DOD')
    sel = PassThroughSelection(
        'PTSelInputTest',
        ConfGenerator=alg,
        RequiredSelection=reqSel,
        InputDataSetter='TESTINPUTS')
    assert sel.algorithm().TESTINPUTS == [reqSel.outputLocation()]


def test_clone_PassThroughSelection():
    alg = MockConfGenerator()
    reqSel = AutomaticData(Location='Pass/Through/DOD')
    sel = PassThroughSelection(
        'PTSelCloneTest',
        ConfGenerator=alg,
        RequiredSelection=reqSel,
        InputDataSetter='INPUTS')
    selClone = sel.clone(name='PTSelCloneTestClone')
    assert selClone.outputLocation() == sel.outputLocation()
    assert selClone.requiredSelections() == sel.requiredSelections()
    assert selClone.algorithm().INPUTS == sel.algorithm().INPUTS
    assert selClone.algorithm() != sel.algorithm()


def test_clone_PassThroughSelection_with_used_name_raises():
    with raises(NameError):
        alg = MockConfGenerator()
        reqSel = AutomaticData(Location='Pass/Through/DOD')
        sel = PassThroughSelection(
            'PTSelCloneTest2',
            ConfGenerator=alg,
            RequiredSelection=reqSel,
            InputDataSetter='INPUTS')
        sel.clone(name='PTSelCloneTest2')
