/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef TESLALINECHECKER_H
#define TESLALINECHECKER_H 1

// Include files
// from DaVinci.
#include "GaudiKernel/IIncidentListener.h"
#include "Kernel/DaVinciHistoAlgorithm.h"
#include <string>

class IIncidentSvc;

/** @class TeslaLineChecker TeslaLineChecker.h
 *
 *
 *  @author Sascha Stahl
 *  @date   2016-07-20
 */

class TeslaLineChecker : public extends<DaVinciHistoAlgorithm, IIncidentListener> {
public:
  /// Standard constructor
  TeslaLineChecker( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode initialize() override; ///< Algorithm initialization
  StatusCode execute() override;    ///< Algorithm execution

  /// Handle the ChangeRun incident
  void handle( const Incident& incident ) override;

protected:
private:
  bool                     ignoreLine( const std::string& line );
  std::vector<std::string> m_requestedLines;
  std::vector<std::string> m_teslaLines;
  std::vector<std::string> m_ignoredLines;
  std::string              m_decRepLoc;
  bool                     m_execute = false;
  bool                     m_checkDuplicates;
  bool                     m_checkNonTurbo;
};
#endif // TESLALINECHECKER_H
