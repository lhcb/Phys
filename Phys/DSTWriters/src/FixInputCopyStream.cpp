/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files

// from Gaudi
#include "GaudiKernel/IDataStoreLeaves.h"
// local
#include "FixInputCopyStream.h"

//-----------------------------------------------------------------------------
// Implementation file for class : FixInputCopyStream
//
// 2011-01-10 : Juan Palacios
//-----------------------------------------------------------------------------

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
FixInputCopyStream::FixInputCopyStream( const std::string& name, ISvcLocator* pSvcLocator )
    : GaudiAlgorithm( name, pSvcLocator ), m_leavesTool( NULL ) {}

//=============================================================================
// Initialization
//=============================================================================
StatusCode FixInputCopyStream::initialize() {
  const StatusCode sc = GaudiAlgorithm::initialize();
  if ( sc.isFailure() ) return sc;

  return toolSvc()->retrieveTool( "DataSvcFileEntriesTool", "InputCopyStreamTool", m_leavesTool );
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode FixInputCopyStream::execute() {
  m_leavesTool->leaves();
  return StatusCode::SUCCESS;
}

//=============================================================================
//  Finalize
//=============================================================================
StatusCode FixInputCopyStream::finalize() {
  toolSvc()->releaseTool( m_leavesTool ).ignore();
  m_leavesTool = NULL;

  return GaudiAlgorithm::finalize(); // must be called after all other actions
}

//=============================================================================

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( FixInputCopyStream )
