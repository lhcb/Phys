/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef FIXINPUTCOPYSTREAM_H
#define FIXINPUTCOPYSTREAM_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiAlgorithm.h"

class IDataStoreLeaves;

/** @class FixInputCopyStream FixInputCopyStream.h
 *
 *  Algorithm to fix interference problems between OutputStream and
 *  InputCopyStream. To guarantee correct InputCopyStream behaviour, place
 *  an instance of this algorithm in a sequence such that it runs before
 *  each <b>OutputStream</b>.
 *
 *  @author Juan Palacios
 *  @date   2011-01-10
 */
class FixInputCopyStream : public GaudiAlgorithm {
public:
  /// Standard constructor
  FixInputCopyStream( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode initialize() override; ///< Algorithm initialization
  StatusCode execute() override;    ///< Algorithm execution
  StatusCode finalize() override;   ///< Algorithm finalization

private:
  /// Pointer to the (public) tool used to retrieve the objects in a file.
  IDataStoreLeaves* m_leavesTool;
};
#endif // FIXINPUTCOPYSTREAM_H
