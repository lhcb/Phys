/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: PhotonMakerAlg.h,v 1.1 2009-04-21 19:15:41 pkoppenb Exp $
#ifndef RESOLVEDPI0MAKER_H
#define RESOLVEDPI0MAKER_H 1
// Include files
#include "CaloDet/DeCalorimeter.h"
#include "GaudiKernel/GenericVectorTypes.h"
#include "GaudiKernel/SymmetricMatrixTypes.h"
#include "Kernel/ICaloParticleMaker.h"
#include "ParticleMakerBase.h"

namespace LHCb {
  class ProtoParticle;
  class CaloHypo;
} // namespace LHCb

/** @class PhotonMakerAlg PhotonMakerAlg.h
 *
 *  Call PhotonMaker
 *
 *  @author P. Koppenburg
 *  @date   2009-04-21
 */

class PhotonMakerAlg : public ParticleMakerBase {
public:
  PhotonMakerAlg( const std::string& name, ISvcLocator* pSvcLocator );
  virtual ~PhotonMakerAlg();

  // Make the particles
  StatusCode makeParticles( LHCb::Particle::Vector& particles ) override;

  StatusCode initialize() override;
  StatusCode finalize() override;

protected:
private:
  std::string         m_photonMakerType;
  ICaloParticleMaker* m_photonMaker;
  bool                m_setPV;
};
#endif // RESOLVEDPI0MAKER_H
