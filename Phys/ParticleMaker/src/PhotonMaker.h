/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id:
// ============================================================================
#ifndef PHOTONMAKER_H
#define PHOTONMAKER_H 1
// Include files
#include "Event/Particle.h"
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/GenericVectorTypes.h"
#include "GaudiKernel/SymmetricMatrixTypes.h"
#include "Kernel/ICaloParticleMaker.h"

namespace LHCb {
  class ProtoParticle;
  class CaloHypo;
} // namespace LHCb

/** @class PhotonMaker PhotonMaker.h
 *
 *  The specialized producer of photons
 *
 *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
 *  @date   2003-01-19
 */

class PhotonMaker : public GaudiTool, virtual public ICaloParticleMaker {
public:
  PhotonMaker( const std::string& type, const std::string& name, const IInterface* parent );
  virtual ~PhotonMaker();

  // Make the particles
  StatusCode makeParticles( LHCb::Particle::Vector& particles ) override;
  void       setPoint( const Gaudi::XYZPoint pos ) override { m_point = pos; }
  void       setPoint( const Gaudi::XYZPoint pos, const Gaudi::SymMatrix3x3 cov ) override {
    m_point    = pos;
    m_pointErr = cov;
  }
  void setPoint( const LHCb::Vertex* vert ) override {
    m_point    = vert->position();
    m_pointErr = vert->covMatrix();
  }

  StatusCode initialize() override;
  StatusCode finalize() override;

protected:
  // confidence level evaluator
  double confLevel( const LHCb::ProtoParticle* pp, bool useSwitch = false ) const;
  // setters

private:
  int                                              ClusterCode( const LHCb::ProtoParticle* pp, std::string type ) const;
  std::map<std::string, std::pair<double, double>> m_clusterMasks;

  long m_Id;
  // Input PP container
  std::string m_input;
  // nominal production vertex
  Gaudi::XYZPoint     m_point;
  Gaudi::SymMatrix3x3 m_pointErr;

  // techniques for CL evaluation
  std::vector<std::string> m_clBase;
  std::vector<std::string> m_clSwitch;
  std::vector<std::string> m_knownCLs;

  // Filters
  double        m_clCut;
  double        m_ptCut;
  bool          m_converted;
  bool          m_unconverted;
  unsigned long m_count[3];
  double        m_minPrs;
  std::string   m_part;
  bool          m_addHcal;
  double        m_maxHcal;
  double        m_minHcal;
  double        m_maxPrs;
  double        m_mas;
  bool          clFind( std::string technique, bool useSwitch ) const;
};

#endif // PHOTONMAKER_H
