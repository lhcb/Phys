/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files

// from EventSys
#include "Event/Particle.h"
#include "Event/Vertex.h"

// local
#include "ParticleMakerBase.h"

//-----------------------------------------------------------------------------
// Implementation file for class : ParticleMakerBase
//
// 2009-04-21 P. Koppenburg
//-----------------------------------------------------------------------------

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
ParticleMakerBase::ParticleMakerBase( const std::string& name, ISvcLocator* pSvcLocator )
    : DaVinciAlgorithm( name, pSvcLocator ) {
  declareProperty( "Input", m_input = LHCb::ProtoParticleLocation::Charged );
  declareProperty( "Particle", m_pid = "UNDEFINED", "Particle to create : pion, kaon, muon..." );
  declareProperty( "AddBremPhotonTo", m_addBremPhoton, "ParticleIDs to be Brem-corrected (default : electrons only)" );
  m_addBremPhoton.push_back( "e+" );
}

//=========================================================================
//
//========================================================================
StatusCode ParticleMakerBase::initialize() {
  const StatusCode sc = DaVinciAlgorithm::initialize();
  if ( sc.isFailure() ) return sc;

  if ( getDecayDescriptor() == "" ) setDecayDescriptor( m_pid );

  // BremStrahlung correction
  m_brem = tool<IBremAdder>( "BremAdder", "BremAdder", this );

  if ( this->inputLocations().empty() ) {
    if ( !m_input.empty() ) { this->inputLocations().push_back( m_input ); }
  } else {
    m_input = "";
  }

  return sc;
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode ParticleMakerBase::execute() {
  LHCb::Particle::Vector newParts;

  StatusCode sc = makeParticles( newParts );
  if ( sc.isFailure() ) return sc;

  // CRJ Seems not needed any more
  // LHCb::Particle::ConstVector constParts ; /// @todo this is a hack due to CaloParticle...
  // constParts.reserve(newParts.size());

  for ( auto* p : newParts ) {
    // constParts.push_back(p);
    addBrem( p );
  }

  const bool ok = !newParts.empty();

  this->markNewTrees( newParts );

  if ( msgLevel( MSG::DEBUG ) ) {
    // Log number of vertices and particles
    debug() << "Number of particles = " << this->particles().size() << endmsg;
    if ( !this->primaryVertices().empty() ) {
      debug() << "Number of primary vertices = " << this->primaryVertices().size() << endmsg;
    } else {
      debug() << "No primary vertices" << endmsg;
    }
  }

  setFilterPassed( ok );

  return sc;
}

//=============================================================================

StatusCode ParticleMakerBase::loadEventInput() {
  if ( msgLevel( MSG::VERBOSE ) ) {
    verbose() << ">>> ProtoParticleMakerBase::loadEventInput: load ProtoParticles from " << this->inputLocations()
              << endmsg;
  }

  m_protos.clear();

  for ( const auto& loc : this->inputLocations() ) {
    const auto* pp = getIfExists<LHCb::ProtoParticle::Container>( loc );
    if ( pp ) {
      if ( msgLevel( MSG::VERBOSE ) ) {
        verbose() << "load " << pp->size() << " ProtoParticles from " << loc << endmsg;
      }
      for ( const auto* proto : *pp ) { m_protos.push_back( proto ); }
    } else {
      Info( "No ProtoParticles at " + loc ).ignore();
      continue;
    }
  }

  return StatusCode::SUCCESS;
}

//=============================================================================

void ParticleMakerBase::addBrem( LHCb::Particle* particle ) {
  bool ok = false;
  for ( const auto& p : m_addBremPhoton ) {
    if ( p == m_pid ) {
      ok = true;
      break;
    }
  }

  if ( !ok ) return;
  if ( !bremAdder()->addBrem( particle ) ) return;

  if ( msgLevel( MSG::DEBUG ) )
    debug() << " ------- BremStrahlung has been added to the particle " << particle << " (PID=" << m_pid << ")"
            << endmsg;

  counter( "Applying Brem-correction to " + Gaudi::Utils::toString( particle->particleID().pid() ) ) += 1;
}

//=============================================================================
