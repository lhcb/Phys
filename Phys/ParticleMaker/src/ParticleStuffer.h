/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: ParticleStuffer.h,v 1.7 2006-05-10 12:27:38 pkoppenb Exp $
#ifndef PARTICLESTUFFER_H
#define PARTICLESTUFFER_H 1

// Include files
// from STL
#include <string>

// from Gaudi
#include "GaudiAlg/GaudiTool.h"

// from DaVinciTools
#include "Kernel/IParticleStuffer.h"
#include "Kernel/IParticleTransporter.h"

// Forward declarations
class IParticlePropertySvc;

/** @class ParticleStuffer ParticleStuffer.h
 *  Fill a particle given a vertex and a particle ID
 *  @author Paul Colrain
 *  @date   14/03/2002
 *  Modified by S. Amato to transport parameters to the vertex position 11/10/2002
 */
class ParticleStuffer : public GaudiTool, virtual public IParticleStuffer {

public:
  /// Standard constructor
  ParticleStuffer( const std::string& type, const std::string& name, const IInterface* parent );

  /// Destructor
  ~ParticleStuffer();

  /// Retrieve  the ParticlePropertyService.
  StatusCode initialize() override;

  /// Fill Composite Particle from Vertex
  StatusCode fillParticle( const LHCb::Particle::ConstVector& daughters, const LHCb::Vertex&, const LHCb::ParticleID&,
                           LHCb::Particle& ) override;

  /// Fill Composite Particle from Vertex
  StatusCode fillParticle( const LHCb::Particle::ConstVector& daughters, const LHCb::Vertex&,
                           LHCb::Particle& ) override;

private:
  LHCb::IParticlePropertySvc* m_ppSvc;           ///< Reference to ParticlePropertySvc
  IParticleTransporter*       m_pTransporter;    ///< Reference to ParticleTransporter
  std::string                 m_transporterType; ///< Type of transporter to use
};

#endif // PARTICLESTUFFER_H
