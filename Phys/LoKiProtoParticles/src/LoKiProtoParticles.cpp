/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id:$
// ============================================================================
#include "LoKi/LoKiProtoParticles.h"
// ============================================================================
#include "LoKi/PPTypes.h"
#include "LoKi/ProtoParticles.h"
// ============================================================================
/** @file Implementation file
 *  @date 2015-01-17
 *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
 */
// ============================================================================
