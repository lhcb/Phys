#!/usr/bin/env python
###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# =============================================================================
# $Id$
# =============================================================================
## @file  LoKiArrayFunctors/decorators.py
#  The set of basic decorator for objects from LoKiHlt library
#  The file is a part of LoKi and Bender projects
#
#  This file is a part of LoKi project -
#   'C++ ToolKit  for Smart and Friendly Physics Analysis'
#
#  The package has been designed with the kind help from
#  Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
#  contributions and advices from G.Raven, J.van Tilburg,
#  A.Golutvin, P.Koppenburg have been used in the design.
#
#  @author Vanya BELYAEV ibelyaev@physics.syr.edu
#  @daet 2007-06-09
# =============================================================================
"""
The set of basic decorators for objects from LoKiHlt library

This file is a part of LoKi project -
'C++ ToolKit  for Smart and Friendly Physics Analysis'

The package has been designed with the kind help from
Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
contributions and advices from G.Raven, J.van Tilburg,
A.Golutvin, P.Koppenburg have been used in the design.
"""
from __future__ import print_function
# =============================================================================
__author__ = "Vanya BELYAEV ibelyaev@physics.syr.edu"
# =============================================================================
import LoKiArrayFunctors.Array
from LoKiArrayFunctors.decorators import *
from LoKiCore.functions import *
from LoKiCore.math import *
from LoKiPhys.decorators import P, PT, Q, ALL


# =============================================================================
def testAll():

    print((ALV01 < 0) & (AETA > 0) | (AM < 50) & AALL)

    print(aval_max(P, PT), aval_min(P, PT, 0 != Q))
    print(aval_min(P, PT, 0 != Q, 10000))
    print(ASUM(P, PT > 1))

    print(ANUM(ALL) <= ANUM(ALL))
    print(ANUM(ALL) <= 1)


# =============================================================================
if '__main__' == __name__:
    testAll()

# =============================================================================
# The END
# =============================================================================
