/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
// Include files
// ============================================================================
// GaudiKernel
// ============================================================================
#include "GaudiKernel/VectorMap.h"
// ============================================================================
// GaudiAlg
// ============================================================================
#include "GaudiAlg/GaudiTool.h"
#include "GaudiAlg/Tuple.h"
#include "GaudiAlg/TupleObj.h"
#include "GaudiAlg/Tuples.h"
// ============================================================================
#include "Kernel/IParticleDictTool.h"
#include "Kernel/IParticleTupleTool.h" // Interface
// ============================================================================
// LoKi
// ============================================================================
#include "LoKi/IHybridFactory.h"
#include "LoKi/PhysTypes.h"
// ============================================================================
namespace LoKi {
  // ==========================================================================
  namespace Hybrid {
    // ========================================================================
    /** @class Dict2Tuple Dict2Tuple.h Components/Dict2Tuple.h
     *  Writing a Dictionary into the ntuple
     *
     *  If Prepend is True (default), prepends the name of the instance to all
     *  branch names, i.e., `head_instanceName_varName`, to avoid overwrites
     *  with multiple instances, especially of default variables like
     *  `DTF_CHI2` and `DTF_NDOF`.
     *
     *  @author Sebastian Neubert
     *  @date   2013-07-11
     */
    class Dict2Tuple : public GaudiTool, virtual public IParticleTupleTool {
    public:
      // ======================================================================
      /// Standard constructor
      Dict2Tuple( const std::string& type, const std::string& name, const IInterface* parent )
          : GaudiTool( type, name, parent )
          , m_sourcename()
          , m_numvar( 1 )
          , m_source( 0 )
          , m_prefix( "" )
          , m_prepend( true ) {
        declareInterface<IParticleTupleTool>( this );
        ///
        declareProperty( "Source", m_sourcename, "Type/Name for Source Dictionary Tool" );
        declareProperty( "NumVar", m_numvar, "Number of variables expected to be filled into dictionary" );
        declareProperty( "Prefix", m_prefix, "Use custom prefix (instead of head)" );
        declareProperty( "Prepend", m_prepend, "Prepend toolname + \"_\" before all variable names" );
      }
      // ======================================================================
      /** Fill the tuple.
       *  @see IParticleTupelTool
       *  @param top      the top particle of the decay.
       *  @param particle the particle about which some info are filled.
       *  @param head     prefix for the tuple column name.
       *  @param tuple    the tuple to fill
       *  @return status code
       */
      StatusCode fill( const LHCb::Particle* top, const LHCb::Particle* particle, const std::string& head,
                       Tuples::Tuple& tuple ) override;
      // ======================================================================
      /// initialization of the tool
      StatusCode initialize() override {
        StatusCode sc = GaudiTool::initialize();
        if ( sc.isFailure() ) { return sc; } // RETURN

        // get the dicttool
        m_source = tool<IParticleDictTool>( m_sourcename, this );
        if ( m_source == NULL ) { return Error( "Unable to find the source DictTool " + m_sourcename, sc ); }
        //
        // reserve memory for dictionary
        m_dict.reserve( m_numvar );
        return StatusCode::SUCCESS;
      }
      // ======================================================================
    private:
      // ======================================================================
      std::string             m_sourcename;
      unsigned int            m_numvar; // number of variables expected to be filled into dictionary
      IParticleDictTool*      m_source;
      IParticleDictTool::DICT m_dict;
      std::string             m_prefix;
      bool                    m_prepend;
      // ======================================================================
    };
    // ========================================================================
  } // namespace Hybrid
  // ==========================================================================
} // namespace LoKi
// ============================================================================
/*  Fill the tuple.
 *  @see IParticleTupleTool
 *  @param top      the top particle of the decay.
 *  @param particle the particle about which some info are filled.
 *  @param head     prefix for the tuple column name.
 *  @param tuple    the tuple to fill
 *  @return status code
 */
// ============================================================================
StatusCode LoKi::Hybrid::Dict2Tuple::fill( const LHCb::Particle* top, const LHCb::Particle* particle,
                                           const std::string& head, Tuples::Tuple& tuple ) {
  if ( 0 == top ) {
    Warning( "LHCb::Particle* 'Top' points to NULL" ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  }
  if ( 0 == particle ) {
    Warning( "LHCb::Particle*       points to NULL" ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  }
  //
  if ( !tuple.valid() ) { return Error( "Invalid tuple " ); }

  // make sure we start with a clean dictionary
  m_dict.clear();

  // call IParticleDictTool to aquire the dictionary
  /// request the dictionary of variables from the source
  m_source->fill( particle, m_dict ).ignore();

  // prepend the head of the current branch to the variable to make sure
  // columns are uniquely named
  // or if prefix is defined it is used instead
  std::string head_ = head + "_";
  if ( m_prefix != "" ) head_ = m_prefix + "_";
  // Put the items in the dictionary into the tuple
  std::string tlnm_( m_sourcename.substr( m_sourcename.find( "/" ) + 1 ) + "_" ); // the tool name
  if ( !m_prepend ) tlnm_ = "";
  for ( IParticleDictTool::DICT::const_iterator item = m_dict.begin(); m_dict.end() != item; ++item ) {
    // fill N-tuple
    tuple->column( head_ + tlnm_ + item->first, item->second ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  }
  //
  return StatusCode::SUCCESS;
}
// ============================================================================
/// Declaration of the Tool Factory
// ============================================================================
DECLARE_COMPONENT( LoKi::Hybrid::Dict2Tuple )
// ============================================================================
