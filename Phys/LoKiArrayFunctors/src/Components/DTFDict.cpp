/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef DTFDICTTOOL_H
#define DTFDICTTOOL_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "Kernel/IDecayTreeFit.h"
#include "Kernel/IParticleDictTool.h" // Interface used by this tool
#include "LoKi/AuxDTFBase.h"
#include "LoKi/GetTools.h"
#include "LoKi/PhysTypes.h"

#include "GaudiKernel/Chrono.h"

#include "Kernel/PVSentry.h"

#include "Kernel/Escape.h"
#include "Kernel/IParticlePropertySvc.h"
#include "Kernel/ISubstitutePID.h"
#include "Kernel/ParticleProperty.h"

// std::lib
#include <map>
#include <string>

namespace LoKi {
  namespace Hybrid {

    /** @class DTFDict
     *  Implementation of a Particle->Particle Transformation on
     *  a dictionary of Functors
     *  The particle for which the DictOfFunctors should be evaluated
     *  is transformed by applying the DecayTreeFitter
     *
     *  @author Sebastian Neubert
     *  @date   2013-07-10
     */
    class DTFDict : public GaudiTool, public LoKi::AuxDTFBase, virtual public IParticleDictTool {
    public:
      // ======================================================================
      /// Standard constructor
      DTFDict( const std::string& type, const std::string& name, const IInterface* parent )
          : GaudiTool( type, name, parent ), AuxDTFBase( 0 ) {
        declareInterface<IParticleDictTool>( this );

        declareProperty( "Source", m_sourcename, "Type/Name for Dictionary Tool" );

        declareProperty( "daughtersToConstrain", m_constraints, "list of particles to contrain" );

        declareProperty( "constrainToOriginVertex", m_usePV, "flag to switch on PV contraint" );

        declareProperty( "Substitutions", m_map, "PID-substitutions :  { 'decay-component' : 'new-pid' }" );
      }

      // ======================================================================

      StatusCode initialize() override {
        StatusCode sc = GaudiTool::initialize();
        if ( sc.isFailure() ) { return sc; } // RETURN
        // acquire the DictTool containing all the functors
        // that should be evaluated on the transformed particle
        m_source = tool<IParticleDictTool>( m_sourcename, this );
        if ( m_source == NULL ) return GaudiTool::Error( "Unable to find the source DictTool " + m_sourcename, sc );

        debug() << "Successfully initialized DTFDict" << endmsg;

        loadFitter( "LoKi::DecayTreeFit/DTFDictFitter" );
        setConstraint( m_constraints );

        if ( !m_map.empty() ) {
          m_substitute = tool<ISubstitutePID>( "SubstitutePIDTool", this );
          sc           = m_substitute->decodeCode( m_map );
        }

        m_ppSvc = svc<LHCb::IParticlePropertySvc>( "LHCb::ParticlePropertySvc", true );

        return sc;
      }

      // ======================================================================

      StatusCode fill( const LHCb::Particle* p, IParticleDictTool::DICT& dict ) const override {

        Chrono chrono( chronoSvc(), name() + "::fill()" );
        if ( nullptr == p ) {
          GaudiTool::Error( "LHCb::Particle* points to NULL, dictionary will not be filled" ).ignore();
          return StatusCode::FAILURE; // RETURN
        }
        // 2. get primary vertex (if required)
        const LHCb::VertexBase* vertex = 0;
        if ( m_usePV ) {
          vertex = bestVertex( p );
          if ( 0 == vertex ) {
            GaudiTool::Warning( "``Best vertex'' points to null, constraits will be disabled!" ).ignore();
          }
        }
        //

        LHCb::DecayTree tree( *p );

        // substitute
        if ( m_substitute && !substitute( tree ) ) return StatusCode::FAILURE;

        applyConstraints(); // you need to do this for each individual fit!
        // printConstraints(std::cout);

        IDecayTreeFit*  myfitter  = fitter(); // from AuxDTFBase
        StatusCode      sc        = myfitter->fit( tree.head(), vertex );
        LHCb::Particle* prefitted = nullptr;
        if ( sc.isFailure() ) {
          // Hand an empty pointer to source and let it decide how to fill the
          // dictionary. This is needed because have too little information
          // on the structure of the dict at this point.
          GaudiTool::Warning( "Error from IDecayTreeFit", sc ).ignore();
          dict.insert( "DTF_CHI2", -1 );
          dict.insert( "DTF_NDOF", 0 );
          /// request the dictionary from the source DictTool
          m_source->fill( prefitted, dict ).ignore();
        } else // everything went well
        {
          dict.insert( "DTF_CHI2", myfitter->chi2() );
          dict.insert( "DTF_NDOF", myfitter->nDoF() );
          LHCb::DecayTree tree = myfitter->fittedTree();
          // evaluate the dict of fucntors on the head of the refitted tree
          prefitted = tree.head();
          // lock the relation table Particle -> Primary Vertex
          // need to call getDesktop, since algorithm doesn't inherit from IDVAlgorithm
          // 3rd argument ensures that relations of daughters are locked as well
          DaVinci::PVSentry sentry( this->getDesktop(), prefitted, true );
          /// request the dictionary from the source DictTool
          m_source->fill( prefitted, dict ).ignore();
        } // sentry needs to go out of scope, so that destructor is called
        // tree.release();
        return StatusCode::SUCCESS;
      }

      std::string getName( const int id ) const {

        const auto* prop = m_ppSvc->find( LHCb::ParticleID( id ) );

        if ( !prop ) LoKi::AuxFunBase::Exception( "Unknown PID" );

        // if (msgLevel(MSG::VERBOSE)) verbose() << "ID " << id << " gets name "
        //                                      << Decays::escape(prop->name()) << endmsg ;
        return Decays::escape( prop->name() );
      }

      StatusCode substitute( LHCb::DecayTree& tree ) const {

        if ( msgLevel( MSG::DEBUG ) ) debug() << "Calling substitute" << endmsg;

        const auto substituted = m_substitute->substitute( tree.head() );

        // debugging
        if ( msgLevel( MSG::VERBOSE ) || 0 == substituted ) {
          const auto mp = tree.cloneMap();

          for ( const auto& i : mp ) {

            if ( i.first->particleID().pid() == i.second->particleID().pid() ) {
              info() << "A " << getName( i.first->particleID().pid() ) << " remains unchanged" << endmsg;
            } else {
              info() << "A " << getName( i.first->particleID().pid() ) << " is substituted by a "
                     << getName( i.second->particleID().pid() ) << endmsg;
            }
          }
        }

        if ( 0 == substituted ) {
          return LoKi::AuxFunBase::Error( "No particles have been substituted. Check your substitution options." );
        }

        return StatusCode::SUCCESS;
      }

      // ======================================================================
      virtual ~DTFDict(){}; ///< Destructor
      // ======================================================================

    protected:
      IParticleDictTool* m_source;
      std::string        m_sourcename;

      std::vector<std::string> m_constraints;
      // std::map<std::string,std::string> m_options;
      bool m_usePV;

      ISubstitutePID::SubstitutionMap m_map; // mapping : { 'decay-component' : "new-pid" }

      ISubstitutePID* m_substitute = nullptr;

      LHCb::IParticlePropertySvc* m_ppSvc = nullptr;
    };
  } // end namespace Hybrid
} // end namespace LoKi

DECLARE_COMPONENT( LoKi::Hybrid::DTFDict )

#endif
