/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
// Include files
// ============================================================================
// GaudiAlg
// ============================================================================
#include "GaudiAlg/GaudiTool.h"
// ============================================================================
#include "Kernel/IParticleValue.h" // Interface
// ============================================================================
namespace LoKi {
  // ==========================================================================
  /** @class SpecialValue
   *  the simplest possible implementation of IPArticleValue integrface
   *  @see IParticlValue
   *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
   *  @date 2015-03-12
   */
  class SpecialValue : public extends<GaudiTool, IParticleValue> {
  public:
    // ========================================================================
    /// Standard constructor
    SpecialValue( const std::string& type, const std::string& name, const IInterface* parent )
        : base_class( type, name, parent ), m_value( 42 ) {
      declareProperty( "Value", m_value, "Value to be used" );
    }
    // ========================================================================
    double operator()( const LHCb::Particle* /* p */ ) const override { return m_value; }
    // ========================================================================
  private:
    // ========================================================================
    /// the value
    double m_value; // the value
    // ========================================================================
  };
  // ==========================================================================
} // namespace LoKi
// ============================================================================
DECLARE_COMPONENT( LoKi::SpecialValue )
// ============================================================================
// The END
// ============================================================================
