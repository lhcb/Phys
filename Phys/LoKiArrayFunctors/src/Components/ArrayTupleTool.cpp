/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
// Include files
// ============================================================================
//  STD & STL
// ============================================================================
#include <algorithm>
#include <utility>
// ============================================================================
// LoKiPhys
// ============================================================================
#include "LoKi/Operators.h"
#include "LoKi/Particles11.h"
#include "LoKi/Particles8.h"
#include "LoKi/Particles9.h"
#include "LoKi/PhysTypes.h"
// ============================================================================
// Local
// ============================================================================
#include "TupleTool.h"
// ============================================================================
namespace LoKi {
  // ==========================================================================
  namespace Hybrid {
    // ========================================================================
    /** @class ArrayTupleTool
     *  "Specific" tuple tool to fill infomation about "other" particles
     *
     *  @code
     *  xxx.Source     = "SOURCE('Phys/StdLoosePions/Particles', PT>1*GeV )"
     *  xxx. Variables = { "var1"  : "PT"         , "var2" : "ETA" }
     *  xxx.AVariables = { "doca" : "ADOCA(1,2)"  , "docachi2" : "ACHI2DOCA(1,2)"}
     *  @endcode
     *   - "Other" particles are required to be different from the signal particle
     *      and its top-particle: there is requirement of no common paricles,
     *      protoparticles and tracks
     *   - For "A"-variables, temporary array of two elemements:
     *     (signal particle and one "other" particle from the list)
     *     is constructed and each "A"-functor is applied to this array.
     *
     *  @see Tuples::TupleObj
     *  @see LoKi::ATypes::Combination
     *  @see LoKi::ATypes::AFun
     *
     *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
     *  @date 2016-03-19
     */
    class ArrayTupleTool : public LoKi::Hybrid::TupleTool {
    public:
      // ======================================================================
      /** helper class to keep the N-tuple items
       *  it is needed due to absence of the default constructor for
       *  the class LoKi::PhysTypes::Fun
       *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
       *  @date 2016-06-05
       */
      class AItem {
      public:
        // ====================================================================
        /// the default constructor
        AItem( const LHCb::Particle* p = nullptr )
            : m_name(), m_particle( p ), m_fun( LoKi::BasicFunctors<LoKi::ATypes::Combination>::Constant( -1.e+10 ) ) {}
        // ====================================================================
      public:
        // ====================================================================
        double operator()( const LHCb::Particle* p ) const {
          const LHCb::Particle::ConstVector vct{{m_particle, p}};
          return m_fun.fun( LoKi::ATypes::Combination( vct ) );
        }
        // ====================================================================
      public:
        // ====================================================================
        /// the variable name
        std::string m_name; // the variable name
        /// the particle
        const LHCb::Particle* m_particle; /// the paticle
        /// the functor
        LoKi::ATypes::AFun m_fun; /// the functor
        // ====================================================================
      };
      // ======================================================================
    public:
      // ======================================================================
      /** Fill the tuple.
       *  @see IParticleTupelTool
       *  @param top      the top particle of the decay.
       *  @param particle the particle about which some info are filled.
       *  @param head     prefix for the tuple column name.
       *  @param tuple    the tuple to fill
       *  @return status code
       */
      StatusCode fill( const LHCb::Particle* top, const LHCb::Particle* particle, const std::string& head,
                       Tuples::Tuple& tuple ) override;
      // ======================================================================
    protected:
      // ======================================================================
      /// initialization of the tool
      StatusCode initVariables() override {
        // get varibales
        StatusCode sc = LoKi::Hybrid::TupleTool::initVariables();
        if ( sc.isFailure() ) { return Error( "Can't decode varibales", sc ); }
        //
        // get the factory
        IHybridFactory* the_factory = tool<IHybridFactory>( factory(), this );
        //
        sc = the_factory->get( m_source_code, m_source, preambulo() );
        if ( sc.isFailure() ) { return Error( "Unable to decode ``source'' code:" + m_source_code, sc ); }
        //
        debug() << "The decoded ``source code'' is " << m_source << endmsg;
        //
        m_aitems.clear();
        m_aitems.reserve( m_avars.size() );
        for ( Map::const_iterator ivar = m_avars.begin(); m_avars.end() != ivar; ++ivar ) {
          AItem      item;
          StatusCode sc = the_factory->get( ivar->second, item.m_fun, preambulo() );
          if ( sc.isFailure() ) { return Error( "Unable to decode " + ivar->first + " : " + ivar->second, sc ); }
          //
          item.m_name = ivar->first;
          m_aitems.push_back( std::make_pair( ivar->first, item ) );
          //
          debug() << "The decoded 'A'-variable name is '" << m_aitems.back().first << "'\t, the functor : '"
                  << m_aitems.back().second.m_fun << "'" << endmsg;
        }
        //
        release( the_factory ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ ); // we do not need the factory
                                                                                        // anymore
        //
        return StatusCode::SUCCESS;
      }
      // ======================================================================
    public:
      // ======================================================================
      /// the update handler
      void propHandler( Gaudi::Details::PropertyBase& p ) { return LoKi::Hybrid::TupleTool::propHandler( p ); }
      // ======================================================================
      ArrayTupleTool( const std::string& type, const std::string& name, const IInterface* parent );
      // ======================================================================
    private:
      // ======================================================================
      /// the source of ``other'' particles (code)
      std::string m_source_code; //
      /// the source itself
      LoKi::Types::Source m_source; // { "name":"functor"} map
      /// "A"-variables
      Map m_avars;
      /// the actual type of containter of items
      typedef std::vector<std::pair<std::string, AItem>> AItems;
      /// "A"-items
      AItems m_aitems;
      // ======================================================================
    };
    // ========================================================================
  } // namespace Hybrid
  // ==========================================================================
} //                                                      end of namespace LoKi
// ============================================================================
namespace {
  // ==========================================================================
  /// empty source
  class EmptySource : public LoKi::Types::Sources {
    EmptySource* clone() const override { return new EmptySource( *this ); }
    result_type  operator()() const override {
      Error( "Empty source is invoked" ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
      return LHCb::Particle::ConstVector();
    }
  };
  // ==========================================================================
} // namespace
// ============================================================================
LoKi::Hybrid::ArrayTupleTool::ArrayTupleTool( const std::string& type, const std::string& name,
                                              const IInterface* parent )
    : LoKi::Hybrid::TupleTool( type, name, parent ), m_source_code(), m_source( EmptySource() ) {
  ///
  declareProperty( "Source", m_source_code, "The source of ``other'' particles" )
      ->declareUpdateHandler( &LoKi::Hybrid::ArrayTupleTool::propHandler, this );
  //
  declareProperty( "AVariables", m_avars, "The {'name':'functor'}-map of A-columns for N-tuple " )
      ->declareUpdateHandler( &LoKi::Hybrid::ArrayTupleTool::propHandler, this );
}
// ============================================================================
/*  Fill the tuple.
 *  @see IParticleTupleTool
 *  @param top      the top particle of the decay.
 *  @param particle the particle about which some info are filled.
 *  @param head     prefix for the tuple column name.
 *  @param tuple    the tuple to fill
 *  @return status code
 */
// ============================================================================
StatusCode LoKi::Hybrid::ArrayTupleTool::fill( const LHCb::Particle* top, const LHCb::Particle* particle,
                                               const std::string& head, Tuples::Tuple& tuple ) {
  if ( 0 == top ) { Warning( "LHCb::Particle* 'Top' points to NULL" ).ignore(); }
  if ( 0 == particle ) { Warning( "LHCb::Particle*       points to NULL" ).ignore(); }
  //
  if ( !tuple.valid() ) { return Error( "Invalid tuple " ); }
  //
  std::string head_ = head + "_";
  //
  // get ``other'' particles
  LHCb::Particle::ConstVector input1 = m_source();
  LHCb::Particle::ConstVector input2;
  input2.reserve( input1.size() );
  //
  LoKi::Types::Cut cuts = LoKi::Particles::IsAParticleInTree( particle ) ||
                          LoKi::Particles::HasProtosInTree( particle ) || LoKi::Particles::HasTracksInTree( particle );
  if ( top ) {
    cuts = cuts || LoKi::Particles::HasTracksInTree( top ) || LoKi::Particles::IsAParticleInTree( top ) ||
           LoKi::Particles::HasProtosInTree( top );
  }
  cuts = !cuts;
  //
  std::copy_if( input1.begin(), input1.end(), std::back_inserter( input2 ), std::cref( cuts ) );
  //
  counter( "#filtered1" ) += input1.size();
  counter( "#filtered2" ) += input2.size();
  //
  tuple->farray( items(), input2.begin(), input2.end(), head_ + "len", 1000 )
      .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  //
  /// activate 'A'-variables
  for ( AItems::iterator item = m_aitems.begin(); m_aitems.end() != item; ++item ) {
    item->second.m_particle = particle;
  }
  ///
  tuple->farray( m_aitems, input2.begin(), input2.end(), head_ + "len", 1000 )
      .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  //
  return StatusCode::SUCCESS;
}
// ============================================================================
/// Declaration of the Tool Factory
DECLARE_COMPONENT( LoKi::Hybrid::ArrayTupleTool )
// ============================================================================
// The END
// ============================================================================
