/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef LIB_HEPDRONETRANSFORM_H
#define LIB_HEPDRONETRANSFORM_H

#include "Kernel/IParticleDictTool.h"
#include <map>
#include <memory>
#include <ostream>
#include <string>
#include <utility>
#include <vector>

#include "HDKerasModel.h"

#include "MVADictTools/Options.h"

typedef IParticleDictTool::DICT DICT;

/** @class HEPDroneTransform
 *  Policy class to be used by the DictTransform template
 *  Implementing the HEPDrone reader backed
 *
 *  @author Konstantin Gizdov
 *  @date   2018-02-08
 */

class HEPDroneTransform {
public:
  typedef std::map<std::string, std::string> optmap; /// map to parse options

  /// the policy methods needed for collaboration with DictTransform
  bool Init( const optmap& options, std::ostream& info, const bool debug = false );
  bool operator()( const DICT& in, DICT& out ) const;

private:
  /// HDKerasModel for evaluation
  std::unique_ptr<HDKerasModel> m_model = std::make_unique<HDKerasModel>();

  /// NN JSON config
  lwt::JSONConfig m_config{};

  // Helper Functions
  /// check JSON configuration file
  bool checkDroneFile( std::ostream& info );

  /// read JSON and configure NN
  void readDroneFile( std::ostream& );

  /// parse tool options
  bool parseOpts( const optmap&, std::ostream& );

  /// convert input vector to NN input tensor
  Tensor dictInputToTensor( const DICT& in ) const;

  /// convert NN output tensor to output vector
  std::vector<std::pair<std::string, double>> tensorOutToVec( const Tensor& in ) const;

  bool          m_setup_success{false}; ///< setup status
  bool          m_keep_all_vars{true};  ///< keep input variables together with response
  bool          m_json{true};           ///< are we using JSON or HD5 config file
  std::string   m_drone_file{};         ///< path to configuration file
  std::string   m_name{};               ///< name of output branch
  std::ostream* m_info{};               ///< store location of info buffer for internal use

  bool m_debug{false}; ///< debug flag
};

#endif // LIB_HEPDRONETRANSFORM_H
