/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE utestTMVA
#include <boost/test/unit_test.hpp>
#include <iostream>
#include <sstream>

#include "GaudiKernel/System.h"

#include "MVADictTools/TMVATransform.h"

//-------- Basic Test: can we instantiate an object and call a method? ---
bool inittest() {
  TMVATransform         tmva;
  TMVATransform::optmap options;
  options["Name"] = "test";

  options["XMLFile"] = System::getEnv( "MVADICTTOOLSROOT" ) + "/options/TestPhi2KK.xml";
  return tmva.Init( options, std::cout );
}

// let's use this function in a test case:
BOOST_AUTO_TEST_CASE( initTest ) { BOOST_CHECK( inittest() ); }
