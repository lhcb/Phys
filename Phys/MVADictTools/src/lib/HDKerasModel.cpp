/*****************************************************************************\
* (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include <fstream>
#include <string>

#include <MVADictTools/HDKerasModel.h>

Tensor HDKerasLayer::getMatrixTensor( const lwt::LayerConfig& config, size_t n_inputs ) {
  size_t n_elements = config.weights.size();
  KASSERT( n_elements > 0, "Layer has 0 neurons" );
  KASSERT( n_inputs > 0, "Invalid weights # rows" );

  if ( ( n_elements % n_inputs ) != 0 ) {
    std::string problem = "matrix elements not divisible by number"
                          " of columns. Elements: " +
                          std::to_string( n_elements ) + ", Inputs: " + std::to_string( n_inputs );
    throw std::logic_error( problem );
  }

  m_n_outputs = n_elements / n_inputs;
  KASSERT( config.bias.size() == m_n_outputs, "Number of biases does not match number of outputs" );

  Tensor temp( n_inputs, m_n_outputs );
  for ( unsigned i = 0; i < m_n_outputs; ++i ) {
    for ( unsigned j = 0; j < n_inputs; ++j ) {
      temp( j, i ) = config.weights[j + ( i * n_inputs )]; // read the transposed and flattened matrix
    }
  }
  return temp;
}

bool HDKerasLayerActivation::LoadLayer( const lwt::LayerConfig& config, size_t n_inputs ) {
  const size_t n __attribute__( ( unused ) ) = n_inputs;
  switch ( config.activation ) {
  case lwt::Activation::LINEAR:
    activation_type_ = kLinear;
    break;
  case lwt::Activation::RECTIFIED:
    activation_type_ = kRelu;
    break;
  case lwt::Activation::SOFTPLUS:
    activation_type_ = kSoftPlus;
    break;
  case lwt::Activation::HARD_SIGMOID:
    activation_type_ = kHardSigmoid;
    break;
  case lwt::Activation::SIGMOID:
    activation_type_ = kSigmoid;
    break;
  case lwt::Activation::TANH:
    activation_type_ = kTanh;
    break;
  case lwt::Activation::SOFTMAX:
    activation_type_ = kSoftMax;
    break;

  default:
    return false; // cannot load layer without activation
  }

  return true;
}

bool HDKerasLayerDense::LoadLayer( const lwt::LayerConfig& config, size_t n_inputs ) {
  weights_ = getMatrixTensor( config, n_inputs );

  biases_.Resize( m_n_outputs );
  for ( size_t i = 0; i < config.bias.size(); ++i ) { biases_.data_[i] = config.bias[i]; }

  KASSERT( activation_.LoadLayer( config ), "Failed to load activation" );
  KerasLayerDense::activation_ = activation_;

  return true;
}

bool HDKerasLayerConvolution2d::LoadLayer( const lwt::LayerConfig& config, size_t n_inputs ) {
  // CURRENTLY DOES NOT SUPPORT JSON CONFIGURATION
  const size_t n __attribute__( ( unused ) ) = n_inputs;
  KASSERT( activation_.LoadLayer( config ), "Failed to load activation" );
  KerasLayerConvolution2d::activation_ = activation_;
  return false;
}

bool HDKerasLayerFlatten::LoadLayer( const lwt::LayerConfig& config, size_t n_inputs ) {
  const lwt::LayerConfig m __attribute__( ( unused ) ) = config;
  const size_t           n __attribute__( ( unused ) ) = n_inputs;
  return true;
}

bool HDKerasLayerElu::LoadLayer( const lwt::LayerConfig& config, size_t n_inputs ) {
  const lwt::LayerConfig m __attribute__( ( unused ) ) = config;
  m_n_outputs                                          = n_inputs;
  alpha_                                               = 1.0f;
  return true;
}

bool HDKerasLayerMaxPooling2d::LoadLayer( const lwt::LayerConfig& config, size_t n_inputs ) {
  // CURRENTLY DOES NOT SUPPORT JSON CONFIGURATION
  const lwt::LayerConfig m __attribute__( ( unused ) ) = config;
  const size_t           n __attribute__( ( unused ) ) = n_inputs;
  return false;
}

bool HDKerasLayerLSTM::LoadLayer( const lwt::LayerConfig& config, size_t n_inputs ) {
  // CURRENTLY DOES NOT SUPPORT JSON CONFIGURATION
  const size_t n __attribute__( ( unused ) ) = n_inputs;
  KASSERT( innerActivation_.LoadLayer( config ), "Failed to load inner activation" );
  KASSERT( activation_.LoadLayer( config ), "Failed to load activation" );
  KerasLayerLSTM::innerActivation_ = innerActivation_;
  KerasLayerLSTM::activation_      = activation_;
  return false;
}

bool HDKerasLayerEmbedding::LoadLayer( const lwt::LayerConfig& config, size_t n_inputs ) {
  // CURRENTLY DOES NOT SUPPORT JSON CONFIGURATION
  const lwt::LayerConfig m __attribute__( ( unused ) ) = config;
  const size_t           n __attribute__( ( unused ) ) = n_inputs;
  return false;
}

bool HDKerasLayerBatchNormalization::LoadLayer( const lwt::LayerConfig& config, size_t n_inputs ) {
  // CURRENTLY DOES NOT SUPPORT JSON CONFIGURATION
  const lwt::LayerConfig m __attribute__( ( unused ) ) = config;
  const size_t           n __attribute__( ( unused ) ) = n_inputs;
  return false;
}

bool HDKerasModel::LoadJSONModel( const std::string& filename ) {
  std::ifstream json_file( filename );
  auto          config = lwt::parse_json( json_file );
  return LoadJSONModel( config );
}

bool HDKerasModel::LoadJSONModel( const lwt::JSONConfig& config ) {
  unsigned int num_layers = config.layers.size();
  unsigned int num_inputs = config.inputs.size();

  for ( unsigned int i = 0; i < num_layers; i++ ) {
    HDKerasLayer* layer = NULL;

    switch ( config.layers[i].architecture ) {
    case lwt::Architecture::DENSE:
      layer = new HDKerasLayerDense();
      break;
    // currently unsupported
    // case lwt::Architecture::Convolution2d:
    //     layer = new HDKerasLayerConvolution2d();
    //     break;
    // case lwt::Architecture::Flatten:
    //     layer = new HDKerasLayerFlatten();
    //     break;
    // case lwt::Architecture::Activation:
    //     layer = new HDKerasLayerActivation();
    //     break;
    // case lwt::Architecture::MAXOUT:
    //     layer = new HDKerasLayerMaxPooling2d();
    //     break;
    case lwt::Architecture::LSTM:
      layer = new HDKerasLayerLSTM();
      break;
    case lwt::Architecture::EMBEDDING:
      layer = new HDKerasLayerEmbedding();
      break;
    case lwt::Architecture::NORMALIZATION:
      layer = new HDKerasLayerBatchNormalization();
      break;
    default:
      switch ( config.layers[i].activation ) {
      case lwt::Activation::ELU:
        layer = new HDKerasLayerElu();
        break;
      default:
        break;
      }
      break;
    }

    KASSERT( layer, "Unknown or currently unsupported layer type arch: %u act: %u",
             static_cast<unsigned>( config.layers[i].architecture ),
             static_cast<unsigned>( config.layers[i].activation ) );

    bool result = layer->LoadLayer( config.layers[i], num_inputs );
    if ( !result ) {
      printf( "Failed to load JSON layer %u", i );
      delete layer;
      return false;
    }
    num_inputs = layer->GetNOutputs(); // update num of inputs for next layer

    layers_.push_back( layer );
  }
  return true;
}
