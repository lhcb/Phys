/*****************************************************************************\
* (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include <MVADictTools/HEPDroneTransform.h>

#include <alloca.h>
#include <fstream>
#include <math.h>
#include <sstream>
#include <string>
#include <utility>
#include <vector>

#include "Utils.h"

bool HEPDroneTransform::Init( const optmap& options, std::ostream& info, const bool debug ) {
  m_debug         = debug;
  m_info          = &info;
  m_setup_success = parseOpts( options, info );
  if ( !m_setup_success ) { return false; }

  readDroneFile( info );
  if ( !m_setup_success ) { return false; }

  return true;
}

bool HEPDroneTransform::operator()( const DICT& in, DICT& out ) const {
  // Check initialization
  if ( !m_setup_success ) { return false; }

  Tensor t_in, t_out;
  t_in                  = dictInputToTensor( in );         // construct an input tensor from inputs
  bool response_success = m_model->Apply( &t_in, &t_out ); // get mva response
  if ( !response_success ) return false;
  if ( m_keep_all_vars ) {
    out = in;
  } else {
    out.clear();
  }
  // convert output tensor to vector of responses
  std::vector<std::pair<std::string, double>> responses = tensorOutToVec( t_out );
  for ( auto const& response : responses ) { out.insert( out.end(), m_name + "_" + response.first, response.second ); }
  return true;
}

bool HEPDroneTransform::checkDroneFile( std::ostream& info ) {
  std::ifstream file_in( resolveEnv( m_drone_file ) );
  // Check existence of WeightFile: locally
  if ( file_in.good() ) { return true; }
  file_in.close();
  // else ERROR
  if ( m_drone_file == "" ) {
    info << "ERROR  ";
    info << "JSON file not given." << std::endl;
  } else {
    info << "JSON file \"" << m_drone_file << "\" not found." << std::endl;
    info << "  HEPDroneTool will not be run.  The output will be 0 for each event." << std::endl;
  }
  return false;
}

void HEPDroneTransform::readDroneFile( std::ostream& info ) {
  m_setup_success = false;
  if ( !checkDroneFile( info ) ) { return; }
  if ( m_json ) {
    std::ifstream json_file( resolveEnv( m_drone_file ) );
    m_config        = lwt::parse_json( json_file );
    m_setup_success = m_model->LoadJSONModel( m_config );
  } else {
    info << "WARNING: Dangerous configuration!!!\n";
    info << "WARNING: Only do this if you know what you are doing!!!\n";
    info << "WARNING: Your input variables will probably be read out of order!!!\n";
    info << "WARNING: Ordering will be alphabetical. Test the response thorouglhy!!!" << std::endl;
    m_setup_success = m_model->LoadModel( resolveEnv( m_drone_file ) );
  }
  return;
}

bool HEPDroneTransform::parseOpts( const optmap& options, std::ostream& info ) {
  bool    pass = true;
  Options parse( options );
  parse.add<std::string>( "Name", "Name of output branch (Required)", m_name, info );
  parse.add<std::string>( "HEPDroneFile", "File with HEPDrone Model", m_drone_file, info );
  parse.add<bool>( "KeepVars", "Keep input variables, \"1\" or \"0\"", m_keep_all_vars, info, false );
  parse.add<bool>( "JSONConfig", "Using JSON or HD5 config, \"1\" or \"0\"", m_json, info, true );
  pass = parse.check( info );
  return pass;
}

Tensor HEPDroneTransform::dictInputToTensor( const DICT& in ) const {
  // Need to preserve order of inputs to MVA when known
  // Currently only JSON config is supported
  // Other config is unsafe and inputs are read in alphabetical order
  Tensor t_in;
  t_in.dims_ = {static_cast<int>( in.size() )}; // resize Tensor
  std::vector<float> temp( in.size() );         // assume configured inputs size match, error out otherwise
  for ( const auto& input : in ) {
    // find input scale and offset from config
    double scale( 1.0 );
    double offset( 0.0 );
    if ( m_json ) {
      if ( in.size() != m_config.inputs.size() ) {
        std::string problem = "Number of given inputs (" + std::to_string( in.size() ) +
                              ") does not match number of configured inputs(" +
                              std::to_string( m_config.inputs.size() ) + ")";
        throw std::logic_error( problem );
      }
      size_t index = 0;                                // not int for safety
      for ( const auto& nn_input : m_config.inputs ) { // find matching input name
        index++;                                       // counts from 1
        if ( nn_input.name == input.first ) {
          scale  = nn_input.scale;
          offset = nn_input.offset;
          index--; // decrement on success
          break;
        } else if ( index == m_config.inputs.size() ) {
          std::string problem = std::string( "None of the given inputs matches to " ) +
                                "any of the configured inputs by name. " + "Please check your configuration";
          throw std::logic_error( problem );
        }
      }
      temp[index] = ( input.second + offset ) * scale;
    } else {
      temp.push_back( ( input.second + offset ) * scale ); // read in sequentially, alphabetically
    }
  }
  t_in.data_ = temp;
  return t_in;
}

std::vector<std::pair<std::string, double>> HEPDroneTransform::tensorOutToVec( const Tensor& in ) const {
  std::vector<std::pair<std::string, double>> out_vars;
  if ( m_json ) {
    for ( size_t i = 0; i < in.data_.size(); ++i ) {
      if ( i >= m_config.outputs.size() ) {
        std::string problem = std::string( "ERROR: Output tensor is bigger " ) + "than number of configured outputs! " +
                              "Please check your configuration!";
        throw std::logic_error( problem );
      }
      out_vars.push_back( std::make_pair( m_config.outputs[i], in.data_[i] ) );
    }
  } else {
    for ( size_t i = 0; i < in.data_.size(); ++i ) {
      std::stringstream i_;
      i_ << i;
      out_vars.push_back( std::make_pair( i_.str(), in.data_[i] ) );
    }
  }
  return out_vars;
}
