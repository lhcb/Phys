/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

//-----------------------------------------------------------------------------
/** @file ChargedProtoParticleDLLFilter.h
 *
 * Header file for algorithm ChargedProtoParticleDLLFilter
 *
 * @author Chris Jones   Christopher.Rob.Jones@cern.ch
 * @date 2006-05-03
 */
//-----------------------------------------------------------------------------

#ifndef PROTOPARTICLEFILTER_ChargedProtoParticleDLLFilter_H
#define PROTOPARTICLEFILTER_ChargedProtoParticleDLLFilter_H 1

// base class
#include "ProtoParticleANNPIDFilter.h"

// Interfaces
#include "Kernel/IProtoParticleFilter.h"
#include "TrackInterfaces/ITrackSelector.h"

//-----------------------------------------------------------------------------
/** @class ChargedProtoParticleDLLFilter ChargedProtoParticleDLLFilter.h
 *
 *  Tool which extends ProtoParticleFilters to add charged track selection
 *  criteria
 *
 *  @author Chris Jones   Christoper.Rob.Jones@cern.ch
 *  @date   2006-05-03
 */
//-----------------------------------------------------------------------------

class ChargedProtoParticleDLLFilter : public ProtoParticleANNPIDFilter, virtual public IProtoParticleFilter {

public: // Core Gaudi methods
  /// Standard constructor
  ChargedProtoParticleDLLFilter( const std::string& type, const std::string& name, const IInterface* parent );

  virtual ~ChargedProtoParticleDLLFilter(); ///< Destructor

  /// Initialisation
  StatusCode initialize() override;

public: // tool interface methods
  /// Test if filter is satisfied.
  bool isSatisfied( const LHCb::ProtoParticle* const& proto ) const override;

protected:
  // Create a cut object from decoded cut options
  const ProtoParticleSelection::Cut* createCut( const std::string& tag, const std::string& delim,
                                                const std::string& value ) const override;

private:
  /// Track selector tool
  ITrackSelector* m_trSel;
};

#endif // PROTOPARTICLEFILTER_ChargedProtoParticleDLLFilter_H
