/*****************************************************************************\
* (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef RESTORECALORECOCHAIN_H
#define RESTORECALORECOCHAIN_H 1

// Include files
// from DaVinci.
#include "CaloDAQ/ICaloDataProvider.h"
#include "CaloDet/DeCalorimeter.h"
#include "CaloInterfaces/ICounterLevel.h"
#include "CaloUtils/CaloParticle.h"
#include "Event/Particle.h"
#include "Event/ProtoParticle.h"
#include "GaudiKernel/IRndmGenSvc.h"
#include "GaudiKernel/RndmGenerators.h"
#include "Kernel/DaVinciAlgorithm.h"
#include <memory>
// CaloInterfaces
#include "CaloDAQ/ICaloDigitFilterTool.h"
#include "CaloDet/DeCalorimeter.h"
#include "CaloInterfaces/ICaloClusterTool.h"
#include "CaloInterfaces/ICaloHypo2Calo.h"
#include "CaloInterfaces/ICaloHypoEstimator.h"
#include "CaloInterfaces/ICaloHypoTool.h"
#include "CaloInterfaces/IGammaPi0SeparationTool.h"
#include "CaloInterfaces/INeutralIDTool.h"
#include "L0Interfaces/IL0DUFromRawTool.h"
//#include "Event/RawEvent.h"
struct IDVAlgorithm;

/** @class RestoreCaloRecoChain RestoreCaloRecoChain.h Extras/RestoreCaloRecoChain.h
 *
 * Connect stripped candidate with low-level persist-calo data (or rawBank re-processing)
 * [ Particle->ProtoParticle->CaloHypo ]  ==> [ CaloCluster->CaloDigits-> CaloADCs]
 * Propagate back any calo re-calibration to the up to the decay-tree head.
 *
 * algorithm configuration depends on InputType (DST/MDST) and persistified calo data (persist-calo/rawBank)
 * see https://its.cern.ch/jira/browse/LHCBPS-1750 for detail
 *
 *  @author Olivier Deschamps
 *  @date   2017-10-08
 */

typedef std::pair<const LHCb::Particle*, const LHCb::VertexBase*>              CALOBRANCH;
typedef std::vector<std::pair<const LHCb::Particle*, const LHCb::VertexBase*>> CALOTREE;

class RestoreCaloRecoChain : public DaVinciAlgorithm {
public:
  /// Standard constructor
  RestoreCaloRecoChain( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode initialize() override; ///< Algorithm initialization
  StatusCode execute() override;    ///< Algorithm execution

protected:
  bool isPureNeutralCalo( const LHCb::Particle* P ) {
    if ( !P ) return false;
    LHCb::CaloParticle caloP( (LHCb::Particle*)P );
    return caloP.isPureNeutralCalo();
  }

private:
  bool                 resumTree( const LHCb::Particle* p, bool head = false );
  bool                 refitTree( const LHCb::Particle* p );
  bool                 copyTree( const LHCb::Particle* p );
  CALOTREE             getCaloTree( const LHCb::Particle* p, const LHCb::Particle* m = nullptr );
  LHCb::CaloCluster*   getCluster( const LHCb::CaloHypo* hypo );
  const LHCb::CaloAdc* getAdc( const LHCb::CaloCellID id );
  double               getPileUp( const LHCb::CaloCellID cellID, bool converted );
  // store calo objects
  bool updatePart( LHCb::Particle* p );
  bool updateCalo( CALOBRANCH calo );
  bool updateHypo( LHCb::CaloHypo* h );
  bool updateCluster( LHCb::CaloCluster* cluster, std::string type = "default" );
  bool updateDigit( LHCb::CaloDigit* digit );
  bool updateExtraDigits( LHCb::CaloHypo* hypo, std::string type = "default" );
  bool updateProtoInfo( LHCb::ProtoParticle* proto );
  void resetProtoInfo( LHCb::ProtoParticle* proto, LHCb::ProtoParticle::additionalInfo key,
                       const CaloDataType::DataType hflag, const double def = CaloDataType::Default,
                       const bool force = false ) const;
  void resetProtoInfo( LHCb::ProtoParticle* proto, LHCb::ProtoParticle::additionalInfo key, const double data,
                       const double def = CaloDataType::Default, const bool force = false ) const;

  // post-calibration
  double getPostCalib( const LHCb::Particle* p );
  bool   applyPostCalib( const LHCb::Particle* p );
  // using rawBanks
  bool checkRawBank();
  bool checkProto( const LHCb::ProtoParticle* proto );

private:
  // TES pathes
  std::string m_locEcalAdcs;
  std::string m_locPrsAdcs;
  std::string m_locEcalDigits;
  std::string m_locPrsDigits;
  std::string m_locSpdDigits;

  // TES containers
  LHCb::CaloAdcs*   m_eadcs   = nullptr;
  LHCb::CaloAdcs*   m_padcs   = nullptr;
  LHCb::CaloDigits* m_pdigits = nullptr;
  LHCb::CaloDigits* m_sdigits = nullptr;

  // tools
  ICaloClusterTool*           m_covar     = nullptr;
  ICaloClusterTool*           m_spread    = nullptr;
  ICaloHypo2Calo*             m_toPrs     = nullptr;
  ICaloHypo2Calo*             m_toSpd     = nullptr;
  ICaloDigitFilterTool*       m_pileup    = nullptr;
  IL0DUFromRawTool*           m_l0Raw     = nullptr;
  ICaloHypoEstimator*         m_estimator = nullptr;
  IGammaPi0SeparationTool*    m_gammaPi0  = nullptr;
  INeutralIDTool*             m_neutralID = nullptr;
  std::vector<ICaloHypoTool*> m_corrP;
  std::vector<ICaloHypoTool*> m_corrS;

  // DetectorElement
  DeCalorimeter* m_ecalDet = nullptr;
  DeCalorimeter* m_prsDet  = nullptr;
  DeCalorimeter* m_spdDet  = nullptr;

  // properties
  SmartIF<IRndmGenSvc> m_rndmSvc;
  SmartIF<IRndmGenSvc> rndmSvc() const { return m_rndmSvc; }
  ICounterLevel*       counterStat = nullptr;
  std::string          m_prefix;
  bool                 m_verb   = false;
  bool                 m_refit  = true;
  int                  m_update = 5;
  long                 m_seed   = 0;
  // post-calibration
  std::map<int, double> m_calib;
  std::map<int, double> m_calibCNV;
  double                m_smear           = 0;
  bool                  m_uSpd            = false;
  bool                  m_postCalib       = false;
  bool                  m_useRaw          = false;
  bool                  m_updateNeutralID = true;
  bool                  m_useMotherVertex = true;
};
#endif // RESTORECALORECOCHAIN_H
