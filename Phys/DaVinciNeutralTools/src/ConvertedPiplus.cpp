/*****************************************************************************\
* (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id$
// Include files

// local
#include "ConvertedPiplus.h"

// GSL
#include "gsl/gsl_math.h"

//-----------------------------------------------------------------------------
// Implementation file for class : ConvertedPiplus
//
// 2023-02-21 : Daniel Johnson
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( ConvertedPiplus )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
ConvertedPiplus::ConvertedPiplus( const std::string& name, ISvcLocator* pSvcLocator )
    : DaVinciAlgorithm( name, pSvcLocator )
    , m_changePIDTool( 0 )
    , m_nEvents( 0 )
    , m_nAccepted( 0 )
    , m_nCandidates( 0 ) {}

//=============================================================================
// Destructor
//=============================================================================
ConvertedPiplus::~ConvertedPiplus() {}

//=============================================================================
// Initialization
//=============================================================================
StatusCode ConvertedPiplus::initialize() {
  const StatusCode sc = DaVinciAlgorithm::initialize();
  if ( sc.isFailure() ) return sc;

  debug() << "==> Initialize" << endmsg;

  m_changePIDTool = tool<IChangePIDTool>( "NeutralCCChangePIDTool", this );

  return sc;
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode ConvertedPiplus::execute() {

  debug() << "==> Executing ConvertedPiplus" << endmsg;
  ++m_nEvents; // Increment event counter

  const LHCb::Particle::Range pions = particles();
  StatusCode                  sc    = loopOnPions( pions );
  if ( !sc ) return sc;

  setFilterPassed( true ); // Set to true if event is accepted.
  return StatusCode::SUCCESS;
}

//====================================================
// loop on pions
//====================================================
StatusCode ConvertedPiplus::loopOnPions( const LHCb::Particle::Range& pions ) {
  StatusCode sc = StatusCode::SUCCESS;

  // code goes here
  LHCb::Particle::Range::const_iterator m    = pions.begin();
  LHCb::Particle::Range::const_iterator mEnd = pions.end();

  // Get random numbers generator (flat)
  auto          randSvc = svc<IRndmGenSvc>( "RndmGenSvc", true );
  Rndm::Numbers m_uniformDist;
  if ( !m_uniformDist.initialize( randSvc, Rndm::Flat( 0, 1. ) ) )
    return Error( "Failed to initialize Flat Random distribution!" );

  for ( ; m != mEnd; ++m ) {
    if ( abs( ( *m )->particleID().pid() ) != 211 ) {
      return Error( "This tool is only intended for use with pi+/- as parents. Found " +
                    std::to_string( ( *m )->particleID().pid() ) );
    }

    const Gaudi::LorentzVector lv_pion( ( *m )->momentum() );

    // Generate photon in pi+ rest frame
    const double e_photon = lv_pion.M() / 2.;
    debug() << "Fake photon energy : " << e_photon << endmsg;
    // Rotate it
    const double decayAngle_phi   = 2 * Gaudi::Units::pi * m_uniformDist();
    const double decayAngle_theta = Gaudi::Units::pi * m_uniformDist();
    const double z                = cos( decayAngle_theta );
    const double x                = cos( decayAngle_phi ) * sqrt( ( 1 - z * z ) );
    const double y                = sin( decayAngle_phi ) * sqrt( ( 1 - z * z ) );

    std::vector<int> signs{1, -1};

    // Now construct two photons, initially back-to-back in pi+ rest-frame, and boost into lab frame before saving
    for ( auto sign : signs ) {
      Gaudi::LorentzVector lv_photon( sign * e_photon * x, sign * e_photon * y, sign * e_photon * z, e_photon );

      ROOT::Math::Boost boostToLab( lv_pion.Vect() / lv_pion.E() );

      debug() << lv_photon << endmsg;
      debug() << lv_pion << endmsg;
      debug() << lv_pion.Vect() << endmsg;
      const Gaudi::LorentzVector lv_photon_boosted = boostToLab( lv_photon );

      debug() << lv_photon_boosted << endmsg;

      // Now prepare the LHCb particle
      LHCb::Particle newPhoton( LHCb::ParticleID( 22 ) );
      newPhoton.setMomentum( lv_photon_boosted );
      newPhoton.setMomCovMatrix( ( *m )->momCovMatrix() );
      newPhoton.setPosCovMatrix( ( *m )->posCovMatrix() );
      // Save the photon
      this->markTree( &newPhoton );
    }
  }
  return sc;
}
//=============================================================================
//  Finalize
//=============================================================================
StatusCode ConvertedPiplus::finalize() {
  info() << "Passed " << m_nCandidates << " candidates in " << m_nAccepted << " accepted events among " << m_nEvents
         << " events" << endmsg;
  return DaVinciAlgorithm::finalize();
}

//=============================================================================
