/*****************************************************************************\
* (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "RestoreCaloRecoChain.h"
#include "CaloUtils/CaloAlgUtils.h"
#include "CaloUtils/CaloParticle.h"
#include "GaudiKernel/IRndmEngine.h"
#include "GaudiKernel/RndmGenerators.h"
//-----------------------------------------------------------------------------
// Implementation file for class : RestoreCaloRecoChain
//
//
// 2017-10-08 : Olivier Deschamps
// 2023-01-27 : Jiahui Zhuo - Force random seeds
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( RestoreCaloRecoChain )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
RestoreCaloRecoChain::RestoreCaloRecoChain( const std::string& name, ISvcLocator* pSvcLocator )
    : DaVinciAlgorithm( name, pSvcLocator ) {
  declareProperty( "TESprefix", m_prefix = "" );
  declareProperty( "Verbose", m_verb = false ); // activate verbose printing
  declareProperty( "UpdateLevel", m_update = 5 );
  // UpdateLevel  = 5  : re-evaluate (Ecal+Prs) digits => cluster => hypo => particle(s) and refit tree (e.g.
  // recalibration)
  //                4  : re-evaluate  Ecal      digits => cluster => hypo => particle(s) and refit tree
  //                3  : re-evaluate       Prs  digits            => hypo => particle(s) and refit tree
  //                2  : re-evaluate                                 hypo => particle(s) and refit tree
  //                1  : re-evaluate                                 (calo)  particle(s) and refit tree (e.g.
  //                post-calibration) 0  : re-fit tree only
  //               -1  : no change - just  re-activate and check the calo reco chain completness
  declareProperty( "RefitDecayTree", m_refit = true ); // refit decay tree or simply add momenta (+ covariance)
  declareProperty( "UpdateNeutralID", m_updateNeutralID = false ); // some shape variables are truncated on persist-calo
                                                                   // - require whole cluster storage
  // === Post-calibration - for SPECIFIC usage only ===
  // e.g. for high-energy photons in radiative decays analysis
  declareProperty( "Calib", m_calib );           // post-calib coefs (unconverted photons)
  declareProperty( "CalibCNV", m_calibCNV );     // post-calib coefs (converted photons)
  declareProperty( "Smear", m_smear = 0 );       // Smear photon energy for MC to reproduce data
  declareProperty( "SpdScale", m_uSpd = false ); // calib defined as per-1000 deviation per-100 SpdHits (i.e. calib = 1
                                                 // + coeff x SpdHits * 10^-5)
  declareProperty( "Seed", m_seed = 0 );         // Random seed
  declareProperty( "UseMotherVertex",
                   m_useMotherVertex = true ); // to deactivate photon origin in case mother vertex is not physical
                                               // (e.g. when using ParticleAdder as combiner)
}
//=============================================================================
// Initialization
//=============================================================================
StatusCode RestoreCaloRecoChain::initialize() {
  const auto sc = DaVinciAlgorithm::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;                // error printed already by GaudiAlgorithm
  if ( msgLevel( MSG::DEBUG ) ) debug() << "==> Initialize" << endmsg;
  counterStat = tool<ICounterLevel>( "CounterLevel" );

  // storage TES naming
  m_locEcalAdcs   = m_prefix + "/" + LHCb::CaloAdcLocation::Ecal;
  m_locPrsAdcs    = m_prefix + "/" + LHCb::CaloAdcLocation::Prs;
  m_locEcalDigits = m_prefix + "/" + LHCb::CaloDigitLocation::Ecal;
  m_locPrsDigits  = m_prefix + "/" + LHCb::CaloDigitLocation::Prs;
  m_locSpdDigits  = m_prefix + "/" + LHCb::CaloDigitLocation::Spd;

  // Detector elements
  m_ecalDet = getDet<DeCalorimeter>( LHCb::CaloAlgUtils::DeCaloLocation( "Ecal" ) );
  m_prsDet  = getDet<DeCalorimeter>( LHCb::CaloAlgUtils::DeCaloLocation( "Prs" ) );
  m_spdDet  = getDet<DeCalorimeter>( LHCb::CaloAlgUtils::DeCaloLocation( "Spd" ) );

  // Cluster tools
  m_covar     = tool<ICaloClusterTool>( "ClusterCovarianceMatrixTool", "EcalCovarTool", this );
  m_spread    = tool<ICaloClusterTool>( "ClusterSpreadTool", "EcalSpreadTool", this );
  m_pileup    = tool<ICaloDigitFilterTool>( "CaloDigitFilterTool", "FilterTool" );
  m_l0Raw     = tool<IL0DUFromRawTool>( "L0DUFromRawTool", "L0DUFromRawTool", this );
  m_estimator = tool<ICaloHypoEstimator>( "CaloHypoEstimator", "CaloHypoEstimator", this );
  m_estimator->hypo2Calo()->_setProperty( "Seed", "false" ).ignore();
  m_estimator->hypo2Calo()->_setProperty( "PhotonLine", "true" ).ignore();
  m_estimator->hypo2Calo()->_setProperty( "AddNeighbors", "false" ).ignore();
  m_estimator->_setProperty( "SkipNeutralID", "true" ).ignore(); // (obsolete) photonID cannot be updated in DV
  m_gammaPi0  = tool<IGammaPi0SeparationTool>( "GammaPi0SeparationTool", "GammaPi0SeparationTool", this );
  m_neutralID = tool<INeutralIDTool>( "NeutralIDTool", "NeutralIDTool", this );
  m_l0Raw->fillDataMap();

  // Hypo tools
  m_toSpd = tool<ICaloHypo2Calo>( "CaloHypo2Calo", "CaloHypo2Spd", this );
  m_toPrs = tool<ICaloHypo2Calo>( "CaloHypo2Calo", "CaloHypo2Prs", this );
  m_toSpd->setCalos( "Ecal", "Spd" );
  m_toPrs->setCalos( "Ecal", "Prs" );
  m_corrP.push_back( tool<ICaloHypoTool>( "CaloECorrection", "PhotonEcorrection", this ) ); // the name must contains
                                                                                            // 'Photon'
  m_corrP.push_back( tool<ICaloHypoTool>( "CaloSCorrection", "PhotonScorrection", this ) );
  m_corrP.push_back( tool<ICaloHypoTool>( "CaloLCorrection", "PhotonLcorrection", this ) );
  m_corrS.push_back( tool<ICaloHypoTool>( "CaloECorrection", "SplitPhotonEcorrection",
                                          this ) ); // the name must contains 'SplitPhoton'
  m_corrS.push_back( tool<ICaloHypoTool>( "CaloSCorrection", "SplitPhotonScorrection", this ) );
  m_corrS.push_back( tool<ICaloHypoTool>( "CaloLCorrection", "SplitPhotonLcorrection", this ) );

  // Post-calibration setting
  m_postCalib = ( m_calib.size() != 0 || m_calibCNV.size() != 0 || m_smear != 0. );
  if ( m_update < 1 && m_postCalib ) {
    Warning( "Requested post-Calibration requires update level >= 1", StatusCode::SUCCESS ).ignore();
    m_postCalib = false;
  }
  if ( m_postCalib ) {
    if ( m_calibCNV.size() == 0 && m_calib.size() != 0 ) m_calibCNV = m_calib;
    info() << " ==== APPLY ECAL POST-CALIBRATION TO SINGLE PHOTONS and MOTHER === " << endmsg;
    info() << " ====   - Calib coefs (noConv) : " << m_calib << endmsg;
    info() << " ====   - Calib coefs (conv) : " << m_calibCNV << endmsg;
    info() << " ====   - Energy smearing : " << m_smear << endmsg;
    info() << " +++  PROPAGATE TO CALOHYPO (For DTF)" << endmsg;
    if ( m_uSpd ) info() << " +++  Apply SpdMult-dependent calibration" << endmsg;
  }

  // Svc
  m_rndmSvc = svc<IRndmGenSvc>( "RndmGenSvc", true );

  // Force seed
  m_rndmSvc->engine()->setSeeds( {m_seed} ).ignore();

  return sc;
}

//========================================
StatusCode RestoreCaloRecoChain::execute() {
  if ( msgLevel( MSG::DEBUG ) ) debug() << "==> Execute" << endmsg;
  // Input particles
  const LHCb::Particle::Range parts = particles();
  if ( m_verb )
    info() << " === Found " << parts.size() << " particles in the input container(s) " << inputLocations() << endmsg;

  setFilterPassed( !parts.empty() );
  if ( parts.empty() ) return StatusCode::SUCCESS;

  // get ecal/prs ADCs & prs/spd extra-digits
  m_padcs   = getIfExists<LHCb::CaloAdcs>( m_locPrsAdcs );
  m_eadcs   = getIfExists<LHCb::CaloAdcs>( m_locEcalAdcs );
  m_sdigits = getIfExists<LHCb::CaloDigits>( m_locSpdDigits );
  m_pdigits = getIfExists<LHCb::CaloDigits>( m_locPrsDigits );
  if ( counterStat->isVerbose() ) {
    if ( m_eadcs ) counter( "Cont. EcalAdc" ) += m_eadcs->size();
    if ( m_padcs ) counter( "Cont. PrsAdc" ) += m_padcs->size();
    if ( m_pdigits ) counter( "Cont. PrsDig" ) += m_pdigits->size();
    if ( m_sdigits ) counter( "Cont. SpdDig" ) += m_sdigits->size();
  }
  // check persist-reco (ecal) chain is there
  bool hasEcalData = ( m_eadcs != nullptr );
  m_useRaw         = ( !hasEcalData ) ? checkRawBank() : false; // if no persist-calo check for calo rawBanks
  if ( counterStat->isQuiet() ) counter( "Calo data available" ) += hasEcalData || m_useRaw;
  if ( !hasEcalData && !m_useRaw && m_update > 1 )
    return Warning( "Neither Persist-calo nor RawBank data are found", StatusCode::SUCCESS, 1 );
  if ( hasEcalData ) counter( "Using Persist-calo" ) += hasEcalData;
  if ( m_useRaw ) counter( "Using RawBank" ) += m_useRaw;
  if ( hasEcalData && m_updateNeutralID ) {
    Warning( "neutralPID update requires RawBank ", StatusCode::SUCCESS, 1 ).ignore();
    m_updateNeutralID = false;
  }

  // L0DU decoder to extract SpdMult (needed for Pile-up subtraction re-evaluation)
  if ( m_update > 1 ) m_l0Raw->decodeBank(); //

  // loop over inputs particles and propagate the update
  for ( const auto part : parts ) { updatePart( (LHCb::Particle*)part ); }
  return StatusCode::SUCCESS;
}

// ========================== //
// ==== UPDATE particle  ==== //
// ========================== //
bool RestoreCaloRecoChain::updatePart( LHCb::Particle* part ) {
  if ( !part ) return false;
  // collect calo particles in the tree
  if ( m_verb ) info() << " == Particle : " << part->particleID().pid() << endmsg;
  auto calotree = getCaloTree( part );
  if ( m_verb ) info() << " => found " << calotree.size() << " neutral calo particles in the descendants" << endmsg;
  if ( counterStat->isQuiet() ) counter( "CaloParticle in decay tree" ) += calotree.size();

  // loop over tree particles and update calo
  bool ok = true;
  for ( const auto& p : calotree ) {
    if ( m_verb ) info() << "  = Calo Particle : " << ( p.first )->particleID().pid() << endmsg;
    ok = ok && updateCalo( p );
  }

  // Update the decay tree (reFit or reSum)
  if ( m_update >= 0 && calotree.size() != 0 ) {
    double pre  = part->momentum().P();
    ok          = ok && ( m_refit ) ? refitTree( part ) : resumTree( part, true );
    double post = part->momentum().P();
    // reset CL
    if ( counterStat->isQuiet() )
      counter( "Refit DeltaP(" + ppSvc()->find( part->particleID() )->name() + ")/P" ) += ( post - pre ) / pre;
  }
  return ok;
}

// ============================== //
// ==== UPDATE CALO Particle ==== //
// ============================== //

bool RestoreCaloRecoChain::updateCalo( CALOBRANCH calo ) {
  LHCb::Particle*    p       = (LHCb::Particle*)calo.first;
  LHCb::CaloParticle temp    = LHCb::CaloParticle( p );
  const auto&        endTree = temp.caloEndTree();
  bool               ok      = true;
  double             CL      = 1.;
  for ( const auto* e : endTree ) {
    const LHCb::ProtoParticle* proto = e->proto();
    if ( !proto ) continue; // should never occur
    // try to reconnect proto->hypo using reco from rawBank - if successful no need to (re)update a priori (but let's
    // do)
    checkProto( proto );
    // browse down the persist-reco chain
    const LHCb::CaloHypo* hypo = proto->calo().front();
    ok                         = ok && updateHypo( (LHCb::CaloHypo*)hypo );
    if ( ok && m_update > 1 ) {
      updateProtoInfo( (LHCb::ProtoParticle*)proto );
      if ( m_updateNeutralID ) {
        if ( hypo->hypothesis() == LHCb::CaloHypo::Pi0Merged ) CL *= proto->info( LHCb::ProtoParticle::IsPhoton, +1. );
        if ( hypo->hypothesis() == LHCb::CaloHypo::Photon ) CL *= proto->info( LHCb::ProtoParticle::IsNotH, 0. );
        p->setConfLevel( CL );
      }
    }
  }

  if ( m_update > 0 ) {
    if ( counterStat->isVerbose() ) counter( "update CaloParticle" ) += 1;
    double             pre   = p->momentum().P();
    LHCb::CaloParticle calop = LHCb::CaloParticle( p ); // re-evaluate caloParticle after update !
    if ( calo.second && m_useMotherVertex ) calop.setReferencePoint( calo.second );
    calop.updateTree();
    if ( m_postCalib ) applyPostCalib( p ); // only single photon so far
    double      post = p->momentum().P();
    std::string type = ppSvc()->find( p->particleID() )->name();
    if ( p->particleID().pid() == 111 ) type += ( p->proto() ) ? "[M]" : "[R]";
    if ( counterStat->isVerbose() ) counter( "Updated DeltaP(" + type + ")/P" ) += ( post - pre ) / pre;
  }

  return ok;
}

// ========================= //
// ==== UPDATE CALOHYPO ==== //
// ========================= //
bool RestoreCaloRecoChain::updateHypo( LHCb::CaloHypo* hypo ) {
  if ( !hypo ) return false;

  bool ok = true;

  // ===>  merged pi0 => update split-photon hypo <=== //
  if ( hypo->hypothesis() == LHCb::CaloHypo::Pi0Merged ) {
    // update the associated cluster
    ok = updateCluster( getCluster( hypo ), "Pi0Merged" );
    updateExtraDigits( hypo, "Pi0Merged" );
    // update the splitPhotons
    const auto& hypos = hypo->hypos();
    for ( const LHCb::CaloHypo* split : hypos ) { ok = ok && updateHypo( (LHCb::CaloHypo*)split ); }
    // mergedPi0 have no kinematical parameters by itself return
    return ok;
  }

  // ===> Photon & PhotonFromMergedPi0 <=== //
  std::ostringstream otype( "" );
  otype << hypo->hypothesis();
  std::string type = otype.str();
  if ( hypo->hypothesis() != LHCb::CaloHypo::Photon && hypo->hypothesis() != LHCb::CaloHypo::PhotonFromMergedPi0 ) {
    Warning( "CaloHypo '" + type + "' is not supported", StatusCode::SUCCESS, 10 ).ignore();
    return false;
  }

  // 1- update the cluster & extra-digits
  auto* cluster = getCluster( hypo );
  if ( !cluster ) return false;
  ok          = updateCluster( cluster, type );
  bool hasSpd = updateExtraDigits( hypo, type );

  // 2 - update the CaloHypo when requested
  if ( m_update > 1 ) {
    hypo->setLh( 0. ); // (ab)use likelihood parameter for post-calibration
    if ( counterStat->isVerbose() ) counter( "update CaloHypo" ) += 1;
    double pre    = hypo->e(); // keep original hypo energy
    double energy = cluster->e();
    hypo->setPosition( std::make_unique<LHCb::CaloPosition>( cluster->position() ) ); // init CaloHypo::CaloPosition to
                                                                                      // cluster
    double pileup = getPileUp( cluster->seed(), hasSpd );
    if ( pileup > 0 && pileup < energy )
      cluster->position().parameters()( LHCb::CaloPosition::E ) = energy - pileup; // substr. pileup
    // apply E/S/L correction tools (ordered)
    const std::vector<ICaloHypoTool*>& tools = ( hypo->hypothesis() == LHCb::CaloHypo::Photon ) ? m_corrP : m_corrS;
    for ( auto* tool : tools ) { tool->process( hypo ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ ); }
    double post = hypo->e();
    if ( pileup > 0. && pileup < energy )
      cluster->position().parameters()( LHCb::CaloPosition::E ) = energy; // restore cluster energy
    if ( counterStat->isVerbose() ) counter( "DeltaE(Hypo " + type + ")" ) += ( post - pre );
  }
  return ok;
}

// ============================== //
// ==== UPDATE EXTRA-DIGITS ===== //
// ============================== //
bool RestoreCaloRecoChain::updateExtraDigits( LHCb::CaloHypo* hypo, std::string type ) {

  unsigned int nPrs = 0;
  unsigned int nSpd = 0;
  if ( ( hypo->digits() ).size() == 0 ) { // either no ExtraDigit or they are missing - try to re-create them
    std::vector<LHCb::CaloDigit*> pdigits;
    std::vector<LHCb::CaloDigit*> sdigits;
    if ( m_pdigits && m_pdigits->size() != 0 ) pdigits = m_toPrs->digits( *hypo, "Prs" );
    if ( m_sdigits && m_sdigits->size() != 0 ) sdigits = m_toSpd->digits( *hypo, "Spd" );
    nPrs = pdigits.size();
    nSpd = sdigits.size();
    hypo->digits().clear();
    if ( nSpd != 0 || nPrs != 0 ) counter( "Recreate extra-digit links" ) += nSpd + nPrs;
    for ( const auto* digit : pdigits ) {
      updateDigit( (LHCb::CaloDigit*)digit ); // for Prs only
      hypo->addToDigits( digit );
    }
    for ( const auto* digit : sdigits ) { hypo->addToDigits( digit ); }
  } else {
    for ( const LHCb::CaloDigit* digit : hypo->digits() ) {
      const LHCb::CaloCellID id = digit->cellID();
      if ( id.calo() == 0 )
        nSpd++;
      else if ( id.calo() == 1 ) {
        nPrs++;
        updateDigit( (LHCb::CaloDigit*)digit ); // for Prs only
      }
    }
  }
  if ( counterStat->isQuiet() ) counter( type + " has extra-digits " ) += nPrs + nSpd;
  if ( counterStat->isVerbose() ) counter( "Extra-digits (Prs) " + type ) += nPrs;
  if ( counterStat->isVerbose() ) counter( "Extra-digits (Spd) " + type ) += nSpd;
  return ( nSpd != 0 ); // flag converted photons
}

// ============================= //
// ==== UPDATE CALOCLUSTER ===== //
// ============================= //
bool RestoreCaloRecoChain::updateCluster( LHCb::CaloCluster* cluster, std::string type ) {

  if ( !cluster ) return false;

  // get cluster entries
  const auto&  entries  = cluster->entries();
  unsigned int nEntries = entries.size();
  unsigned int nDigits  = 0;
  unsigned int nAdcs    = 0;
  if ( entries.size() == 0 ) {
    counter( "Missing cluster Entries" ) += 1;
    if ( msgLevel( MSG::DEBUG ) )
      debug() << "Missing entries in " << cluster->parent()->registry()->identifier() << endmsg;
    return false;
  }
  for ( auto entry : entries ) {
    const LHCb::CaloDigit* dd = entry.digit();
    if ( !dd ) {
      counter( "Missing digit" ) += 1;
      continue;
    }
    nDigits++;
    nAdcs += updateDigit( (LHCb::CaloDigit*)dd );
  }

  bool isOK = ( nEntries == nDigits && nDigits == nAdcs && nEntries != 0 );
  if ( counterStat->isQuiet() ) counter( "CaloReco chain completed (" + type + ")" ) += isOK;

  // apply cluster estimators
  if ( isOK && m_update > 3 ) {
    double pre = cluster->e();
    m_covar->process( (LHCb::CaloCluster*)cluster ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    m_spread->process( (LHCb::CaloCluster*)cluster ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    double post = cluster->e();
    if ( counterStat->isVerbose() ) counter( "DeltaE(Cluster " + type + ")" ) += ( post - pre );
  }
  return true;
}

// ============================ //
// ==== UPDATE ECAL DIGITS ==== //
// ============================ //

bool RestoreCaloRecoChain::updateDigit( LHCb::CaloDigit* digit ) {

  if ( !digit ) return false;
  if ( m_useRaw ) return true; // digit are directly calibrated from raw decoding

  const LHCb::CaloCellID id  = digit->cellID();
  const auto*            adc = getAdc( id );
  if ( !adc ) {
    counter( "Missing " + id.caloName() + " ADC" ) += 1;
    return false;
  }

  DeCalorimeter* det = nullptr;

  if ( id.calo() == 2 )
    det = m_ecalDet;
  else if ( id.calo() == 1 )
    det = m_prsDet;
  else
    Warning( "cannot recalibrate " + id.caloName() + " digit", StatusCode::SUCCESS ).ignore();

  if ( !det ) return false;

  // recalibrate the digit
  if ( m_update == 5 || ( id.calo() == 2 && m_update == 4 ) || ( id.calo() == 1 && m_update == 3 ) ) {
    double energy = det->cellEnergy( adc->adc(), id );
    if ( counterStat->isVerbose() ) counter( "DeltaE(Digit " + id.caloName() + ")" ) += ( energy - digit->e() );
    digit->setE( energy );
  }

  return true;
}

// ============================ //
// @@@@ GET CALO PARTICLES @@@@ //
// ============================ //
CALOTREE RestoreCaloRecoChain::getCaloTree( const LHCb::Particle* p, const LHCb::Particle* m ) {
  CALOTREE tree;
  if ( p == nullptr ) return tree;
  if ( isPureNeutralCalo( p ) && p->daughters().size() == 0 ) {
    const LHCb::VertexBase* v  = ( m ) ? m->endVertex() : nullptr;
    CALOBRANCH              pp = std::make_pair( p, v );
    tree.push_back( pp );
  } else {
    for ( const auto& d : p->daughters() ) {
      for ( const auto& dd : getCaloTree( d, p ) ) { tree.push_back( dd ); }
    }
  }
  return tree;
}

// ===================== //
// @@@@ GET CLUSTER @@@@ //
// ===================== //
LHCb::CaloCluster* RestoreCaloRecoChain::getCluster( const LHCb::CaloHypo* hypo ) {
  // get cluster from smartref ...
  const LHCb::CaloCluster* cluster = nullptr;
  cluster                          = LHCb::CaloAlgUtils::ClusterFromHypo( hypo );
  if ( cluster ) return (LHCb::CaloCluster*)cluster;
  if ( counterStat->isQuiet() ) counter( "Missing cluster" ) += 1;
  Warning( "Calo reco chain has not been found", StatusCode::SUCCESS, 10 ).ignore();
  return (LHCb::CaloCluster*)cluster;
}

// ===================== //
// @@@@ GET ADC @@@@ //
// ===================== //
const LHCb::CaloAdc* RestoreCaloRecoChain::getAdc( const LHCb::CaloCellID id ) {
  if ( id.calo() == 2 && m_eadcs ) return m_eadcs->object( id );
  if ( id.calo() == 1 && m_padcs ) return m_padcs->object( id );
  return nullptr;
}

// =========================== //
// @@@@ GET PILEUP OFFSET @@@@ //
// =========================== //
double RestoreCaloRecoChain::getPileUp( const LHCb::CaloCellID id, bool converted ) {

  if ( m_pileup->method( "Ecal" ) == 10 ) {
    double ooffset = m_pileup->offset( id, converted );
    double oscale  = m_pileup->getScale();
    double scale   = m_l0Raw->data( "Spd(Mult)" );
    if ( scale != oscale ) {
      // rebuild the pileup subtraction
      if ( !m_ecalDet->valid( id ) || id.isPin() ) return 0.;
      double min  = m_ecalDet->pileUpSubstractionMin();
      double step = double( m_ecalDet->pileUpSubstractionBin() );
      if ( scale <= min ) return 0.;
      double       rscale = ( scale != 6016 ) ? scale / ( 1. - scale / 6016. ) : scale;
      const double aa     = 4. * scale / 6016.;
      if ( aa < 1 ) { rscale = 0.5 * 6016. * ( 1. - std::sqrt( 1. - aa ) ); }
      double                       bin    = double( rscale - min );
      const CaloVector<CellParam>& cells  = m_ecalDet->cellParams();
      double                       offset = bin / step * cells[id].pileUpOffset();
      return ( offset - ooffset > 0. ) ? offset - ooffset : 0.;
    }
  } else {
    Warning( "Pileup subtraction not implemented", StatusCode::SUCCESS, 10 ).ignore();
    return 0.;
  }
  return 0.;
}

// ================================ //
// ==== re-evaluate decay tree ==== //
// ================================ //
bool RestoreCaloRecoChain::resumTree( const LHCb::Particle* p, bool head ) {
  if ( p->isBasicParticle() ) return true;
  Gaudi::LorentzVector mom;
  Gaudi::SymMatrix4x4  cov;
  for ( const LHCb::Particle* d : p->daughters() ) {
    resumTree( d );
    mom += d->momentum();
    cov += d->momCovMatrix();
  }
  LHCb::Particle* part = (LHCb::Particle*)p;
  part->setMomentum( mom );
  part->setMomCovMatrix( cov );
  part->setMeasuredMass( part->momentum().M() ); // set the measured mass
  std::string type = ppSvc()->find( p->particleID() )->name();
  if ( head && counterStat->isQuiet() ) counter( "Resum " + type + " tree" ) += 1;
  return true;
}

// =========================== //
// ==== re-fit decay tree ==== //
// =========================== //
bool RestoreCaloRecoChain::refitTree( const LHCb::Particle* p ) {
  const LHCb::VertexBase* pv = bestVertex( p );
  bool                    ok = ( decayTreeFitter()->fit( p, pv ) ).isSuccess();
  if ( ok ) { ok = ok && copyTree( p ); }
  std::string type = ppSvc()->find( p->particleID() )->name();
  if ( counterStat->isQuiet() ) counter( "Refit " + type + " tree" ) += ok;
  if ( !ok ) {
    ok = resumTree( p );
    if ( counterStat->isQuiet() ) counter( "Refit failed - Resum " + type + " tree" ) += ok;
  }
  return ok;
}

// =================================== //
// ==== Reset Particle kinematics ==== //
// =================================== //
bool RestoreCaloRecoChain::copyTree( const LHCb::Particle* p ) {
  // ===== apply combinedParticles instead of DTF =====
  if ( p->daughters().empty() ) return true;
  if ( getCaloTree( p ).size() == 0 ) return true;
  bool ok = true;
  // reset the daughters first
  LHCb::Particle::ConstVector daugs;
  for ( const LHCb::Particle* d : p->daughters() ) {
    bool dok = copyTree( d );
    ok       = dok && ok;
    daugs.push_back( d );
  }
  LHCb::Vertex   rhsvtx;
  LHCb::Particle rhs( p->particleID() );
  particleCombiner()->combine( daugs, rhs, rhsvtx ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );

  LHCb::Particle* part = (LHCb::Particle*)p;
  // part->setMeasuredMass    (rhs.measuredMass()    );
  part->setMeasuredMass( rhs.momentum().M() ); // force MM = M
  part->setMeasuredMassErr( rhs.measuredMassErr() );
  part->setMomentum( rhs.momentum() );
  part->setReferencePoint( rhs.referencePoint() );
  part->setMomCovMatrix( rhs.momCovMatrix() );
  part->setPosCovMatrix( rhs.posCovMatrix() );
  part->setPosMomCovMatrix( rhs.posMomCovMatrix() );
  part->setWeight( rhs.weight() );
  LHCb::Vertex* v = (LHCb::Vertex*)part->endVertex();
  if ( nullptr == v ) return ok;
  v->setTechnique( rhsvtx.technique() );
  v->setChi2AndDoF( rhsvtx.chi2(), rhsvtx.nDoF() );
  v->setPosition( rhsvtx.position() );
  v->setCovMatrix( rhsvtx.covMatrix() );
  return ok;
}

// ========================================== //
// ==== get PostCalibration coefficient  ==== //
// ========================================== //
double RestoreCaloRecoChain::getPostCalib( const LHCb::Particle* p ) {
  if ( !p ) return 1.;
  const LHCb::ProtoParticle* proto = p->proto();
  if ( !proto ) return 1.;
  LHCb::CaloCellID id  = LHCb::CaloCellID( (int)proto->info( LHCb::ProtoParticle::CaloNeutralID, 0. ) );
  bool             cnv = proto->info( LHCb::ProtoParticle::CaloDepositID, 0. ) < 0;
  if ( cnv && m_calibCNV.empty() ) return 1;
  if ( !cnv && m_calib.empty() ) return 1.;
  double coeff = ( cnv ) ? m_calibCNV[id.area()] : m_calib[id.area()];

  // exctract post-calibration factor
  double calib = 1.;
  if ( m_uSpd ) {
    double scale = m_l0Raw->data( "Spd(Mult)" );
    calib        = 1 + coeff / 1000. * scale / 100.; // !!  Coeff is per-mil energy deviation per 100 SpdHits
  } else {
    calib = coeff;
  }

  // apply smearing when requested
  double smear = 1.;
  if ( m_smear > 0 ) {
    Rndm::Numbers shot( rndmSvc(), Rndm::Gauss( 0.0, m_smear ) );
    smear = 1. + shot();
    calib *= smear;
  }
  return calib;
}

// ================================= //
// ==== apply PostCalibration   ==== //
// ================================= //

bool RestoreCaloRecoChain::applyPostCalib( const LHCb::Particle* p ) {

  if ( !p->isBasicParticle() || !isPureNeutralCalo( p ) ) return true; // rescale only calorimetric objects
  if ( p->particleID().pid() != 22 ) return true; // only single photon so far (to be done for merged pi0 ?)
  bool            ok   = false;
  LHCb::CaloHypo* hypo = (LHCb::CaloHypo*)p->proto()->calo().begin()->data();
  if ( !hypo ) return true;
  if ( hypo->lh() != 99. ) {
    double scale = getPostCalib( p );
    hypo->setLh( 99. ); // (ab)use likelihood parameter
    if ( hypo->position() ) {
      LHCb::CaloPosition::Parameters& parameters = hypo->position()->parameters();
      parameters( LHCb::CaloPosition::E )        = hypo->e() * scale;
      hypo->position()->setCovariance( hypo->position()->covariance() * scale );
      // not needed when calo particle parameter are re-evaluated from post-calibrated CaloHypo
      LHCb::Particle*            part = (LHCb::Particle*)p;
      const Gaudi::LorentzVector mom  = part->momentum() * scale;
      const Gaudi::SymMatrix4x4  cov  = part->momCovMatrix() * scale;
      part->setMomentum( mom );
      part->setMomCovMatrix( cov );
      ok = true;
      if ( counterStat->isQuiet() ) counter( "PostCalib scale" ) += scale;
    }
  }
  if ( counterStat->isVerbose() ) counter( "PostCalibrated photons" ) += ok;

  return true;
}

// ========================================== //
// ==== reconnect proto -> hypo from raw ==== //
// ========================================== //

bool RestoreCaloRecoChain::checkProto( const LHCb::ProtoParticle* proto ) {
  if ( !proto ) return false;
  const LHCb::CaloHypo* hypo    = proto->calo().front();
  auto*                 cluster = getCluster( hypo );
  if ( cluster != nullptr && cluster->entries().size() != 0 ) return false; // persist-reco data is here
  // uncomplete reco chain - try to recover with reconstruction chain from rawBanks
  if ( !m_useRaw ) return false;
  LHCb::CaloHypos* hypos = nullptr;
  if ( hypo->hypothesis() == LHCb::CaloHypo::Pi0Merged )
    hypos = getIfExists<LHCb::CaloHypos>( "/Event/Rec/Calo/MergedPi0s", false );
  else if ( hypo->hypothesis() == LHCb::CaloHypo::Photon )
    hypos = getIfExists<LHCb::CaloHypos>( "/Event/Rec/Calo/Photons", false );
  else
    return false;
  if ( !hypos ) return false;
  // look for cluster with same seed as stored in proto::addionnal info
  LHCb::CaloCellID id = LHCb::CaloCellID( (int)proto->info( LHCb::ProtoParticle::CaloNeutralID, 0. ) );
  for ( LHCb::CaloHypos::const_iterator it = hypos->begin(); it != hypos->end(); ++it ) {
    auto* c = getCluster( *it );
    if ( c->seed() == id ) {
      LHCb::ProtoParticle* p = (LHCb::ProtoParticle*)proto;
      p->clearCalo();
      p->addToCalo( *it );
      if ( counterStat->isVerbose() ) counter( "Restored proto->calo" ) += 1;
      return true; // reconnection succesfull
    }
  }
  // not found - look a neighbour seed (re-calibration may shift the cluster)
  for ( LHCb::CaloHypos::const_iterator it = hypos->begin(); it != hypos->end(); ++it ) {
    auto* c    = getCluster( *it );
    int   drow = std::abs( (int)c->seed().row() - (int)id.row() );
    int   dcol = std::abs( (int)c->seed().col() - (int)id.col() );
    if ( c->seed().area() == id.area() && drow <= 1 && dcol <= 1 ) {
      LHCb::ProtoParticle* p = (LHCb::ProtoParticle*)proto;
      p->clearCalo();
      p->addToCalo( *it );
      if ( counterStat->isVerbose() ) counter( "Restored proto->calo (Neigh.)" ) += 1;
      return true; // reconnection succesfull
    }
  }
  // TO BE CHECKED : MC MATCHING WITH THE UPDATED CALOHYPO
  return false;
}

// ==================================== //
// ==== check rawBank is available ==== //
// ==================================== //

bool RestoreCaloRecoChain::checkRawBank() {
  const LHCb::RawEvent* rawEvt = getIfExists<LHCb::RawEvent>( LHCb::RawEventLocation::Calo, false );
  if ( !rawEvt ) rawEvt = getIfExists<LHCb::RawEvent>( LHCb::RawEventLocation::Default, false );
  if ( !rawEvt ) return false;
  bool bank = ( 0 == ( rawEvt->banks( LHCb::RawBank::EcalE ) ).size() ) ? false : true;
  if ( !bank ) bank = ( 0 == ( rawEvt->banks( LHCb::RawBank::EcalPacked ) ).size() ) ? false : true;
  return bank;
}

void RestoreCaloRecoChain::resetProtoInfo( LHCb::ProtoParticle* proto, LHCb::ProtoParticle::additionalInfo key,
                                           const CaloDataType::DataType hflag, const double def,
                                           const bool force ) const {
  if ( !proto ) return;
  const LHCb::CaloHypo* hypo = proto->calo().front();
  if ( !hypo ) return;
  const auto data = m_estimator->data( hypo, hflag, def );
  resetProtoInfo( proto, key, data, def, force );
  return;
}

void RestoreCaloRecoChain::resetProtoInfo( LHCb::ProtoParticle* proto, LHCb::ProtoParticle::additionalInfo key,
                                           const double data, const double def, const bool force ) const {
  if ( !proto ) return;
  if ( data != def || force ) {
    double pre = proto->info( key, def );
    if ( proto->hasInfo( key ) ) proto->eraseInfo( key ); // erase existing key
    proto->addInfo( key, data );                          // only store when different from default
    std::ostringstream mess;
    mess << "Diff ProtoP::" << key << " [" << ( proto->calo().front() )->hypothesis() << "]";
    if ( counterStat->isVerbose() ) counter( mess.str() ) += data - pre;
  }
}

bool RestoreCaloRecoChain::updateProtoInfo( LHCb::ProtoParticle* proto ) {
  if ( !proto ) return false;
  const LHCb::CaloHypo* hypo = proto->calo().front();
  if ( !hypo ) return false;

  using namespace CaloDataType;

  // special check for CaloCellID reset
  auto*                  cluster = getCluster( hypo );
  const LHCb::CaloCellID id      = ( cluster ) ? cluster->seed() : LHCb::CaloCellID();
  if ( id.all() != proto->info( LHCb::ProtoParticle::CaloNeutralID, 0. ) ) {
    if ( msgLevel( MSG::DEBUG ) ) debug() << "Seed has changed !! " << endmsg;
    resetProtoInfo( proto, LHCb::ProtoParticle::CaloNeutralID, CellID );
    if ( counterStat->isQuiet() ) counter( "Change seed value in proto::info" ) += 1;
  }
  // update Cluster/ExtraDigit info
  double hcal = proto->info( LHCb::ProtoParticle::CaloNeutralHcal2Ecal, 0. ) *
                proto->info( LHCb::ProtoParticle::CaloNeutralEcal, 0. ); // cache Hcal info (not available here)
  resetProtoInfo( proto, LHCb::ProtoParticle::CaloNeutralEcal, ClusterE );
  double ecal      = proto->info( LHCb::ProtoParticle::CaloNeutralEcal, 0. );
  double hcal2ecal = ( ecal ) ? hcal / ecal : 0.;
  resetProtoInfo( proto, LHCb::ProtoParticle::CaloNeutralHcal2Ecal, hcal2ecal ); // rebuild Hcal/Ecal
  resetProtoInfo( proto, LHCb::ProtoParticle::CaloNeutralPrs, HypoPrsE );
  // resetProtoInfo(proto, LHCb::ProtoParticle::ShowerShape    , Spread           ); // ** input to neutralID  &&
  // isPhoton (as Fr2) resetProtoInfo(proto, LHCb::ProtoParticle::CaloNeutralSpd , HypoSpdM         ); // ** input to
  // neutralID
  resetProtoInfo( proto, LHCb::ProtoParticle::CaloClusterCode, ClusterCode );
  resetProtoInfo( proto, LHCb::ProtoParticle::CaloClusterFrac, ClusterFrac, 1. );
  resetProtoInfo( proto, LHCb::ProtoParticle::Saturation, Saturation, 0 );
  double dep = ( m_estimator->data( hypo, ToSpdM ) > 0 ) ? -1. * m_estimator->data( hypo, ToPrsE )
                                                         : m_estimator->data( hypo, ToPrsE );
  proto->addInfo( LHCb::ProtoParticle::CaloDepositID, dep );
  // hypothesis-dependent info
  if ( hypo->hypothesis() == LHCb::CaloHypo::Pi0Merged )
    resetProtoInfo( proto, LHCb::ProtoParticle::ClusterMass, HypoM );
  if ( hypo->hypothesis() != LHCb::CaloHypo::Pi0Merged ) {
    resetProtoInfo( proto, LHCb::ProtoParticle::ClusterAsX, ClusterAsX, 0 );
    resetProtoInfo( proto, LHCb::ProtoParticle::ClusterAsY, ClusterAsY, 0 );
  }

  // Re-evaluate neutral ID ?
  if ( m_updateNeutralID ) {
    resetProtoInfo( proto, LHCb::ProtoParticle::CaloNeutralE49, E49 );
    resetProtoInfo( proto, LHCb::ProtoParticle::CaloNeutralE19, E19 );               // ** input to neutralID
    resetProtoInfo( proto, LHCb::ProtoParticle::CaloPrsNeutralE49, PrsE49 );         // ** input to neutralID
    resetProtoInfo( proto, LHCb::ProtoParticle::CaloPrsNeutralE19, PrsE19 );         // ** input to neutralID
    resetProtoInfo( proto, LHCb::ProtoParticle::CaloPrsNeutralE4max, PrsE4Max );     // ** input to neutralID
    resetProtoInfo( proto, LHCb::ProtoParticle::CaloNeutralPrsM, HypoPrsM );         // ** input to neutralID
    resetProtoInfo( proto, LHCb::ProtoParticle::CaloShapeFr2r4, isPhotonFr2r4 );     // -- input to isPhoton
    resetProtoInfo( proto, LHCb::ProtoParticle::CaloShapeAsym, isPhotonAsym );       // -- input to isPhoton
    resetProtoInfo( proto, LHCb::ProtoParticle::CaloShapeKappa, isPhotonKappa );     // -- input to isPhoton
    resetProtoInfo( proto, LHCb::ProtoParticle::CaloShapeE1, isPhotonEseed );        // -- input to isPhoton
    resetProtoInfo( proto, LHCb::ProtoParticle::CaloShapeE2, isPhotonE2 );           // -- input to isPhoton
    resetProtoInfo( proto, LHCb::ProtoParticle::CaloPrsShapeE2, isPhotonPrsE2 );     // -- input to isPhoton
    resetProtoInfo( proto, LHCb::ProtoParticle::CaloPrsShapeEmax, isPhotonPrsEmax ); // -- input to isPhoton
    resetProtoInfo( proto, LHCb::ProtoParticle::CaloPrsShapeFr2, isPhotonPrsFr2 );   // -- input to isPhoton
    resetProtoInfo( proto, LHCb::ProtoParticle::CaloPrsShapeAsym, isPhotonPrsAsym ); // -- input to isPhoton
    resetProtoInfo( proto, LHCb::ProtoParticle::CaloPrsM, isPhotonPrsM );            // -- input to isPhoton
    resetProtoInfo( proto, LHCb::ProtoParticle::CaloPrsM15, isPhotonPrsM15 );        // -- input to isPhoton
    resetProtoInfo( proto, LHCb::ProtoParticle::CaloPrsM30, isPhotonPrsM30 );        // -- input to isPhoton
    resetProtoInfo( proto, LHCb::ProtoParticle::CaloPrsM45, isPhotonPrsM45 );        // -- input to isPhoton

    // re-evaluate outputs
    double                              nv[10] = {};
    LHCb::ProtoParticle::additionalInfo vv[10] = {
        LHCb::ProtoParticle::CaloTrMatch,       LHCb::ProtoParticle::CaloDepositID,
        LHCb::ProtoParticle::CaloNeutralE19,    LHCb::ProtoParticle::CaloNeutralHcal2Ecal,
        LHCb::ProtoParticle::CaloPrsNeutralE19, LHCb::ProtoParticle::CaloPrsNeutralE49,
        LHCb::ProtoParticle::ShowerShape,       LHCb::ProtoParticle::CaloPrsNeutralE4max,
        LHCb::ProtoParticle::CaloNeutralPrsM,   LHCb::ProtoParticle::CaloNeutralSpd};
    bool nok = true;
    for ( int i = 0; i < 10; ++i ) {
      nok   = nok && proto->hasInfo( vv[i] );
      nv[i] = proto->info( vv[i], CaloDataType::Default );
    }
    nv[1]                                      = fabs( nv[1] );
    const double*                       nnV    = nv;
    LHCb::ProtoParticle::additionalInfo pp[15] = {
        LHCb::ProtoParticle::CaloNeutralID,    LHCb::ProtoParticle::ShowerShape,
        LHCb::ProtoParticle::CaloShapeFr2r4,   LHCb::ProtoParticle::CaloShapeAsym,
        LHCb::ProtoParticle::CaloShapeKappa,   LHCb::ProtoParticle::CaloShapeE1,
        LHCb::ProtoParticle::CaloShapeE2,      LHCb::ProtoParticle::CaloPrsShapeFr2,
        LHCb::ProtoParticle::CaloPrsShapeAsym, LHCb::ProtoParticle::CaloPrsShapeEmax,
        LHCb::ProtoParticle::CaloPrsShapeE2,   LHCb::ProtoParticle::CaloPrsM,
        LHCb::ProtoParticle::CaloPrsM15,       LHCb::ProtoParticle::CaloPrsM30,
        LHCb::ProtoParticle::CaloPrsM45};
    bool   pok    = true;
    double pv[15] = {};
    for ( int i = 0; i < 15; ++i ) {
      pok   = pok && proto->hasInfo( pp[i] );
      pv[i] = proto->info( pp[i], CaloDataType::Default );
    }
    pv[0]              = (double)LHCb::CaloCellID( (int)pv[0] ).area();
    const double* ppV  = pv;
    const auto    pt   = LHCb::CaloMomentum( proto ).pt();
    double        notE = ( pt > 75.0 ) ? m_neutralID->isNotE( nnV ) : -1.; // match minPt in NeutralIDTool
    double        notH = ( pt > 75.0 ) ? m_neutralID->isNotH( nnV ) : -1.;
    double        isPh = ( pt > 2000.0 ) ? m_gammaPi0->isPhoton( ppV ) : +1.; // match minPt in GammaPi0SeparationTool
    if ( nok ) resetProtoInfo( proto, LHCb::ProtoParticle::IsNotE, notE, -1., true );
    if ( nok ) resetProtoInfo( proto, LHCb::ProtoParticle::IsNotH, notH, -1., true );
    if ( pok ) resetProtoInfo( proto, LHCb::ProtoParticle::IsPhoton, isPh, +1., true );
  }

  /* The following info() can't be re-evaluated with partial info :
     if( hypo -> hypothesis() == LHCb::CaloHypo::Photon )proto->addInfo( LHCb::ProtoParticle::ClusterMass, mass );   //
     require all MergedPi0 CaloHypos resetProtoInfo(proto, LHCb::ProtoParticle::PhotonID , NeutralID , -1. , true ); //
     old neutralID - no relation table
  */

  return true;
}
