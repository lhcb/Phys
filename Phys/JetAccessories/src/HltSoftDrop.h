/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#ifndef HLTSOFTDROP_H
#define HLTSOFTDROP_H 1

// ============================================================================
// Includes
// ----------------------------------------------------------------------------
// Gaudi
// ----------------------------------------------------------------------------
#include "GaudiAlg/GaudiTool.h"
// ----------------------------------------------------------------------------
// DaVinci
// ----------------------------------------------------------------------------
#include "Kernel/DaVinciAlgorithm.h"
// ----------------------------------------------------------------------------
// Event
// ----------------------------------------------------------------------------
#include "Event/Particle.h"
#include "LoKi/Geometry.h"
#include "LoKi/Kinematics.h"
// ----------------------------------------------------------------------------
// FastJet
// ----------------------------------------------------------------------------
#include "fastjet/ClusterSequence.hh"
#include "fastjet/JetDefinition.hh"
#include "fastjet/contrib/RecursiveSymmetryCutBase.hh"
#include "fastjet/contrib/SoftDrop.hh"
// ============================================================================

/**
 *  Soft Drop class for use in the Hlt and offline.
 *
 *  @see arXiv:1402.2657
 *
 *  This class essentially acts as a DaVinci wrapper for the code provided by
 *  the fastjet contrib package. For each jet in the container of input jets,
 *  HltSoftDrop converts the jet from an LHCb::Particle to a
 *  fastjet::PseudoJet, performs the SoftDrop algorithm on the jet, then
 *  converts the output back into an LHCb::Particle. The output jet will always
 *  have two daughters, the subjets at the terminal stage of SoftDrop. The
 *  daughters of each subjet are the actual jet constituents, which already
 *  exist in the TES.
 *
 *  @class  HltSoftDrop
 *  @file   HltSoftDrop.h
 *  @author Jake Pfaller
 *  @date   11-11-2024
 */
class HltSoftDrop : public DaVinciAlgorithm {

public:
  // --------------------------------------------------------------------------
  /** Constructor
   */
  HltSoftDrop( const std::string& name, ISvcLocator* svc );

  // --------------------------------------------------------------------------
  /** Destructor
   */
  ~HltSoftDrop(){};

  // --------------------------------------------------------------------------
  /** Initialize the algorithm
   *
   *  @return StatusCode
   */
  StatusCode initialize() override;

  // --------------------------------------------------------------------------
  /** Excecute the algorithm
   *
   *  Applies the SoftDrop algorithm to a container of jets. Please note that
   *  the input jets should have already been clustered using a tool such as
   *  HltJetBuilder.
   *
   *  @return StatusCode
   */
  StatusCode execute() override;

  // --------------------------------------------------------------------------
  /** Finalize
   *
   *  @return StatusCode
   */
  StatusCode finalize() override;

protected:
  // Useful types -------------------------------------------------------------

  typedef fastjet::PseudoJet Jet;
  typedef LHCb::Particle     Prt;

  // --------------------------------------------------------------------------
  /** Convert an LHCb:Particle to a fastjet:PseudoJet
   *
   *  @param p Input particle to convert
   *  @param index User index to assign to the jet
   *  @return Output jet of type fastjet::PseudoJet
   */
  Jet makeJet( const Prt* p, const int& index );

  // Members ------------------------------------------------------------------

  // Beta parameter
  double m_beta;
  // Value of the cut on the symmetry measure
  double m_symmetryCut;
  // Choice of symmetry measure
  int m_symmetryMeasure;
  // Jet radius
  double m_r;
  // Strategy to decide which subjet to recurse into
  int m_recursionChoice;
  // Jet definition for reclustering
  fastjet::JetDefinition m_jd;

private:
  // Default constructor is disabled
  HltSoftDrop() = delete;

  // Copy constructor is disabled
  HltSoftDrop( HltSoftDrop& ) = delete;

  // Assignment operator is disabled
  HltSoftDrop& operator=( const HltSoftDrop& ) = delete;
};

// ============================================================================

#endif // HLTSOFTDROP_H