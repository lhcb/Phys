/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// ============================================================================
// Includes
// ----------------------------------------------------------------------------
// Local
// ----------------------------------------------------------------------------
#include "HltSoftDrop.h"
// ============================================================================

// ----------------------------------------------------------------------------
/** Constructor */
HltSoftDrop::HltSoftDrop( const std::string& name, ISvcLocator* svc ) : DaVinciAlgorithm( name, svc ) {

  declareProperty( "Beta", m_beta = 0, "SoftDrop beta parameter" );
  declareProperty( "SymmetryCut", m_symmetryCut = 0.1, "SoftDrop symmetry cut parameter" );
  declareProperty( "SymmetryMeasure", m_symmetryMeasure = 0,
                   "See fastjet::contrib::RecursiveSymmetryCutBase::SymmetryMeasure" );
  declareProperty( "JetR", m_r = 1.0, "Jet radius for reclustering" );
  declareProperty( "RecursionChoice", m_recursionChoice = 0,
                   "See fastjet::contrib::RecursiveSymmetryCutBase::recursion_choice" );
}

// ----------------------------------------------------------------------------
/** Initialize the algorithm */
StatusCode HltSoftDrop::initialize() {

  // Initialize the parent algorithm
  DaVinciAlgorithm::initialize().ignore();

  // Initialize the jet definition
  m_jd = fastjet::JetDefinition( fastjet::cambridge_aachen_algorithm, 1000. );

  return StatusCode::SUCCESS;
}

// ----------------------------------------------------------------------------
/** Excecute the algorithm */
StatusCode HltSoftDrop::execute() {

  // Grab the input jets
  Prt::Range inputs = particles();

  // Container for the subjets
  LHCb::Particles* sjs = new LHCb::Particles();
  put( sjs, outputLocation() + "/Subjets" );

  // Loop over the input jets
  for ( unsigned int ip = 0; ip < inputs.size(); ip++ ) {

    // Recluster the input constituents because an in-scope cluster sequence
    // is needed for fastjet::SoftDrop
    Prt::ConstVector dtrs = inputs[ip]->daughtersVector();
    std::vector<Jet> fjInputs;
    for ( unsigned int i = 0; i < dtrs.size(); i++ ) { fjInputs.push_back( makeJet( dtrs[i], i ) ); }
    fastjet::ClusterSequence cs( fjInputs, m_jd );
    Jet                      inJet = cs.inclusive_jets()[0];

    // Perform the Soft Drop algorithm
    fastjet::contrib::SoftDrop softDrop(
        m_beta, m_symmetryCut,
        static_cast<fastjet::contrib::RecursiveSymmetryCutBase::SymmetryMeasure>( m_symmetryMeasure ), m_r,
        std::numeric_limits<double>::infinity(),
        static_cast<fastjet::contrib::RecursiveSymmetryCutBase::RecursionChoice>( m_recursionChoice ) );
    Jet outJet = softDrop( inJet );
    if ( !outJet.has_constituents() || !outJet.has_pieces() ) continue;

    // Output jet and subjets
    Prt *pJet = new Prt(), *pSubjet0 = new Prt(), *pSubjet1 = new Prt();

    // Loop over the subjets
    for ( int iSubjet = 0; iSubjet < 2; iSubjet++ ) {

      // Grab the appropriate subjet
      Jet  subjet  = outJet.pieces()[iSubjet];
      Prt* pSubjet = ( iSubjet ) ? pSubjet1 : pSubjet0;

      // Loop over the constituents
      for ( Jet j : subjet.constituents() ) {
        // Grab the particle
        pSubjet->addToDaughters( dtrs[j.user_index()] );
      }

      // Create the output subjet
      pSubjet->setMomentum( Gaudi::LorentzVector( subjet.px(), subjet.py(), subjet.pz(), subjet.e() ) );
      pSubjet->setEndVertex( inputs[ip]->endVertex() );
      sjs->insert( pSubjet );
      pJet->addToDaughters( pSubjet );
    }

    // Create the output jet
    pJet->setMomentum( Gaudi::LorentzVector( outJet.px(), outJet.py(), outJet.pz(), outJet.e() ) );
    pJet->setEndVertex( inputs[ip]->endVertex() );
    markParticle( pJet );
  }

  setFilterPassed( true );
  return StatusCode::SUCCESS;
}

// ----------------------------------------------------------------------------
/** Finalize */
StatusCode HltSoftDrop::finalize() { return StatusCode::SUCCESS; }

// ----------------------------------------------------------------------------
/** Convert an LHCb:Particle to a fastjet:Pseudojet */
fastjet::PseudoJet HltSoftDrop::makeJet( const Prt* p, const int& index ) {

  if ( !p ) return Jet();
  const Gaudi::LorentzVector& v = p->momentum();
  Jet                         jet( v.Px(), v.Py(), v.Pz(), v.E() );
  jet.set_user_index( index );
  return jet;
}

// ============================================================================
// Declare the algorithm factory
DECLARE_COMPONENT( HltSoftDrop )
// ============================================================================