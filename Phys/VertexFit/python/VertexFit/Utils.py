###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Configurables import RefitVeloTracksAlg, RecalcLSAdaptPV3DFitterWeightsAlg, GaudiSequencer, TrackStateInitAlg, LoKi__VoidFilter
from FastVelo.FastVeloAlgConf import FastVeloKalmanConf


def recalc_pv_weights_run2_seq(DataType,
                               Turbo=True,
                               RootInTES=None,
                               TracksLocations=None,
                               PVsLocation=None,
                               ClearPVTracks=True,
                               Name=''):
    '''Make a sequence to refit tracks as HLT1 VELO tracks, recalculate their weights as
    used in the original PV fit using LSAdaptPV3DFitter, then add the refitted tracks to the PV's
    track lists with those weights. This reproduces the track weights as in Run 2 tracking, so, eg,
    PV refitting can be performed on mDST. The resolution on the recalculated weights is ~0.02.

    *** NOTE: to refit the tracks as VELO tracks requires the VELO clusters to be available,
    eg, Turbo data after late 2017, any Stripping mDST data, or any data where the VELO raw
    bank is saved. If the VELO clustes can't be found, this will do nothing.
    *** You shouldn't need to run this on full DST Stripping data, Turbo data with PersistReco,
    or any Run 1 data. Doing so could lead to undefined behaviour.

    Arguments:
    DataType: DataType, for automatic setting of input locations.
    Turbo: whether it's Turbo data, default True.
    RootInTES: should be set to '/Event/<stream>/Turbo' for 2017-2018 Turbo, or '/Event/<stream>' for Stripping,
      in order to find VELO clusters.

    Expert arguments:
    TracksLocations: TES locations of tracks to refit, default ['Rec/Track/Best'] for Stripping,
      ['Tracks'] for 2015-2016 Turbo, ['Track/Best/Long'] for 2017-2018 Turbo.
    PVsLocation: TES location of PVs to calculate weights for, default 'Rec/Vertex/Primary' for Stripping and
      2017-2018 Turbo, 'Primary' for 2015-2016 Turbo.
    ClearPVTracks: Whether to clear the PV's existing lists of tracks, default True. This ensures consistent
      behaviour for events where a line with PersistReco has also fired and the original PV tracks have been
      saved - so the refitted tracks are always used to refit the PV.
    Name: prefix for algo names.'''

    # Check that we have Run 2 data.
    if int(DataType) < 2015:
        raise ValueError(
            'This configuration of algorithms is only valid for Run 2!')

    # Set locations for Turbo data, if they've not been manually overridden.
    # They're different for 2015-2016 and 2017-2018.
    if Turbo:
        if int(DataType) <= 2016:
            if not TracksLocations:
                TracksLocations = ['Tracks']
            if not PVsLocation:
                PVsLocation = 'Primary'
        else:
            if not TracksLocations:
                TracksLocations = ['Track/Best/Long']

    seq = GaudiSequencer(Name + 'RecalcPVWeightsRun2Seq')

    # Filter to check that VELO clusters exist.
    clusterfilter = LoKi__VoidFilter(
        Name + 'VeloClustersFilter',
        Code='CONTAINS("Raw/Velo/LiteClusters") > 0')
    if RootInTES:
        clusterfilter.RootInTES = RootInTES
    seq.Members.append(clusterfilter)

    # Add the track refitter
    refitalg = RefitVeloTracksAlg(Name + 'RefitVeloTracksAlg')
    init = TrackStateInitAlg(Name + "VeloOnlyInitAlg")
    FastVeloKalmanConf().configureFastKalmanFit(init)
    refitalg.addTool(init.StateInitTool)
    if TracksLocations:
        refitalg.TracksLocations = TracksLocations
    if RootInTES:
        refitalg.RootInTES = RootInTES
        init.StateInitTool.FastVeloFitLHCbIDs.RootInTES = RootInTES
        from Configurables import FastVeloHitManager, Tf__DefaultVeloPhiHitManager, Tf__DefaultVeloRHitManager
        for tool in FastVeloHitManager("FastVeloHitManager"), Tf__DefaultVeloPhiHitManager("DefaultVeloPhiHitManager"),\
            Tf__DefaultVeloRHitManager("DefaultVeloRHitManager"):
            tool.RootInTES = RootInTES
    seq.Members.append(refitalg)

    weightsalg = RecalcLSAdaptPV3DFitterWeightsAlg(
        Name + 'RecalcLSAdaptPV3DFitterWeightsAlg')
    # Use the output of the track refitter as input to the weights calculator.
    weightsalg.TracksLocations = [refitalg.getProp('OutputLocation')]
    if PVsLocation:
        weightsalg.PVsLocation = PVsLocation
    if RootInTES:
        weightsalg.RootInTES = RootInTES
    weightsalg.ClearPVTracks = ClearPVTracks
    seq.Members.append(weightsalg)

    return seq
