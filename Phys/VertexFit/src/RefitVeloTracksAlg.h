/*****************************************************************************\
* (c) Copyright 2019-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

// Include files
#include <string>
#include <vector>

// from Gaudi
#include <Gaudi/Property.h>
#include <GaudiAlg/GaudiAlgorithm.h>
#include <GaudiKernel/ToolHandle.h>

#include <Event/Track.h>

struct ITrackStateInit;

/** @class RefitVeloTracksAlg RefitVeloTracksAlg.h
 *
 *  Copy existing tracks selecting only their VELO LHCbIDs then fit them using the given StateInitTool.
 *  Requires VELO clusters to be available.
 *
 *  @author Michael Alexander
 *  @date   2019-10-14
 */
class RefitVeloTracksAlg : public GaudiAlgorithm {

public:
  /// Standard constructor
  using GaudiAlgorithm::GaudiAlgorithm;

  StatusCode initialize() override; ///< Algorithm initialization
  StatusCode execute() override;    ///< Algorithm execution

protected:
  ToolHandle<ITrackStateInit> m_veloFitter;

  Gaudi::Property<std::vector<std::string>> m_tracksLocations{
      this, "TracksLocations", std::vector<std::string>( 1, LHCb::TrackLocation::Default ),
      "TES locations for tracks to be copied and reweighted."};
  Gaudi::Property<std::vector<std::string>> m_partsLocations{
      this, "ParticlesLocations", std::vector<std::string>(),
      "TES locations for particles whose tracks will be copied and refitted."};
  Gaudi::Property<std::string> m_veloTracksLocation{this, "OutputLocation",
                                                    std::string( "Rec/Track/RefittedVeloTracks" ),
                                                    "Location to store the refitted VELO tracks."};
  Gaudi::Property<std::string> m_stateInitToolName{this, "StateInitToolName", "StateInitTool",
                                                   "Name of the TrackStateInitTool instance."};
};
