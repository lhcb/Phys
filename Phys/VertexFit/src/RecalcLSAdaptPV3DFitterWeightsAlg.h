/*****************************************************************************\
* (c) Copyright 2019-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

// Include files
#include <string>
#include <vector>

// from Gaudi
#include <Gaudi/Property.h>
#include <GaudiAlg/GaudiAlgorithm.h>
#include <GaudiKernel/Point3DTypes.h>

#include <Event/RecVertex.h>
#include <Event/Track.h>

/** @class RecalcLSAdaptPV3DFitterWeightsAlg RecalcLSAdaptPV3DFitterWeightsAlg.h
 *
 *  Recalculate the weights for tracks as calculated in LSAdaptPV3DFitter and add tracks to PV's track lists with the
 *  recalculated weights. This is enables removal of tracks from PVs on mDST data where the PV tracks have not been
 *  persisted, ie, Run 2 mDST Stripping or Turbo data. In order for the weights to be recalculated correctly, the tracks
 *  must be refitted as HLT1 VELO tracks, using RefitVeloTracksAlg with the appropriate configuration. To do this, the
 *  VELO clusters must have been persisted, ie, in any Stripping data, Turbo data after late 2017, or any data where
 *  the VELO raw banks have been persisted. The resolution on the recalculated weights is ~0.02.
 *
 *  You shouldn't need to use this algo on full DST Stripping, Turbo with PersistReco, or any Run 1 data. Doing so could
 *  lead to undefined behaviour.
 *
 *  For most use cases, this should be configured via
 *
 *  ~~~~~~~~~~~~~{.py}
 *  from VertexFit.Utils import recalc_pv_weights_run2_seq
 *  from Configurables import DaVinci
 *
 *  recalcseq = recalc_pv_weights_run2_seq(DataType = ..., # the DataType as given to DaVinci
 *                                         Turbo = ..., # Whether it's Turbo data or not
 *                                         RootInTES = ... ) # The RootInTES for DaVinci
 *  DaVinci().UserAlgorithms.insert(0, recalcseq)
 *  ~~~~~~~~~~~~~
 *
 *  \sa RefitVeloTracksAlg, LSAdaptPV3DFitter
 *
 *  @author Michael Alexander
 *  @date   2019-10-14
 */
class RecalcLSAdaptPV3DFitterWeightsAlg : public GaudiAlgorithm {

public:
  /// Standard constructor
  using GaudiAlgorithm::GaudiAlgorithm;

  StatusCode execute() override; ///< Algorithm execution

protected:
  double trackWeight( const LHCb::Track&, const Gaudi::XYZPoint& seed ) const;

  Gaudi::Property<double> m_ipChi2Max{this, "TrackMaxChi2", 12.,
                                      "Max IP chi2 for a track to be considered part of the PV"};
  Gaudi::Property<double> m_minTrackWeight{this, "MinTrackWeight", 0.00000001, "Minimum weight a track can be given"};
  Gaudi::Property<std::vector<std::string>> m_tracksLocations{
      this, "TracksLocations", std::vector<std::string>( 1, LHCb::TrackLocation::Default ),
      "TES locations for tracks to be weighted."};
  Gaudi::Property<std::string> m_pvsLocation{this, "PVsLocation", LHCb::RecVertexLocation::Primary,
                                             "Location to store the refitted VELO tracks for each weighted track."};
  Gaudi::Property<bool>        m_clearTracks{
      this, "ClearPVTracks", true,
      "Whether to clear the existing list of tracks from PVs before adding the tracks with recalculated weights."};
};
