/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files

//
#include "Event/RecVertex.h"
#include "Event/Track.h"
#include "LoKi/PhysExtract.h"
#include "TrackInterfaces/IPVOfflineTool.h"
// local
#include "PVTrackRemover.h"

//-----------------------------------------------------------------------------
// Implementation file for class : PVTrackRemover
//
// 2010-12-07 : Juan Palacios
//-----------------------------------------------------------------------------

// Declaration of the Tool Factory
DECLARE_COMPONENT( PVTrackRemover )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
PVTrackRemover::PVTrackRemover( const std::string& type, const std::string& name, const IInterface* parent )
    : GaudiTool( type, name, parent ), m_pvToolType( "PVOfflineTool" ) {

  declareInterface<IPVReFitter>( this );

  declareProperty( "IPVOffline", m_pvToolType );
}
//=============================================================================
StatusCode PVTrackRemover::initialize() {
  m_pvTool = tool<IPVOfflineTool>( m_pvToolType );
  warning() << "PVTrackRemover is no longer maintained and thus deprecated." << endmsg;

  if ( 0 == m_pvTool ) { return Error( "Could not load IPVOfflineTool " + m_pvToolType ); }

  return StatusCode::SUCCESS;
}
//=============================================================================
StatusCode PVTrackRemover::reFit( LHCb::VertexBase* ) const { return Error( "PVTrackRemover::reFit makes no sense!" ); }
//=============================================================================
StatusCode PVTrackRemover::remove( const LHCb::Particle* particle, LHCb::VertexBase* PV ) const {

  if ( 0 == PV ) return Error( "NULL input PV" );

  LHCb::RecVertex* outPV = dynamic_cast<LHCb::RecVertex*>( PV );

  if ( 0 == outPV ) return Error( "Could now cast VertexBase to RecVertex" );

  const LHCb::RecVertex referencePV( *outPV );

  std::vector<const LHCb::Track*> tracks;

  LoKi::Extract::tracks( particle, std::back_inserter( tracks ) );

  m_pvTool->removeTracksAndRecalculatePV( &referencePV, tracks, *outPV )
      .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );

  return StatusCode::SUCCESS;
}
