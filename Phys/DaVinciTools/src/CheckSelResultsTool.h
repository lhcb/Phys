/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id$
#ifndef CHECKSELRESULTSTOOL_H
#define CHECKSELRESULTSTOOL_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "Kernel/ICheckSelResults.h" // Interface

/** @class CheckSelResultsTool CheckSelResultsTool.h
 *
 *  Implementation of ICheckSelResults interface.
 *
 *  Checks if an algorithm has passed.
 *
 *  @author Patrick Koppenburg
 *  @date   2008-10-31
 */
class CheckSelResultsTool : public GaudiTool, virtual public ICheckSelResults {
public:
  /// Standard constructor
  CheckSelResultsTool( const std::string& type, const std::string& name, const IInterface* parent );

  ~CheckSelResultsTool(); ///< Destructor

  bool isSelected() const override {
    Exception( "Method isSelected() is not implemented" );
    return false;
  };

  bool isSelected( const Selection& selection ) const override;

  bool isSelected( const Selections& selections, const bool ANDMode = false ) const override;
};
#endif // CHECKSELRESULTSTOOL_H
