###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Kernel/SelectionLine
--------------------
#]=======================================================================]

gaudi_add_library(SelectionLine
    SOURCES
        src/HistogramUtilities.cpp
        src/SelectionLine.cpp
    LINK
        PUBLIC
            Gaudi::GaudiAlgLib
            Gaudi::GaudiKernel
            LHCb::HltInterfaces
        PRIVATE
            AIDA::aida
            Gaudi::GaudiUtilsLib
            LHCb::HltEvent
            ROOT::Hist
)
