/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include <string>
#include <utility>
#include <vector>

namespace AIDA {
  class IHistogram1D;
  class IHistogram2D;
  class IProfile1D;
} // namespace AIDA

namespace HistogramUtilities {

  bool setBinLabels( AIDA::IHistogram1D* hist, const std::vector<std::string>& labels );
  bool setBinLabels( AIDA::IProfile1D* hist, const std::vector<std::string>& labels );

  bool setBinLabels( AIDA::IHistogram1D* hist, const std::vector<std::pair<unsigned, std::string>>& labels );
  bool setBinLabels( AIDA::IProfile1D* hist, const std::vector<std::pair<unsigned, std::string>>& labels );

  bool setBinLabels( AIDA::IHistogram2D* hist, const std::vector<std::string>& xlabels,
                     const std::vector<std::string>& ylabels );

  bool setAxisLabels( AIDA::IHistogram1D* hist, const std::string& xAxis, const std::string& yAxis );
  bool setAxisLabels( AIDA::IProfile1D* prof, const std::string& xAxis, const std::string& yAxis );

} // namespace HistogramUtilities
