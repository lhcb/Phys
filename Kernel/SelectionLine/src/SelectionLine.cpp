/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "HistogramUtilities.h"
#include <AIDA/IAxis.h>
#include <AIDA/IHistogram.h>
#include <AIDA/IHistogram1D.h>
#include <AIDA/IProfile1D.h>
#include <Event/HltDecReports.h>
#include <GaudiAlg/GaudiHistos.icpp>
#include <GaudiAlg/ISequencerTimerTool.h>
#include <GaudiKernel/IAlgContextSvc.h>
#include <GaudiKernel/IAlgManager.h>
#include <GaudiKernel/IIncidentSvc.h>
#include <GaudiKernel/StatusCode.h>
#include <GaudiKernel/StringKey.h>
#include <GaudiKernel/ThreadLocalContext.h>
#include <GaudiKernel/TypeNameString.h>
#include <Kernel/SelectionLine.h>
#include <chrono>
#include <cmath>
#include <forward_list>
#include <iterator>
#include <numeric>
#include <tuple>
#include <type_traits>
#include <utility>
#include <vector>

using namespace HistogramUtilities;

namespace {
  static const double timeHistoLowBound = -3;
} // namespace

//-----------------------------------------------------------------------------
// Implementation file for class : HltLine
//
// 2008-09-13 : Gerhard Raven
//-----------------------------------------------------------------------------

void Selection::Line::Stage::updateHandler( Gaudi::Details::PropertyBase& ) {
  if ( !m_initialized ) {
    m_dirty = true;
    return;
  }
  if ( m_algorithm ) m_algorithm->release();
  if ( !m_property.value().empty() ) {
    m_algorithm = m_parent.getSubAlgorithm( m_property.value() );
    if ( !m_algorithm )
      throw GaudiException( "could not obtain algorithm for ", m_property.value(), StatusCode::FAILURE );
    m_algorithm->addRef();
  }
  m_dirty = false;
}

StatusCode Selection::Line::Stage::initialize( ISequencerTimerTool* timer ) {
  m_initialized = true;
  if ( m_dirty ) updateHandler( m_property );
  // TODO: bind timer call...
  if ( timer && algorithm() ) setTimer( timer->addTimer( algorithm()->name() ) );
  // empty transition is allowed...
  return ( algorithm() ? algorithm()->sysInitialize() : StatusCode::SUCCESS );
}

StatusCode Selection::Line::Stage::execute( const EventContext& ctx, ISequencerTimerTool* timertool ) {
  assert( !m_dirty );
  if ( !algorithm() ) return StatusCode::SUCCESS;
  if ( !algorithm()->isEnabled() ) return StatusCode::SUCCESS;
  if ( algorithm()->execState( ctx ).state() == AlgExecState::State::Done ) return StatusCode::SUCCESS;
  // TODO: bind timer at init time
  if ( timertool ) timertool->start( timer() );
  StatusCode result = algorithm()->sysExecute( ctx );
  if ( timertool ) timertool->stop( timer() );
  return result;
}

Selection::Line::SubAlgos Selection::Line::retrieveSubAlgorithms() const {
  using list = std::forward_list<std::pair<const Algorithm*, unsigned>>;
  list subAlgo;
  subAlgo.emplace_front( this, 0 );
  for ( auto i = std::begin( subAlgo ); i != std::end( subAlgo ); ++i ) {
    auto depth = i->second + 1;
    auto seq   = dynamic_cast<const Gaudi::Sequence*>( std::get<0>( *i ) );
    if ( seq ) {
      auto subs = seq->subAlgorithms();
      std::accumulate( std::begin( *subs ), std::end( *subs ), i,
                       [&]( list::iterator j, const Algorithm* k ) { return subAlgo.emplace_after( j, k, depth ); } );
    }
  }
  subAlgo.pop_front(); // remove ourselves...

  if ( msgLevel( MSG::DEBUG ) ) {
    debug() << "Dumping sub algorithms :" << endmsg;
    for ( const auto& i : subAlgo ) {
      debug() << std::string( 3 + 3 * i.second, ' ' ) << std::get<0>( i )->name() << endmsg;
    }
  }

  // transform map such that it has algo, # of sub(sub(sub()))algorithms
  SubAlgos table;
  for ( auto i = std::begin( subAlgo ); i != std::end( subAlgo ); ++i ) {
    auto j = std::find_if_not( std::next( i ), std::end( subAlgo ),
                               [&]( list::const_reference k ) { return k.second > i->second; } );
    table.emplace_back( std::get<0>( *i ), std::distance( i, j ), numberOfCandidates( std::get<0>( *i ) ) );
  }
  return table;
}

IANNSvc& Selection::Line::annSvc() const {
  m_hltANNSvc = serviceLocator()->service<IANNSvc>( s_ANNSvc );
  if ( !m_hltANNSvc ) { Assert( m_hltANNSvc, " no ANNSvc??" ); }
  return *m_hltANNSvc;
}

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
Selection::Line::Line( const std::string& name, ISvcLocator* pSvcLocator )
    : base_class( name, pSvcLocator )
    , m_timerTool{nullptr}
    , m_algMgr{nullptr}
    , m_errorHisto{nullptr}
    , m_timeHisto{nullptr}
    , m_stepHisto{nullptr}
    , m_candHisto{nullptr}
    , m_hltANNSvc{nullptr}
    , m_acceptCounter{nullptr}
    , m_errorCounter{nullptr}
    , m_slowCounter{nullptr}
    , m_timer( 0 )
    , m_nAcceptOnError( 0 ) {
  for ( unsigned i = 0; i < m_stages.size(); ++i ) {
    m_stages[i] = std::make_unique<Stage>( *this, transition( stage( i ) ) );
    declareProperty( m_stages[i]->property().name(), m_stages[i]->property() );
  }
  declareProperty( "HltDecReportsLocation", m_outputContainerName = LHCb::HltDecReportsLocation::Default );
  // TODO: install updateHandler, refuse changes after initialize...
  declareProperty( "DecisionName", m_decision = name + "Decision" );
  declareProperty( "ANNSvc", s_ANNSvc = "HltANNSvc" );
  declareProperty( "IgnoreFilterPassed", m_ignoreFilter = false );
  declareProperty( "MeasureTime", m_measureTime = false );
  declareProperty( "ReturnOK", m_returnOK = false );
  declareProperty( "Turbo", m_turbo = false );
  declareProperty( "AcceptOnError", m_acceptOnError = true );
  declareProperty( "AcceptOnIncident", m_acceptOnIncident = true );
  declareProperty( "AcceptIfSlow", m_acceptIfSlow = false );
  declareProperty( "MaxAcceptOnError",
                   m_maxAcceptOnError = -1 ); // -1: no quota # TODO: make this a throttelable rate...
  declareProperty( "FlagAsSlowThreshold", m_slowThreshold = 500000, "microseconds" );
  declareProperty( "IncidentsToBeFlagged", m_incidents );
}

//=============================================================================
// Initialisation. Check parameters
//=============================================================================
StatusCode Selection::Line::initialize() {
  // initialize the base:
  StatusCode status = base_class::initialize();
  if ( status.isFailure() ) return status;

  /// lock the context
  Gaudi::Utils::AlgContext lock1( this, contextSvc() );

  if ( msgLevel( MSG::DEBUG ) ) debug() << "==> Initialize" << endmsg;
  m_algMgr = svc<IAlgManager>( "ApplicationMgr" );

  // register for incidents...
  auto incidentSvc = service<IIncidentSvc>( "IncidentSvc" );
  for ( const std::string& s : m_incidents ) {
    bool rethrow  = false;
    bool oneShot  = false;
    long priority = 0;
    incidentSvc->addListener( this, s, priority, rethrow, oneShot );
  }

  m_timerTool = tool<ISequencerTimerTool>( "SequencerTimerTool" );
  if ( m_timerTool->globalTiming() ) m_measureTime = true;

  if ( m_measureTime ) {
    m_timer = m_timerTool->addTimer( name() );
    m_timerTool->increaseIndent();
  } else {
    release( m_timerTool ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    m_timerTool = 0;
  }

  //== Initialize the stages
  for ( auto& i : m_stages ) {
    const StatusCode sc = i->initialize( m_timerTool );
    if ( sc.isFailure() ) { return Error( "Failed to initialize " + i->name(), sc ); }
  }

  //== pick up (recursively!) our sub algorithms and their depth count
  //   so we can figure out in detail where we stalled...
  m_subAlgo = retrieveSubAlgorithms();

  // NOTE: when checking filterPassed: a sequencer can be 'true' even if some member is false...
  // ANSWER: in case positive, we skip checking all algos with depth count > current one...
  //        in case negative, we descend, and repeat there, or, if next entry has no
  //        depth count larger, we fill bin at current position..

  //== Create the monitoring histograms
  m_errorHisto = book1D( "error", name() + " error", -0.5, 7.5, 8 );
  m_timeHisto  = book1D( "walltime", name() + " log(wall time/ms)", timeHistoLowBound, 6 );
  m_ncandHisto = book1D( "number of candidates", name() + " log2(number of candidates)", 0, 16, 16 );
  m_stepHisto =
      book1D( "rejection stage", name() + " rejection stage", -0.5, m_subAlgo.size() - 0.5, m_subAlgo.size() );
  m_candHisto = bookProfile1D( "candidates accepted", name() + " candidates accepted", -0.5, m_subAlgo.size() - 0.5,
                               m_subAlgo.size() );
  // if possible, add labels to axis...
  // Remove common part of the name for easier label reading (assumes name is suff. descriptive)
  std::vector<std::string> stepLabels;
  const std::string        common = name();
  for ( const auto& i : m_subAlgo ) {
    std::string                  stepname = std::get<0>( i )->name();
    const std::string::size_type j        = stepname.find( common );
    if ( j != std::string::npos ) stepname.erase( j, common.size() );
    stepLabels.push_back( std::move( stepname ) );
  }
  if ( !setBinLabels( m_stepHisto, stepLabels ) ) { error() << "Could not set bin labels in step histo" << endmsg; }
  if ( !setBinLabels( m_candHisto, stepLabels ) ) { error() << "Could not set bin labels in cand histo" << endmsg; }

  //== and the counters
  m_acceptCounter = &counter( "#accept" );
  declareInfo( "COUNTER_TO_RATE[#accept]", *m_acceptCounter, std::string( "Events accepted by " ) + m_decision );
  m_errorCounter = &counter( "#errors" ); // do not export to DIM -- we have a histogram for this..
  // declareInfo("#errors",counter("#errors"),std::string("Errors seen by ") + m_decision);
  m_slowCounter = &counter( "#slow events" ); // do not export to DIM -- we have a histogram for this..

  m_nAcceptOnError = 0;

  if ( m_measureTime ) m_timerTool->decreaseIndent();

  return status;
}

//=============================================================================
std::unique_ptr<std::function<unsigned()>> Selection::Line::numberOfCandidates( const Algorithm* ) const {
  return {nullptr};
}

// TODO: on a runchange, reset all counters like m_nAcceptOnError...

//=============================================================================
// Main execution
//=============================================================================
StatusCode Selection::Line::execute( const EventContext& ctx ) {
  auto startClock = std::chrono::high_resolution_clock::now();

  /// lock the context
  Gaudi::Utils::AlgContext lock1( this, contextSvc(), ctx );

  if ( m_measureTime ) m_timerTool->start( m_timer );

  if ( msgLevel( MSG::DEBUG ) ) debug() << "==> Execute" << endmsg;
  StatusCode           result = StatusCode::SUCCESS;
  LHCb::HltDecReports* reports =
      getOrCreate<LHCb::HltDecReports, LHCb::HltDecReports>( evtSvc(), m_outputContainerName );
  if ( reports->hasDecisionName( m_decision ) ) { return Error( "HltDecReports already contains report" ); }

  const auto& key = id();

  // TODO: add c'tor with only ID
  LHCb::HltDecReport report( 0, 0, 0, key.second );
  if ( report.invalidIntDecisionID() ) {
    warning() << " DecisionName=" << key.first << " has invalid intDecisionID=" << key.second << endmsg;
  }
  bool accept      = !m_stages.empty(); // make sure an empty line always rejects events...
  m_caughtIncident = false;             // only interested in incidents during stages->execute...

  for ( unsigned i = 0; i < m_stages.size(); ++i ) {
    result = m_stages[i]->execute( ctx );
    if ( m_caughtIncident ) {
      report.setErrorBits( report.errorBits() | 0x02 );
      m_caughtIncident = false;
    }
    if ( result.isFailure() ) {
      report.setErrorBits( report.errorBits() | 0x01 );
      break;
    }
    accept = m_stages[i]->passed( ctx );
    if ( !accept ) break;
    report.setExecutionStage( i + 1 );
  }
  // Is the line a Turbo line? If so, inform the HltDecReports
  if ( m_turbo ) report.setExecutionStage( 0x80 | report.executionStage() );

  // plot the wall clock time spent..
  using ms         = std::chrono::duration<float, std::chrono::milliseconds::period>;
  auto elapsedTime = std::chrono::duration_cast<ms>( std::chrono::high_resolution_clock::now() - startClock );
  // protect against 0
  const auto logElapsedTimeMS = elapsedTime.count() > 0 ? std::log10( elapsedTime.count() ) : timeHistoLowBound;

  fill( m_timeHisto, logElapsedTimeMS, 1.0 );
  if ( elapsedTime.count() > m_slowThreshold ) report.setErrorBits( report.errorBits() | 0x4 );

  // did not(yet) accept, but something bad happened...
  if ( !accept && report.errorBits() != 0 && ( m_nAcceptOnError < m_maxAcceptOnError || m_maxAcceptOnError < 0 ) ) {
    accept = ( m_acceptOnError && ( ( report.errorBits() & 0x01 ) != 0 ) ) ||
             ( m_acceptOnIncident && ( ( report.errorBits() & 0x02 ) != 0 ) ) ||
             ( m_acceptIfSlow && ( ( report.errorBits() & 0x04 ) != 0 ) );
    if ( accept ) ++m_nAcceptOnError;
  }

  auto nCandidates = numberOfCandidates();

  report.setDecision( accept ? 1u : 0u );
  report.setNumberOfCandidates( nCandidates );
  if ( !m_ignoreFilter ) execState( ctx ).setFilterPassed( accept );

  // TODO: allow insert at the beginning, and non-const access to update...
  // TODO: allow a-priori complete report, only update 'our' entry here...
  if ( m_insertion_hint > reports->size() ) {
    auto h           = reports->insert( std::end( *reports ), key.first, report );
    m_insertion_hint = std::distance( std::begin( *reports ), h.first );
  } else {
    auto   h    = reports->insert( std::next( std::begin( *reports ), m_insertion_hint ), key.first, report );
    size_t hint = std::distance( std::begin( *reports ), h.first );
    if ( hint != m_insertion_hint ) {
      warning() << " HltDecReports changed 'shape' -- expected to be at location " << m_insertion_hint
                << " but ended up at " << hint << " updating hint..." << endmsg;
      m_insertion_hint = hint;
    }
  }

  // update monitoring
  *m_acceptCounter += accept;
  // don't flag slow events as error, so mask the bit
  *m_errorCounter += ( ( report.errorBits() & ~0x4 ) != 0 );
  *m_slowCounter += ( ( report.errorBits() & 0x4 ) != 0 );
  if ( accept && nCandidates > 0 ) { fill( m_ncandHisto, std::log2( nCandidates ), 1.0 ); }

  fill( m_errorHisto, report.errorBits(), 1.0 );
  // make stair plot
  auto last = m_subAlgo.begin(), seq = m_subAlgo.end();
  while ( last != m_subAlgo.end() ) {
    if ( std::get<0>( *last )->execState( ctx ).filterPassed() ) {
      last += std::get<1>( *last );
      // There are some algorithms that have subalgorithms, but are not pure sequences and
      // fail even if all children have passed, make sure we record them as the point of failure.
      // Note: it would be neat if this came after the children in the plot, but we cannot figure
      // out which algorithms are like this at initialization without heuristics, so we do the best
      // we can and at least fill the right bin number.
      if ( seq != end( m_subAlgo ) && std::distance( seq, last ) == std::get<1>( *seq ) ) {
        last = seq;
        break;
      }
    } else {
      if ( std::get<1>( *last ) == 1 ) {
        // don't have subalgos, so this is where we stopped
        break;
      } else {
        // Do have subalgos remember where we start descending
        seq = last;
      }
      ++last; // descend into subalgorithms to figure out which one failed.....
    }
  }
  fill( m_stepHisto, std::distance( m_subAlgo.begin(), last ), 1.0 );

  for ( auto i = std::begin( m_subAlgo ); i != last; ++i ) {
    const auto& fn_n = std::get<2>( *i );
    if ( fn_n ) fill( m_candHisto, std::distance( std::begin( m_subAlgo ), i ), ( *fn_n )(), 1.0 );
  }
  if ( m_measureTime ) m_timerTool->stop( m_timer );

  return m_returnOK ? StatusCode::SUCCESS : result;
}

std::vector<const Gaudi::Algorithm*> Selection::Line::algorithms() const {
  std::vector<const Gaudi::Algorithm*> subs;
  subs.reserve( m_subAlgo.size() );
  std::transform( std::begin( m_subAlgo ), std::end( m_subAlgo ), std::back_inserter( subs ),
                  []( SubAlgos::const_reference i ) { return std::get<0>( i ); } );
  return subs;
}

//=========================================================================
// listen for incident during processing...
//=========================================================================
void Selection::Line::handle( const Incident& ) { m_caughtIncident = true; }

//=========================================================================
//  Obtain pointer to an instance of the requested algorithm.
//=========================================================================
Gaudi::Algorithm* Selection::Line::getSubAlgorithm( const std::string& algname ) {
  Gaudi::Utils::TypeNameString name( algname );
  Gaudi::Algorithm*            myAlg{nullptr};
  //= Check wether the specified algorithm already exists.
  IAlgorithm* myIAlg = m_algMgr->algorithm<IAlgorithm>( name.name() );
  if ( myIAlg ) {
    myAlg = dynamic_cast<Gaudi::Algorithm*>( myIAlg );
    subAlgorithms()->push_back( myAlg );
    return myAlg;
  }

  auto& optsSvc = serviceLocator()->getOptsSvc();
  // It doesn't. Create it, and while doing so, ensure some magic properties are propagated...
  if ( !optsSvc.isSet( name.name() + ".Context" ) )
    optsSvc.set( name.name() + ".Context", getProperty( "Context" ).toString() );
  if ( !optsSvc.isSet( name.name() + ".RootInTES" ) )
    optsSvc.set( name.name() + ".RootInTES", getProperty( "RootInTES" ).toString() );

  return createSubAlgorithm( name.type(), name.name(), myAlg ).isSuccess() ? myAlg : nullptr;
}
//=============================================================================
